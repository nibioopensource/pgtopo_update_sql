{
		"version": "1.0",
		"topology": {
			"srid": "4326"
		},
		"tables": [
			{
				"name": "surface",
				"primary_key": "identifier",
				"attributes": [
					{ "name": "identifier", "type": "serial" },
					{ "name": "created_by", "type": "text", "default": "unknown-creator"},
					{ "name": "last_modified_by", "type": "text", "default": "unmodified" },
					{
						"name": "kind",
						"type": "text",
						"default": "unknown-surface-kind",
						"allowed_values": [
							"unknown-surface-kind",
							"residential-surface-kind",
							"service-surface-kind"
						]
					}
				],
				"topogeom_columns": [
					{
						"name": "g",
						"type": "areal",
						"allow_overlaps": false,
						"allow_multi": false,
						"allow_gaps": false
					 }
				]
			},
			{
				"name": "border",
				"primary_key": "identifier",
				"attributes": [
					{ "name": "identifier", "type": "serial" },
					{ "name": "created_by", "type": "text", "default": "unknown-creator"},
					{ "name": "last_modified_by", "type": "text", "default": "unmodified" },
					{ "name": "kind", "type": "text", "default": "unknown-border-kind" }
				],
				"topogeom_columns": [
					{
						"name": "g",
						"type": "lineal"
						"allow_overlaps": false,
						"allow_multi": false,
						"allow_crossings": false
					}
				]
			},
			{
				"name": "path",
				"primary_key": "identifier",
				"attributes": [
					{ "name": "identifier", "type": "serial" },
					{ "name": "created_by", "type": "text", "default": "unknown-creator"},
					{ "name": "last_modified_by", "type": "text", "default": "unmodified" },
					{ "name": "kind", "type": "text", "default": "unknown-path-kind" }
				],
				"topogeom_columns": [
					{
						"name": "g",
						"type": "lineal"
						"allow_overlaps": false,
						"allow_multi": false,
						"allow_crossings": true
					}
				]
			}
		],
		"surface_layer": {
			"table_name": "surface",
			"geo_column": "g"
		},
		"border_layer": {
			"table_name": "border",
			"geo_column": "g"
		},
		"path_layer": {
			"table_name": "border",
			"geo_column": "g"
		},
		"operations": {
			"AddSurface": {
			},
			"AddPath": {
				"snapTolerance": {
					"units": 1e-10,
					"unitsAreMeters": false
				},
				"minAllowedPathLength": {
					"units": 200,
					"unitsAreMeters": true
				},
			},
			"RemovePath": {
			},
			"SplitPaths": {
				"maxSplits": 4
			},
			"UpdateAttributes": {
			},
			"GetFeaturesAsTopoJSON": {
			},
			"SurfaceMerge": {
				"sharedSurfaceAttributes": [
					"type",
					"name"
				],
				"maxSurfaceArea": {
					"units": 200,
					"unitsAreMeters": true
				},
				"surfaceSQLFilter":
					":S1.type not = 'water'",
			},
			"AddBordersSplitSurfaces": {
				"parameters": {
					"default": {
						"snapTolerance": {
							"units": 1e-10,
							"unitsAreMeters": false
						},
						"minAllowedNewSurfacesArea": {
							"units": 200,
							"unitsAreMeters": true
						},
						"maxAllowedSplitSurfacesCount": 0
					},
					"forOpenLines": {
						"minAllowedNewSurfacesArea": {
							"units": 10,
							"unitsAreMeters": true
						},
						"maxAllowedSplitSurfacesCount": 2
					},
					"forClosedLines": {
						"maxAllowedSplitSurfacesCount": 1
					}
				}
			}
		}
	}
