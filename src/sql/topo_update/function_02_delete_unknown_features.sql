--
-- Turns a.b.c into (("a")."b")."c"
--
CREATE OR REPLACE FUNCTION topo_update.__insert_to_update_identifier_syntax(id TEXT)
RETURNS TEXT AS $BODY$ --{
DECLARE
	parent_id TEXT;
	comps TEXT[];
	len INTEGER;
	ret TEXT;
BEGIN

	-- with x as ( select regexp_split_to_array('a.b.c.d', '\.') as a) select
	-- repeat('(', array_upper(a,1)-1) || '"' || array_to_string(a, '")."')
	-- || '"' from x;

	comps = string_to_array(id, '.');
	len := array_upper(comps, 1);
	RAISE DEBUG 'Comps: %, len: %', comps, len;
	IF len = 1
	THEN
		RETURN format('%I', comps[1]);
	END IF;
	parent_id = topo_update.__insert_to_update_identifier_syntax(
		array_to_string(comps[1:len-1], '.') -- TODO: avoid this back and forth
	);
	RAISE DEBUG 'Parent id: %, comps[len]: %', parent_id, comps[len];
	ret := format('(%s).%I', parent_id, comps[len]);
	RAISE DEBUG 'Returning: %', ret;
	RETURN ret;
END;
$BODY$ LANGUAGE 'plpgsql'; --}

-- Delete records from a given topology layer where
-- the feature intersects the given bounding box but
-- the value of an ID column is not found amongst the
-- set of "keep" values.
--
-- Return number of features deleted
--
-- Includes cleaning of the deleted TopoGeometry objects
--
CREATE OR REPLACE FUNCTION topo_update.delete_unknown_features(

	-- Reference to a table we want to delete
	-- feature recods from
	layerTable REGCLASS,

	-- Name of column containing the TopoGeometry column
	-- used to determine intersection with given bounding box
	layerColumn NAME,

	-- Name of column containing the ID value for the table
	layerIDColumn NAME,

	-- Bounding box used to determine which features
	-- to delete
	bbox GEOMETRY,

	-- Array of ID values to keep
	keepIDValues TEXT[]

)
RETURNS INT AS
$BODY$ -- {
DECLARE
	sql TEXT;
	delete_count INTEGER;
	proc_name TEXT = 'topo_update.delete_unknown_features';
	quoted_id TEXT;
	layer topology.layer;
	topo topology.topology;
	rec RECORD;
	intersecting_faces INT[];
BEGIN

	-- TODO: take these from caller
	SELECT t topo, l layer
		FROM topology.layer l, topology.topology t, pg_class c, pg_namespace n
		WHERE l.schema_name = n.nspname
		AND l.table_name = c.relname
		AND c.oid = layerTable
		AND c.relnamespace = n.oid
		AND l.feature_column = layerColumn
		AND l.topology_id = t.id
		INTO rec;
	topo := rec.topo;
	layer := rec.layer;

	RAISE DEBUG 'layer.feature_type is %, child is %', layer.feature_type, layer.child_id;

	-- Optimize for non-hierarchical layers
	IF layer.child_id IS NULL
	THEN --{
		IF layer.feature_type = 2
		THEN --{ layer is lineal

			sql := format(
				$$
					WITH deleted AS
					(
						DELETE FROM %2$s
						WHERE %5$I IN (
							SELECT l.%5$I fid
							FROM
								%1$I.edge e,
								%1$I.relation r,
								%2$s l
							WHERE r.layer_id = %3$L
							AND ( NOT l.%5$I::text = ANY($2) )
							AND id(l.%4$I) = r.topogeo_id
							AND r.element_id IN ( e.edge_id, -e.edge_id )
							AND ST_Intersects(e.geom, $1)
						)
						RETURNING id(%4$I)
					)
					SELECT array_agg(id) did FROM deleted;
				$$,
				topo.name,       -- %1
				layerTable,      -- %2
				layer.layer_id,  -- %3
				layerColumn,     -- %4
				layerIDColumn    -- %5
			);
			RAISE DEBUG 'SQL: %', sql;
			EXECUTE sql
			USING
				bbox, -- $1
				keepIDValues -- $2
			INTO rec
			;

			delete_count = COALESCE(array_upper(rec.did, 1), 0);
			RAISE DEBUG 'DELETED: % linear features', delete_count;

			sql := format(
				$$
					DELETE FROM %1$I.relation
					WHERE layer_id = %2$L
					AND topogeo_id = ANY($1)
				$$,
				topo.name,
				layer.layer_id
			);
			RAISE DEBUG 'SQL: %', sql;
			EXECUTE sql
			USING rec.did;

			RETURN delete_count;

		ELSIF layer.feature_type = 3
		THEN -- }{ layer is areal

			-- 1. Find all faces on the side
			--    of edges intersecting the bbox
			sql := format(
				$$
					WITH edges AS (
						SELECT e.edge_id, e.right_face, e.left_face
						FROM %1$I.edge e
						WHERE ST_Intersects(e.geom, $1)
					), edge_faces AS (
						SELECT edge_id, right_face face_id
						FROM edges
							UNION
						SELECT edge_id, left_face
						FROM edges
					)
					SELECT array_agg(distinct face_id)
					FROM edge_faces
				$$,
				topo.name
			);
			RAISE DEBUG 'SQL: %', sql;
			EXECUTE sql USING bbox INTO intersecting_faces;

			RAISE DEBUG 'Intersecting edge faces: %', intersecting_faces;

			IF intersecting_faces IS NULL THEN
				RAISE DEBUG 'No edge intersects bbox %', ST_AsEWKT(bbox);
				SELECT ARRAY[topology.GetFaceByPoint(
					topo.name,
					ST_PointOnSurface(bbox),
					0
				)]
				INTO
				intersecting_faces;
				RAISE DEBUG 'GetFaceByPoint returned face %', intersecting_faces;
			END IF;

			sql := format(
				$$
					WITH
					deleted AS
					(
						DELETE FROM %2$s
						WHERE ( NOT %5$I::text = ANY($2) )
						AND id(%4$I) IN (
							SELECT topogeo_id
							FROM %1$I.relation
							WHERE layer_id = %3$L
							AND element_id = ANY($1)
						)
						RETURNING id(%4$I)
					)
					SELECT array_agg(id) did FROM deleted;
				$$,
				topo.name,       -- %1
				layerTable,      -- %2
				layer.layer_id,  -- %3
				layerColumn,     -- %4
				layerIDColumn    -- %5
			);
			RAISE DEBUG 'SQL: %', sql;
			EXECUTE sql
			USING
				intersecting_faces, -- $1
				keepIDValues -- $2
			INTO rec
			;

			delete_count = COALESCE(array_upper(rec.did, 1), 0);
			RAISE DEBUG 'DELETED: % areal features', delete_count;

			sql := format(
				$$
					DELETE FROM %1$I.relation
					WHERE layer_id = %2$L
					AND topogeo_id = ANY($1)
				$$,
				topo.name,
				layer.layer_id
			);
			RAISE DEBUG 'SQL: %', sql;
			EXECUTE sql
			USING rec.did;

			RETURN delete_count;

		END IF; --} layer is non-hierarchical and areal

	END IF; --} layer is non-hierarchical

	-- Compute ID name
	quoted_id := topo_update.__insert_to_update_identifier_syntax(layerIDColumn);

	-- Unoptimized query
	-- TODO: optimize for simple linear and simple areal cases
	-- TODO: optimize this upstream, see https://trac.osgeo.org/postgis/ticket/853
	sql := format(
		$$
WITH deleted AS (
	DELETE FROM %1$s
	WHERE ST_Intersects(%2$I, $1)
	AND NOT %3$s::text = ANY($2)
	RETURNING %2$I tg
) SELECT topology.clearTopoGeom(tg) FROM deleted
		$$,
		layerTable,   -- %1
		layerColumn,  -- %2
		quoted_id -- %3
	);

	RAISE DEBUG '%: %', proc_name, sql;

	EXECUTE sql
	USING
		bbox, -- $1
		keepIDValues -- $2
	;

	GET DIAGNOSTICS delete_count = ROW_COUNT;

	RAISE DEBUG 'Deleted % features in % layer', delete_count, layerTable::text;

	RETURN delete_count;

END;
$BODY$ --}
LANGUAGE plpgsql VOLATILE;

