-- Update a non spatial columns feature in a table by id 
--
-- Returns NULL or the ID of the updated rows
-- if a feature was updated the id of the updated row are returned
--
-- TODO check if we need this and if we can use topo_update.update_feature directly instead
-- Also check this issue
-- 
CREATE OR REPLACE FUNCTION topo_update.update_feature_attribute(

	-- The 'properties' component of a GeoJSON Feature object
	-- See https://geojson.org/ but with out a geometry 
	feature JSONB,

	-- Target layer table (where to update the new feature in)
	layerTable REGCLASS,

	-- Name of the primary key column in the target layer table
	layerIdColumn NAME,

	-- JSON mapping of PG columns and values
	-- from JSON attributes and values
	-- See topo_update.json_props_to_pg_cols() function
	layerColMap JSONB

) RETURNS TEXT
AS $BODY$ -- {
DECLARE

	colnames TEXT[];
	colvals TEXT[];

	col_updates TEXT[];
	col_upd TEXT;

	sql TEXT;
	
	id_value TEXT;

	id_json_name TEXT;

	-- Counter for num update rows, used decide if to return true or false;
	v_cnt int;
	
	proc_name text = 'topo_update.update_feature_attribute';
	
	feature_properties JSONB;

BEGIN

	feature_properties = feature ->>  'properties';
	
	-- Get json name for primary key 
	id_json_name = (jsonb_array_elements_text( layerColMap -> layerIdColumn ));
	
	-- Get id
	id_value = feature_properties ->> id_json_name;
	
	RAISE DEBUG 'id_value: %', id_value;

	IF id_value IS NULL THEN
	
		IF id_json_name IS NULL THEN
			RAISE EXCEPTION '%: % not found in % using %',
				proc_name,
				id_json_name,
				layerColMap,
				layerIdColumn
				;
		END IF;
			
		RAISE EXCEPTION '%: feature ID extracted from props %'
			' with mapping path % is using name % '
			', cannot proceed',
			proc_name,
			feature_properties,
			layerColMap,
			id_json_name
			;
	END IF;
	
	-- Remove primary key from update map
	layerColMap = layerColMap - layerIdColumn;


	SELECT c.colnames, c.colvals
	FROM topo_update.json_props_to_pg_cols(feature_properties, layerColMap) c
	INTO colnames, colvals;

	RAISE DEBUG 'COMPUTED: colnames: %', colnames;
	RAISE DEBUG 'COMPUTED: colvals: %', colvals;

	-- Naive approach: always UPDATE, no matter what

	

	
	
	FOR i IN 1 .. array_upper(colnames, 1)
	LOOP
		col_upd := format('%s = %s', colnames[i], colvals[i]);
		RAISE DEBUG 'col_upd: %', col_upd;
		col_updates := array_append(col_updates, col_upd);
	END LOOP;

	RAISE DEBUG 'col_updates: %', col_updates;

	sql := format($$
UPDATE %1$s t
SET %2$s
FROM %1$s ot
WHERE ot.%3$I = t.%3$I
  AND ot.%3$I = %4$L
		$$,
		layerTable,
		array_to_string(col_updates, ', '),
		layerIdColumn,
		id_value
	);

	-- TODO: ADD more WHERE clauses to skip setting values which did not
	--			 change ?
	RAISE DEBUG 'SQL: %', sql;

	EXECUTE sql;

	GET DIAGNOSTICS v_cnt = row_count;
	
	IF v_cnt = 0 THEN
		RETURN NULL;
	ELSE
		RETURN id_value;
	END IF;

END;
$BODY$ --}
LANGUAGE plpgsql;
