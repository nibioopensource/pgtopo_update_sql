--
-- Take a PostgreSQL RECORD value and a pgCol JSON mapping
-- file (col_map) and return a JSONB object with keys specified
-- by the mapping file and values taken from the RECORD.
--
-- The format of the pgCol JSON mapping file is the same
-- as used by the topo_update.json_props_to_pg_cols function
-- to allow for re-using the same mapping file specification
-- both ways.
--
-- {
CREATE OR REPLACE FUNCTION topo_update.record_to_jsonb(
	pgrec ANYELEMENT,
	col_map JSONB
)
RETURNS JSONB
AS $BODY$ -- }{
DECLARE
	ret JSONB;
	auto_json JSONB;
	k TEXT;
	v JSONB;
	rec RECORD;
	map_json_path JSONB;
	json_path TEXT[];
	parent_path TEXT[];
	obj JSONB;
	i INT;
	procName TEXT := 'topo_update.record_to_jsonb';
BEGIN

	auto_json := to_jsonb(pgrec);

	RAISE DEBUG '%: REC(json): %', procName, auto_json;

	ret := '{}'; -- start with empty object

	FOR k IN SELECT jsonb_object_keys( col_map )
	LOOP
		map_json_path = col_map -> k;
		RAISE DEBUG '%: K: % -- P: %', procName, k, map_json_path;

		json_path := array_agg(x) from jsonb_array_elements_text( map_json_path ) x;
		RAISE DEBUG '%: json_path: %', procName, json_path;

		v := auto_json #> string_to_array(k, '.');
		RAISE DEBUG '%: V from K: %', procName, v;

		IF v IS NULL THEN
			CONTINUE;
		END IF;

		-- Ensure each element in the "to" array, other
		-- than the first, has an object in the returned
		-- JSONB for containing it
		FOR i IN 2..array_upper(json_path, 1)
		LOOP
			parent_path := json_path[1:i-1];
			obj := ret #> parent_path;
			IF obj IS NULL
			THEN
				ret := jsonb_set(ret, parent_path, '{}');
			ELSIF NOT jsonb_typeof(obj) = 'object'
			THEN
				RAISE EXCEPTION 'Invalid mapping: json path exists as both object and simple value';
			END IF;
		END LOOP;

		-- Assign the value to the target
		ret := jsonb_set(
			ret,
			json_path,
			auto_json #> string_to_array(k, '.')
		);

		--RAISE DEBUG '%: RET: %', procName, ret;

	END LOOP;
	RETURN ret;
END;
$BODY$
LANGUAGE 'plpgsql'; -- }
