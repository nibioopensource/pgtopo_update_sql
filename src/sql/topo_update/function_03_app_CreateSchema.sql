CREATE OR REPLACE FUNCTION topo_update._app_validate_layer_config(
	layer_element_name TEXT,
	cfg_tables JSONB,
	cfg_layer JSONB,
	layertype TEXT
)
RETURNS VOID AS $BODY$ --{
DECLARE
	rec RECORD;
	cfg_layer_table JSONB;
	cfg_layer_geo_col JSONB;
	cfg_layer_id_col JSONB;
	cfg_layer_version_col JSONB;
BEGIN
	IF NOT cfg_layer ? 'table_name' THEN
		RAISE EXCEPTION 'Missing "table_name" in "%" element of appconfig',
			layer_element_name;
	END IF;
	IF NOT cfg_layer ? 'geo_column' THEN
		RAISE EXCEPTION 'Missing "geo_column" in "%" element of appconfig',
			layer_element_name;
	END IF;

	-- Find tables corresponding to surface layer
	FOR rec IN SELECT jsonb_array_elements( cfg_tables ) cfg
	LOOP
		IF rec.cfg ->> 'name' = cfg_layer ->> 'table_name' THEN
			cfg_layer_table := rec.cfg;
			EXIT;
		END IF;
	END LOOP;

	IF cfg_layer_table IS NULL THEN
		RAISE EXCEPTION '% table "%" not found in "tables" element',
			layer_element_name,
			cfg_layer ->> 'table_name';
	END IF;
	-- Find and check defined surface geocolumn
	FOR rec IN SELECT jsonb_array_elements( cfg_layer_table -> 'topogeom_columns' ) cfg
	LOOP --{
		IF rec.cfg ->> 'name' = cfg_layer ->> 'geo_column' THEN
			cfg_layer_geo_col := rec.cfg;
			EXIT;
		END IF;
	END LOOP; --}
	IF cfg_layer_geo_col IS NULL THEN
		RAISE EXCEPTION 'Table "%" does not have a "%" column (used as geo_column) defined in "topogeom_columns" array',
			cfg_layer_table ->> 'name',
			cfg_layer ->> 'geo_column';
	END IF;
	IF cfg_layer_geo_col ->> 'type' != layertype THEN
		RAISE EXCEPTION 'TopoGeom column % of layer table "%" must be have type "%", not "%"',
			cfg_layer_geo_col ->> 'name',
			cfg_layer_table ->> 'name',
			layertype,
			cfg_layer_geo_col ->> 'type'
		;
	END IF;
	-- Ensure layer table has a primary key defined
	IF NOT cfg_layer_table ? 'primary_key' THEN
		RAISE EXCEPTION '% table "%" lacks a "primary_key" element',
			layer_element_name, cfg_layer_table ->> 'name'
		;
	END IF;
	-- Find version and primary_key columns
	FOR rec IN SELECT jsonb_array_elements( cfg_layer_table -> 'attributes' ) cfg
	LOOP --{
		IF rec.cfg ->> 'name' = cfg_layer ->> 'version_column' THEN
			cfg_layer_version_col := rec.cfg;
		END IF;
		--RAISE DEBUG 'Attribue %, we are looking for %', rec.cfg ->> 'name' , cfg_layer_table ->> 'primary_key' ;
		IF rec.cfg ->> 'name' = cfg_layer_table ->> 'primary_key' THEN
			cfg_layer_id_col := rec.cfg;
		END IF;
	END LOOP; --}
	-- Verify primary key column is found in table
	IF cfg_layer_id_col IS NULL THEN
		RAISE EXCEPTION '% table "%" primary key attribute "%" not found',
			layer_element_name,
			cfg_layer_table ->> 'name',
			cfg_layer_table ->> 'primary_key'
		;
	END IF;
END;
$BODY$ LANGUAGE 'plpgsql'; --}

CREATE OR REPLACE FUNCTION topo_update._app_check_surface_layer(
	appconfig JSONB,
	cfg_tables JSONB
)
RETURNS VOID AS $BODY$ --{
DECLARE
	cfg_surface_layer JSONB;
BEGIN
	-- Check surface_layer
	cfg_surface_layer := appconfig -> 'surface_layer';
	IF cfg_surface_layer IS NULL THEN
		RAISE EXCEPTION 'Missing "surface_layer" element in appconfig';
	END IF;
	IF jsonb_typeof(cfg_surface_layer) != 'object' THEN
		RAISE EXCEPTION 'Invalid "surface_layer" element in appconfig (must be object, not %)', jsonb_typeof(cfg_surface_layer);
	END IF;
	PERFORM topo_update._app_validate_layer_config('surface_layer', cfg_tables, cfg_surface_layer, 'areal');
END;
$BODY$ LANGUAGE 'plpgsql'; --}

CREATE OR REPLACE FUNCTION topo_update._app_check_border_layer(
	appconfig JSONB,
	cfg_tables JSONB
)
RETURNS VOID AS $BODY$ --{
DECLARE
	cfg_border_layer JSONB;
BEGIN
	-- Check border_layer
	cfg_border_layer := appconfig -> 'border_layer';
	IF cfg_border_layer IS NULL THEN
		RAISE EXCEPTION 'Missing "border_layer" element in appconfig';
	END IF;
	IF jsonb_typeof(cfg_border_layer) != 'object' THEN
		RAISE EXCEPTION 'Invalid "border_layer" element in appconfig (must be object, not %)', jsonb_typeof(cfg_border_layer);
	END IF;
	PERFORM topo_update._app_validate_layer_config('border_layer', cfg_tables, cfg_border_layer, 'lineal');

END;
$BODY$ LANGUAGE 'plpgsql'; --}

CREATE OR REPLACE FUNCTION topo_update._app_check_path_layer(
	appconfig JSONB,
	cfg_tables JSONB
)
RETURNS VOID AS $BODY$ --{
DECLARE
	cfg_path_layer JSONB;
BEGIN
	-- Check path_layer
	cfg_path_layer := appconfig -> 'path_layer';
	IF cfg_path_layer IS NULL THEN
		RAISE EXCEPTION 'Missing "path_layer" element in appconfig';
	END IF;
	IF jsonb_typeof(cfg_path_layer) != 'object' THEN
		RAISE EXCEPTION 'Invalid "path_layer" element in appconfig (must be object, not %)', jsonb_typeof(cfg_path_layer);
	END IF;
	PERFORM topo_update._app_validate_layer_config('path_layer', cfg_tables, cfg_path_layer, 'lineal');

END;
$BODY$ LANGUAGE 'plpgsql'; --}

CREATE OR REPLACE FUNCTION topo_update._app_check_point_layer(
	appconfig JSONB,
	cfg_tables JSONB
)
RETURNS VOID AS $BODY$ --{
DECLARE
	cfg_point_layer JSONB;
BEGIN
	-- Check point_layer
	cfg_point_layer := appconfig -> 'point_layer';
	IF cfg_point_layer IS NULL THEN
		RAISE EXCEPTION 'Missing "point_layer" element in appconfig';
	END IF;
	IF jsonb_typeof(cfg_point_layer) != 'object' THEN
		RAISE EXCEPTION 'Invalid "point_layer" element in appconfig (must be object, not %)', jsonb_typeof(cfg_point_layer);
	END IF;
	PERFORM topo_update._app_validate_layer_config('point_layer', cfg_tables, cfg_point_layer, 'puntal');

END;
$BODY$ LANGUAGE 'plpgsql'; --}

-- Deprecated on 2025-01
DROP FUNCTION IF EXISTS topo_update._app_check_at_least_border_or_surface_layer(JSONB, JSONB);
CREATE OR REPLACE FUNCTION topo_update._app_check_at_least_a_layer(
	appconfig JSONB,
	cfg_tables JSONB
)
RETURNS VOID AS $BODY$ --{
DECLARE
	layers_found INT := 0;
BEGIN
	IF appconfig ? 'border_layer' THEN
		layers_found := layers_found + 1;
		PERFORM topo_update._app_check_border_layer(appconfig, cfg_tables);
	END IF;
	IF appconfig ? 'surface_layer' THEN
		layers_found := layers_found + 1;
		PERFORM topo_update._app_check_surface_layer(appconfig, cfg_tables);
	END If;
	IF appconfig ? 'path_layer' THEN
		layers_found := layers_found + 1;
		PERFORM topo_update._app_check_path_layer(appconfig, cfg_tables);
	END If;
	IF appconfig ? 'point_layer' THEN
		layers_found := layers_found + 1;
		PERFORM topo_update._app_check_point_layer(appconfig, cfg_tables);
	END If;

	IF layers_found = 0 THEN
		RAISE EXCEPTION 'At least one of "border_layer", "surface_layer", "path_layer" or "point_layer" elements are required';
	END IF;

END;
$BODY$ LANGUAGE 'plpgsql'; --}


CREATE OR REPLACE FUNCTION topo_update._app_validate_config(
	appconfig JSONB
)
RETURNS VOID AS $BODY$ --{
DECLARE
	appconfig_version TEXT;
	cfg_tables JSONB;
	cfg_domains JSONB;
	rec RECORD;
	op_count INT := 0;
	cfg JSONB;
BEGIN

	-- Check version
	appconfig_version := appconfig ->> 'version';
	IF appconfig_version IS NULL THEN
		RAISE EXCEPTION 'Missing version in appconfig';
	END IF;
	IF appconfig_version != '1.0' THEN
		RAISE EXCEPTION 'Unsupported appconfig version %', appconfig -> 'version';
	END IF;

	-- Check domains
	cfg_domains := appconfig -> 'domains';
	IF cfg_tables IS NULL THEN
		IF jsonb_typeof(cfg_tables) != 'array' THEN
			RAISE EXCEPTION 'Invalid "domains" element in appconfig (must be array, not %)', jsonb_typeof(cfg_domains);
		END IF;
	END IF;

	-- Check tables
	cfg_tables := appconfig -> 'tables';
	IF cfg_tables IS NULL THEN
		RAISE EXCEPTION 'Missing "tables" element in appconfig';
	END IF;
	IF jsonb_typeof(cfg_tables) != 'array' THEN
		RAISE EXCEPTION 'Invalid "tables" element in appconfig (must be array, not %)', jsonb_typeof(cfg_tables);
	END IF;

	-- Validate "operations"
	-- TODO: avoid duplicated checks when multiple operations require
	--       the same status
	FOR rec IN SELECT * FROM jsonb_each(appconfig -> 'operations')
	LOOP --{
		cfg := rec.value;
		CASE rec.key
			WHEN 'AddBordersSplitSurfaces' THEN
				-- AddBorderSplitSurface always avoids:
				--   o Surfaces overlap
				--   o Surfaces gaps
				--   o Surfaces forming multi-polygons
				--   o Borders overlap
				--   o Borders cross
				--   o Borders dangling
				--   o Borders forming multi-linestrings
				-- TODO: shall it be made incompatible with
				--       those characteristics being allowed ?
				PERFORM topo_update._app_check_surface_layer(appconfig, cfg_tables);
				PERFORM topo_update._app_check_border_layer(appconfig, cfg_tables);
			WHEN 'SurfaceMerge' THEN
				-- SurfaceMerge operations require definition
				-- of Border and Surface layers
				PERFORM topo_update._app_check_surface_layer(appconfig, cfg_tables);
				PERFORM topo_update._app_check_border_layer(appconfig, cfg_tables);
			WHEN 'AddBorder' THEN
				-- AddBorder delegates allowing or not following
				-- situation based on APP_CONFIG:
				--   o Borders overlap
				--   o Borders cross
				--   o Borders dangling
				--   o Borders forming multi-linestrings
				PERFORM topo_update._app_check_border_layer(appconfig, cfg_tables);
			WHEN 'AddSurface' THEN
				-- AddSurface delegates allowing or not following
				-- situation based on APP_CONFIG:
				--   o Surfaces overlap
				--   o Surfaces gaps
				--   o Surfaces forming multi-polygons
				-- TODO: shall it be made incompatible with
				--       those characteristics being allowed ?
				PERFORM topo_update._app_check_surface_layer(appconfig, cfg_tables);
			WHEN 'UpdateAttributes' THEN
				-- TODO: check at least a table exists with primary key
			WHEN 'GetFeaturesAsTopoJSON' THEN
				-- Check at least a function border_layer or surface_layer exist
				PERFORM topo_update._app_check_at_least_a_layer(appconfig, cfg_tables);
			WHEN 'AddPath' THEN
				PERFORM topo_update._app_check_path_layer(appconfig, cfg_tables);
			WHEN 'RemovePath' THEN
				PERFORM topo_update._app_check_path_layer(appconfig, cfg_tables);
			WHEN 'SplitPaths' THEN
				PERFORM topo_update._app_check_path_layer(appconfig, cfg_tables);
			WHEN 'AddPoint' THEN
				PERFORM topo_update._app_check_point_layer(appconfig, cfg_tables);
			WHEN 'RemovePoint' THEN
				PERFORM topo_update._app_check_point_layer(appconfig, cfg_tables);
			WHEN 'MovePoint' THEN
				PERFORM topo_update._app_check_point_layer(appconfig, cfg_tables);
			ELSE
				RAISE EXCEPTION 'Unsupported operation %', rec.key;
		END CASE;
		op_count := op_count + 1;
	END LOOP; --}
	IF op_count < 1 THEN
		RAISE EXCEPTION 'Invalid appconfig: no operation defined';
	END IF;

END;
$BODY$ LANGUAGE 'plpgsql'; --}

-- Assumes appname is first item in search_path
CREATE OR REPLACE FUNCTION topo_update._app_CreateSchema_table_attribute(
	attconfig jsonb
)
RETURNS TEXT AS $BODY$ --{
DECLARE
	attname NAME;
	atttype NAME;
	sql TEXT;
	def_json JSONB;
	def_type TEXT;
	rec RECORD;
BEGIN

	-- atttype := format('%s', (attconfig ->> 'type')::regtype);
	atttype := attconfig ->> 'type';
	IF atttype = 'integer' THEN
		atttype = 'int4'; -- or quoting it will fail
	ELSIF atttype = 'smallint' THEN
		atttype = 'int2'; -- or quoting it will fail
	ELSIF atttype ilike 'geometry(%' THEN
		atttype = 'geometry'; -- or quoting it will fail
	END IF;

	sql := format('%I %I',
		attconfig ->> 'name',
		atttype
	);

	-- Attribute modifiers
	IF attconfig ? 'modifiers' THEN --{
		sql := format('%s (%s)',
			sql,
			(SELECT array_to_string(array_agg(quote_literal(x)), ',')
				FROM jsonb_array_elements_text(attconfig -> 'modifiers') x )
		);
	END IF; --}

	-- Attribute default
	def_json := attconfig -> 'default';
	IF def_json IS NOT NULL THEN --{
		IF jsonb_typeof(def_json) = 'object'
		THEN --{{
			-- Array of literal values
			def_type := def_json ->> 'type';
			IF def_type IS NULL THEN
				RAISE EXCEPTION 'Missing "type" from attribute default object %', def_json;
			END IF;
			IF def_type = 'function' THEN --{
				IF NOT def_json ? 'name' THEN
					RAISE EXCEPTION 'Missing "name" from attribute default function object %', def_json;
				END IF;
				sql := format('%s DEFAULT %I()',
					sql,
					def_json ->> 'name'
				);
			END IF; --}
		ELSIF jsonb_typeof(def_json) = 'array' THEN --}{
			-- Array of literal values
			sql := format('%s DEFAULT ( %s )',
				sql,
				(SELECT array_to_string(array_agg(quote_literal(x)), ',')
					FROM jsonb_array_elements_text(def_json) x )
			);
		ELSE --}{
			-- Literal value
			sql := format('%s DEFAULT %L',
				sql,
				def_json #>> '{}'
			);
		END IF; --}}
	END IF; --}

	RAISE DEBUG 'ATT: %', sql;

	RETURN sql;
END;
$BODY$ LANGUAGE 'plpgsql'; --}

-- Assumes appname is first item in search_path
CREATE OR REPLACE FUNCTION topo_update._app_CreateSchema_table(
	appname name,
	tabconfig jsonb
)
RETURNS regclass AS $BODY$ --{
DECLARE
	tabname NAME;
	sql TEXT;
	sql_att TEXT;
	sql_attlist TEXT[];
	rec RECORD;
	attsql TEXT[];
	pkey NAME;
BEGIN

	tabname := tabconfig ->> 'name';
	IF tabname IS NULL THEN
		RAISE EXCEPTION 'Missing "name" in table config %', tabconfig;
	END IF;

	RAISE DEBUG 'Should create table %', tabname;

	sql := format('CREATE TABLE %1$I (', tabname);
	FOR rec IN SELECT jsonb_array_elements( tabconfig -> 'attributes' ) cfg
	LOOP --{
		sql_att := topo_update._app_CreateSchema_table_attribute(rec.cfg);
		sql_attlist := array_append(sql_attlist, sql_att);
	END LOOP; --}
	sql := format('%1$s %2$s',
		sql,
		array_to_string(sql_attlist, ', ')
	);

	-- Add primary key
	pkey := tabconfig ->> 'primary_key';
	IF pkey IS NOT NULL THEN --{
		sql := format('%s, PRIMARY KEY(%I)',
			sql,
			pkey
		);
	END IF; --}

	sql := sql || ')';

	-- TODO: add table constraints ?

	RAISE DEBUG 'SQL: %', sql;
	EXECUTE sql;


	-- Create TopoGeometry columns
	FOR rec IN SELECT jsonb_array_elements( tabconfig -> 'topogeom_columns' ) cfg
	LOOP --{
		sql := format('SELECT topology.AddTopoGeometryColumn(%L, %L, %L, %L, %L)',
			appname || '_sysdata_webclient',
			appname,
			tabname,
			rec.cfg ->> 'name',
			rec.cfg ->> 'type'
		);
		RAISE DEBUG 'SQL: %', sql;
		EXECUTE sql;

		IF NOT COALESCE( (rec.cfg -> 'allow_overlaps')::boolean, true)
		THEN --{
			PERFORM topo_update.add_unique_topo_primitive_constraint(
				tabname::regclass,
				rec.cfg ->> 'name',
				pkey
			);
		END IF; --}

		IF NOT COALESCE( (rec.cfg ->> 'allow_multi')::boolean, true)
		THEN --{
			RAISE WARNING 'Disallowing multi for % layer %.% not implemented yet',
				rec.cfg ->> 'type', tabname, rec.cfg ->> 'name';
		END IF; --}

		IF NOT COALESCE( (rec.cfg ->> 'allow_crossings')::boolean, true)
		THEN --{
			IF rec.cfg ->> 'type' != 'lineal' THEN --{
				RAISE EXCEPTION 'Disallowing crossings is not supported for % layers (%.%)',
					rec.cfg ->> 'type', tabname, rec.cfg ->> 'name';
			END IF; --}
			RAISE WARNING 'Disallowing crossings for % layer %.% not implemented yet',
				rec.cfg ->> 'type', tabname, rec.cfg ->> 'name';
		END IF; --}

		IF NOT COALESCE( (rec.cfg ->> 'allow_gaps')::boolean, true)
		THEN --{
			PERFORM topo_update.add_all_topo_primitive_used_constraint(
				tabname::regclass,
				rec.cfg ->> 'name'
			);
		END IF; --}

	END LOOP; --}



	RETURN tabname::regclass;

END;
$BODY$ LANGUAGE 'plpgsql'; --}

-- Assumes appname is first item in search_path
CREATE OR REPLACE FUNCTION topo_update._app_CreateSchema_domain(
	appname name,
	domconfig jsonb
)
RETURNS VOID AS $BODY$ --{
DECLARE
	domname NAME;
	sql TEXT;
	sql_att TEXT;
	sql_attlist TEXT[];
	rec RECORD;
	attsql TEXT[];
	pkey NAME;
BEGIN

	domname := domconfig ->> 'name';
	IF domname IS NULL THEN
		RAISE EXCEPTION 'Missing "name" in domain config %', domconfig;
	END IF;

	RAISE DEBUG 'Should create domain %', domname;

	sql_att := topo_update._app_CreateSchema_table_attribute(domconfig);

	sql := format('CREATE DOMAIN %1$s', sql_att);

	RAISE DEBUG 'SQL: %', sql;
	EXECUTE sql;


END;
$BODY$ LANGUAGE 'plpgsql'; --}

CREATE OR REPLACE FUNCTION topo_update.app_CreateSchema(
	appname NAME,
	appconfig JSONB
)
RETURNS void AS $BODY$ --{
DECLARE
	sql TEXT;
	topoconfig JSONB;
	rec RECORD;
	cur_search_path TEXT;
	new_search_path TEXT;
	topo_name TEXT;
	topo_id INT;
	appconfig_version TEXT;
BEGIN

	-- 0. Initialization

	-- 0.1. Get current search_path
	SELECT reset_val
	FROM pg_settings
	WHERE name = 'search_path'
	INTO cur_search_path;

	-- 0.3. Set topology name
	topo_name := appname || '_sysdata_webclient';

	-- 0.2. Validate appconfig
	PERFORM topo_update._app_validate_config(appconfig);

	-- 1. Create application topology schema
	topoconfig := appconfig -> 'topology';
	sql := format('SELECT topology.CreateTopology(%1$L, %2$L, %3$L)',
		topo_name,
		COALESCE((topoconfig ->> 'srid')::int, 0),
		COALESCE((topoconfig ->> 'snap_tolerance')::float8, 0)
	);
	RAISE DEBUG 'SQL: %', sql;
	EXECUTE sql INTO topo_id;
	IF topo_id IS NULL THEN
		RAISE EXCEPTION 'Could not create topology using sql %', sql;
	END IF;
	RAISE NOTICE 'Created topology % with id %', topo_name, topo_id;

	-- 2. Create application data tables schema

	-- 2.1. Create the schema

	sql := format('CREATE SCHEMA %I', appname);
	RAISE DEBUG 'SQL: %', sql;
	EXECUTE sql;

	-- 2.2. Set search_path to start with the just-created schema
	new_search_path := quote_ident(appname) || ',' || cur_search_path;
	EXECUTE format('SET search_path TO %s', new_search_path);

	-- 2.3. Ensure all required extension are created
	IF appconfig ? 'extensions' THEN --{
		FOR rec IN SELECT jsonb_array_elements( appconfig -> 'extensions' ) cfg
		LOOP --{
			sql := format('CREATE EXTENSION IF NOT EXISTS %I SCHEMA public', rec.cfg ->> 0);
			RAISE DEBUG 'SQL: %', sql;
			EXECUTE sql;
		END LOOP; --}
	END IF; --}

	-- 2.3.5. Create all application domains
	FOR rec IN SELECT jsonb_array_elements( appconfig -> 'domains' ) cfg
	LOOP --{
		PERFORM topo_update._app_CreateSchema_domain(appname, rec.cfg);
	END LOOP; --}

	-- 2.4. Create all application tables
	FOR rec IN SELECT jsonb_array_elements( appconfig -> 'tables' ) cfg
	LOOP --{
		PERFORM topo_update._app_CreateSchema_table(appname, rec.cfg);
	END LOOP; --}

	-- 2.5. Reset search_path
	EXECUTE format('SET search_path TO %s', cur_search_path);

	-- 3. Create application functions

	-- TODO: decide where to store the functions
	--       (it could be in app schema)

	-- 3.1. Create the schema
	sql := format('CREATE SCHEMA %I', appname || '_sysdata_webclient_functions');
	RAISE DEBUG 'SQL: %', sql;
	EXECUTE sql;

	-- 3.2. Set search_path to start with the just-created schema
	new_search_path := quote_ident(appname || '_sysdata_webclient_functions') || ',' || cur_search_path;
	EXECUTE format('SET search_path TO %s', new_search_path);

	-- 3.3. Store app configuration in the schema
	CREATE TABLE app_config(cfg JSONB);
	INSERT INTO app_config(cfg) VALUES (appconfig);

	-- TODO: optionally generate wrapper functions ?

	-- 3.4. Reset search_path
	EXECUTE format('SET search_path TO %s', cur_search_path);

END;
$BODY$ LANGUAGE 'plpgsql'; --}
