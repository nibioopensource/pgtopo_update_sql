CREATE OR REPLACE FUNCTION topo_update._app_do_AddSurface_Dynamic(
	appname NAME,
	appconfig JSONB,
	surface JSONB
)
RETURNS TEXT AS
$BODY$ --{
DECLARE
	cfg_surfaceLayer JSONB;
	cfg_surfaceLayerTable JSONB;
	cfg_op JSONB;
	cfg_op_params JSONB;
	cfg JSONB;
	surfaceLayerTable REGCLASS;
	surfaceLayerGeomColumn NAME;
	surfaceLayerIdColumn NAME;
	snapTolerance FLOAT8;
	surface_geom GEOMETRY;
	surface_is_multi BOOLEAN;
	rec RECORD;
	colMap JSONB;
	surfaceID TEXT;
BEGIN

	-- Check if the operation is enabled
	IF NOT appconfig -> 'operations' ? 'AddSurface' THEN
		RAISE EXCEPTION 'AddSurface operation not enabled for application %', appname;
	END IF;

	cfg_surfaceLayer := appconfig -> 'surface_layer';

	--RAISE DEBUG 'surfaceLayer: %', cfg_surfaceLayer;

	FOR rec IN SELECT jsonb_array_elements( appconfig -> 'tables' ) cfg
	LOOP --{
		IF rec.cfg ->> 'name' = cfg_surfacelayer ->> 'table_name' THEN
			cfg_surfaceLayerTable := rec.cfg;
			EXIT;
		END IF;
	END LOOP; --}

	--RAISE DEBUG 'surfaceLayerTable: %', cfg_surfaceLayerTable;

	surfaceLayerTable := ( appname || '.' || ( cfg_surfacelayer ->> 'table_name' ) );
	surfaceLayerGeomColumn := cfg_surfacelayer ->> 'geo_column';
	surfaceLayerIdColumn := cfg_surfacelayerTable ->> 'primary_key';

	-- Set parameters defaults
	snapTolerance := 0;

	-- Read parameters defaults from appconfig
	cfg_op_params := appconfig -> 'operations' -> 'AddSurface' -> 'parameters';
	IF cfg_op_params IS NOT NULL THEN --{

		-- Read snapTolerance from config, if found
		cfg := cfg_op_params -> 'snapTolerance';
		IF cfg ? 'units' THEN
			snapTolerance := cfg -> 'units';
			IF cfg -> 'unitsAreMeters' THEN
				RAISE EXCEPTION 'Specifying snapTolerance in meters is unsupported';
			END IF;
		END IF;

	END IF; --}

    RAISE DEBUG 'snapTolerance: %', snapTolerance;


	-- Validate surface geometry,
    surface_geom := ST_GeomFromGeoJSON(surface -> 'geometry');
	IF surface_geom IS NULL THEN
		RAISE EXCEPTION 'Missing geometry in surface';
	END IF;

	IF ST_GeometryType(surface_geom) = 'ST_MultiPolygon' THEN
		IF ST_NumGeometries(surface_geom) > 1 THEN
			surface_is_multi := true;
		END IF;
	ELSIF ST_GeometryType(surface_geom) = 'ST_Polygon' THEN
		surface_is_multi := false;
	ELSE
		RAISE EXCEPTION 'Surface must be either Polygon or MultiPolygon, is % instead', ST_GeometryType(surface_geom);
	END IF;

	--RAISE DEBUG 'Surface is multi ? %', surface_is_multi;
	--RAISE DEBUG 'Surface geom is %', ST_AsEWKT(surface_geom);

	-- TODO: get colmap
	--colMap := ...

	SELECT id FROM topo_update.insert_feature(
		surface_geom,
		surface -> 'properties',

		surfaceLayerTable,
		surfaceLayerGeomColumn,
		surfaceLayerIdColumn,

		colMap,

		snapTolerance
	)
	INTO surfaceID;

	return surfaceID;

END;
$BODY$ LANGUAGE plpgsql; --}

CREATE OR REPLACE FUNCTION topo_update.app_do_AddSurface(

	-- Application name, will be used to fetch
	-- configuration from application schema.
	-- See doc/APP_CONFIG.md
	appname NAME,

	-- A GeoJSON Feature object representing
	-- a single Surface (as a Polygon or MultiPolygon)
	-- to be used to create a new Surface object
	--
	-- Properties of the feature are used to
	-- to contain values to be used for the Surface record
	--
	-- Format of the properties object is as follows:
	--
	--   "properties": {
	--      <column_name>: <value>
	--      [, <column_name>: <value> ] ...
	--   }
	--
	--
	surface JSONB

)
RETURNS TEXT AS
$BODY$ --{
DECLARE
	sql TEXT;
	appconfig JSONB;
BEGIN

	sql := format('SELECT cfg FROM %I.app_config',
			appname || '_sysdata_webclient_functions');
	EXECUTE sql INTO STRICT appconfig;

	--RAISE DEBUG 'APPCONFIG: %', appconfig;

	RETURN topo_update._app_do_AddSurface_Dynamic(
		appname,
		appconfig,
		surface
	);
END;
$BODY$ LANGUAGE plpgsql; --}
