CREATE OR REPLACE FUNCTION topo_update._app_do_AddPoint_Dynamic(
	appname NAME,
	appconfig JSONB,
	point JSONB
)
RETURNS TEXT AS
$BODY$ --{
DECLARE
	cfg_pointLayer JSONB;
	cfg_pointLayerTable JSONB;
	cfg_op JSONB;
	cfg_op_params JSONB;
	cfg JSONB;
	pointLayerTable REGCLASS;
	pointLayerGeomColumn NAME;
	pointLayerIdColumn NAME;
	snapTolerance FLOAT8;
	point_geom GEOMETRY;
	rec RECORD;
	colMap JSONB;
	pointID TEXT;
BEGIN

	-- Check if the operation is enabled
	IF NOT appconfig -> 'operations' ? 'AddPoint' THEN
		RAISE EXCEPTION 'AddPoint operation not enabled for application %', appname;
	END IF;

	cfg_pointLayer := appconfig -> 'point_layer';

	--RAISE DEBUG 'pointLayer: %', cfg_pointLayer;

	FOR rec IN SELECT jsonb_array_elements( appconfig -> 'tables' ) cfg
	LOOP --{
		IF rec.cfg ->> 'name' = cfg_pointlayer ->> 'table_name' THEN
			cfg_pointLayerTable := rec.cfg;
			EXIT;
		END IF;
	END LOOP; --}

	--RAISE DEBUG 'pointLayerTable: %', cfg_pointLayerTable;

	pointLayerTable := ( appname || '.' || ( cfg_pointlayer ->> 'table_name' ) );
	pointLayerGeomColumn := cfg_pointlayer ->> 'geo_column';
	pointLayerIdColumn := cfg_pointlayerTable ->> 'primary_key';

	-- Set parameters defaults
	snapTolerance := 0;

	-- Read parameters defaults from appconfig
	cfg_op_params := appconfig -> 'operations' -> 'AddPoint';
	IF cfg_op_params IS NOT NULL THEN --{

		-- Read snapTolerance from config, if found
		cfg := cfg_op_params -> 'snapTolerance';
		IF cfg ? 'units' THEN
			snapTolerance := cfg -> 'units';
			IF cfg -> 'unitsAreMeters' THEN
				RAISE EXCEPTION 'Specifying snapTolerance in meters is unsupported';
			END IF;
		END IF;

	END IF; --}

    RAISE DEBUG 'snapTolerance: %', snapTolerance;


	-- Validate point geometry,
	point_geom := ST_GeomFromGeoJSON(point -> 'geometry');
	IF point_geom IS NULL THEN
		RAISE EXCEPTION 'Missing geometry in point';
	END IF;

	IF ST_GeometryType(point_geom) != 'ST_Point' THEN
		RAISE EXCEPTION 'Point must be Point, is % instead', ST_GeometryType(point_geom);
	END IF;

	-- TODO: get colmap
	--colMap := ...

	SELECT id FROM topo_update.insert_feature(
		point_geom,
		point -> 'properties',

		pointLayerTable,
		pointLayerGeomColumn,
		pointLayerIdColumn,

		colMap,

		snapTolerance
	)
	INTO pointID;

	return pointID;

END;
$BODY$ LANGUAGE plpgsql; --}

CREATE OR REPLACE FUNCTION topo_update.app_do_AddPoint(

	-- Application name, will be used to fetch
	-- configuration from application schema.
	-- See doc/APP_CONFIG.md
	appname NAME,

	-- A GeoJSON Feature object representing
	-- a single Points (as a LineString MultiLineString)
	-- to be used to create a new Point object
	--
	-- Properties of the feature are used to
	-- to contain values to be used for the Point record
	--
	-- Format of the properties object is as follows:
	--
	--   "properties": {
	--      <column_name>: <value>
	--      [, <column_name>: <value> ] ...
	--   }
	--
	--
	point JSONB

)
RETURNS TEXT AS
$BODY$ --{
DECLARE
	sql TEXT;
	appconfig JSONB;
BEGIN

	sql := format('SELECT cfg FROM %I.app_config',
			appname || '_sysdata_webclient_functions');
	EXECUTE sql INTO STRICT appconfig;

	--RAISE DEBUG 'APPCONFIG: %', appconfig;

	RETURN topo_update._app_do_AddPoint_Dynamic(
		appname,
		appconfig,
		point
	);
END;
$BODY$ LANGUAGE plpgsql; --}
