CREATE OR REPLACE FUNCTION topo_update._app_do_AddPath_minLengthMetersToTopoUnits(
	appname NAME,
	appconfig JSONB,
	pathset JSONB,
	lenM float8
)
RETURNS FLOAT8 AS $BODY$ --{
DECLARE
	area FLOAT8;
	geom geometry;
	geog geography;
	topoSRID INT;
	topoProj4Text TEXT;
	topoUnits TEXT;
	topoToMeter TEXT;
	topoProj TEXT;
BEGIN

	-- Find topo units
	topoSRID := appconfig -> 'topology' ->> 'srid';
	IF topoSRID IS NULL THEN
		-- should we fetch from actual topology otherwise ?
		RAISE EXCEPTION 'Topology SRID must be defined in appconfig';
	END IF;

	-- Find topo proj4text
	SELECT proj4text
	FROM spatial_ref_sys
	WHERE srid = topoSRID
	INTO topoProj4Text;

	topoUnits := (regexp_match(topoProj4Text, '\+units=([^ ]*) '))[1];
	IF topoUnits IS NOT NULL THEN --{

		-- If topo units are already meters we can return immediately
		IF topoUnits = 'm' THEN
			RETURN lenM;
		END IF;

		-- If topo units have known conversion to meters,
		-- we will be able to convert directly
		IF topoUnits = 'us-ft' THEN
			topoToMeter := '0.3048006096';
		ELSIF topoUnits = 'ft' THEN
			topoToMeter := '0.3048';
		ELSE
			RAISE EXCEPTION 'Unsupported conversion from meters to % units, used by topology', topoUnits;
		END IF;

	ELSE --}{

		-- Check if the projection defines an explicit conversion to meters
		topoToMeter := (regexp_match(topoProj4Text, '\+to_meter=([^ ]*) '))[1];

	END IF; --}

	-- If we found a known conversion to meters, we go for it
	IF topoToMeter IS NOT NULL THEN
		RETURN lenM * topoToMeter;
	END IF;


	-- Being there no direct conversion, we should be
	-- using a latlong projection, let's check

	topoProj := (regexp_match(topoProj4Text, '\+proj=([^ ]*) '))[1];
	IF topoProj != 'longlat' THEN
		RAISE EXCEPTION 'Unsupported topology projection for using square meters: %', topoProj;
	END IF;

	-- As topology is using latlong we'll need to
	-- return an area (in square degrees) which may
	-- be different based on the latitude of the
	-- area we are dealing with.
	--

    -- Extract input geometry
    geom := ST_GeomFromGeoJSON(pathset -> 'geometry');

	-- Pick point with highest latitude/longitude values
	geom := ST_PointN(
		ST_BoundingDiagonal(
			geom
		),
		1
	);
	-- Make sure we're in latlon projection, which
	-- is required for _ST_BestSRID to work
	geom := ST_Transform(geom, topoSRID);
	-- Switch to metrical units projection
	geom := ST_Transform(geom, _ST_BestSRID(geom));
	-- Build a line of lenM meters around the geometry's highest lat/lon value
	--geom := ST_Buffer(geom, sqrt(200/PI()), 16);
	geom := ST_MakeLine(geom, ST_Translate(geom, lenM, 0));
	-- TODO: densify the geometry to have less distortion upon projecting ?
	RAISE NOTICE 'Built geom length in meters: % (% targeted)', ST_Length(geom), lenM;
	-- Switch back to latlon (topology) CRS (shall we query it rather than hardcoding it?)
	geom := ST_Transform(geom, topoSRID);
	-- Compute area in target topology SRID
	RETURN ST_Length(geom);

END;
$BODY$ LANGUAGE 'plpgsql'; --}

-- 'usr' parameter is the appconfig
CREATE OR REPLACE FUNCTION topo_update._app_do_AddPath_ColMapProvider(act char, usr JSONB)
RETURNS JSONB AS $BODY$ --{
DECLARE
	map JSONB;
	typData JSONB;
    actmap JSONB := '{"M": "modified", "S": "split", "C": "new"}';
	props JSONB := usr -> 'properties';
	jsk TEXT[];
	rec RECORD;
BEGIN

	typData := usr -> 'path';

	map := topo_update.json_col_map_from_pg(
		typData ->> 'table',
		ARRAY[
			typData ->> 'idcol',
			typData ->> 'geomcol'
		]
	);
	--RAISE DEBUG '-map for %: %', actmap ->> act, jsonb_pretty(map);

	-- For each element of map, prepend to the json array
	-- the (type,act) array
	FOR rec IN SELECT jsonb_object_keys(map) k
	LOOP --{

		jsk := array_agg(x) FROM jsonb_array_elements_text(map -> rec.k) x;

		--RAISE DEBUG 'XXX- map k:% = %', rec.k, jsk;
		jsk := ARRAY[
				actmap ->> act
			] || jsk;
		--RAISE DEBUG 'XXX+ map k:% = %', rec.k, jsk;

		IF props #> jsk IS NULL THEN
			--RAISE DEBUG 'props do not contain % element, dropping mapping for it', jsk;
			map := map - rec.k;
		ELSE
			map := jsonb_set(map, ARRAY[rec.k], to_jsonb(jsk));
		END IF;
	END LOOP; --}

	--RAISE DEBUG '+map for % %: %', actmap ->> act, typmap ->> typ, jsonb_pretty(map);

	RETURN map;
END;
$BODY$ LANGUAGE 'plpgsql'; --}
CREATE OR REPLACE FUNCTION topo_update._app_do_AddPath_Dynamic(
	appname NAME,
	appconfig JSONB,
	path JSONB
)
RETURNS TABLE(fid text, act char, frm text) AS
$BODY$ --{
DECLARE
	cfg_pathLayer JSONB;
	cfg_pathLayerTable JSONB;
	cfg_op JSONB;
	cfg_op_params JSONB;
	cfg JSONB;
	pathLayerTable REGCLASS;
	pathLayerGeomColumn NAME;
	pathLayerIdColumn NAME;
	snapTolerance FLOAT8;
	minAllowedPathLength FLOAT8;
	path_geom GEOMETRY;
	path_is_multiline BOOLEAN;
	rec RECORD;
	colMap JSONB;
	pathID TEXT;
BEGIN

	-- Check if the operation is enabled
	IF NOT appconfig -> 'operations' ? 'AddPath' THEN
		RAISE EXCEPTION 'AddPath operation not enabled for application %', appname;
	END IF;

	cfg_pathLayer := appconfig -> 'path_layer';

	--RAISE DEBUG 'pathLayer: %', cfg_pathLayer;

	FOR rec IN SELECT jsonb_array_elements( appconfig -> 'tables' ) cfg
	LOOP --{
		IF rec.cfg ->> 'name' = cfg_pathlayer ->> 'table_name' THEN
			cfg_pathLayerTable := rec.cfg;
			EXIT;
		END IF;
	END LOOP; --}

	--RAISE DEBUG 'pathLayerTable: %', cfg_pathLayerTable;

	pathLayerTable := ( appname || '.' || ( cfg_pathlayer ->> 'table_name' ) );
	pathLayerGeomColumn := cfg_pathlayer ->> 'geo_column';
	pathLayerIdColumn := cfg_pathlayerTable ->> 'primary_key';

	-- Set parameters defaults
	snapTolerance := 0;
	minAllowedPathLength := 0;

	-- Read parameters defaults from appconfig
	cfg_op_params := appconfig -> 'operations' -> 'AddPath' -> 'parameters';
	IF cfg_op_params IS NOT NULL THEN --{

		-- Read snapTolerance from config, if found
		cfg := cfg_op_params -> 'snapTolerance';
		IF cfg ? 'units' THEN
			snapTolerance := cfg -> 'units';
			IF cfg -> 'unitsAreMeters' THEN
				RAISE EXCEPTION 'Specifying snapTolerance in meters is unsupported';
			END IF;
		END IF;

		-- Read minAllowedPathLength from config, if found
		cfg := cfg_op_params -> 'minAllowedPathLength';
		IF cfg ? 'units' THEN
			minAllowedPathLength := cfg -> 'units';
			IF cfg -> 'unitsAreMeters' THEN
				minAllowedPathLength = topo_update._app_do_AddPath_minLengthMetersToTopoUnits(appname, appconfig, path, minAllowedPathLength);
			END IF;
		END IF;

	END IF; --}

    RAISE DEBUG 'snapTolerance: %', snapTolerance;
    RAISE DEBUG 'minAllowedPathLength: %', minAllowedPathLength;

	RETURN QUERY
	SELECT * FROM topo_update.add_path(
		path,

		pathLayerTable,
		pathLayerGeomColumn,
		pathLayerIdColumn,

		'topo_update._app_do_AddPath_ColMapProvider'::regproc,
		jsonb_build_object(
			'properties', path -> 'properties',
			'path', jsonb_build_object(
				'table', pathLayerTable,
				'geomcol', pathLayerGeomColumn,
				'idcol', pathLayerIdColumn
			)
		),

		snapTolerance,
		minAllowedPathLength
	);

END;
$BODY$ LANGUAGE plpgsql; --}

CREATE OR REPLACE FUNCTION topo_update.app_do_AddPath(

	-- Application name, will be used to fetch
	-- configuration from application schema.
	-- See doc/APP_CONFIG.md
	appname NAME,

	-- A GeoJSON Feature object representing
	-- a single Path (as a LineString or MultiLineString)
	-- to be used to create a new Path object
	--
	-- Properties of the feature are used to
	-- to contain values to be used for the Path record
	--
	-- Format of the properties object is as follows:
	--
	--   "properties": {
	--      <column_name>: <value>
	--      [, <column_name>: <value> ] ...
	--   }
	--
	--
	path JSONB

)
RETURNS TABLE(fid text, act char, frm text) AS
$BODY$ --{
DECLARE
	sql TEXT;
	appconfig JSONB;
BEGIN

	sql := format('SELECT cfg FROM %I.app_config',
			appname || '_sysdata_webclient_functions');
	EXECUTE sql INTO STRICT appconfig;

	--RAISE DEBUG 'APPCONFIG: %', appconfig;

	RETURN QUERY
	SELECT * FROM topo_update._app_do_AddPath_Dynamic(
		appname,
		appconfig,
		path
	);
END;
$BODY$ LANGUAGE plpgsql; --}
