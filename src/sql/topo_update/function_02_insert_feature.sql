--
-- Insert a feature in a table by TopoGeometry/properties pair
--
CREATE OR REPLACE FUNCTION topo_update.insert_feature(

	-- A TopoGeometry
	feature_topogeom topology.TopoGeometry,

	-- The 'properties' component of a GeoJSON Feature object
	-- See https://geojson.org/
	feature_properties JSONB,

	-- Target layer table (where to insert the new feature in)
	layerTable REGCLASS,

	-- Name of the TopoGeometry column in the target layer table
	layerGeomColumn NAME,

	-- Name of the column identifying the records in the target layer table
	layerIDColumn NAME,

	-- JSON mapping of PG columns and values
	-- from JSON attributes and values
	-- See topo_update.json_props_to_pg_cols() function
	layerColMap JSONB,

	-- Feature identifier value, as text representation
	OUT id TEXT
)
AS
$BODY$ -- {
DECLARE
	sql TEXT;
	colnames TEXT[];
	colvals TEXT[];
BEGIN

	SELECT c.colnames, c.colvals
	FROM topo_update.json_props_to_pg_cols(feature_properties, layerColMap) c
	INTO colnames, colvals;

	RAISE DEBUG 'COMPUTED: colnames: %', colnames;
	RAISE DEBUG 'COMPUTED: colvals: %', colvals;

	colnames := array_append(colnames, format('%I', layerGeomColumn));

	colvals := array_append(colvals, format('%L', feature_topogeom));

	sql := format('INSERT INTO %s (%s) VALUES(%s) RETURNING %I',
		layerTable,
		array_to_string(colnames, ','),
		array_to_string(colvals, ','),
		layerIDColumn
	);

	RAISE DEBUG 'SQL: %', sql;

	EXECUTE sql INTO id;

END;
$BODY$ --}
VOLATILE
LANGUAGE plpgsql;

--
-- Insert a feature in a table by Geometry/properties pair
--
CREATE OR REPLACE FUNCTION topo_update.insert_feature(

	-- The 'geometry' component of the feature
	-- Will be reprojected to topology SRS, if needed
	feature_geometry GEOMETRY,

	-- The 'properties' component of a GeoJSON Feature object
	-- See https://geojson.org/
	feature_properties JSONB,

	-- Target layer table (where to insert the new feature in)
	layerTable REGCLASS,

	-- Name of the TopoGeometry column in the target layer table
	layerGeomColumn NAME,

	-- Name of the column identifying the records in the target layer table
	layerIDColumn NAME,

	-- JSON mapping of PG columns and values
	-- from JSON attributes and values
	-- See topo_update.json_props_to_pg_cols() function
	layerColMap JSONB,

	-- Tolerance to use for insertion of topology primitives
	-- used to represent the feature geometry.
	-- May be NULL to use topology-default tolerance.
	-- The value is to be given in the units of the target topology
	-- projection.
	tolerance FLOAT8,

	-- Feature identifier value, as text representation
	OUT id TEXT,

	-- Feature TopoGeometry
	OUT topogeom topology.TopoGeometry
)
AS
$BODY$ -- {
DECLARE
	toponame TEXT;
	topo_srid INT;
	layer_id INT;
	sql TEXT;
	rec RECORD;
	procName TEXT := 'topo_update.insert_feature';
BEGIN

	-- TODO: do this one in the calling function and receive this
	--			 information pre-computed
	SELECT t.name, l.layer_id, t.srid
		FROM topology.layer l, topology.topology t, pg_class c, pg_namespace n
		WHERE l.schema_name = n.nspname
		AND l.table_name = c.relname
		AND c.oid = layerTable
		AND c.relnamespace = n.oid
		AND l.feature_column = layerGeomColumn
		AND l.topology_id = t.id
		INTO toponame, layer_id, topo_srid;

	RAISE DEBUG '%: feature_geometry SRID: %', procName, ST_SRID(feature_geometry);
	RAISE DEBUG '%: topo name: %', procName, toponame;
	RAISE DEBUG '%: topo srid: %', procName, topo_srid;
	RAISE DEBUG '%: layer_id: %', procName, layer_id;

	-- Reproject input geometry if needed
	IF ST_Srid(feature_geometry) != topo_srid
	THEN
		RAISE DEBUG '%: feature srid (%) does not match topology srid (%), reprojecting',
			procName,
			ST_Srid(feature_geometry),
			topo_srid;
		feature_geometry := ST_Transform(feature_geometry, topo_srid);
	END IF;

	topogeom := topology.toTopoGeom(
		feature_geometry,
		toponame,
		layer_id,
		tolerance
	);

	RAISE DEBUG '%: TopoGeom: % (srid: %)',
		procName,
		topogeom,
		ST_SRID(topogeom);

	SELECT * FROM topo_update.insert_feature(
		topogeom,
		feature_properties,
		layerTable,
		layerGeomColumn,
		layerIDColumn,
		layerColMap
	)
	INTO id;

END;
$BODY$ --}
VOLATILE
LANGUAGE plpgsql;

--
-- Insert a feature in a table
--
CREATE OR REPLACE FUNCTION topo_update.insert_feature(

	-- A GeoJSON Feature object
	-- See https://geojson.org/
	feature JSONB,

	-- SRID of geometry, or NULL for leaving it as unknown
	srid INT,

	-- Target layer table (where to insert the new feature in)
	layerTable REGCLASS,

	-- Name of the TopoGeometry column in the target layer table
	layerGeomColumn NAME,

	-- Name of the column identifying the records in the target layer table
	layerIDColumn NAME,

	-- JSON mapping of PG columns and values
	-- from JSON attributes and values
	-- See topo_update.json_props_to_pg_cols() function
	layerColMap JSONB,

	-- Tolerance to use for insertion of topology primitives
	-- used to represent the feature geometry.
	-- May be NULL to use topology-default tolerance.
	-- The value is to be given in the units of the target topology
	-- projection.
	tolerance FLOAT8,

	-- Feature identifier value, as text representation
	OUT id TEXT,

	-- Feature TopoGeometry
	OUT topogeom topology.TopoGeometry
)
AS
$BODY$ -- {
DECLARE
	sql TEXT;
	rec RECORD;
	geom GEOMETRY;
	props JSONB;
BEGIN

	props := feature -> 'properties';
	-- Will use the "crs" member in the GeoJSON, if any
	geom := ST_GeomFromGeoJSON(feature -> 'geometry');
	--RAISE DEBUG 'GEOM: %', ST_AsEWKT(geom);

	-- Override SRID if given
	RAISE DEBUG 'SRID: %', srid;
	IF srid IS NOT NULL THEN
		geom := ST_SetSRID(geom, srid);
	END IF;

	SELECT * FROM topo_update.insert_feature(
		geom,
		props,
		layerTable,
		layerGeomColumn,
		layerIDColumn,
		layerColMap,
		tolerance
	)
	INTO rec;

	id := rec.id;
	topogeom := rec.topogeom;

END;
$BODY$ --}
VOLATILE
LANGUAGE plpgsql;

