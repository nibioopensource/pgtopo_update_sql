-- Transform a tolerance value from a projection to another
CREATE OR REPLACE FUNCTION topo_update.min_transformed_tolerance(
	location geometry,
	tolerance float8,
	target_srid int
)
RETURNS FLOAT8 AS $BODY$ --{
DECLARE
	hull_input GEOMETRY;
	buff_hull_input GEOMETRY;
	trans_hull_input GEOMETRY;
	trans_buff_hull_input GEOMETRY;
BEGIN

	-- Short cut, if target srid == source srid, return
	-- the input tolerance
	IF ST_Srid(location) = target_srid THEN
		RETURN tolerance;
	END IF;

	-- Algorithm:
	--  1. Take the convex hull of the location
	--  2. Buffer the convex hull with the given
	--     tolerance.
	--  3. Transform both the hull and the buffer
	--     to target SRID
	--  4. Compute min distance between the
	--     facets of the transformed input
	--     and the facets of the transformed
	--     buffered input

	hull_input := ST_ConvexHull(location);
	RAISE DEBUG 'Input hull: %', ST_AsText(hull_input);

	buff_hull_input := ST_ExteriorRing(
		ST_Buffer(
			hull_input,
			tolerance
		)
	);
	RAISE DEBUG 'Buf input hull: %', ST_AsText(buff_hull_input);

	trans_hull_input := ST_Transform(
		hull_input,
		target_srid
	);
	RAISE DEBUG 'Trans input hull: %', ST_AsText(trans_hull_input);

	IF ST_Dimension(trans_hull_input) > 1 THEN
		trans_hull_input := ST_ExteriorRing(
			trans_hull_input
		);
		RAISE DEBUG 'Trans input hull exterior ring: %', ST_AsText(trans_hull_input);
	END IF;

	trans_buff_hull_input := ST_Transform(
		buff_hull_input,
		target_srid
	);
	RAISE DEBUG 'Trans buf input hull: %', ST_AsText(trans_buff_hull_input);

	RETURN ST_Distance(
		trans_hull_input,
		trans_buff_hull_input
	);
END;
$BODY$ LANGUAGE 'plpgsql'; --}


