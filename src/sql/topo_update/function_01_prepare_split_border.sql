DROP FUNCTION IF EXISTS topo_update._select_split_component(GEOMETRY, TEXT, BOOLEAN);

--
-- Split a linestring geometry by edges of a given topology
-- and return components of the split having both
-- endpoints within minimum tolerance distance from those edges
-- as a geometry collection.
--
-- Optionally (and by default) only return the longest such component
-- as a single geometry.
--
CREATE OR REPLACE FUNCTION topo_update._select_split_component(

	-- A NON-closed linestring
	geom_in GEOMETRY,

	-- Reference topology
	topo topology.topology,

	longestOnly BOOLEAN DEFAULT true,

	-- Pass 0 to use the topology precision,
	-- or computed min tolerance
	snapTolerance float8 DEFAULT 0
)
RETURNS GEOMETRY
AS $BODY$ --{
DECLARE
	sql TEXT;
	geom_out GEOMETRY;
	intersecting_edges GEOMETRY;
	proc_name TEXT = 'topo_update._select_split_component';
	rec RECORD;
	split_components GEOMETRY[];
	endpoint GEOMETRY;
	tol FLOAT8;
	-- We can increase this a lot becuase we now use the number of faces generated as test.
	-- We also have checks on min area this means that we avoid all those small surfaces.
	max_geometries_intersects INT = 20;
BEGIN

	tol := snapTolerance;
	IF tol = 0 THEN
		tol := topo.precision;
	END IF;
	IF tol = 0 THEN
		tol := topology._st_mintolerance(geom_in);
	END IF;

	RAISE DEBUG '%: working tolerance: %', proc_name, tol;

	-- Collect the edges intersecting
	-- the input geometry
	sql := format(
		$$
SELECT ST_Collect(e.geom) as geom
FROM %1$I.edge e
WHERE ST_Intersects(e.geom, $1)
		$$,
		topo.name
	);
	EXECUTE sql
	USING geom_in
	INTO intersecting_edges;

	RAISE DEBUG '%: % edges intersect input line, collected as: %',
		proc_name, ST_NumGeometries(intersecting_edges), ST_AsEWKT(intersecting_edges);

	-- TODO: check intersection exists with at least 1 edge ?
	-- TODO: check there are at least 2 intersections with the edges ?
	-- TODO: exclude dangling edges from the check ?

	-- Snap the input geometry to the intersecting edges
	geom_in := ST_Snap(geom_in, intersecting_edges, tol);
	RAISE DEBUG '%: input line (of type %) snapped to intersecting edges: %',
		proc_name, ST_GeometryType(geom_in), ST_AsEWKT(geom_out);

	-- Split the input geometry by
	-- snapped edges
	geom_out := ST_Split(geom_in, intersecting_edges);

	RAISE DEBUG '%: snapped input line split by intersecting edges into % components, collected as: %',
		proc_name, ST_NumGeometries(geom_out), ST_AsEWKT(geom_out);

	-- We need to accept
	IF ST_GeometryType(geom_in) = 'ST_MultiLineString' THEN
		max_geometries_intersects := max_geometries_intersects * 4;
	END IF;

	IF ST_NumGeometries(geom_out) > max_geometries_intersects THEN
		RAISE EXCEPTION 'Non closed linestring of type % intersects more than % (% times) with existing edges',
			ST_GeometryType(geom_in), max_geometries_intersects-1, ST_NumGeometries(geom_out)-1;
	END IF;

	-- Select, among all components of the splitted input geometry,
	-- the component touching existing edges on both sides.
	FOR rec IN
		SELECT (ST_Dump(geom_out)).geom
	LOOP
		RAISE DEBUG '%: split component:  %', proc_name, ST_AsEWKT(rec.geom);

		-- Skip component if it does not touch intersecting
		-- edges on both sides

		endpoint := ST_StartPoint(rec.geom);

		IF NOT ST_DWithin(
			endpoint,
			intersecting_edges,
			tol)
		THEN
			RAISE DEBUG '%: component start point not within min tolerance from edges',
				proc_name;
			CONTINUE;
		END IF;

		endpoint := ST_EndPoint(rec.geom);

		IF NOT ST_DWithin(
			endpoint,
			intersecting_edges,
			tol)
		THEN
			RAISE DEBUG '%: component end point not within min tolerance from edges',
				proc_name;
			CONTINUE;
		END IF;

		split_components := array_append(split_components, rec.geom);
	END LOOP;

	IF longestOnly THEN
		SELECT geom
		FROM unnest(split_components) geom
		ORDER BY ST_Length(geom)
		LIMIT 1
		INTO rec;
		RETURN rec.geom;
	ELSE
		RETURN ST_Collect(split_components);
	END IF;
END;
$BODY$ --}
LANGUAGE 'plpgsql';

--
-- Prepare a line geometry for use in splitting a face in the
-- given topology.
--
-- Face splitting can happen by two occurrences:
--		1. Input line forms a loop by itself
--		2. Input line forms loops with existing edges
--
-- This function modifies incoming line so that it is only
-- composed by the portion of it that will effectively split
-- at least one face.
--
-- Non-simple lines are turned into a ring, so to form a loop
-- by themselves (occurrence 1 above).
--
-- Non-closed lines are checked for intersecting existing
-- edges in at least 2 points and cut to only return the
-- portions touching existing edges at both endpoints.
--
-- NOTE: the returned line is still not _guaranteed_ to split a
--       face, because it could connect two isolated edges
--
CREATE OR REPLACE FUNCTION topo_update.prepare_split_border(
	line GEOMETRY,
	topo topology.topology,
	tolerance FLOAT8 DEFAULT 0
)
RETURNS GEOMETRY
AS $BODY$ --{
DECLARE
	sql TEXT;
	num_edge_intersects INT;
	proc_name text = 'topo_update._prepare_split_border';
	input_line GEOMETRY =  line;
	closed_line GEOMETRY;
BEGIN

	IF NOT ST_IsSimple(line) THEN
		-- Non-simple line means there's a self-intersection.
		-- In this case we try to build a closed ring out of it,
		-- dropping loosing ends as we go.
		BEGIN
			closed_line := ST_BuildArea(ST_UnaryUnion(line))::geometry;
			RAISE DEBUG '%: ST_BuildArea result is %',
				proc_name, ST_AsEWKT(closed_line);
			IF closed_line IS NOT NULL THEN
				closed_line := ST_ExteriorRing(closed_line);
				RAISE DEBUG '%: ST_ExteriorRing result is %',
					proc_name, ST_AsEWKT(closed_line);
				line := closed_line;
			END IF;
		EXCEPTION WHEN others THEN
			RAISE EXCEPTION '%: BuildArea failed on non-simple input: % -> %',
				proc_name, ST_AsEWKT(input_line,13), SQLERRM;
		END;
	END IF;



	IF NOT ST_IsClosed(line) THEN
		-- If input line is not closed, split it by existing edges
		-- and pick all non-dangling components
		--
		-- Bail out if there are no non-dangling components
		--
		line := topo_update._select_split_component(
			line,
			topo,
			false, -- keep all components
			tolerance
		);

		RAISE DEBUG '%: split components are %', proc_name, ST_AsText(line);

		IF line IS NULL THEN
			RAISE EXCEPTION 'Non closed linestring does not intersect at least twice with existing edges for input: % -> %',
				proc_name, ST_AsEWKT(input_line,13);
		END IF;
	END IF;

	RETURN line;
END; --}
$BODY$ LANGUAGE 'plpgsql';


-- Kept for backward compatibility
CREATE OR REPLACE FUNCTION topo_update.prepare_split_border(
	line GEOMETRY,
	toponame TEXT
)
RETURNS GEOMETRY
AS
$BODY$
	SELECT topo_update.prepare_split_border(
		$1,
		(select t from topology.topology t WHERE name = $2)
	);
$BODY$ LANGUAGE 'sql';
