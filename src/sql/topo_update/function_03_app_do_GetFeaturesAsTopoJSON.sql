-- TODO: move this in its own file, with its own tests
CREATE OR REPLACE FUNCTION topo_update.srid_to_json_crs(
	srid INT
)
RETURNS JSONB AS $$
DECLARE
	crs TEXT;
	p_srid INT = $1;
BEGIN
	SELECT auth_name || ':' || auth_srid
	FROM public.spatial_ref_sys s
	WHERE s.srid = p_srid
	INTO crs;

	IF FOUND THEN
		RETURN jsonb_build_object(
			'properties', jsonb_build_object('name', crs),
			'type', 'name'
		);
	ELSE
		RAISE EXCEPTION 'Could not found spatial_ref_sys entry for SRID %', srid;
	END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION topo_update._app_do_GetFeaturesAsTopoJSON_Dynamic(
	appname NAME,
	appconfig JSONB,
	bbox GEOMETRY,
	out_srid INTEGER,
	max_decimal_digits INTEGER
)
RETURNS JSONB AS
$BODY$ --{
DECLARE
	rec RECORD;
	cfg_Operation JSONB;
	cfg_tables JSONB;
	cfg_table JSONB;
	cfg_layer JSONB;

	ident_quoted_attnames TEXT[];
	geoms_json JSONB[];
	arcs_json JSONB[];

	srid INT;

	--attributes_json JSONB;

	role TEXT;
	roles_supported TEXT[] := ARRAY[ 'border_layer', 'surface_layer', 'path_layer', 'point_layer' ];
	roles_found INT := 0;

	sql TEXT;

	ret JSONB;

	v_cnt int;

BEGIN

	-- Check if the operation is enabled
	cfg_Operation := appconfig -> 'operations' -> 'GetFeaturesAsTopoJSON';
	IF cfg_Operation IS NULL THEN
		RAISE EXCEPTION 'GetFeaturesAsTopoJSON operation not enabled for application %', appname;
	END IF;

	-- Get topology info
	srid := appconfig #>> ARRAY[ 'topology', 'srid' ];

	--RAISE DEBUG 'borderLayer: %', cfg_borderLayer;

	-- Create edgemap table
	CREATE TEMP TABLE topo_update_topology_topojson_edgemap (
		arc_id serial primary key,
		edge_id int
	);
	CREATE INDEX ON topo_update_topology_topojson_edgemap(edge_id);

	cfg_tables := appconfig -> 'tables';
	IF cfg_tables IS NULL THEN
		RAISE EXCEPTION 'Config of application % is broken: missing "tables" element', appname;
	END IF;

	-- Dump intersecting features from border or surface layers

	FOR role IN SELECT unnest( roles_supported )
	LOOP --{
		RAISE DEBUG 'Looking for %', role;

		cfg_layer = appconfig -> role;
		IF cfg_layer IS NULL THEN
			RAISE DEBUG 'Role % is not defined in app % config', role, appname;
			CONTINUE;
		END IF;
		roles_found := roles_found + 1;

		-- Find tables corresponding to layer
		FOR rec IN SELECT jsonb_array_elements( cfg_tables ) cfg
		LOOP
			IF rec.cfg ->> 'name' = cfg_layer ->> 'table_name' THEN
				cfg_table := rec.cfg;
				EXIT;
			END IF;
		END LOOP;

		IF cfg_table IS NULL THEN
			RAISE EXCEPTION 'App % config is broken: table "%" referenced in % not found in "tables"',
				appname,
				cfg_layer ->> 'table_name',
				role;
		END IF;

		RAISE DEBUG 'Should dump table %.% (for layer %)',
			cfg_table ->> 'name',
			cfg_layer ->> 'geo_column',
			role
		;

		-- Build array of quoted attribute names
		SELECT array_agg( quote_ident(a ->> 'name') )
		FROM (
			SELECT jsonb_array_elements( cfg_table -> 'attributes' ) a
		) foo
		INTO ident_quoted_attnames;

		RAISE DEBUG 'Quoted att names: %', array_to_string(ident_quoted_attnames, ', ');

		sql := format(
			$$
				SELECT
					$2 || array_agg(
						jsonb_concat(
							topology.AsTopoJSON(t.%1$I, %2$L)::jsonb,
							jsonb_build_object(
								'properties',
								to_jsonb( l )
							)
						)
						ORDER BY t.%6$I
					)
				FROM %4$I.%5$I t, lateral ( SELECT %3$s ) l
				WHERE ST_Intersects(
					%1$I, $1
				)
			$$,
			cfg_layer ->> 'geo_column', ----------------------- %1
			'pg_temp.topo_update_topology_topojson_edgemap', -- %2
			array_to_string(ident_quoted_attnames, ','), ------ %3
			appname, ------------------------------------------ %4
			cfg_table ->> 'name', ----------------------------- %5
			cfg_table ->> 'primary_key' ----------------------- %6
		);

		--RAISE DEBUG 'SQL: %', sql;

		EXECUTE sql USING bbox, geoms_json
		INTO geoms_json;

		--RAISE DEBUG 'geometries after %: %',
		--	role, jsonb_pretty(to_jsonb(geoms_json));

	END LOOP; --}

	IF roles_found = 0 THEN
		RAISE EXCEPTION
			'GetFeaturesAsTopoJSON requires at least one of %, '
			'but application "%" has none of them',
			roles_supported, appname;
	END IF;

	-- Add arcs

	IF out_srid = 0 THEN
		out_srid = srid;
	END IF;

	sql := format(
		$$
			SELECT
					array_agg(
						ST_AsGeoJSON(%3$s, %2$s)::jsonb -> 'coordinates'
						ORDER BY em.arc_id
					)
			FROM pg_temp.topo_update_topology_topojson_edgemap em
			JOIN %1$I.edge_data e ON (em.edge_id = e.edge_id)
		$$,
		format('%s_sysdata_webclient', appname),
		max_decimal_digits,
		CASE WHEN srid != out_srid THEN
			format('ST_Transform(e.geom, %s)', out_srid)
		ELSE
			'e.geom'
		END
	);
	RAISE DEBUG 'SQL: %', sql;

	EXECUTE sql INTO arcs_json;

--	RAISE DEBUG 'arcs: %', jsonb_pretty(arcs_json);

	DROP TABLE topo_update_topology_topojson_edgemap;

	-- TODO: reduce output if there are no objects ?

	ret := jsonb_build_object(
		'type', 'Topology',
		'crs', topo_update.srid_to_json_crs(out_srid),
		'objects', jsonb_build_object(
			'collection', jsonb_build_object(
				'type', 'GeometryCollection',
				'geometries', to_jsonb(geoms_json)
			)
		),
		'arcs', to_jsonb(arcs_json)
	);

	RETURN jsonb_strip_nulls(ret);

END;
$BODY$ LANGUAGE plpgsql; --}

-- Enhanced before release by the version taking
-- optional out_srid and maxdecimaldigits parameters
-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/266
-- This old signature is retained for backward compatibility and will
-- use out_srid=0 and maxdecimaldigits=9
CREATE OR REPLACE FUNCTION topo_update._app_do_GetFeaturesAsTopoJSON_Dynamic(
	appname NAME,
	appconfig JSONB,
	bbox GEOMETRY
)
RETURNS JSONB AS $$
	SELECT topo_update._app_do_GetFeaturesAsTopoJSON_Dynamic($1,$2,$3,0,9);
$$ LANGUAGE 'sql';

CREATE OR REPLACE FUNCTION topo_update.app_do_GetFeaturesAsTopoJSON(

	-- Application name, will be used to fetch
	-- configuration from application schema.
	-- See doc/APP_CONFIG.md
	appname NAME,

	-- A Geometry object whose bounding box will be used
	-- to select the features to be returned. Only intesecting
	-- features will be returned.
	bbox GEOMETRY,

	-- Identifier of the spatial reference system to use for the
	-- output arc coordinates. If 0, it will use the app topology SRID.
	out_srid INTEGER,

	-- Maximum number of decimal digits to use for the arc coordinates.
	max_decimal_digits INTEGER

)
--
-- Returns Border and Surface features in TopoJSON format, see
-- https://github.com/mbostock/topojson-specification/blob/master/README.md
--
RETURNS JSONB AS
$BODY$ --{
DECLARE
	sql TEXT;
	appconfig JSONB;
BEGIN

	sql := format('SELECT cfg FROM %I.app_config',
			(appname || '_sysdata_webclient_functions') );

	--RAISE WARNING 'SQL: %', sql;

	BEGIN
		EXECUTE sql INTO STRICT appconfig;
	EXCEPTION
	WHEN undefined_table THEN
		RAISE EXCEPTION $$Unexistent application '%'$$, appname
			USING DETAIL = format('%s (%s)', SQLERRM, SQLSTATE);
		RETURN NULL;
	END;

	--RAISE WARNING 'APPCONFIG: %', appconfig;

	RETURN topo_update._app_do_GetFeaturesAsTopoJSON_Dynamic(
		appname,
		appconfig,
		bbox,
		out_srid,
		max_decimal_digits
	);
END;
$BODY$ LANGUAGE plpgsql; --}

-- Enhanced before release by the version taking
-- optional out_srid and maxdecimaldigits parameters
-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/266
-- This old signature is retained for backward compatibility and will
-- use out_srid=0 and maxdecimaldigits=9
CREATE OR REPLACE FUNCTION topo_update.app_do_GetFeaturesAsTopoJSON(
	appname NAME,
	bbox GEOMETRY
)
RETURNS JSONB AS $$
	SELECT topo_update.app_do_GetFeaturesAsTopoJSON($1,$2,0,9);
$$ LANGUAGE 'sql';
