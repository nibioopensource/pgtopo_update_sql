CREATE OR REPLACE FUNCTION topo_update._app_do_AddBordersSplitSurface_minAreaMetersToTopoUnits(
	appname NAME,
	appconfig JSONB,
	borderset JSONB,
	areaM2 float8
)
RETURNS FLOAT8 AS $BODY$ --{
DECLARE
	area FLOAT8;
	geom geometry;
	geog geography;
	topoSRID INT;
	topoProj4Text TEXT;
	topoUnits TEXT;
	topoToMeter TEXT;
	topoProj TEXT;
BEGIN

	-- Find topo units
	topoSRID := appconfig -> 'topology' ->> 'srid';
	IF topoSRID IS NULL THEN
		-- should we fetch from actual topology otherwise ?
		RAISE EXCEPTION 'Topology SRID must be defined in appconfig';
	END IF;

	-- Find topo proj4text
	SELECT proj4text
	FROM spatial_ref_sys
	WHERE srid = topoSRID
	INTO topoProj4Text;

	topoUnits := (regexp_match(topoProj4Text, '\+units=([^ ]*) '))[1];
	IF topoUnits IS NOT NULL THEN --{

		-- If topo units are already meters we can return immediately
		IF topoUnits = 'm' THEN
			RETURN areaM2;
		END IF;

		-- If topo units have known conversion to meters,
		-- we will be able to convert directly
		IF topoUnits = 'us-ft' THEN
			topoToMeter := '0.3048006096';
		ELSIF topoUnits = 'ft' THEN
			topoToMeter := '0.3048';
		ELSE
			RAISE EXCEPTION 'Unsupported conversion from square meters to % units, used by topology', topoUnits;
		END IF;

	ELSE --}{

		-- Check if the projection defines an explicit conversion to meters
		topoToMeter := (regexp_match(topoProj4Text, '\+to_meter=([^ ]*) '))[1];

	END IF; --}

	-- If we found a known conversion to meters, we go for it
	IF topoToMeter IS NOT NULL THEN
		RETURN areaM2 * topoToMeter * topoToMeter;
	END IF;


	-- Being there no direct conversion, we should be
	-- using a latlong projection, let's check

	topoProj := (regexp_match(topoProj4Text, '\+proj=([^ ]*) '))[1];
	IF topoProj != 'longlat' THEN
		RAISE EXCEPTION 'Unsupported topology projection for using square meters: %', topoProj;
	END IF;

	-- As topology is using latlong we'll need to
	-- return an area (in square degrees) which may
	-- be different based on the latitude of the
	-- area we are dealing with.
	--

    -- Extract input geometry
    geom := ST_GeomFromGeoJSON(borderset -> 'geometry');

	-- Pick point with highest latitude/longitude values
	geom := ST_PointN(
		ST_BoundingDiagonal(
			geom
		),
		1
	);
	-- Make sure we're in latlon projection, which
	-- is required for _ST_BestSRID to work
	geom := ST_Transform(geom, topoSRID);
	-- Switch to metrical units projection
	geom := ST_Transform(geom, _ST_BestSRID(geom));
	-- Build a rectangle of ~200 square meters around the geometry's highest lat/lon value
	--geom := ST_Buffer(geom, sqrt(200/PI()), 16);
	geom := ST_Expand(geom, sqrt(areaM2)/2.0);
	-- TODO: densify the geometry to have less distortion upon projecting ?
	RAISE NOTICE 'Built geom area in square meters: % (% targeted)', ST_Area(geom), areaM2;
	-- Switch back to latlon (topology) CRS (shall we query it rather than hardcoding it?)
	geom := ST_Transform(geom, topoSRID);
	-- Compute area in target topology SRID
	RETURN ST_Area(geom);

END;
$BODY$ LANGUAGE 'plpgsql'; --}

-- 'usr' parameter is the appconfig
CREATE OR REPLACE FUNCTION topo_update._app_do_AddBordersSplitSurface_ColMapProvider(typ char, act char, usr JSONB)
RETURNS JSONB AS $BODY$ --{
DECLARE
	map JSONB;
	typData JSONB;
    typmap JSONB := '{"S": "Surface", "B": "Border"}';
    actmap JSONB := '{"M": "modified", "S": "split", "C": "new"}';
	props JSONB := usr -> 'properties';
	jsk TEXT[];
	rec RECORD;
BEGIN

	IF typ = 'S' THEN
		typData := usr -> 'surface';
	ELSE
		typData := usr -> 'border';
	END IF;

	map := topo_update.json_col_map_from_pg(
		typData ->> 'table',
		ARRAY[
			typData ->> 'idcol',
			typData ->> 'geomcol'
		]
	);
	--RAISE DEBUG '-map for % %: %', actmap ->> act, typmap ->> typ, jsonb_pretty(map);

	-- For each element of map, prepend to the json array
	-- the (type,act) array
	FOR rec IN SELECT jsonb_object_keys(map) k
	LOOP --{

		jsk := array_agg(x) FROM jsonb_array_elements_text(map -> rec.k) x;

		--RAISE DEBUG 'XXX- map k:% = %', rec.k, jsk;
		jsk := ARRAY[
				typmap ->> typ,
				actmap ->> act
			] || jsk;
		--RAISE DEBUG 'XXX+ map k:% = %', rec.k, jsk;

		IF props #> jsk IS NULL THEN
			--RAISE DEBUG 'props do not contain % element, dropping mapping for it', jsk;
			map := map - rec.k;
		ELSE
			map := jsonb_set(map, ARRAY[rec.k], to_jsonb(jsk));
		END IF;
	END LOOP; --}

	--RAISE DEBUG '+map for % %: %', actmap ->> act, typmap ->> typ, jsonb_pretty(map);

	RETURN map;
END;
$BODY$ LANGUAGE 'plpgsql'; --}


CREATE OR REPLACE FUNCTION topo_update._app_do_AddBordersSplitSurfaces_Dynamic(
	appname NAME,
	appconfig JSONB,
	borderset JSONB
)
RETURNS TABLE(fid text, typ char, act char, frm text) AS
$BODY$ --{
DECLARE
	cfg_surfaceLayer JSONB;
	cfg_surfaceLayerTable JSONB;
	cfg_borderLayer JSONB;
	cfg_borderLayerTable JSONB;
	cfg_op JSONB;
	cfg_op_params JSONB;
	cfg JSONB;
	surfaceLayerTable REGCLASS;
	surfaceLayerGeomColumn NAME;
	surfaceLayerIdColumn NAME;
	borderLayerTable REGCLASS;
	borderLayerGeomColumn NAME;
	borderLayerIdColumn NAME;
	snapTolerance FLOAT8;
	minToleratedFaceArea FLOAT8;
	maxAllowedSplitSurfaceCount INT;
	maxAllowedNewSurfaceCount INT;
	rec RECORD;
	borderset_geom GEOMETRY;
	borderset_polygonal GEOMETRY;
	borderset_is_multiline BOOL;
	behaviourType TEXT;
BEGIN

	-- Check if the operation is enabled
	IF NOT appconfig -> 'operations' ? 'AddBordersSplitSurfaces' THEN
		RAISE EXCEPTION 'AddBordersSplitSurfaces operation not enabled for application %', appname;
	END IF;

	cfg_surfaceLayer := appconfig -> 'surface_layer';
	cfg_borderLayer := appconfig -> 'border_layer';

	--RAISE DEBUG 'surfacelayer: %', cfg_surfaceLayer;
	--RAISE DEBUG 'borderLayer: %', cfg_borderLayer;

	FOR rec IN SELECT jsonb_array_elements( appconfig -> 'tables' ) cfg
	LOOP --{
		IF rec.cfg ->> 'name' = cfg_surfacelayer ->> 'table_name' THEN
			cfg_surfaceLayerTable := rec.cfg;
		ELSIF rec.cfg ->> 'name' = cfg_borderlayer ->> 'table_name' THEN
			cfg_borderLayerTable := rec.cfg;
		END IF;
		IF cfg_borderLayerTable IS NOT NULL AND
		   cfg_surfaceLayerTable IS NOT NULL
		THEN
			EXIT;
		END IF;
	END LOOP; --}

	--RAISE DEBUG 'surfaceLayerTable: %', cfg_surfaceLayerTable;
	--RAISE DEBUG 'borderLayerTable: %', cfg_borderLayerTable;

	surfaceLayerTable := ( appname || '.' || ( cfg_surfaceLayer ->> 'table_name' ) );
	surfaceLayerGeomColumn := cfg_surfacelayer ->> 'geo_column';
	surfaceLayerIdColumn := cfg_surfacelayerTable ->> 'primary_key';

	borderLayerTable := ( appname || '.' || ( cfg_borderlayer ->> 'table_name' ) );
	borderLayerGeomColumn := cfg_borderlayer ->> 'geo_column';
	borderLayerIdColumn := cfg_borderlayerTable ->> 'primary_key';

	-- Set parameters defaults
	snapTolerance := 0;
	minToleratedFaceArea := 0;
	maxAllowedSplitSurfaceCount := 5;

	-- Validate borderset geometry,
    borderset_geom := ST_GeomFromGeoJSON(borderset -> 'geometry');
	IF borderset_geom IS NULL THEN
		RAISE EXCEPTION 'Missing geometry in borderset';
	END IF;

	IF ST_GeometryType(borderset_geom) = 'ST_MultiLineString' THEN
		IF ST_NumGeometries(borderset_geom) > 1 THEN
			borderset_is_multiline := true;
		END IF;
	ELSIF ST_GeometryType(borderset_geom) = 'ST_LineString' THEN
		borderset_is_multiline := false;
	ELSE
		RAISE EXCEPTION 'Borderset must be either LineString or MultiLineString, is % instead', ST_GeometryType(borderset_geom);
	END IF;

	--RAISE DEBUG 'Borderset is multiline ? %', borderset_is_multiline;

	--RAISE DEBUG 'Borderset geom is %', ST_AsText(borderset_geom);

	-- Analyze borderset geometry to determine behaviour
	borderset_polygonal := ST_BuildArea(ST_UnaryUnion(borderset_geom));
	IF borderset_polygonal IS NULL THEN
		-- RAISE DEBUG 'Borderset does NOT form rings';
		-- Lines do not form a ring
		behaviourType := 'forOpenLines';
	ELSE
		-- RAISE DEBUG 'Borderset form rings';
		IF borderset_is_multiline THEN
			RAISE EXCEPTION 'Invalid borderset: multiple lines cannot form rings, use single line for that';
		END IF;
		behaviourType := 'forClosedLines';
		-- TODO: rewrite borderset with just the boundary of the found area ?
	END IF;

	cfg_op := appconfig -> 'operations' -> 'AddBordersSplitSurfaces' -> 'parameters';
	--RAISE DEBUG 'XXX behaviourType:%, params:%', behaviourType, jsonb_pretty(cfg_op);
	IF cfg_op IS NOT NULL THEN --{

		-- Read parameters from appconfig
		cfg_op_params :=
			COALESCE(cfg_op -> 'default', '{}')
			||
			COALESCE(cfg_op -> behaviourType, '{}')
		;
	END IF; --}

	--RAISE DEBUG 'XXX Operation configuration (behavior %): %', behaviourType, jsonb_pretty(cfg_op_params);

	-- Read parameters defaults from appconfig
	IF cfg_op_params IS NOT NULL THEN --{
		-- Read minAllowedNewSurfaceArea from config, if found
		cfg := cfg_op_params -> 'minAllowedNewSurfacesArea';
		IF cfg ? 'units' THEN
			minToleratedFaceArea := cfg -> 'units';
			IF cfg -> 'unitsAreMeters' THEN
				minToleratedFaceArea = topo_update._app_do_AddBordersSplitSurface_minAreaMetersToTopoUnits(appname, appconfig, borderset, minToleratedFaceArea);
			END IF;
		END IF;

		-- Read snapTolerance from config, if found
		cfg := cfg_op_params -> 'snapTolerance';
		IF cfg ? 'units' THEN
			snapTolerance := cfg -> 'units';
			IF cfg -> 'unitsAreMeters' THEN
				RAISE EXCEPTION 'Specifying snapTolerance in meters is unsupported';
			END IF;
		END IF;

		-- Read maxAllowedSplitSurfaceCount from config, if found
		cfg := cfg_op_params;
		IF cfg ? 'maxAllowedSplitSurfacesCount' THEN
			maxAllowedSplitSurfaceCount = cfg ->> 'maxAllowedSplitSurfacesCount';
		END IF;

		-- Read maxAllowedNewSurfaceCount from config, if found
		cfg := cfg_op_params;
		IF cfg ? 'maxAllowedNewSurfacesCount' THEN
			maxAllowedNewSurfaceCount = cfg ->> 'maxAllowedNewSurfacesCount';
		END IF;
	END IF; --}

	RAISE DEBUG 'snapTolerance: %', snapTolerance;
	RAISE DEBUG 'minToleratedFaceArea: %', minToleratedFaceArea;
	RAISE DEBUG 'maxAllowedSplitSurfaceCount: %', maxAllowedSplitSurfaceCount;


	RETURN QUERY
	SELECT * FROM topo_update.add_border_split_surface(
		borderset,
		surfaceLayerTable,
		surfaceLayerGeomColumn,
		surfaceLayerIdColumn,
		borderLayerTable,
		borderLayerGeomColumn,
		borderLayerIdColumn,
		snapTolerance,
		'topo_update._app_do_AddBordersSplitSurface_ColMapProvider'::regproc,
		jsonb_build_object(
			'properties', borderset -> 'properties',
			'surface', jsonb_build_object(
				'table', surfaceLayerTable,
				'geomcol', surfaceLayerGeomColumn,
				'idcol', surfaceLayerIdColumn
			),
			'border', jsonb_build_object(
				'table', borderLayerTable,
				'geomcol', borderLayerGeomColumn,
				'idcol', borderLayerIdColumn
			)
		),
		minToleratedFaceArea,
		maxAllowedSplitSurfaceCount,
		maxAllowedNewSurfaceCount
	);

END;
$BODY$ LANGUAGE plpgsql; --}

CREATE OR REPLACE FUNCTION topo_update.app_do_AddBordersSplitSurfaces(

	-- Application name, will be used to fetch
	-- configuration from application schema.
	-- See doc/APP_CONFIG.md
	appname NAME,

	-- A GeoJSON Feature object representing
	-- a set of Borders (either as a LineString
	-- or MultiLineString) to be used to create
	-- new Surface objects.
	--
	-- In case of multiple Linestrings they all
	-- must be simple and not form any closed ring
	-- when noded.
	--
	-- Different parameters can be specified in appconfig
	-- to handle the two cases (open lines, closed lines).
	--
	-- Properties of the feature are used to
	-- to contain values to be used for:
	--   - Borders created as a result of the split of
	--     an existing Border crossed by new borders
	--     (split or modified)
	--   - Borders created where there was previously
	--     no Border (new)
	--   - Surfaces created as a result of the split of
	--     an existing Surface crossed by new borders
	--     (split or modified)
	--   - Surfaces created where there was previosly
	--     no Surface (new)
	--
	-- Format of the properties object is as follows:
	--
	--   "properties": {
	--		"Border": {
	--        "new": {
	--          <column_name>: <value>
	--          [, <column_name>: <value> ] ...
	--        },
	--        "modified": {
	--          <column_name>: <value>
	--          [, <column_name>: <value> ] ...
	--        }
	--        "split": {
	--          <column_name>: <value>
	--          [, <column_name>: <value> ] ...
	--        }
	--		},
	--		"Surface": {
	--        "new": {
	--          <column_name>: <value>
	--          [, <column_name>: <value> ] ...
	--        },
	--        "modified": {
	--          <column_name>: <value>
	--          [, <column_name>: <value> ] ...
	--        }
	--        "split": {
	--          <column_name>: <value>
	--          [, <column_name>: <value> ] ...
	--        }
	--		}
	--   }
	--
	-- Column names which are NOT found in the "properties" object
	-- for a given feature-type/action combination will inherit values
	-- from the feature they relate to (for example: most adjacent surface
	-- or object previously covering the space taken by the new one).
	--
	-- NOTE: new Borders never relate to existing ones, at the moment
	--
	--
	borderset JSONB

)
RETURNS TABLE(fid text, typ char, act char, frm text) AS
$BODY$ --{
DECLARE
	sql TEXT;
	appconfig JSONB;
BEGIN

	RAISE DEBUG 'topo_update.app_do_AddBordersSplitSurfaces(''%'', $J$%$J$)',
		appname, borderset;
	sql := format('SELECT cfg FROM %I.app_config',
			appname || '_sysdata_webclient_functions');
	EXECUTE sql INTO STRICT appconfig;

	--RAISE DEBUG 'APPCONFIG: %', appconfig;

	RETURN QUERY
	SELECT * FROM topo_update._app_do_AddBordersSplitSurfaces_Dynamic(
		appname,
		appconfig,
		borderset
	);
END;
$BODY$ LANGUAGE plpgsql; --}
