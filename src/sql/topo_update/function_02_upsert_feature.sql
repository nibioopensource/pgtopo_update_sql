-- Update existing feature or insert a new one.
CREATE OR REPLACE FUNCTION topo_update.upsert_feature(

	-- The elements forming the Feature's TopoGeometry.
	--
	-- An existing TopoGeometry contents will be replaced
	-- by the contents of the given array.
	--
	feature_topoelems topology.TopoElementArray,

	-- The 'properties' component of a GeoJSON Feature object
	-- See https://geojson.org/
	feature_properties JSONB,

	-- Target layer table (where to insert the new feature in)
	layerTable REGCLASS,

	-- Name of the TopoGeometry column in the target layer table
	layerTopoGeomColumn NAME,

	-- Name of the primary key column in the target layer table
	layerIdColumn NAME,

	-- JSON mapping of PG columns and values
	-- from JSON attributes and values
	-- See topo_update.json_props_to_pg_cols() function
	layerColMap JSONB,

	-- Feature identifier value, as text representation
	OUT id TEXT,

	-- Feature TopoGeometry
	OUT topogeom topology.TopoGeometry
)
AS $BODY$ -- {
DECLARE
	props JSONB;
	geom GEOMETRY;
	json_prop_array TEXT[];
	sql TEXT;
	rec RECORD;
	known bool;
	topo topology.topology;
	layer topology.layer;
	proc_name TEXT := 'topo_update.upsert_feature';
BEGIN
	props := feature_properties;

	json_prop_array := array_agg(x) FROM
		jsonb_array_elements_text( layerColMap -> layerIdColumn ) x;

	id := props #>> json_prop_array;
	IF id IS NULL THEN
		RAISE EXCEPTION '%: feature ID extracted from props %'
			' with mapping path % is null'
			', cannot proceed',
			proc_name,
			props,
			json_prop_array
			;
	END IF;

	RAISE DEBUG 'Feature % has elems %', id, feature_topoelems;
	RAISE DEBUG 'Target table is %', layerTable;
	RAISE DEBUG 'Target table spatial column is %', layerTopoGeomColumn;
	RAISE DEBUG 'Target table id column is %', layerIdColumn;

	sql := format('SELECT %1$I FROM %2$s WHERE %1$I = %3$L LIMIT 2',
		layerIdColumn, layerTable, id);
	RAISE DEBUG 'SQL: %', sql;
	known := false;
	FOR rec IN EXECUTE sql
	LOOP
		IF known THEN
			RAISE EXCEPTION 'Multiple features in table % have ID %', layerTable, id;
		ELSE
			known := true;
		END IF;
	END LOOP;

	IF known THEN
		RAISE DEBUG 'Feature with id % exists in table %, will UPDATE if needed',id, layerTable;
		rec := topo_update.update_feature(
			feature_topoelems,
			feature_properties,
			layerTable,
			layerTopoGeomColumn,
			layerIdColumn,
			id::text,
			layerColMap
		);

		topogeom := rec.topogeom;
	ELSE
		RAISE DEBUG 'Feature with id % does not exist in table %, will INSERT',id, layerTable;

		-- Fetch topology and layer info from layer references
		SELECT t topo, l layer
			FROM topology.layer l, topology.topology t, pg_class c, pg_namespace n
			WHERE l.schema_name = n.nspname
			AND l.table_name = c.relname
			AND c.oid = layerTable
			AND c.relnamespace = n.oid
			AND l.feature_column = layerTopoGeomColumn
			AND l.topology_id = t.id
			INTO rec;
		topo := rec.topo;
		layer := rec.layer;

		topogeom := topology.CreateTopoGeom(
			topo.name,
			layer.feature_type,
			layer.layer_id,
			feature_topoelems
		);

		RAISE DEBUG 'New feature will have TopoGeom: %', topogeom;

		id := topo_update.insert_feature(
			topogeom,
			feature_properties,
			layerTable,
			layerTopoGeomColumn,
			layerIdColumn,
			layerColMap
		);

	END IF;
END;
$BODY$ --}
LANGUAGE plpgsql;

-- Update existing feature or insert a new one.
CREATE OR REPLACE FUNCTION topo_update.upsert_feature(

	-- The 'geometry' component of a GeoJSON Feature object
	feature_geometry GEOMETRY,

	-- The 'properties' component of a GeoJSON Feature object
	-- See https://geojson.org/
	feature_properties JSONB,

	-- Target layer table (where to insert the new feature in)
	layerTable REGCLASS,

	-- Name of the TopoGeometry column in the target layer table
	layerGeomColumn NAME,

	-- Name of the primary key column in the target layer table
	layerIdColumn NAME,

	-- JSON mapping of PG columns and values
	-- from JSON attributes and values
	-- See topo_update.json_props_to_pg_cols() function
	layerColMap JSONB,

	-- Tolerance to use for insertion of topology primitives
	-- used to represent the feature geometry.
	-- May be NULL to use topology-default tolerance.
	-- The value is to be given in the units of the target topology
	-- projection.
	tolerance FLOAT8,

	-- Wether the TopoGeometry object in an already
	-- existing Feature record will be modified
	-- instead of replaced by a new TopoGeometry
	modifyExistingTopoGeom BOOLEAN DEFAULT TRUE,

	-- Feature identifier value, as text representation
	OUT id TEXT,

	-- Feature TopoGeometry
	OUT topogeom topology.TopoGeometry,

	-- Feature's old TopoGeometry, in case of an update,
	-- If modifyExistingTopoGeom is true this will have
	-- the same value of the "topogeom" output parameter.
	OUT old_topogeom topology.TopoGeometry,

	-- Name of a field that represents the "version" of
	-- the whole record. If the value of such field does
	-- not change, the whole record is expected to not
	-- have changed, which means we don't need to update it
	layerVersionColumn NAME DEFAULT NULL
)
AS
$BODY$ -- {
DECLARE
	props JSONB;
	geom GEOMETRY;
	json_prop_array TEXT[];
	sql TEXT;
	rec RECORD;
	known bool;
	proc_name TEXT := 'topo_update.upsert_feature';
BEGIN
	props := feature_properties;

	json_prop_array := array_agg(x) FROM
		jsonb_array_elements_text( layerColMap -> layerIdColumn ) x;

	id := props #>> json_prop_array;
	IF id IS NULL THEN
		RAISE EXCEPTION '%: feature ID extracted from props %'
			' with mapping path % is null'
			', cannot proceed',
			proc_name,
			props,
			json_prop_array
			;
	END IF;

	geom := feature_geometry;

	RAISE DEBUG 'Feature % is a %  and has area % and length % with srid %', id, ST_GeometryType(geom), ST_Area(geom), ST_Length(geom), ST_SRID(geom);
	RAISE DEBUG 'Target table is %', layerTable;
	RAISE DEBUG 'Target table spatial column is %', layerGeomColumn;
	RAISE DEBUG 'Target table id column is %', layerIdColumn;
	RAISE DEBUG 'Tolerance is %', tolerance;

	sql := format('SELECT %1$I FROM %2$s WHERE %1$I = %3$L LIMIT 2',
								layerIdColumn, layerTable, id);
	RAISE DEBUG 'SQL: %', sql;
	known := false;
	FOR rec IN EXECUTE sql
	LOOP
		IF known THEN
			RAISE EXCEPTION 'Multiple features in table % have ID %', layerTable, id;
		ELSE
			known := true;
		END IF;
	END LOOP;

	IF known THEN
		RAISE DEBUG 'Feature with id % exists in table %, will UPDATE if needed',id, layerTable;
		rec := topo_update.update_feature(
			feature_geometry,
			feature_properties,
			layerTable,
			layerGeomColumn,
			layerIdColumn,
			id::text,
			layerColMap,
			tolerance,
			modifyExistingTopoGeom,
			layerVersionColumn
		);

		topogeom := rec.topogeom;
		old_topogeom := rec.old_topogeom;
	ELSE
		RAISE DEBUG 'Feature with id % does not exist in table %, will INSERT',id, layerTable;
		rec := topo_update.insert_feature(
			feature_geometry,
			feature_properties,
			layerTable,
			layerGeomColumn,
			layerIdColumn,
			layerColMap,
			tolerance
		);

		topogeom := rec.topogeom;
		id := rec.id;
	END IF;
END;
$BODY$ --}
LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION topo_update.upsert_feature(

	-- A GeoJSON Feature object
	-- See https://geojson.org/
	feature JSONB,

	-- SRID of geometry, or NULL for leaving it as unknown
	srid INT,

	-- Target layer table (where to insert the new feature in)
	layerTable REGCLASS,

	-- Name of the TopoGeometry column in the target layer table
	layerGeomColumn NAME,

	-- Name of the primary key column in the target layer table
	layerIdColumn NAME,

	-- JSON mapping of PG columns and values
	-- from JSON attributes and values
	-- See topo_update.json_props_to_pg_cols() function
	layerColMap JSONB,

	-- Tolerance to use for insertion of topology primitives
	-- used to represent the feature geometry.
	-- May be NULL to use topology-default tolerance.
	tolerance FLOAT8,

	-- Wether the TopoGeometry object in an already
	-- existing Feature record will be modified
	-- instead of replaced by a new TopoGeometry
	modifyExistingTopoGeom BOOLEAN DEFAULT TRUE,

	-- Feature identifier value, as text representation
	OUT id TEXT,

	-- Feature TopoGeometry
	OUT topogeom topology.TopoGeometry,

	-- Feature's old TopoGeometry, in case of an update,
	-- If modifyExistingTopoGeom is true this will have
	-- the same value of the "topogeom" output parameter.
	OUT old_topogeom topology.TopoGeometry
)
AS
$BODY$ -- {
DECLARE
	props JSONB;
	geom GEOMETRY;
	rec RECORD;
BEGIN
	props := feature -> 'properties';
	geom := ST_GeomFromGeoJSON(feature -> 'geometry');

	-- TODO: use the deprecated "crs" member in GeoJSON, if found ?
	RAISE DEBUG 'SRID: %', srid;
	IF srid IS NOT NULL THEN
		geom := ST_SetSRID(geom, srid);
	END IF;

	SELECT *
	FROM topo_update.upsert_feature(
			geom,
			props,
			layerTable,
			layerGeomColumn,
			layerIdColumn,
			layerColMap,
			tolerance,
			modifyExistingTopoGeom
		)
	INTO rec; -- id, topogeom;

	id := rec.id;
	topogeom := rec.topogeom;
	old_topogeom := rec.old_topogeom;
END;
$BODY$ --}
LANGUAGE plpgsql;
