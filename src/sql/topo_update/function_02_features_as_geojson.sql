--
-- This function takes a set of identifiers of
-- a topological layer's features and encodes them
-- in GeoJSON format.
--
-- Attributes of the features will be included in the returned GeoJSON
-- based on mapping files
--
CREATE OR REPLACE FUNCTION topo_update.features_as_geojson(

	layerTable regclass,
	layerTopoGeomColumn name,
	layerIDColumn name,

	-- Array of feature identifiers values
	featureIds TEXT[],

	-- Attributes mapping, if NULL (the default) will use a 1:1
	-- mapping of all attributes by the TopoGeometry field,
	-- See topo_update.json_col_map_from_pg
	layerColMap JSONB DEFAULT NULL, -- See json_props_to_pg_cols

	-- If this parameter is given, use the given
	-- function to modify JSONB Features written
	-- for extracted features
	--
	-- The function is required to accept the following parameters:
	--
	--	o INOUT feat JSONB
	--		(mandatory)
	--		Represent the GeoJSON feature as built using the given
	--		column mamppings (layerColMap)
	--
	--  o usr ANYELEMENT
	--		(optional)
	--		The value of the featureJsonModifierArg parameter passed
	--		to this function.
	--
	-- The function's optionally modified INOUT `feat` parameter
	-- will be used in the GeoJSON feature collection returned by
	-- this function.
	--
	-- If not given, the GeoJSON feature will solely be built
	-- using the provided column maps.
	--
	featureJsonModifier REGPROC DEFAULT NULL,

	-- User argument to be passed to featureJsonModifier.
	-- Can be omitted to not pass such parameter.
	featureJsonModifierArg ANYELEMENT DEFAULT NULL::int,

	-- Override projection
	--
	-- If value of this parameter is not-null use the
	-- specified CRS for the output GeoJSON, reprojecting
	-- to that system all the geometries.
	--
	outSRID INT DEFAULT NULL,

	-- Specify max number of decimal digits to use in GeoJSON output
	--
	-- The maxdecimaldigits argument may be used when calling
	-- ST_AsGeoJSON to change the number of decimal digits used
	-- in output
	--
	maxdecimaldigits INT DEFAULT 9

)
RETURNS JSONB AS
$BODY$ -- {
DECLARE
	rec RECORD;
	sql TEXT;
	crs TEXT;
	crsSRID INT;
	geojson JSONB;
	topo topology.topology;
	featureLayer topology.layer;
	featureProps JSONB;
	featureTopoGeom topology.TopoGeometry;
	featureGeom Geometry;
	featureJSON JSONB;
	geojsonFeatures JSONB[] = ARRAY[]::JSONB[];
	procName TEXT := 'topo_update.features_as_geojson';
BEGIN

	IF layerColMap IS NULL THEN
		layerColMap := topo_update.json_col_map_from_pg(layerTable, ARRAY[layerTopoGeomColumn]);
	END IF;

	sql := format(
		$$
			SELECT
			s as feature_record,
			s.%3$I as feature_topogeom,
			s.%2$I::TEXT AS feature_id
			FROM %1$s s
			WHERE %2$I::TEXT = ANY($1)
			ORDER BY %2$I
		$$,
		layerTable,
		layerIDColumn,
		layerTopoGeomColumn
	);
	RAISE DEBUG '%: SQL": %', procName, sql;
	FOR rec IN EXECUTE sql USING featureIds
	LOOP --{

		IF topo IS NULL THEN
			-- Fetch topology info from the TopoGeometry
			SELECT *
				FROM topology.topology
				WHERE id = topology_id(rec.feature_topogeom)
				INTO topo;
			IF NOT FOUND THEN
				RAISE EXCEPTION
					'Cannot find topology (id=%) '
					'referenced by TopoGeometry of Feature % in layer %',
					topology_id(rec.feature_topogeom),
					rec.feature_id,
					layerTable
				;
			END IF;

			-- Fetch layer info from the TopoGeometry
			SELECT *
				FROM topology.layer
				WHERE topology_id = topo.id
				AND layer_id = layer_id(rec.feature_topogeom)
				INTO featureLayer;
			IF NOT FOUND THEN
				RAISE EXCEPTION
					'Cannot find layer (id=%) of topology % '
					'referenced by TopoGeometry of feature % in layer %',
					layer_id(rec.feature_topogeom),
					topo.name,
					rec.feature_id,
					layerTable
				;
			END IF;
		ELSE
			-- Sanity check: all Feature TopoGeometry objects are defined
			-- against the SAME topology
			IF topology_id(rec.feature_topogeom) != topo.id
			THEN
				RAISE EXCEPTION 'TopoGeometry of feature % in layer % is defined on'
					' Topology % instead of % (%)',
					rec.feature_id,
					layerTable,
					topology_id(rec.feature_topogeom),
					topo.id,
					topo.name
				;
			END IF;
			-- Sanity check: all TopoGeometry objects are defined
			-- against the SAME topology layer
			IF layer_id(rec.feature_topogeom) != featureLayer.layer_id
			THEN
				RAISE EXCEPTION 'TopoGeometry of Feature % is defined on'
					' layer % instead of % (%.%.%)',
					rec.feature_id,
					layer_id(rec.feature_topogeom),
					featureLayer.layer_id,
					featureLayer.schema_name,
					featureLayer.table_name,
					featureLayer.feature_column
				;
			END IF;
		END IF;

		-- Build properties of the Feature
		featureProps := topo_update.record_to_jsonb(
			rec.feature_record,
			layerColMap
		);
		IF featureProps IS NULL THEN
			RAISE EXCEPTION 'Properties for feature % as extracted by colMap % are NULL',
				to_jsonb(rec.feature_record), featureLayerColMap;
		END IF;

		RAISE DEBUG '%: feature % has props %', procName, rec.feature_id, featureProps;

		-- Write the feature object
		featureGeom = ST_CollectionHomogenize(
			rec.feature_topogeom::geometry
		);

		--RAISE DEBUG '%: homogenized feature: %', procName, ST_AsEWKT(featureGeom);

		IF outSRID IS NOT NULL
		THEN
			featureGeom := ST_Transform(featureGeom, outSRID);
		END IF;

		-- Remove points that will be seen as repeated
		-- when precision is reduced due to number of
		-- decimal digits
		-- TODO: featureGeom := ST_RemoveRepeatedPoints(featureGeom, prec);

		--RAISE DEBUG '%: transformed feature geom: %', procName, ST_AsEWKT(featureGeom);

		featureJSON := jsonb_build_object(
			'type', 'Feature',
			'geometry', ST_AsGeoJSON(
				featureGeom,
				maxdecimaldigits => maxdecimaldigits,
				options => 0
			)::jsonb,
			'properties', featureProps
		);

		--RAISE DEBUG '%: feature: %', procName, featureJSON;

		-- Append feature features to the global features array
		geojsonFeatures := geojsonFeatures || featureJSON;

	END LOOP; --}

	IF outSRID IS NOT NULL
	THEN
		IF outSRID = 0 THEN
			RAISE EXCEPTION 'If an output SRID is given, it cannot be 0';
		END IF;
		crsSRID = outSRID;
	ELSE
		crsSRID = topo.srid;
	END IF;

	-- Initialize geojson with CRS member, if SRID is known
	IF crsSRID = 0 THEN
		geojson := jsonb_build_object();
	ELSE
		SELECT auth_name || ':' || auth_srid
		FROM public.spatial_ref_sys
		WHERE srid = crsSRID
		INTO crs;

		IF FOUND THEN
			geojson := jsonb_build_object(
				'crs', jsonb_build_object(
					'properties', jsonb_build_object('name', crs),
					'type', 'name'
				)
			);
		ELSE
			RAISE EXCEPTION 'Could not found spatial_ref_sys entry for SRID %', crsSRID;
		END IF;
	END IF;

	RAISE DEBUG '%: GeoJSON header: %', procName, geojson;

	-- Add features and type to the GsoJSON
	geojson := geojson || jsonb_build_object(
		'type', 'FeatureCollection',
		'features', to_jsonb(geojsonFeatures)
	);

	RAISE DEBUG '%: GeoJSON being returned: %', procName, geojson;

	RETURN geojson;
END;
$BODY$ --}
LANGUAGE plpgsql;
