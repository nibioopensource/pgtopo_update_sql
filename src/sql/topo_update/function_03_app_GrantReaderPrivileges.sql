-- Grant application reader privileges to given role name
--{
CREATE OR REPLACE FUNCTION topo_update.app_GrantReaderPrivileges(
  app_name TEXT,
  editor_role_name NAME
)
RETURNS VOID
AS $BODY$
BEGIN

  EXECUTE FORMAT(
    $_GRANTS_$

GRANT SELECT
  ON ALL TABLES IN SCHEMA %1$I
  TO %2$I;

GRANT SELECT
  ON ALL TABLES IN SCHEMA %4$I
  TO %2$I;

GRANT USAGE
  ON SCHEMA %3$I
  TO %2$I;

GRANT SELECT
  ON TABLE %3$I.app_config
  TO %2$I;

GRANT USAGE
  ON SCHEMA topo_update
  TO %2$I;

GRANT USAGE
  ON SCHEMA %1$s
  TO %2$I;

GRANT USAGE
  ON SCHEMA %4$I
  TO %2$I;

    $_GRANTS_$,
    app_name, -------------------------------------------- %1
    editor_role_name, ------------------------------------ %2
    format('%s_sysdata_webclient_functions', app_name), -- %3
    format('%s_sysdata_webclient', app_name) ------------- %4
  );

END
$BODY$ LANGUAGE 'plpgsql' VOLATILE STRICT;
--}
