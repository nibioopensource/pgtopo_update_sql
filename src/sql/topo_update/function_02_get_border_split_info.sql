
-- {
-- This function will be called to extra extra info about other related to a split operation
--
--
-- # RETURN VALUE
--
-- The function returns a table containig information about
-- the created and splitted borders and surfaces; the
-- returned table has fields:
--
--	- fid text
--
--		Feature identifier (text representation of the value of
--		the specified layer's IdColumn)
--
--	- typ char
--
--		Character representing the type of Feature the record
--		is about, with these possible values:
--
--			S	The Feature is a Surfaces
--
--			B	The Feature is a Border
--
--	- act char
--
--		Character representing the action taken on the Feature,
--		with the following possible values:
--
--			U	For a border, this will give list of borders (U) used by the changed surfaces in the input 
--				due to a split thats not already in the input list.

-- 			TODO add U for surfaces also, but not sure if we need this

--				For surfaces feature it will give a list of surfaces that touches any of the changed surfaces in the input 
--				due to a split thats not already in the input list.
--
--			T	Only used for surface.  A surfaces that touches any of the changed borderes in the input 
--				due to a split thats not already in the input list.
--
--
--		
-- }{
CREATE OR REPLACE FUNCTION topo_update.get_border_split_info(

	-- Surface layer table
	surfaceLayerTable REGCLASS,

	-- Surface layer TopoGeometry column name
	surfaceLayerGeomColumn NAME,

	-- Surface layer primary key column name
	surfaceLayerIdColumn NAME,

	-- Border layer table
	borderLayerTable REGCLASS,

	-- Name of Border layer TopoGeometry column
	borderLayerGeomColumn NAME,

	-- Surface layer primary key column name
	borderLayerIdColumn NAME,
	
	--TABLE(fid text, typ char, act char, frm text)
	-- Surface layer table
	outputFrom_add_border_split_surface REGCLASS
	
	
)
RETURNS TABLE(fid text, typ char, act char) AS $BODY$
DECLARE

	rec RECORD;
	sql text;
	topo topology.topology;
	surfaceLayer topology.layer;
	borderLayer topology.layer;

BEGIN

	-- TODO avoid using temp tables
	-- Create a temp table with last split data
	sql =  format('CREATE TEMP table old_split_border_result AS (SELECT s.fid, s.typ, s.act FROM %I s)', outputFrom_add_border_split_surface );
	EXECUTE sql;

	-- Create a temp table with new result
	DROP TABLE IF EXISTS pg_temp.temp_result_get_border_split_info;
	CREATE TEMP TABLE temp_result_get_border_split_info(fid text, typ char, act char);	
	
	-- Get info about topology
	SELECT t topo, l layer
	FROM topology.layer l, topology.topology t
	WHERE format('%I.%I', l.schema_name, l.table_name)::REGCLASS = surfaceLayerTable::REGCLASS
	AND l.feature_column = surfaceLayerGeomColumn
	AND l.topology_id = t.id
	INTO rec;

	topo := rec.topo;
	surfaceLayer := rec.layer;

	SELECT t topo, l layer
	FROM topology.layer l, topology.topology t
	WHERE format('%I.%I', l.schema_name, l.table_name)::REGCLASS = borderLayerTable::REGCLASS
	AND l.feature_column = borderLayerGeomColumn
	AND l.topology_id = t.id
	INTO rec;

	borderLayer := rec.layer;


	-- Find list of borders (T) used/touched by the changed surfaces in the input due to a split thats not already in the input list.	
	sql := format(
		$$
	CREATE TEMP TABLE result_add_used_borders AS (
		SELECT 
		DISTINCT ed_to.%8$s::TEXT AS fid
		FROM 
		%2$s fa_from,
		%1$s.relation r_from,
		%1$s.edge_data e_from,
		%1$s.relation r_to,
		%6$s ed_to,
		pg_temp.old_split_border_result tr
		WHERE tr.typ = 'S' AND fa_from.%4$I::TEXT = tr.fid AND
		(fa_from.%3$s).type = 3 AND
		(fa_from.%3$s).layer_id = %5$s AND
		(fa_from.%3$s).layer_id = r_from.layer_id AND
		(fa_from.%3$s).id = r_from.topogeo_id AND
		(r_from.element_id = e_from.right_face OR r_from.element_id = e_from.left_face) AND
		(e_from.edge_id = r_to.element_id OR e_from.edge_id = r_to.element_id) AND
		%9$s = r_to.layer_id AND
		(ed_to.%7$s).type = 2 AND
		(ed_to.%7$s).layer_id = %9$s AND
		(ed_to.%7$s).id = r_to.topogeo_id
	)
		$$,
		topo.name,
		surfaceLayerTable,
		surfaceLayerGeomColumn,
		surfaceLayerIdColumn,
		surfaceLayer.layer_id, -- 5
		borderLayerTable,
		borderLayerGeomColumn,
		borderLayerIdColumn,
		borderLayer.layer_id --9
	);
	EXECUTE sql;

	-- TODO, we may also need surfaces touhing changed surfaces
    -- Modify to add toching surface
	
	
	
	-- Find surfaces touching changed borderes
	sql := format(
		$$
	CREATE TEMP TABLE result_add_toched_affected_surfaces AS (
		SELECT 
		DISTINCT fa_from.%4$s::TEXT AS fid
		FROM 
		%2$s fa_from,
		%1$s.relation r_from,
		%1$s.edge_data e_from,
		%1$s.relation r_to,
		%6$s ed_to,
		pg_temp.old_split_border_result tr
		WHERE tr.typ = 'B' AND ed_to.%8$I::TEXT = tr.fid AND
		(fa_from.%3$s).type = 3 AND
		(fa_from.%3$s).layer_id = %5$s AND
		(fa_from.%3$s).layer_id = r_from.layer_id AND
		(fa_from.%3$s).id = r_from.topogeo_id AND
		(r_from.element_id = e_from.right_face OR r_from.element_id = e_from.left_face) AND
		(e_from.edge_id = r_to.element_id OR e_from.edge_id = r_to.element_id) AND
		%9$s = r_to.layer_id AND
		(ed_to.%7$s).type = 2 AND
		(ed_to.%7$s).layer_id = %9$s AND
		(ed_to.%7$s).id = r_to.topogeo_id
	)
		$$,
		topo.name,
		surfaceLayerTable,
		surfaceLayerGeomColumn,
		surfaceLayerIdColumn,
		surfaceLayer.layer_id, -- 5
		borderLayerTable,
		borderLayerGeomColumn,
		borderLayerIdColumn,
		borderLayer.layer_id --9
	);

	EXECUTE sql;

	-- Add list of used touched borderes
	INSERT INTO temp_result_get_border_split_info(fid,typ,act)
	SELECT s.fid, 'B', 'U' 
	FROM result_add_used_borders s 
	WHERE
    NOT EXISTS (
        SELECT 1 FROM pg_temp.old_split_border_result a WHERE a.fid = s.fid AND a.typ = 'B'
    );

	DROP TABLE IF EXISTS pg_temp.result_add_used_borders;

	-- Add surfaces touching changed borderes
	INSERT INTO temp_result_get_border_split_info(fid,typ,act)
	SELECT s.fid, 'S', 'T' 
	FROM result_add_toched_affected_surfaces s 
	WHERE
    NOT EXISTS (
        SELECT 1 FROM pg_temp.old_split_border_result a WHERE a.fid = s.fid AND a.typ = 'S'
    );
    
    DROP TABLE IF EXISTS pg_temp.result_add_toched_affected_surfaces;

    DROP TABLE IF EXISTS pg_temp.old_split_border_result;

	-- Return result
    
    RETURN QUERY SELECT s.fid, s.typ, s.act FROM temp_result_get_border_split_info s;


	

END;
$BODY$ LANGUAGE plpgsql; --}

