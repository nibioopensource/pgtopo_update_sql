CREATE OR REPLACE FUNCTION topo_update._app_do_UpdateAttributes_Dynamic(
	appname NAME,
	appconfig JSONB,
	tablename TEXT,
	id TEXT,
	properties JSONB
)
RETURNS BOOLEAN AS
$BODY$ --{
DECLARE
	cfg_table JSONB;
	tgt_table REGCLASS;
	id_column NAME;
	map JSONB;

	colnames TEXT[];
	colvals TEXT[];
	col_updates TEXT[];
	col_upd TEXT;
	sql TEXT;

	v_cnt int;

	rec RECORD;
BEGIN

	-- Check if the operation is enabled
	IF NOT appconfig -> 'operations' ? 'UpdateAttributes' THEN
		RAISE EXCEPTION 'UpdateAttributes operation not enabled for application %', appname;
	END IF;

	--RAISE DEBUG 'borderLayer: %', cfg_borderLayer;

	FOR rec IN SELECT jsonb_array_elements( appconfig -> 'tables' ) cfg
	LOOP --{
		IF rec.cfg ->> 'name' = tablename THEN
			cfg_table := rec.cfg;
			EXIT;
		END IF;
	END LOOP; --}

	--RAISE DEBUG 'table: %', cfg_table;

	IF cfg_table IS NULL THEN
		RAISE EXCEPTION
			$$Table '%' is not defined in configuration of app '%'$$,
			tablename, appname;
	END IF;

	id_column := cfg_table ->> 'primary_key';

	IF id_column IS NULL THEN
		RAISE EXCEPTION
			$$Table '%' in app '%' does not have a primary key defined, cannot update$$,
			tablename, appname;
	END IF;

	-- Get the table's regclass

	SELECT c.oid::regclass
	FROM
		pg_catalog.pg_class c,
		pg_catalog.pg_namespace n
	WHERE
		n.nspname = appname AND
		c.relnamespace = n.oid AND
		c.relname = tablename
	INTO tgt_table;

	IF tgt_table IS NULL THEN
		RAISE EXCEPTION
			$$Table '%.%' does not exist: app '%' is broken$$,
			appname, tablename, appname;
	END IF;

	-- TODO: perform the update

	map := topo_update.json_col_map_from_pg( tgt_table );

	--RAISE WARNING 'COMPUTED: map: %', map;

	SELECT c.colnames, c.colvals
	FROM topo_update.json_props_to_pg_cols(properties, map) c
	INTO colnames, colvals;

	--RAISE WARNING 'COMPUTED: colnames: %', colnames;
	--RAISE WARNING 'COMPUTED: colvals: %', colvals;

	IF colnames IS NULL THEN
		-- Should this be an error instead ?
		RAISE DEBUG 'No properties passed to app_do_UpdateAttributes, nothing to do';
		RETURN false;
	END IF;

	-- Naive approach: always UPDATE, no matter what

	FOR i IN 1 .. array_upper(colnames, 1)
	LOOP
		-- TODO: check the target column is ALLOWED to be changed
		--       by the operation, in appconfig
		col_upd := format('%s = %s', colnames[i], colvals[i]);
		RAISE DEBUG 'col_upd: %', col_upd;
		col_updates := array_append(col_updates, col_upd);
	END LOOP;

	RAISE DEBUG 'col_updates: %', col_updates;

	sql := format(
		$$
			UPDATE %1$s t
			SET %2$s
			FROM %1$s ot
			WHERE ot.%3$I = t.%3$I
			  AND ot.%3$I = %4$L
		$$,
		tgt_table,
		array_to_string(col_updates, ', '),
		id_column,
		id
	);

	-- TODO: ADD more WHERE clauses to skip setting values which did not
	--			 change ?
	--RAISE WARNING 'SQL: %', sql;

	EXECUTE sql;

	GET DIAGNOSTICS v_cnt = row_count;

	IF v_cnt = 0 THEN
		RETURN false;
	ELSE
		RETURN true;
	END IF;

END;
$BODY$ LANGUAGE plpgsql; --}

CREATE OR REPLACE FUNCTION topo_update.app_do_UpdateAttributes(

	-- Application name, will be used to fetch
	-- configuration from application schema.
	-- See doc/APP_CONFIG.md
	appname NAME,

	-- Name of a table defined in the appconfig
	tablename NAME,

	-- Identifier of a record in the table to be updated
	-- The table needs to have a `primary_key` defined
	-- in appconfig or an exception will be thrown
	id NAME,

	-- A JSONB object containing an element for each
	-- attribute that has to be updated
	--
	-- Format of the properties object is as follows:
	--
	--   {
	--      <column_name>: <value>
	--      [, <column_name>: <value> ] ...
	--   }
	--
	--
	properties JSONB

)
--
-- Returns true if a record was updated, false otherwise
--
RETURNS BOOLEAN AS
$BODY$ --{
DECLARE
	sql TEXT;
	appconfig JSONB;
BEGIN

	sql := format('SELECT cfg FROM %I.app_config',
			(appname || '_sysdata_webclient_functions') );

	--RAISE WARNING 'SQL: %', sql;

	BEGIN
		EXECUTE sql INTO STRICT appconfig;
	EXCEPTION
	WHEN undefined_table THEN
		RAISE EXCEPTION $$Unexistent application '%'$$, appname
			USING DETAIL = format('%s (%s)', SQLERRM, SQLSTATE);
	END;

	--RAISE DEBUG 'APPCONFIG: %', appconfig;

	RETURN topo_update._app_do_UpdateAttributes_Dynamic(
		appname,
		appconfig,
		tablename,
		id,
		properties
	);
END;
$BODY$ LANGUAGE plpgsql; --}
