
CREATE OR REPLACE FUNCTION topo_update.find_dangling_borders(

	-- Border layer table
	borderLayerTable REGCLASS,

	-- Name of Border layer TopoGeometry column
	borderLayerGeomColumn NAME,

	-- Border layer primary key column name
	borderLayerIdColumn NAME,

	-- Surface layer table
	surfaceLayerTable REGCLASS,

	-- Surface layer TopoGeometry column name
	surfaceLayerGeomColumn NAME,

	-- Bounding box to restrict the search
	bbox GEOMETRY DEFAULT NULL
)
RETURNS SETOF INT
AS $BODY$ --{
DECLARE
	topo topology.topology;
	borderLayer topology.layer;
	surfaceLayer topology.layer;
	rec RECORD;
	sql TEXT;
BEGIN
	-- Fetch topology and layer info from border layer references
	SELECT t topo, l layer
		FROM topology.layer l, topology.topology t, pg_class c, pg_namespace n
		WHERE l.schema_name = n.nspname
		AND l.table_name = c.relname
		AND c.oid = borderLayerTable
		AND c.relnamespace = n.oid
		AND l.feature_column = borderLayerGeomColumn
		AND l.topology_id = t.id
		INTO rec;
	topo := rec.topo;
	borderLayer := rec.layer;

	-- Fetch topology and layer info from surface layer references
	SELECT t topo, l layer
		FROM topology.layer l, topology.topology t, pg_class c, pg_namespace n
		WHERE l.schema_name = n.nspname
		AND l.table_name = c.relname
		AND c.oid = surfaceLayerTable
		AND c.relnamespace = n.oid
		AND l.feature_column = surfaceLayerGeomColumn
		AND l.topology_id = t.id
		INTO rec;

	IF id(topo) != id(rec.topo) THEN
		RAISE EXCEPTION 'Surface and Border layers need to be in same topology';
	END IF;
	surfaceLayer := rec.layer;

	-- Find borders defined by dangling edges
	sql := format(
		$$
			WITH dangling_edges_in_bbox AS (
				SELECT
				  rb.topogeo_id tgid,
				  e.edge_id eid,
				  e.left_face fl,
				  e.right_face fr,
				  rsl.topogeo_id sl,
				  rsr.topogeo_id sr
				FROM %1$I.relation rb
				JOIN %1$I.edge_data e
					ON ( e.edge_id in ( rb.element_id, -rb.element_id ) )
				LEFT JOIN %1$I.relation rsl
					ON ( rsl.layer_id = %2$L AND rsl.element_id = e.left_face )
				LEFT OUTER JOIN %1$I.relation rsr
					ON ( rsr.layer_id = %2$L AND rsr.element_id = e.right_face )
				WHERE rb.layer_id = %3$L
				AND rsl.topogeo_id IS NOT DISTINCT FROM rsr.topogeo_id
				AND (
					%4$L is NULL
					OR
					e.geom && %4$L
				)
			)
			SELECT %5$I bid
			FROM %6$s
			WHERE id(%7$I) IN (
				SELECT tgid FROM dangling_edges_in_bbox
			)
		$$,
		topo.name,             -- %1
		surfaceLayer.layer_id, -- %2
		borderLayer.layer_id,  -- %3
		bbox,                  -- %4
		borderLayerIdColumn,   -- %5
		borderLayerTable,      -- %6
		borderLayerGeomColumn  -- %7
	);
	RAISE DEBUG 'SQL: %', sql;
	RETURN QUERY EXECUTE sql;
	--FOR rec IN EXECUTE sql LOOP
	--	RAISE WARNING 'Border % contains edges with same surface (or no surface) on both sides', rec.bid;
	--END LOOP;

	--RAISE EXCEPTION 'Not implemented yet';
END;
$BODY$ LANGUAGE plpgsql; --}


