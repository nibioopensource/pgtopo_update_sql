--{
CREATE OR REPLACE FUNCTION topo_update.find_interiors_intersect(
	layerTable REGCLASS,
	layerTopoGeomColumn NAME,
	layerIdColumn NAME,
	-- An optional SQL filter to select
	-- a subset of layer records to consider
	sqlFilter TEXT DEFAULT NULL
)
RETURNS TABLE(fid1 TEXT, fid2 TEXT, tgid1 INT, tgid2 INT, etyp INT, eids INT[])
AS $BODY$
DECLARE
	sql TEXT;
	rec RECORD;
	rec2 RECORD;
	featureFetchSQL TEXT;
	topo topology.topology;
	layer topology.layer;
	trigname TEXT;
	procName TEXT := 'topo_update.find_interiors_intersect';
BEGIN

	-- Fetch topology and layer info from layer references
	SELECT t topo, l layer
		FROM topology.layer l, topology.topology t, pg_class c, pg_namespace n
		WHERE l.schema_name = n.nspname
		AND l.table_name = c.relname
		AND c.oid = layerTable
		AND c.relnamespace = n.oid
		AND l.feature_column = layerTopoGeomColumn
		AND l.topology_id = t.id
		INTO rec;
	topo := rec.topo;
	layer := rec.layer;

	-- Sanity check: this function does not support hierarchical layers
	IF layer.child_id IS NOT NULL THEN
		RAISE EXCEPTION '% function does not support hierarchical layers', procName;
	END IF;

	--
	-- Find TopoGeometry objects with common elements
	--
	sql := format(
		$$
			SELECT
				r1.topogeo_id id1,
				r2.topogeo_id id2,
				r2.element_type,
				array_agg(r2.element_id) elems
			FROM
				%1$I.relation r1,
				%1$I.relation r2
			WHERE r1.element_type = r2.element_type
			AND r1.layer_id = r2.layer_id
			AND r1.layer_id = %2$L
			AND r1.element_id = r2.element_id
			AND r1.topogeo_id < r2.topogeo_id
			GROUP BY r1.layer_id, id1, id2, r2.element_type
			ORDER BY id1, id2
		$$,
		topo.name,
		layer.layer_id
	);
	--RAISE DEBUG 'SQL: %', sql;

	--
	-- Find TopoGeometry objects with common elements
	--
	FOR rec IN EXECUTE sql
	LOOP --{
		RAISE DEBUG 'TopoGeoms % and % in layer % (%) share %: %',
			rec.id1, rec.id2,
			layerTable, layerTopoGeomColumn,
			CASE
			WHEN rec.element_type = 1 THEN
				'nodes'
			WHEN rec.element_type = 2 THEN
				'edges'
			WHEN rec.element_type = 3 THEN
				'faces'
			END,
			array_to_string(rec.elems, ',')
		;

		--
		-- find records in layer table where TopoGeom id match
		-- the TopoGeom id of this overlapping pair ...
		--

		IF featureFetchSQL IS NULL
		THEN
			featureFetchSQL := format(
				$$
					WITH layer AS (
						SELECT * FROM %2$s
						%4$s
					)
					SELECT l1.%1$I id1, l2.%1$I id2
					FROM layer l1, layer l2
					WHERE id(l1.%3$I) = $1
					AND id(l2.%3$I) = $2
					ORDER BY id1, id2
				$$,
				layerIdColumn,
				layerTable,
				layerTopoGeomColumn,
				CASE
				WHEN sqlFilter IS NULL THEN
					''
				ELSE
					format(' WHERE %s', sqlFilter)
				END
			);
			RAISE DEBUG 'SQL: %', sql;
		END IF;

		FOR rec2 IN EXECUTE featureFetchSQL USING rec.id1, rec.id2
		LOOP
			RAISE DEBUG 'Features % and % in layer % (%) share %: %',
				rec2.id1, rec2.id2,
				layerTable, layerTopoGeomColumn,
				CASE
				WHEN rec.element_type = 1 THEN
					'nodes'
				WHEN rec.element_type = 2 THEN
					'edges'
				WHEN rec.element_type = 3 THEN
					'faces'
				END,
				array_to_string(rec.elems, ',')
			;
			tgid1 := rec.id1;
			tgid2 := rec.id2;
			fid1 := rec2.id1;
			fid2 := rec2.id2;
			etyp := rec.element_type;
			eids := rec.elems;
			RETURN NEXT;
		END LOOP;

	END LOOP; --}

	-- Find records in layer having the same TopoGeometry
	sql := format(
		$$
			WITH layer AS (
				SELECT ctid, * FROM %2$s
				%4$s
				-- TODO: skip fids already checked in previous tests ?
			)
			SELECT
				id(l1.%3$I) tg_id,
				l1.%1$I fid1, l2.%1$I fid2
			FROM layer l1, layer l2
			WHERE l2.ctid > l1.ctid
			AND id(l1.%3$I) = id(l2.%3$I)
		$$,
		layerIdColumn,
		layerTable,
		layerTopoGeomColumn,
		CASE
		WHEN sqlFilter IS NULL THEN
			''
		ELSE
			format(' WHERE %s', sqlFilter)
		END
	);
	FOR rec IN EXECUTE sql
	LOOP
		RAISE DEBUG 'Features % and % in layer % (%) have the same TopoGeometry %',
			rec.fid1, rec.fid2,
			layerTable, layerTopoGeomColumn,
			rec.tg_id;
		tgid1 := rec.tg_id;
		tgid2 := rec.tg_id;
		fid1 := rec.fid1;
		fid2 := rec.fid2;
		etyp := NULL;
		eids := NULL;
		RETURN NEXT;
	END LOOP;

	--
	-- If a layer is linear, find shared non-endpoint nodes
	--
	IF layer.feature_type = 2
	THEN
		-- Query to fetch info about non-boundary nodes
		-- shared between linel TopoGeometries in the same
		-- layer.
		--
		sql := format(
			$$
				-- Get all node ids and their count of connected
				-- edges (degree) for each border
				WITH layer AS (
					SELECT * FROM %4$s
					%5$s
				),
				feat_nodes AS (
					SELECT
						b.%1$I fid,
						b.%2$I tg,
						n.node_id nid,
						n.geom node_geom,
						ST_CollectionHomogenize(
							b.%2$I::geometry
						) feat_geom,
						count(e.edge_id) > 1 interior
					FROM
						%3$I.node n,
						%3$I.edge e,
						layer b,
						%3$I.relation r
					WHERE
						(
							n.node_id = e.start_node
							OR
							n.node_id = e.end_node
						)
						AND e.edge_id = abs(r.element_id)
						AND r.topogeo_id = id(%2$I)
						AND r.layer_id = layer_id(%2$I)
					GROUP by
						b.%1$I,
						b.%2$I,
						n.node_id
				)
				-- Get all TopoGeometry pairs sharing
				-- a node interior to either ones
				SELECT
					fn1.fid fid1,
					fn2.fid fid2,
					fn1.tg tg1,
					fn2.tg tg2,
					fn1.feat_geom feat_geom1,
					fn2.feat_geom feat_geom2,
					array_agg(fn1.nid) nids,
					array_agg(fn1.node_geom ORDER BY fn1.node_geom) ngeoms
				FROM
					feat_nodes fn1,
					feat_nodes fn2
				WHERE
					fn1.fid < fn2.fid
					AND fn1.nid = fn2.nid
					AND (
						fn1.interior
						OR
						fn2.interior
					)
				GROUP BY
					fid1, fid2, tg1, tg2, feat_geom1, feat_geom2
			$$,
			layerIdColumn,       -- %1
			layerTopoGeomColumn, -- %2
			topo.name,           -- %3
			layerTable,          -- %4
			CASE                 -- %5
			WHEN sqlFilter IS NULL THEN
				''
			ELSE
				format(' WHERE %s', sqlFilter)
			END
		);

		--RAISE DEBUG 'sql: %', sql;

		FOR rec IN EXECUTE sql
		LOOP
			-- If there's only a single shared node check if it's
			-- the endpoint of a closed lineal topogeometry
			IF array_upper(rec.nids, 1) = 1
			THEN
				IF NOT ST_RelateMatch(
					ST_Relate(
						rec.feat_geom1,
						rec.ngeoms[1],
						2
					),
					'T********'
				)
				THEN
					RAISE DEBUG 'Point % is on the boundary of feature 1: %',
						ST_AsText(rec.ngeoms[1]),
						ST_AsText(rec.feat_geom1)
					;
					-- This single shared node is on the endpoint of
					-- a closed ring in feature 1
					IF NOT ST_RelateMatch(
						ST_Relate(
							rec.feat_geom2,
							rec.ngeoms[1],
							2
						),
						'T********'
					)
					THEN
						RAISE DEBUG 'Point % is on the boundary of feature 2: %',
							ST_AsText(rec.ngeoms[1]),
							ST_AsText(rec.feat_geom1)
						;
						-- This single shared node is on the endpoint of
						-- a closed ring in feature 2 as well, accepting it
						CONTINUE;
					END IF;
				END IF;
			END IF;

			RAISE DEBUG 'Lineal features % and % in layer % (%) intersect on non-boundary node %',
				rec.fid1, rec.fid2,
				layerTable, layerTopoGeomColumn,
				ST_AsEWKT(rec.ngeoms[1])
			;
			fid1 := rec.fid1;
			fid2 := rec.fid2;
			tgid1 := id(rec.tg1);
			tgid2 := id(rec.tg2);
			etyp := 1;
			eids := rec.nids;
			RETURN NEXT;
		END LOOP;

	END IF;


END;
$BODY$ LANGUAGE 'plpgsql'; --}
