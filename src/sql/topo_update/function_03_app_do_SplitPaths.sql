CREATE OR REPLACE FUNCTION topo_update._app_do_SplitPaths_Dynamic(
	appname NAME,
	appconfig JSONB,
	pathset JSONB
)
RETURNS TABLE(fid text, act char, frm text) AS
$BODY$ --{
DECLARE
	cfg_pathLayer JSONB;
	cfg_pathLayerTable JSONB;
	cfg_op JSONB;
	cfg_op_params JSONB;
	cfg JSONB;
	pathLayerTable REGCLASS;
	pathLayerGeomColumn NAME;
	pathLayerIdColumn NAME;
	rec RECORD;
	sql TEXT;
	maxSplits INT;
BEGIN

	-- Check if the operation is enabled
	IF NOT appconfig -> 'operations' ? 'SplitPaths' THEN
		RAISE EXCEPTION 'SplitPaths operation not enabled for application %', appname;
	END IF;

	cfg_pathLayer := appconfig -> 'path_layer';
	cfg_op := appconfig -> 'operations' -> 'SplitPaths';

	--RAISE DEBUG 'pathLayer: %', cfg_pathLayer;

	FOR rec IN SELECT jsonb_array_elements( appconfig -> 'tables' ) cfg
	LOOP --{
		IF rec.cfg ->> 'name' = cfg_pathlayer ->> 'table_name' THEN
			cfg_pathLayerTable := rec.cfg;
		END IF;
		IF cfg_pathLayerTable IS NOT NULL
		THEN
			EXIT;
		END IF;
	END LOOP; --}

	--RAISE DEBUG 'pathLayerTable: %', cfg_pathLayerTable;

	pathLayerTable := ( appname || '.' || ( cfg_pathlayer ->> 'table_name' ) );
	pathLayerGeomColumn := cfg_pathlayer ->> 'geo_column';
	pathLayerIdColumn := cfg_pathlayerTable ->> 'primary_key';

	-- Set parameters defaults
	maxSplits := 0;

	-- Read parameters defaults from appconfig
	cfg_op_params := appconfig -> 'operations' -> 'SplitPaths';
	IF cfg_op_params IS NOT NULL THEN --{

		-- Read maxSplits from config, if found
		cfg := cfg_op_params -> 'maxSplits';
		maxSplits := cfg ->> 'units';

	END IF; --}

	RAISE DEBUG 'maxSplits: %', maxSplits;

	RETURN QUERY
	SELECT * FROM topo_update.split_paths(
		pathset,

		pathLayerTable,
		pathLayerGeomColumn,
		pathLayerIdColumn,

		'topo_update._app_do_AddPath_ColMapProvider'::regproc,
		jsonb_build_object(
			'properties', pathset -> 'properties',
			'path', jsonb_build_object(
				'table', pathLayerTable,
				'geomcol', pathLayerGeomColumn,
				'idcol', pathLayerIdColumn
			)
		),

		maxSplits => maxSplits
	);

END;
$BODY$ LANGUAGE plpgsql VOLATILE; --}

CREATE OR REPLACE FUNCTION topo_update.app_do_SplitPaths(

	-- Application name, will be used to fetch
	-- configuration from application schema.
	-- See doc/APP_CONFIG.md
	appname NAME,

	-- The path set to use for splitting
	pathset JSONB

)
RETURNS TABLE(fid text, act char, frm text) AS
$BODY$ --{
DECLARE
	sql TEXT;
	appconfig JSONB;
BEGIN

	sql := format('SELECT cfg FROM %I.app_config',
			appname || '_sysdata_webclient_functions');
	EXECUTE sql INTO STRICT appconfig;

	--RAISE WARNING 'APPCONFIG: %', appconfig;

	RETURN QUERY
	SELECT * FROM topo_update._app_do_SplitPaths_Dynamic(
		appname,
		appconfig,
		pathset
	);
END;
$BODY$ LANGUAGE plpgsql VOLATILE; --}
