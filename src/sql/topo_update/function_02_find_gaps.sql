--{
CREATE OR REPLACE FUNCTION topo_update.find_gaps(
	layerTable REGCLASS,
	layerTopoGeomColumn NAME,
	-- An optional SQL filter to select
	-- a subset of layer records to consider
	sqlFilter TEXT DEFAULT NULL
)
RETURNS TABLE(etyp INT, eid INT)
AS $BODY$
DECLARE
	sql TEXT;
	rec RECORD;
	rec2 RECORD;
	featureFetchSQL TEXT;
	topo topology.topology;
	layer topology.layer;
	trigname TEXT;
	procName TEXT := 'topo_update.find_gaps';
	elemTypeName TEXT;
	cte TEXT;
BEGIN

	-- Fetch topology and layer info from layer references
	SELECT t topo, l layer
		FROM topology.layer l, topology.topology t, pg_class c, pg_namespace n
		WHERE l.schema_name = n.nspname
		AND l.table_name = c.relname
		AND c.oid = layerTable
		AND c.relnamespace = n.oid
		AND l.feature_column = layerTopoGeomColumn
		AND l.topology_id = t.id
		INTO rec;
	topo := rec.topo;
	layer := rec.layer;

	cte := format(
		$$
		WITH layer AS (
			SELECT %1$I tg FROM %2$s
			%3$s
		)
		$$,
		layerTopoGeomColumn,
		layerTable,
		CASE
		WHEN sqlFilter IS NULL THEN
			''
		ELSE
			format(' WHERE %s', sqlFilter)
		END
	);

	CASE layer.feature_type
	WHEN 1 THEN -- puntal
		elemTypeName := 'Node';
		sql := format(
			$$
				SELECT node_id eid
				FROM %1$I.node
			$$,
			topo.name
		);
	WHEN 2 THEN -- lineal
		elemTypeName := 'Edge';
		sql := format(
			$$
				SELECT edge_id eid
				FROM %1$I.edge
			$$,
			topo.name
		);
	WHEN 3 THEN -- areal
		elemTypeName := 'Face';
		sql := format(
			$$
				SELECT face_id eid
				FROM %1$I.face
				WHERE face_id > 0
			$$,
			topo.name
		);
	ELSE
		RAISE EXCEPTION '%: unsupported feature_type % for layer %',
			procName, layer.feature_type, layerTable;
	END CASE;

	-- Sanity check: this function does not support hierarchical layers
	IF layer.child_id IS NOT NULL THEN
		RAISE EXCEPTION '% function does not support hierarchical layers', procName;
	END IF;

	-- Exclude elements which are found in definition of
	-- TopoGeometry present in target layer
	--
	sql := format(
		$$
			%1$s
			%2$s
			EXCEPT
			SELECT DISTINCT abs(r.element_id)
			FROM %3$I.relation r, layer t
			WHERE r.element_type = $1
			AND r.layer_id = $2
			AND r.topogeo_id = id(tg)
		$$,
		cte,        -- %1
		sql,        -- %2
		topo.name  -- %3
	);
	RAISE DEBUG 'SQL: %', sql;

	FOR rec IN EXECUTE sql USING layer.feature_type, layer.layer_id
	LOOP
		RAISE DEBUG '% % not covered by any TopoGeometry found in layer % (%)',
			elemTypeName,
			rec.eid,
			layerTable, layerTopoGeomColumn;
		etyp := layer.feature_type;
		eid := rec.eid;
		RETURN NEXT;
	END LOOP;


END;
$BODY$ VOLATILE LANGUAGE 'plpgsql'; --}
