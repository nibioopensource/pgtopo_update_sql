CREATE OR REPLACE FUNCTION topo_update._app_do_RemovePath_Dynamic(
	appname NAME,
	appconfig JSONB,
	path_id TEXT
)
RETURNS VOID AS
$BODY$ --{
DECLARE
	cfg_pathLayer JSONB;
	cfg_pathLayerTable JSONB;
	cfg_op JSONB;
	cfg_op_params JSONB;
	cfg JSONB;
	pathLayerTable REGCLASS;
	pathLayerGeomColumn NAME;
	pathLayerIdColumn NAME;
	rec RECORD;
	sql TEXT;
	removedPath RECORD;
	removePrimitives BOOLEAN := true;
BEGIN

	-- Check if the operation is enabled
	IF NOT appconfig -> 'operations' ? 'RemovePath' THEN
		RAISE EXCEPTION 'RemovePath operation not enabled for application %', appname;
	END IF;

	cfg_pathLayer := appconfig -> 'path_layer';
	cfg_op := appconfig -> 'operations' -> 'RemovePath';

	--RAISE DEBUG 'pathLayer: %', cfg_pathLayer;

	FOR rec IN SELECT jsonb_array_elements( appconfig -> 'tables' ) cfg
	LOOP --{
		IF rec.cfg ->> 'name' = cfg_pathlayer ->> 'table_name' THEN
			cfg_pathLayerTable := rec.cfg;
		END IF;
		IF cfg_pathLayerTable IS NOT NULL
		THEN
			EXIT;
		END IF;
	END LOOP; --}

	--RAISE DEBUG 'pathLayerTable: %', cfg_pathLayerTable;

	pathLayerTable := ( appname || '.' || ( cfg_pathlayer ->> 'table_name' ) );
	pathLayerGeomColumn := cfg_pathlayer ->> 'geo_column';
	pathLayerIdColumn := cfg_pathlayerTable ->> 'primary_key';

	-- TODO: cast the parameter into the type of the primary key, not
	--       vice-versa
	sql := format(
		$$
			WITH deleted AS (
				DELETE FROM %1$s
				WHERE %2$I::text = $1
				RETURNING %3$I as tg
			) SELECT topology.clearTopoGeom(tg) FROM deleted
		$$,
		pathLayerTable,      -- %1
		pathLayerIdColumn,   -- %2
		pathLayerGeomColumn  -- %3
	);

	EXECUTE sql
	USING path_id
	INTO removedPath
	;

	IF removedPath IS NULL THEN
		RAISE EXCEPTION 'No Path with id % in table %', path_id, pathLayerTable;
	END IF;

	IF removePrimitives THEN
		-- TODO: pass the bounding box of the removed path ?
		PERFORM topo_update.cleanup_topology(
			( SELECT t FROM topology.topology t
			  WHERE name = appname || '_sysdata_webclient' )
		);
	END IF;


END;
$BODY$ LANGUAGE plpgsql VOLATILE; --}

CREATE OR REPLACE FUNCTION topo_update.app_do_RemovePath(

	-- Application name, will be used to fetch
	-- configuration from application schema.
	-- See doc/APP_CONFIG.md
	appname NAME,

	-- The identifier of a path to remove
	path_id TEXT

)
RETURNS VOID AS
$BODY$ --{
DECLARE
	sql TEXT;
	appconfig JSONB;
BEGIN

	sql := format('SELECT cfg FROM %I.app_config',
			appname || '_sysdata_webclient_functions');
	EXECUTE sql INTO STRICT appconfig;

	--RAISE WARNING 'APPCONFIG: %', appconfig;

	PERFORM topo_update._app_do_RemovePath_Dynamic(
		appname,
		appconfig,
		path_id
	);
END;
$BODY$ LANGUAGE plpgsql VOLATILE; --}
