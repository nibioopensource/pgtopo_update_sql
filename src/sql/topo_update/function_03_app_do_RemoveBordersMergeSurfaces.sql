CREATE OR REPLACE FUNCTION topo_update._app_do_RemoveBordersMergeSurfaces_PolicyProvider(
	surfaceLeftId TEXT,
	surfaceLeftTopoGeom topology.TopoGeometry,
	surfaceLeftProps JSONB,
	surfaceRightId TEXT,
	surfaceRightTopoGeom topology.TopoGeometry,
	surfaceRightProps JSONB,
	usr JSONB
)
RETURNS JSONB AS $BODY$
DECLARE
	procName TEXT := 'topo_update._app_do_RemoveBordersMergeSurfaces_PolicyProvider';
	props JSONB;
	att TEXT;
	attValLeft TEXT;
	attValRight TEXT;
	surfaceLeftGeom GEOMETRY;
	surfaceRightGeom GEOMETRY;
	surfaceLeftArea FLOAT8;
	surfaceRightArea FLOAT8;
	surfaceLeftNPoints INT8;
	surfaceRightNPoints INT8;
BEGIN

	surfaceLeftGeom := surfaceLeftTopoGeom::geometry;
	surfaceRightGeom := surfaceRightTopoGeom::geometry;
	surfaceLeftArea := ST_Area(surfaceLeftGeom);
	surfaceRightArea := ST_Area(surfaceRightGeom);

	RAISE DEBUG '%: surfaceLeft: % -- area %', procName, surfaceLeftId, surfaceLeftArea;
	RAISE DEBUG '%: surfaceRight: % -- area %', procName, surfaceRightId, surfaceRightArea;

	RAISE DEBUG '%: surfaceLeftProps: %', procName, surfaceLeftProps;
	RAISE DEBUG '%: surfaceRightProps: %', procName, surfaceRightProps;

	-- Use attributes of larger area surface if not equal
	IF surfaceLeftArea > surfaceRightArea
	THEN
		RAISE DEBUG '%: using surfaceLeft props (has bigger area)', procName;
		props := surfaceRightProps || surfaceLeftProps;
		return props;
	ELSIF surfaceLeftArea < surfaceRightArea
	THEN
		RAISE DEBUG '%: using surfaceRight props (has bigger area)', procName;
		props := surfaceLeftProps || surfaceRightProps;
		return props;
	END IF;

	-- Use attributes of denser surface if not equal
	surfaceLeftNPoints := ST_NPoints(surfaceLeftGeom);
	surfaceRightNPoints := ST_NPoints(surfaceRightGeom);
	RAISE DEBUG '%: surfaceLeft npoints: %', procName, surfaceLeftNPoints;
	RAISE DEBUG '%: surfaceRight npoints: %', procName, surfaceRightNPoints;
	IF surfaceLeftNPoints > surfaceRightNPoints
	THEN
		RAISE DEBUG '%: using surfaceLeft props (has more vertices)', procName;
		props := surfaceRightProps || surfaceLeftProps;
		return props;
	ELSIF surfaceLeftArea < surfaceRightArea
	THEN
		RAISE DEBUG '%: using surfaceRight props (has more vertices)', procName;
		props := surfaceLeftProps || surfaceRightProps;
		return props;
	END IF;

	-- Use attributes of surface with "least" identifier
	IF surfaceLeftId < surfaceRightId
	THEN
		RAISE DEBUG '%: using surfaceLeft props (has least identifier)', procName;
		props := surfaceRightProps || surfaceLeftProps;
		return props;
	ELSE
		RAISE DEBUG '%: using surfaceRight props (has least identifier)', procName;
		props := surfaceLeftProps || surfaceRightProps;
		return props;
	END IF;

	RAISE WARNING '%: nothing could distinguish the two geoms, not merging??', procName;
	RETURN props;
END;
$BODY$ LANGUAGE 'plpgsql'; --}


CREATE OR REPLACE FUNCTION topo_update._app_do_RemoveBordersMergeSurfaces_Dynamic(
	appname NAME,
	appconfig JSONB,
	border_id TEXT
)
RETURNS TABLE(fid text, typ char, act char, frm text[]) AS
$BODY$ --{
DECLARE
	cfg_surfaceLayer JSONB;
	cfg_surfaceLayerTable JSONB;
	cfg_borderLayer JSONB;
	cfg_borderLayerTable JSONB;
	cfg_op JSONB;
	cfg_op_params JSONB;
	cfg JSONB;
	surfaceLayerTable REGCLASS;
	surfaceLayerGeomColumn NAME;
	surfaceLayerIdColumn NAME;
	borderLayerTable REGCLASS;
	borderLayerGeomColumn NAME;
	borderLayerIdColumn NAME;
	rec RECORD;
BEGIN

	-- Check if the operation is enabled
	IF NOT appconfig -> 'operations' ? 'SurfaceMerge' THEN
		RAISE EXCEPTION 'SurfaceMerge operation not enabled for application %', appname;
	END IF;

	cfg_surfaceLayer := appconfig -> 'surface_layer';
	cfg_borderLayer := appconfig -> 'border_layer';
	cfg_op := appconfig -> 'operations' -> 'SurfaceMerge';

	--RAISE DEBUG 'surfacelayer: %', cfg_surfaceLayer;
	--RAISE DEBUG 'borderLayer: %', cfg_borderLayer;

	FOR rec IN SELECT jsonb_array_elements( appconfig -> 'tables' ) cfg
	LOOP --{
		IF rec.cfg ->> 'name' = cfg_surfacelayer ->> 'table_name' THEN
			cfg_surfaceLayerTable := rec.cfg;
		ELSIF rec.cfg ->> 'name' = cfg_borderlayer ->> 'table_name' THEN
			cfg_borderLayerTable := rec.cfg;
		END IF;
		IF cfg_borderLayerTable IS NOT NULL AND
		   cfg_surfaceLayerTable IS NOT NULL
		THEN
			EXIT;
		END IF;
	END LOOP; --}

	--RAISE DEBUG 'surfaceLayerTable: %', cfg_surfaceLayerTable;
	--RAISE DEBUG 'borderLayerTable: %', cfg_borderLayerTable;

	surfaceLayerTable := ( appname || '.' || ( cfg_surfaceLayer ->> 'table_name' ) );
	surfaceLayerGeomColumn := cfg_surfacelayer ->> 'geo_column';
	surfaceLayerIdColumn := cfg_surfacelayerTable ->> 'primary_key';

	borderLayerTable := ( appname || '.' || ( cfg_borderlayer ->> 'table_name' ) );
	borderLayerGeomColumn := cfg_borderlayer ->> 'geo_column';
	borderLayerIdColumn := cfg_borderlayerTable ->> 'primary_key';

	-- TODO: check if border_id is removable according to cfg_op
	-- HINT: use topo_update._GetMergeableSurfaceBorders_Query


	RETURN QUERY
	SELECT * FROM topo_update.remove_border_merge_surfaces(
		borderLayerTable,
		borderLayerGeomColumn,
		borderLayerIdColumn,
		border_id,
		surfaceLayerTable,
		surfaceLayerGeomColumn,
		surfaceLayerIdColumn,
		'topo_update._app_do_RemoveBordersMergeSurfaces_PolicyProvider'::regproc,
		cfg_op, -- mergePolicyProviderParam
		removePrimitives => true,
		removeDanglingBorders => true
	);

	-- TODO: if surfaces were merged (surface created with a "frm")
	--       we want to remove dangling borders

END;
$BODY$ LANGUAGE plpgsql; --}

CREATE OR REPLACE FUNCTION topo_update.app_do_RemoveBordersMergeSurfaces(

	-- Application name, will be used to fetch
	-- configuration from application schema.
	-- See doc/APP_CONFIG.md
	appname NAME,

	-- The identifier of a border to remove
	border_id TEXT

)
RETURNS TABLE(fid text, typ char, act char, frm text[]) AS
$BODY$ --{
DECLARE
	sql TEXT;
	appconfig JSONB;
BEGIN

	sql := format('SELECT cfg FROM %I.app_config',
			appname || '_sysdata_webclient_functions');
	EXECUTE sql INTO STRICT appconfig;

	--RAISE WARNING 'APPCONFIG: %', appconfig;

	RETURN QUERY
	SELECT * FROM topo_update._app_do_RemoveBordersMergeSurfaces_Dynamic(
		appname,
		appconfig,
		border_id
	);
END;
$BODY$ LANGUAGE plpgsql; --}
