--{
-- Trigger to check that all topo primitives take part in the
-- definition of at least one values in the column passed as argument
--
CREATE OR REPLACE FUNCTION topo_update.check_topo_primitives_all_used()
RETURNS trigger
AS $BODY$
DECLARE
	rec RECORD;
	layerTable REGCLASS;
	layerTopoGeomColumn NAME;
	sqlFilter TEXT;

BEGIN
	RAISE DEBUG 'TG_ARGV: %', TG_ARGV;
	RAISE DEBUG 'TG_RELID: %', TG_RELID;

	layerTable := TG_RELID;
	layerTopoGeomColumn := TG_ARGV[0];
	IF TG_ARGV[1] IS NOT NULL THEN
		sqlFilter := NULLIF(TG_ARGV[1], '');
	END IF;

	FOR rec IN SELECT * FROM topo_update.find_gaps(
			layerTable,
			layerTopoGeomColumn,
			sqlFilter
		)
	LOOP
		RAISE EXCEPTION 'No feature in layer % (%) uses % %',
				layerTable,
				layerTopoGeomColumn,
				CASE rec.etyp
				WHEN 1 THEN
					'node'
				WHEN 2 THEN
					'edge'
				WHEN 3 THEN
					'face'
				END,
				rec.eid;
	END LOOP;

	RETURN NULL;
END;
$BODY$
LANGUAGE 'plpgsql';
--}


--{
--
-- Add a constraint on given table/topogeomColumn pair ensuring that
-- all topology primitives of the type compatible with the layer
-- are referenced by at least one record
--
CREATE OR REPLACE FUNCTION topo_update.add_all_topo_primitive_used_constraint(
	layerTable REGCLASS,
	layerTopoGeomColumn NAME,
	filterSQL TEXT DEFAULT NULL
)
RETURNS TEXT
AS $BODY$
DECLARE
	sql TEXT;
	rec RECORD;
	topo topology.topology;
	layer topology.layer;
	trigname TEXT;
BEGIN

	-- Fetch topology and layer info from layer references
	SELECT t topo, l layer
		FROM topology.layer l, topology.topology t, pg_class c, pg_namespace n
		WHERE l.schema_name = n.nspname
		AND l.table_name = c.relname
		AND c.oid = layerTable
		AND c.relnamespace = n.oid
		AND l.feature_column = layerTopoGeomColumn
		AND l.topology_id = t.id
	INTO rec;
	topo := rec.topo;
	layer := rec.layer;

	-- Check consistency upfront
	FOR rec IN SELECT * FROM topo_update.find_gaps(
			layerTable,
			layerTopoGeomColumn,
			filterSQL
		)
	LOOP
		RAISE EXCEPTION 'No feature in layer % (%) uses % %',
				layerTable,
				layerTopoGeomColumn,
				CASE rec.etyp
				WHEN 1 THEN
					'node'
				WHEN 2 THEN
					'edge'
				WHEN 3 THEN
					'face'
				END,
				rec.eid;
	END LOOP;

	trigName := format('primitives_all_used_in_layer_%s', layer.layer_id);

	sql := format(
		$$
CREATE TRIGGER %1$I
AFTER DELETE OR UPDATE
ON %2$s
FOR EACH STATEMENT
EXECUTE PROCEDURE
topo_update.check_topo_primitives_all_used(%3$L, %4$L)
		$$,
		trigName,              -- %1
		layerTable,            -- %2
		layerTopoGeomColumn,   -- %3
		COALESCE(filterSQL,'') -- %4
	);

	RAISE DEBUG 'SQL: %', sql;

	EXECUTE sql;

	RETURN trigName;

END;
$BODY$ LANGUAGE 'plpgsql';
--}



