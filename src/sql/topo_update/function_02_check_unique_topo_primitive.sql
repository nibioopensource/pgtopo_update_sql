--{
-- Trigger to check that no two topo primitives take part of the
-- definition of two different values in the column passed as argument
--
CREATE OR REPLACE FUNCTION topo_update.check_unique_topo_primitive()
RETURNS trigger
AS $BODY$
DECLARE
	rec RECORD;
	layerTable REGCLASS;
	layerTopoGeomColumn NAME;
	layerIdColumn NAME;
	sqlFilter TEXT;

BEGIN
	RAISE DEBUG 'TG_ARGV: %', TG_ARGV;
	RAISE DEBUG 'TG_RELID: %', TG_RELID;

	layerTable := TG_RELID;
	layerTopoGeomColumn := TG_ARGV[0];
	layerIdColumn := TG_ARGV[1];
	IF TG_ARGV[2] IS NOT NULL THEN
		sqlFilter := NULLIF(TG_ARGV[2], '');
	END IF;

	FOR rec IN SELECT * FROM topo_update.find_interiors_intersect(
			layerTable,
			layerTopoGeomColumn,
			layerIdColumn,
			sqlFilter
		)
	LOOP
		IF rec.tgid1 != rec.tgid2 THEN
			RAISE EXCEPTION 'Features % and % in layer % (%) share %: %',
				rec.fid1, rec.fid2,
				layerTable, layerTopoGeomColumn,
				CASE
				WHEN rec.etyp = 1 THEN
					'nodes'
				WHEN rec.etyp = 2 THEN
					'edges'
				WHEN rec.etyp = 3 THEN
					'faces'
				END,
				array_to_string(rec.eids, ',') ;
		ELSE
			RAISE EXCEPTION 'Features % and % in layer % (%) have same TopoGeometry: %',
				rec.fid1, rec.fid2,
				layerTable, layerTopoGeomColumn,
				rec.tgid1;
		END IF;
	END LOOP;

	RETURN NULL;
END;
$BODY$
LANGUAGE 'plpgsql';
--}


--{
--
-- Add a constraint on given table/topogeomColumn pair ensuring that
-- no two records can reference the same topology primitive
--
CREATE OR REPLACE FUNCTION topo_update.add_unique_topo_primitive_constraint(
	layerTable REGCLASS,
	layerTopoGeomColumn NAME,
	layerIdColumn NAME,
	filterSQL TEXT DEFAULT NULL
)
RETURNS TEXT
AS $BODY$
DECLARE
	sql TEXT;
	rec RECORD;
	topo topology.topology;
	layer topology.layer;
	trigname TEXT;
BEGIN

	-- Fetch topology and layer info from layer references
	SELECT t topo, l layer
		FROM topology.layer l, topology.topology t, pg_class c, pg_namespace n
		WHERE l.schema_name = n.nspname
		AND l.table_name = c.relname
		AND c.oid = layerTable
		AND c.relnamespace = n.oid
		AND l.feature_column = layerTopoGeomColumn
		AND l.topology_id = t.id
	INTO rec;
	topo := rec.topo;
	layer := rec.layer;

	-- Check consitency upfront
	FOR rec IN SELECT * FROM topo_update.find_interiors_intersect(
			layerTable,
			layerTopoGeomColumn,
			layerIdColumn,
			filterSQL
		)
	LOOP
		IF rec.tgid1 != rec.tgid2 THEN
			RAISE EXCEPTION 'Features % and % in layer % (%) share %: %',
				rec.fid1, rec.fid2,
				layerTable, layerTopoGeomColumn,
				CASE
				WHEN rec.etyp = 1 THEN
					'nodes'
				WHEN rec.etyp = 2 THEN
					'edges'
				WHEN rec.etyp = 3 THEN
					'faces'
				END,
				array_to_string(rec.eids, ',') ;
		ELSE
			RAISE EXCEPTION 'Features % and % in layer % (%) have same TopoGeometry: %',
				rec.fid1, rec.fid2,
				layerTable, layerTopoGeomColumn,
				rec.tgid1;
		END IF;
	END LOOP;

	trigName := format('unique_primitives_in_layer_%s', layer.layer_id);

	sql := format(
		$$
CREATE TRIGGER %1$I
AFTER INSERT OR UPDATE
ON %2$s
FOR EACH STATEMENT
EXECUTE PROCEDURE
topo_update.check_unique_topo_primitive(%3$L, %4$L, %5$L)
		$$,
		trigName,              -- %1
		layerTable,            -- %2
		layerTopoGeomColumn,   -- %3
		layerIdColumn,         -- %4
		COALESCE(filterSQL,'') -- %5
	);

	RAISE DEBUG 'SQL: %', sql;

	EXECUTE sql;

	RETURN trigName;

END;
$BODY$ LANGUAGE 'plpgsql';
--}



