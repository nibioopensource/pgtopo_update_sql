
-- Return a point on a face
CREATE OR REPLACE FUNCTION topo_update._point_on_face(topo_name text, face_id int)
RETURNS GEOMETRY AS $BODY$ --{
DECLARE
	mbr GEOMETRY;
BEGIN
	IF face_id = 0 THEN
		EXECUTE format($$
			SELECT mbr FROM %1$I.face WHERE face_id = 0
		$$, topo_name) INTO mbr;
		IF mbr IS NOT NULL THEN
			RAISE EXCEPTION 'topo_update._point_on_face: topology % is corrupted: universal face has not-null MBR', topo_name;
		ELSE
			-- universal face has no geometry...
			RAISE EXCEPTION 'topo_update._point_on_face should not be called with face_id=0';
		END IF;
		--RETURN NULL;
	END IF;

	-- TODO: rather than building the face's
	-- Geometry we could  take a point on its
	-- surface using some optimized function
	-- to do that. See
	-- https://trac.osgeo.org/postgis/ticket/4861
	RETURN ST_PointOnSurface(topology.ST_GetFaceGeometry(topo_name, face_id));
END;
$BODY$ LANGUAGE 'plpgsql'; --}


-- Default role & properties extractor function for update_feautures
CREATE OR REPLACE FUNCTION topo_update._update_features_def_extractor(
	feature JSONB,
	feat_srid INT,
	topo_srid INT,
	OUT role CHAR,
	OUT prop JSONB
) AS $BODY$ -- {
DECLARE
	proc_name TEXT := 'topo_update._update_features_def_extractor';
	feat_type TEXT;
BEGIN

	-- Determine role
	feat_type := feature -> 'geometry' ->> 'type';
	IF feat_type = 'LineString' OR feat_type = 'MultiLineString'
	THEN
		role := 'B';
	ELSIF feat_type = 'Polygon' OR feat_type = 'MultiPolygon'
	THEN
		role := 'S';
	ELSE
		RAISE EXCEPTION '%: unsupported feature type %', proc_name, feat_type;
	END IF;

	-- Extract properties role
	prop := feature -> 'properties';
END;
$BODY$ --}
LANGUAGE plpgsql;

--
-- This function takes a toplogical GeoJSON feature collection with structure
-- specified by
-- http://skjema.geonorge.no/SOSI/produktspesifikasjon/FKB-Ar5/4.6/FKB-Ar546.xsd
-- and writes the content of it into a PostGIS Topology model using a layer
-- for Surface features and a layer for Border features.
--
-- Features which go to either the "Surface" or "Border" layer based
-- on their determined role. By default the role is Surface for
-- polygonal features and Border for lineal features, but the role
-- can be determined by a function passed as the optional argument
-- "roleAndPropsExtractor"
--
-- Features will be identified by an column specified as a parameter
-- to this function ("arealLayerIDColumn", "linealLayerIDColumn") which
-- will map to a GeoJSON Feature property via a mapping file
-- ("arealLayerColMap", "linealLayerColMap").
--
-- The feature identifier will be used to determine whether to
-- UPDATE an existing feature or insert a new one.
--
-- Border features spatial component will be defined by the "geometry"
-- property of the JSON.
--
-- Surface features spatial component will be defined by a
-- "geometry_properties" property of them describing Border rings
-- forming the exterior ("exterior") and the interior ("interiors")
-- rings of the Surface. Rings are specified as an array of signed
-- Border identifiers and are required to be defined in CCW order
-- for the exterior ring and CW order for the interior ring; if such
-- order is not verified an exception will be thrown.
--
-- Column values for layer features will be read by default from the
-- "properities" GeoJSON Feature attribute but can optionally be
-- extracted by an optional "roleAndPropsExtractor" function passed
-- as an argument.
--
-- Optionally, a bounding box can be passed to this function to
-- request that any existing feature intersecting such bounding box
-- but NOT present in the input GeoJSON is deleted.
--
CREATE OR REPLACE FUNCTION topo_update.update_features(

	-- GeoJSON feature collection with
	-- structure as specified by
	-- http://skjema.geonorge.no/SOSI/produktspesifikasjon/FKB-Ar5/4.6/FKB-Ar546.xsd
	featureset JSONB,

	arealLayerTable regclass,
	arealLayerColumn name,
	arealLayerIDColumn name,
	arealLayerColMap JSONB, -- See json_props_to_pg_cols

	linealLayerTable regclass,
	linealLayerColumn name,
	linealLayerIDColumn name,

	linealLayerColMap JSONB, -- See json_props_to_pg_cols

	-- Snap tolerance to use when
	-- inserting or updating features
	-- in the topology. The value is to be given
	-- in the units of the target topology projection.
	tolerance float8,

	-- If this parameter is given, delete,
	-- from the areal and lineal layers any TopoGeometry
	-- which was NOT found in the featureset JSON payload
	-- but still intersects the given bounding box.
	deleteUnknownFeaturesInBbox GEOMETRY DEFAULT NULL,

	-- If this parameter is given, use the given
	-- function to extract role and properties of
	-- a feature found in the featureset GeoJSON feature
	-- collection. The function will need to accept the following
	-- named parameters:
	--		o feature JSONB the feature JSONB object
	--		o feat_srid INT the SRID of the feature object
	--		o topo_srid INT the SRID of the target topology
	--
	-- and return two columns:
	--		o role CHAR being 'S' for Surface and 'B' for Border
	--      o prop JSONB being the feature's properties (as expected
	--                   by the ColMap mapping files)
	--
	-- If not given, the extractor function will determine
	-- "role" based on the feature's "geometry"."type" attribute
	-- ('S' for Polygon or MultiPolygon and 'B' for LineString
	-- or MultiLineString) and use the feature's "properties"
	-- attribute for the "props"
	--
	roleAndPropsExtractor REGPROC DEFAULT NULL,

	-- If this parameter is given, invoke the given function
	-- for every Border and Surface found in the
	-- payload, passing it the following parameters:
	--
	--     o role CHAR being 'S' for Surface and 'B' for Border
	--     o fid the value of the feature identifier, in text form
	--     o geom GEOMETRY of the feature as found in the payload
	--            and projected to topology CRS (the Surface
	--            geometry will be built from costituent Borders)
	--     o tg TopoGeometry of the feature as resulting by the
	--          upsert operation.
	--     o tol the tolerance passed to this function
	--
	-- The function return code is not used, but the function
	-- could raise an exception if the result is considered
	-- invalid.
	resultingFeatureValidator REGPROC DEFAULT NULL,

	-- If this parameter is given, invoke the given function
	-- once at the start, passing it the given featureset and
	-- the deleteUnknownFeaturesInBbox parameter and read them
	-- back.
	--
	-- Names required for the function to accept as
	-- INOUT parameters are:
	--
	--     o payload JSONB feature collection
	--     o bbox GEOMETRY the value of deleteUnknownFeaturesInBbox
	--
	-- If the returned payload is NULL no action will be taken
	-- by this function.
	--
	payloadPreparation REGPROC DEFAULT NULL,

	-- Name of a field that represents the "version" of
	-- the whole lineal record. If the value of such field does
	-- not change, the whole record is expected to not
	-- have changed, which means we don't need to update it
	linealLayerVersionColumn name DEFAULT NULL,

	-- Name of a field that represents the "version" of
	-- the whole areal record. If the value of such field does
	-- not change, the whole record is expected to not
	-- have changed, which means we don't need to update it
	arealLayerVersionColumn name DEFAULT NULL,

	-- If this not null Lines/surface/points that is not covered by this box will not be added or updated.
	-- This is typically used to speed up loading big data sets in the initial face .
	-- When all data are loaded into the database outerBarrierBboxForUpsert will in normal cases be null.
	-- This is temporary solution just to prove that by limiting the number the number of connected edges the performance is quite  stable.
	-- For more info also check out https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/228
	outerBarrierBboxForUpsert GEOMETRY DEFAULT NULL


)
RETURNS VOID AS
$BODY$ -- {
DECLARE
	rec RECORD;
	rec2 RECORD;
	feat JSONB;
	feat_id TEXT;
	feat_role CHAR;
	feat_prop JSONB;
	feat_geom GEOMETRY;
	geom_prop JSONB;
	ring_json JSONB;
	feat_crs TEXT;
	feat_srid INT;
	surface_features JSONB[];
	surface_geom GEOMETRY;
	hdist FLOAT8;
	surface_topoelems topology.TopoElementArray;
	ring GEOMETRY;
	exterior_ring GEOMETRY;
	interior_rings GEOMETRY[];
	affected_rows INT8;
	extractor REGPROC;
	extractSql TEXT;
	validateSql TEXT;
	sql TEXT;

	topo topology.topology;
	borderLayer topology.layer;
	borderLayerIDType name;
	borderLayerVersionType name;
	surfaceLayer topology.layer;
	surfaceLayerIDType name;
	surfaceLayerVersionType name;

	cleanupTopoPrimitives BOOLEAN := FALSE;

	procName TEXT := 'topo_update.update_features';

	json_prop_array TEXT[];

	payloadFeaturesCount INT;
	knownBorderFeaturesCount INT;
	knownSurfaceFeaturesCount INT;

	v_state text;
	v_msg text;
	v_detail text;
	v_hint text;
	v_context text;

	border_lines_removed int;
BEGIN


	IF payloadPreparation IS NOT NULL THEN
		sql := format($$
			SELECT * FROM %s(payload => $1, bbox => $2)
		$$, payloadPreparation);
		RAISE WARNING 'Preparation SQL: %', sql;
		EXECUTE sql USING featureset, deleteUnknownFeaturesInBbox
		INTO rec;
		featureset := rec.payload;
		deleteUnknownFeaturesInBbox := rec.bbox;

		IF featureset IS NULL THEN
			RAISE WARNING 'Payload preparation returned NULL payload, nothing to do';
			RETURN;
		END IF;
	END IF;

	-- Fetch topology and layer info from border layer references
	SELECT t topo, l layer
		FROM topology.layer l, topology.topology t, pg_class c, pg_namespace n
		WHERE l.schema_name = n.nspname
		AND l.table_name = c.relname
		AND c.oid = linealLayerTable
		AND c.relnamespace = n.oid
		AND l.feature_column = linealLayerColumn
		AND l.topology_id = t.id
	--	FOR UPDATE, 2 problems, user needs SUPERUSER (can be fixed),
	--  ms used from 8601 -> 113606 for ((14.355156130507279 67.27980828078813, 14.354976710179235 67.28648792270606, 14.395077865039152 67.28664388346324, 14.395246132501715 67.27996419068565, 14.355156130507279 67.27980828078813))
		INTO rec;
	topo := rec.topo;
	borderLayer := rec.layer;

	--	RAISE NOTICE 'Lock on topology % obtained', topo.name;

	-- Fetch topology and layer info from surface layer references
	SELECT t topo, l layer
		FROM topology.layer l, topology.topology t, pg_class c, pg_namespace n
		WHERE l.schema_name = n.nspname
		AND l.table_name = c.relname
		AND c.oid = arealLayerTable
		AND c.relnamespace = n.oid
		AND l.feature_column = arealLayerColumn
		AND l.topology_id = t.id
		INTO rec;

	IF id(topo) != id(rec.topo) THEN
		RAISE EXCEPTION '%: Lineal and Areal layers need to be in same topology',
			procName;
	END IF;
	surfaceLayer := rec.layer;

	-- Fetch border layer id column type
	SELECT t.typname FROM pg_type t, pg_attribute a
	WHERE a.attrelid = linealLayerTable
	AND a.attname = linealLayerIDColumn
	AND a.atttypid = t.oid
	INTO borderLayerIDType;

	-- Fetch border layer version column type
	IF linealLayerVersionColumn IS NOT NULL THEN
		IF NOT linealLayerColMap ? linealLayerVersionColumn THEN
			RAISE EXCEPTION 'Invalid linealLayerVersionColumn: % not found in linealLayerColMap %',
				linealLayerVersionColumn, linealLayerColMap;
		END IF;
		SELECT t.typname FROM pg_type t, pg_attribute a
		WHERE a.attrelid = linealLayerTable
		AND a.attname = linealLayerVersionColumn
		AND a.atttypid = t.oid
		INTO borderLayerVersionType;
	END IF;

	-- Fetch surface layer id column type
	SELECT t.typname FROM pg_type t, pg_attribute a
	WHERE a.attrelid = arealLayerTable
	AND a.attname = arealLayerIDColumn
	AND a.atttypid = t.oid
	INTO surfaceLayerIDType;

	-- Fetch surface layer version column type
	IF arealLayerVersionColumn IS NOT NULL THEN
		IF NOT arealLayerColMap ? arealLayerVersionColumn THEN
			RAISE EXCEPTION 'Invalid arealLayerVersionColumn: % not found in arealLayerColMap %',
				arealLayerVersionColumn, arealLayerColMap;
		END IF;
		SELECT t.typname FROM pg_type t, pg_attribute a
		WHERE a.attrelid = arealLayerTable
		AND a.attname = arealLayerVersionColumn
		AND a.atttypid = t.oid
		INTO surfaceLayerVersionType;
	END IF;

	IF roleAndPropsExtractor IS NOT NULL THEN
		extractor := roleAndPropsExtractor;
	ELSE
		extractor := 'topo_update._update_features_def_extractor'::regproc;
	END IF;

	extractSql := format(
		'SELECT role, prop FROM %s(feature => $1, feat_srid => $2, topo_srid => $3)',
		extractor::text
	);
	RAISE DEBUG 'Extraction SQL: %', extractSql;

	-- Write validator SQL
	-- Parameters are:
	--	$1 role
	--	$2 fid
	--	$3 geom
	--	$4 tg
	--	$5 tolerance
	IF resultingFeatureValidator IS NOT NULL
	THEN
		validateSql := format(
			'SELECT %s( role => $1, fid => $2, geom => $3, tg => $4, tol => $5 )',
			resultingFeatureValidator::text
		);
		RAISE DEBUG 'Validator SQL: %', validateSql;
	END IF;

	IF NOT featureset ? 'crs'
	THEN
		RAISE EXCEPTION 'Featureset lacks an explicit "crs" field';
	END IF;
	feat_crs := featureset -> 'crs' -> 'properties' ->> 'name';

	RAISE DEBUG 'crs: %', feat_crs;

	SELECT s.srid
	FROM spatial_ref_sys s
	WHERE format('%s:%s', s.auth_name, s.auth_srid) = feat_crs
	INTO feat_srid;

	DROP TABLE IF EXISTS pg_temp.payload_features;

	sql := format(
		$$
	CREATE TEMP TABLE payload_features
	AS
	SELECT
		NULL::text AS id,
		NULL::text AS version,
		false::bool AS known,
		false::bool AS outside_outer_barrier_bbox_for_upsert,
		NULL::geometry AS geom,
		e.*,
		f -> 'geometry_properties' as geom_prop,
		f -> 'geometry' as geom_json
	FROM jsonb_array_elements($1 -> 'features') f,
		%1$s(feature => f, feat_srid => %3$L, topo_srid => %2$L) e
		$$,
		extractor::text, -- %1
		topo.srid, -- %2
		feat_srid, -- %3
		topo.srid  -- %4
	);
	RAISE DEBUG 'SQL: %', sql;
	EXECUTE sql USING featureset;
	GET DIAGNOSTICS payloadFeaturesCount = ROW_COUNT;
	RAISE NOTICE 'Features in payload: %', payloadFeaturesCount;

	-- Extract ID, Version and Geometry
	UPDATE payload_features SET
		id =
			CASE WHEN role = 'B' THEN
				prop #>> (
					SELECT array_agg(x) FROM
					jsonb_array_elements_text(
						linealLayerColMap -> linealLayerIdColumn
					) x
				)
			ELSE
				prop #>> (
					SELECT array_agg(x) FROM
					jsonb_array_elements_text(
						arealLayerColMap -> arealLayerIdColumn
					) x
				)
			END,
		version =
			CASE WHEN role = 'B' THEN
				prop #>> (
					SELECT array_agg(x) FROM
					jsonb_array_elements_text(
						linealLayerColMap -> linealLayerVersionColumn
					) x
				)
			ELSE
				prop #>> (
					SELECT array_agg(x) FROM
					jsonb_array_elements_text(
						arealLayerColMap -> arealLayerVersionColumn
					) x
				)
			END,
		geom =
			CASE WHEN role = 'B' THEN
				ST_Transform(
					ST_SetSRID(
						ST_Force2D(
							ST_GeomFromGeoJSON(geom_json)
						),
						feat_srid
					),
					topo.srid
				)
			ELSE
				NULL -- we will construct the Surface from Border refs
			END
	;

	BEGIN
		CREATE UNIQUE INDEX ON payload_features(id);
	EXCEPTION WHEN unique_violation THEN
	    GET STACKED DIAGNOSTICS v_state = RETURNED_SQLSTATE, v_msg = MESSAGE_TEXT, v_detail = PG_EXCEPTION_DETAIL, v_hint = PG_EXCEPTION_HINT, v_context = PG_EXCEPTION_CONTEXT;
		RAISE NOTICE 'Failed CREATE UNIQUE INDEX ON payload_features(id) at % state  : % message: % detail : % hint : % context: %',
		Timeofday(), v_state, v_msg, v_detail, v_hint, v_context;

		FOR rec IN
			SELECT id, count(role), array_agg(distinct role) roles
			FROM payload_features
			GROUP BY id
			HAVING count(*) > 1
		LOOP
			RAISE EXCEPTION '% with duplicate identifier % found in payload',
				CASE
					WHEN array_upper(rec.roles, 1) > 1 THEN
						'Features'
					WHEN rec.roles[1] = 'B' THEN
						'Borders'
					ELSE
						'Surfaces'
				END,
				rec.id;
		END LOOP;
	END;

	CREATE INDEX ON payload_features(role);

	-- Mark already known borders
	IF linealLayerVersionColumn IS NOT NULL THEN
		sql := format(
			$$
				UPDATE payload_features p
				SET known = true
				FROM %1$s l
				WHERE role = 'B'
				AND l.%2$I = p.id::%4$I
				AND (
					( l.%3$I IS NULL AND p.version IS NULL )
					OR
					( l.%3$I = p.version::%5$I )
				)
			$$,
			linealLayerTable,         -- %1
			linealLayerIDColumn,      -- %2
			linealLayerVersionColumn, -- %3
			borderLayerIDType,        -- %4
			borderLayerVersionType    -- %5
		);
		RAISE DEBUG 'SQL: %', sql;
		EXECUTE sql;
		GET DIAGNOSTICS knownBorderFeaturesCount = ROW_COUNT;
		RAISE NOTICE 'Known Border features in payload: %', knownBorderFeaturesCount;
	END IF;

	-- Mark already known surfaces
	IF arealLayerVersionColumn IS NOT NULL THEN
		sql := format(
			$$
				UPDATE payload_features p
				SET known = true
				FROM %1$s l
				WHERE p.role = 'S'
				AND l.%2$I = p.id::%4$I
				AND (
					( l.%3$I IS NULL AND p.version IS NULL )
					OR
					( l.%3$I = p.version::%5$I )
				)
			$$,
			arealLayerTable,         -- %1
			arealLayerIDColumn,      -- %2
			arealLayerVersionColumn, -- %3
			surfaceLayerIDType,      -- %4
			surfaceLayerVersionType  -- %5
		);
		RAISE DEBUG 'SQL: %', sql;
		EXECUTE sql;
		GET DIAGNOSTICS knownSurfaceFeaturesCount = ROW_COUNT;
		RAISE NOTICE 'Known Surface features in payload: %', knownSurfaceFeaturesCount;
	END IF;

	-- Delete any features intersecting the given area
	-- and not present in payload
	IF deleteUnknownFeaturesInBbox IS NOT NULL
	THEN --{

		IF ST_SRID(deleteUnknownFeaturesInBbox) != topo.srid
		THEN
			RAISE WARNING 'Bounding box passed to update_feature is not in the target topology CRS, transforming it';
			deleteUnknownFeaturesInBbox := ST_Transform(deleteUnknownFeaturesInBbox, topo.srid);
		END IF;

		affected_rows := topo_update.delete_unknown_features(
			arealLayerTable,
			arealLayerColumn,
			arealLayerIDColumn,
			deleteUnknownFeaturesInBbox,
			( SELECT COALESCE(array_agg(id),ARRAY[]::text[])
			  FROM payload_features
			  WHERE role = 'S'
			  AND known
			)
		);
		RAISE NOTICE 'Deleted % rows from areal layer', affected_rows;
		IF affected_rows > 0 THEN
			cleanupTopoPrimitives := TRUE;
		END IF;

		affected_rows := topo_update.delete_unknown_features(
			linealLayerTable,
			linealLayerColumn,
			linealLayerIDColumn,
			deleteUnknownFeaturesInBbox,
			( SELECT COALESCE(array_agg(id),ARRAY[]::text[])
			  FROM payload_features
			  WHERE role = 'B'
			  AND known
			)
		);
		RAISE NOTICE 'Deleted % rows from lineal layer', affected_rows;
		IF affected_rows > 0 THEN
			cleanupTopoPrimitives := TRUE;
		END IF;

		-- Clean up topology primitives if needed
		IF cleanupTopoPrimitives
		THEN --{
			affected_rows := topo_update.cleanup_topology(topo, deleteUnknownFeaturesInBbox);

			RAISE NOTICE 'Deleted % topology primitives', affected_rows;

		END IF; --}

	END IF; --}

	IF outerBarrierBboxForUpsert IS NOT NULL
	THEN
		CREATE INDEX ON payload_features USING GIST(geom);

		UPDATE payload_features p
		SET outside_outer_barrier_bbox_for_upsert = true
		WHERE role = 'B'
		AND NOT ST_Contains(outerBarrierBboxForUpsert,p.geom);

		GET DIAGNOSTICS border_lines_removed = ROW_COUNT;
		RAISE NOTICE 'for outerBarrierBboxForUpsert % , remove % because it touches edge_line_barrier ',
		outerBarrierBboxForUpsert, border_lines_removed;
	END IF;


	-- Upsert the borders
	PERFORM topo_update.upsert_feature(
		geom,
		prop,
		linealLayerTable,
		linealLayerColumn,
		linealLayerIDColumn,
		linealLayerColMap,
		tolerance,
		layerVersionColumn => linealLayerVersionColumn
	)
	FROM payload_features
	WHERE role = 'B'
	AND NOT known
	AND NOT outside_outer_barrier_bbox_for_upsert;

	GET DIAGNOSTICS affected_rows = ROW_COUNT;
	RAISE NOTICE 'Upserted % rows in lineal layer', affected_rows;
	IF affected_rows > 0 THEN
		--cleanupTopoPrimitives := TRUE;
	END IF;

	CREATE TEMP VIEW update_features_borders AS
	SELECT id, geom FROM payload_features
	WHERE role = 'B';

	-- Construct and insert surfaces
	affected_rows := 0;
	FOR rec IN
		SELECT *
		FROM payload_features
		WHERE role = 'S'
		AND NOT known
	LOOP --{

		feat_prop := rec.prop;
		RAISE DEBUG 'Surface extracted_properties: %', feat_prop;

		geom_prop := rec.geom_prop;
		IF geom_prop IS NULL THEN
			RAISE EXCEPTION 'Surface lacks geometry_properties attribute: %', rec;
		END IF;
		RAISE DEBUG 'Surface geometry_properties: %', feat_prop;

		-- Build external ring
		ring_json := geom_prop -> 'exterior';
		RAISE DEBUG 'Surface exterior ring: %', ring_json;
		IF ring_json IS NULL THEN
			RAISE EXCEPTION 'Surface exterior ring attribute';
		END IF;
		WITH ring AS (
			SELECT
				row_number() OVER () seq,
				trim( leading '-' FROM e ) bid,
				e LIKE '-%' AS backward
			FROM
				jsonb_array_elements_text(ring_json) e
		)
		SELECT
			ST_MakeLine(
				CASE
				WHEN ring.backward THEN
					ST_Reverse(b.geom)
				ELSE
					b.geom
				END
				ORDER BY ring.seq
			)
		FROM
			update_features_borders b,
			ring
		WHERE
			b.id = ring.bid
		INTO exterior_ring;

		IF exterior_ring IS NULL THEN
			RAISE EXCEPTION $$
Not all borders referenced in surface exterior ring (%) are found in payload
				$$, ring_json;
		END IF;
		RAISE DEBUG 'Exterior ring: % (SRID:%)', ST_Summary(exterior_ring), ST_SRID(exterior_ring);

		-- Check that exterior ring is closed
		-- TODO How will this check affect performance ??, (If the error print in ST_MakePolygon was better we should not need this check, no we onøy see this lwpoly_from_lwlines: shell must be closed)
		IF NOT ST_IsClosed(exterior_ring) THEN
			RAISE DEBUG 'Exterior ring not closed run snap to grid with tolerance % : % (SRID:%)', tolerance, ST_Summary(exterior_ring), ST_SRID(exterior_ring);

			-- Check if this is a coodinate decimal problem run snap to grid an d chack again
			exterior_ring = ST_SnapToGrid(exterior_ring,tolerance);

			IF NOT ST_IsClosed(exterior_ring) THEN

				RAISE EXCEPTION 'Exterior ring is not closed for surface %  using this %',
					feat_prop #>> (
						SELECT array_agg(x)
						FROM
							jsonb_array_elements_text(
								arealLayerColMap -> arealLayerIDColumn
							) x
					),
					ST_AsEWKT(exterior_ring)
				;
			END IF;
		END IF;

		-- Check that the exterior ring is in CCW order
		IF NOT ST_IsPolygonCCW(ST_MakePolygon(exterior_ring)) THEN
			RAISE EXCEPTION 'Exterior ring of Surface % is not in CCW order, at or near %',
				feat_prop #>> (
					SELECT array_agg(x)
					FROM
						jsonb_array_elements_text(
							arealLayerColMap -> arealLayerIDColumn
						) x
				),
				ST_AsEWKT(ST_StartPoint(exterior_ring))
			;
		END IF;

		-- Build internal rings

		-- TODO we may get the same problems with internal rings yes

		ring_json := geom_prop -> 'interiors';

		WITH rings AS (
			SELECT row_number() OVER () ring_no, spec
			FROM (
				SELECT jsonb_array_elements(ring_json) spec
			) x
		),
		rings_with_signed_edges AS (
			SELECT
				ring_no,
				row_number() OVER (partition by ring_no) edge_no,
				signed_edge_id
			FROM (
				SELECT
					ring_no,
					jsonb_array_elements_text(spec) signed_edge_id
				FROM rings
			) x
		),
		rings_with_id_and_dir AS (
			SELECT
				ring_no,
				edge_no,
				trim( leading '-' FROM signed_edge_id ) bid,
				signed_edge_id LIKE '-%' AS backward
			FROM
				rings_with_signed_edges
		),
		rings_with_geom AS (
			SELECT
				ring.ring_no,
				ST_MakeLine(
						CASE
						WHEN ring.backward THEN
							ST_Reverse(b.geom)
						ELSE
							b.geom
						END
						ORDER BY ring.edge_no
					) geom
				FROM
					update_features_borders b,
					rings_with_id_and_dir ring
				WHERE
					b.id = ring.bid
				GROUP BY ring.ring_no
		)
		SELECT array_agg(geom ORDER BY ring_no) holes
		FROM rings_with_geom
		INTO interior_rings;

		IF interior_rings IS NOT NULL THEN
			RAISE DEBUG 'Surface has % holes', array_upper(interior_rings, 1);
			surface_geom := ST_MakePolygon(exterior_ring, interior_rings);
		ELSE
			surface_geom := ST_MakePolygon(exterior_ring);
		END IF;


		IF outerBarrierBboxForUpsert IS NOT NULL AND NOT ST_Contains(outerBarrierBboxForUpsert,surface_geom) THEN
			RAISE NOTICE 'for outerBarrierBboxForUpsert % , removed surface at %',
			outerBarrierBboxForUpsert,
			ST_Centroid(surface_geom);
		ELSE

		rec2 := ST_IsValidDetail(surface_geom);
		IF NOT rec2.valid THEN
			RAISE EXCEPTION 'Geometry of Surface % is not valid: % at or near %',
				feat_prop #>> (
					SELECT array_agg(x)
					FROM jsonb_array_elements_text( arealLayerColMap -> arealLayerIDColumn ) x
				),
				-- NOTE: lower() gives us a nice output BUT it's
				--       really a workaround for some bug which
				--       seems to stop output from RAISE at the end
				--       of the string
				lower(rec2.reason),
				ST_AsEWKT(rec2.location)
				;
		END IF;

		--RAISE DEBUG 'Surface Geom: %', ST_AsEWKT(surface_geom);

		-- Find all faces covered by the Surface geometry
		sql := format(
			$$
				SELECT
					topology.TopoElementArray_agg(
						ARRAY[f.face_id,3]::topology.TopoElement
					)
				FROM %1$I.face f
				WHERE f.mbr @ $1 -- face mbr is contained in surface's ($1) mbr
				-- TODO: check that face mbr is NOT contained in any surface's hole mbr ?
				AND ST_Covers(
					$1,
					topo_update._point_on_face(%1$L, f.face_id)
				)
			$$,
			topo.name
		);

		RAISE DEBUG 'SQL: %', sql;
		EXECUTE sql
			USING surface_geom
			INTO surface_topoelems;

		-- Pass surface_topoelems to upsert_feature
		rec2 := topo_update.upsert_feature(
				surface_topoelems,
				feat_prop,
				arealLayerTable,
				arealLayerColumn,
				arealLayerIDColumn,
				arealLayerColMap
			);
		affected_rows := affected_rows + 1;

		-- Verify acceptability of new Surface object
		-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/104

		IF rec2.topogeom IS NULL
		THEN
			RAISE EXCEPTION
				'Input Surface % around % covers no topology'
				' face (too much snapping ?)',
				rec.id,
				ST_AsEWKT(ST_PointOnSurface(surface_geom))
			;
		END IF;


		-- Invoke external validator, if given

		IF validateSql IS NOT NULL
		THEN
			EXECUTE validateSql
			USING 'S', rec.id, surface_geom, rec2.topogeom, tolerance;
		END IF;
		END IF;

	END LOOP; --}
	RAISE NOTICE 'Upserted % rows in areal layer', affected_rows;
	IF affected_rows > 0 THEN
		--cleanupTopoPrimitives := TRUE;
	END IF;

	DROP VIEW pg_temp.update_features_borders;
	DROP TABLE pg_temp.payload_features;


END;
$BODY$ --}
LANGUAGE plpgsql;


COMMENT ON FUNCTION topo_update.update_features IS $$
feature - JSON payload with format specified by http://skjema.geonorge.no/SOSI/produktspesifikasjon/FKB-Ar5/4.6/FKB-Ar546.xsd
$$;

