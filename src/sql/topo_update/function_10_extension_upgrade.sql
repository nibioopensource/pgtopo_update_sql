
CREATE OR REPLACE FUNCTION topo_update.extension_upgrade()
RETURNS TEXT AS $BODY$
DECLARE
	oldversion text;
BEGIN
	UPDATE pg_catalog.pg_extension
	SET extversion = 'ANY'
	WHERE extname = 'topo_update'
	RETURNING extversion
	INTO oldversion;

	IF oldversion IS NOT NULL THEN
		RAISE DEBUG 'Old version: %', oldversion;
		ALTER EXTENSION topo_update UPDATE;
	ELSE
		CREATE EXTENSION topo_update;
	END IF;

	RETURN 'topo_update extension is updated, run topo_update.version() for details';
END;
$BODY$
LANGUAGE plpgsql;
