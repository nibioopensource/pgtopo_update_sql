--
-- Generate a JSON mapping file mapping
-- columns of a PostgreSQL relation to a
-- JSON property with the same name.
--
-- Names of PostgreSQL columns to _omit_ from the
-- mapping can be provided.
--
--
CREATE OR REPLACE FUNCTION topo_update.json_col_map_from_pg(
	targetTable REGCLASS,

	-- Column names to exclude from the mapping
	-- can be specified as either simple or composite
	-- names. Composite names have a dot. Example:
	--
	--  - 'simpleColumn'
	--  - 'complexColumn.child'
	--
	-- NOTE: column names containing dots are not supported
	--
	excludeCols TEXT[] DEFAULT ARRAY[]::TEXT[]
)
RETURNS JSONB AS $BODY$ --{
DECLARE
	rec RECORD;
	subrec RECORD;
	map JSONB;
	maprec JSONB;
	submap JSONB;
	pg_key TEXT;
	js_key TEXT[];
BEGIN

	map := '{}';

	FOR rec IN
		SELECT
			a.attname,
			t.typtype,
			t.typrelid
		FROM pg_attribute a
		JOIN pg_type t ON (a.atttypid = t.oid)
		WHERE a.attrelid = targetTable
		AND NOT a.attisdropped
		AND a.attnum > 0
	LOOP
		pg_key := rec.attname;
		IF ARRAY[ pg_key ] <@ excludeCols THEN
			CONTINUE;
		END IF;
		IF rec.typtype = 'b' THEN
			js_key := ARRAY[ rec.attname ];
			maprec := jsonb_build_object(pg_key, js_key);
			map := map || maprec;
		ELSIF rec.typtype = 'c' THEN
			-- Type is composite
			submap := topo_update.json_col_map_from_pg(rec.typrelid);
			FOR subrec IN SELECT jsonb_object_keys( submap ) k
			LOOP
				pg_key := format('%s.%s', rec.attname, subrec.k);
				IF ARRAY[ pg_key ] <@ excludeCols THEN
					CONTINUE;
				END IF;
				js_key := ARRAY[ rec.attname, subrec.k ];
				maprec := jsonb_build_object(pg_key, js_key);
				map := map || maprec;
			END LOOP;
		END IF;
	END LOOP;

	RETURN map;
END;
$BODY$ --}
LANGUAGE 'plpgsql';

