CREATE OR REPLACE FUNCTION topo_update._app_do_AddBorder_Dynamic(
	appname NAME,
	appconfig JSONB,
	border JSONB
)
RETURNS TEXT AS
$BODY$ --{
DECLARE
	cfg_borderLayer JSONB;
	cfg_borderLayerTable JSONB;
	cfg_op JSONB;
	cfg_op_params JSONB;
	cfg JSONB;
	borderLayerTable REGCLASS;
	borderLayerGeomColumn NAME;
	borderLayerIdColumn NAME;
	snapTolerance FLOAT8;
	border_geom GEOMETRY;
	border_is_multiline BOOLEAN;
	rec RECORD;
	colMap JSONB;
	borderID TEXT;
BEGIN

	RAISE NOTICE 'AddBorder operation is deprecated, consider using AddPath';

	-- Check if the operation is enabled
	IF NOT appconfig -> 'operations' ? 'AddBorder' THEN
		RAISE EXCEPTION 'AddBorder operation not enabled for application %', appname;
	END IF;

	cfg_borderLayer := appconfig -> 'border_layer';

	--RAISE DEBUG 'borderLayer: %', cfg_borderLayer;

	FOR rec IN SELECT jsonb_array_elements( appconfig -> 'tables' ) cfg
	LOOP --{
		IF rec.cfg ->> 'name' = cfg_borderlayer ->> 'table_name' THEN
			cfg_borderLayerTable := rec.cfg;
			EXIT;
		END IF;
	END LOOP; --}

	--RAISE DEBUG 'borderLayerTable: %', cfg_borderLayerTable;

	borderLayerTable := ( appname || '.' || ( cfg_borderlayer ->> 'table_name' ) );
	borderLayerGeomColumn := cfg_borderlayer ->> 'geo_column';
	borderLayerIdColumn := cfg_borderlayerTable ->> 'primary_key';

	-- Set parameters defaults
	snapTolerance := 0;

	-- Read parameters defaults from appconfig
	cfg_op_params := appconfig -> 'operations' -> 'AddBorder' -> 'parameters';
	IF cfg_op_params IS NOT NULL THEN --{

		-- Read snapTolerance from config, if found
		cfg := cfg_op_params -> 'snapTolerance';
		IF cfg ? 'units' THEN
			snapTolerance := cfg -> 'units';
			IF cfg -> 'unitsAreMeters' THEN
				RAISE EXCEPTION 'Specifying snapTolerance in meters is unsupported';
			END IF;
		END IF;

	END IF; --}

    RAISE DEBUG 'snapTolerance: %', snapTolerance;


	-- Validate border geometry,
    border_geom := ST_GeomFromGeoJSON(border -> 'geometry');
	IF border_geom IS NULL THEN
		RAISE EXCEPTION 'Missing geometry in border';
	END IF;

	IF ST_GeometryType(border_geom) = 'ST_MultiLineString' THEN
		IF ST_NumGeometries(border_geom) > 1 THEN
			border_is_multiline := true;
		END IF;
	ELSIF ST_GeometryType(border_geom) = 'ST_LineString' THEN
		border_is_multiline := false;
	ELSE
		RAISE EXCEPTION 'Border must be either LineString or MultiLineString, is % instead', ST_GeometryType(border_geom);
	END IF;

	--RAISE DEBUG 'Border is multiline ? %', border_is_multiline;
	--RAISE DEBUG 'Border geom is %', ST_AsEWKT(border_geom);

	-- TODO: get colmap
	--colMap := ...

	SELECT id FROM topo_update.insert_feature(
		border_geom,
		border -> 'properties',

		borderLayerTable,
		borderLayerGeomColumn,
		borderLayerIdColumn,

		colMap,

		snapTolerance
	)
	INTO borderID;

	return borderID;

END;
$BODY$ LANGUAGE plpgsql; --}

CREATE OR REPLACE FUNCTION topo_update.app_do_AddBorder(

	-- Application name, will be used to fetch
	-- configuration from application schema.
	-- See doc/APP_CONFIG.md
	appname NAME,

	-- A GeoJSON Feature object representing
	-- a single Borders (as a LineString MultiLineString)
	-- to be used to create a new Border object
	--
	-- Properties of the feature are used to
	-- to contain values to be used for the Border record
	--
	-- Format of the properties object is as follows:
	--
	--   "properties": {
	--      <column_name>: <value>
	--      [, <column_name>: <value> ] ...
	--   }
	--
	--
	border JSONB

)
RETURNS TEXT AS
$BODY$ --{
DECLARE
	sql TEXT;
	appconfig JSONB;
BEGIN

	sql := format('SELECT cfg FROM %I.app_config',
			appname || '_sysdata_webclient_functions');
	EXECUTE sql INTO STRICT appconfig;

	--RAISE DEBUG 'APPCONFIG: %', appconfig;

	RETURN topo_update._app_do_AddBorder_Dynamic(
		appname,
		appconfig,
		border
	);
END;
$BODY$ LANGUAGE plpgsql; --}
