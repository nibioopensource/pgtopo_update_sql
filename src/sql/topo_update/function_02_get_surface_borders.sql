--
-- This function returns a record for each ring of
-- Borders bounding a Surface.
--
-- The first record is for the ring binding
-- the Surface shell, subsequent records are
-- for rings binding each of the Surface holes
--
-- Records are returned in spatial order of the
-- leftmost, bottomost coordinate of the ring
-- (from left/bottom to right-top)
--
-- Each record contains the following fields:
--   o ringBorders
--        identifiers of the borders forming the ring,
--        in the order they are encountered walking from
--        the leftmost/bottommost Border to the next in the
--        order required to always have the Surface area
--        on the left during the walk (CCW for shell, CW for holes).
--   o signedRingBorders
--        Border identifiers in the same order as the `ringBorders`
--        array but with a '-' suffix for those which are walked
--        backward.
--   o ringBorderGeoms
--        Border geometry in the same order as the `ringBorders` array.
--
-- {
CREATE OR REPLACE FUNCTION topo_update.get_surface_borders(
  -- Caller is responsible to obtain the topology record
  -- and this function does not check it for being the
  -- topology of the given surfaceTopoGeom and of the given
  -- borderLayerTable/borderLayerTopoGeomColumn, so improve
  -- speed. A more generic function could instead do fetch
  -- the topology record from those other parameters and
  -- perform the consistency check
	topo topology.topology,
	surfaceTopoGeom topology.TopoGeometry,
	borderLayerTable regclass,
	borderLayerTopoGeomColumn name,
	borderLayerIdColumn name
) -- }{
RETURNS TABLE(ringBorders TEXT[], signedRingBorders TEXT[], ringBorderGeoms GEOMETRY[])
AS $BODY$
DECLARE
	cte TEXT;
	sql TEXT;
	procName TEXT := 'topo_update.get_surface_borders';
	rec RECORD;
	debug BOOLEAN := true;
	borderLayerID INT;
	surfaceGeom GEOMETRY;

BEGIN

	-- NOTE: we run this query to allow passing a view as
	--       the border table
	-- TODO: take borderLayerID as parameter
	sql := format(
		$$
			SELECT layer_id(%1$I)
			FROM %2$s
			WHERE %1$I IS NOT NULL
			LIMIT 1
		$$,
		borderLayerTopoGeomColumn,
		borderLayerTable
	);
	EXECUTE sql INTO borderLayerID;
	IF borderLayerID IS NULL THEN
		RAISE EXCEPTION 'No borders found in layer %.%', borderLayerTable, borderLayerTopoGeomColumn;
	END IF;

	-- Store list of border identifiers covering
	-- each Surface's boundary edges into a temporay
	-- sbe_borders table
	--
	-- Parameter $1 is the Surface TopoGeometry
	--
	sql := format(
		$$
			CREATE TEMPORARY TABLE
			sbe_borders AS
			-- Surface faces selection
			WITH surface_faces AS (
				SELECT topogeo_id, layer_id, array_agg(element_id) faces FROM %1$I.relation sf
				WHERE topogeo_id = id($1)
				AND element_type = 3 -- face
				AND layer_id = layer_id($1)
				GROUP BY topogeo_id, layer_id
			)
			SELECT
				b.%4$I::text border_id,
				b.%3$I border_tg,
				br.topogeo_id,
				sbe.geom sbe_geom,
				sbe.edge_id sbe_id,
				sbe.left_face = ANY (sf.faces) sbe_have_surface_on_left
			FROM
				-- surface faces
				surface_faces sf
				-- surface boundary edges
				JOIN %1$I.edge_data sbe ON (
					-- Pick only Surface boundary Borders
					(
						(
							sbe.left_face = ANY (sf.faces)
							AND NOT sbe.right_face = ANY(sf.faces)
						)
						OR
						(
							sbe.right_face = ANY (sf.faces)
							AND NOT sbe.left_face = ANY(sf.faces)
						)
					)
				)
				-- border relation
				LEFT JOIN %1$I.relation br ON (
				  br.layer_id = %5$L
				  AND br.element_id IN ( sbe.edge_id, -sbe.edge_id )
				)
				-- border table
				LEFT JOIN %2$s b ON (
					id(b.%3$I) = br.topogeo_id
--					EXISTS (
--						SELECT * FROM %1$I.relation r
--						WHERE r.layer_id = layer_id(b.%3$I)
--						AND r.topogeo_id = id(b.%3$I)
--						AND r.element_type = 2
--						AND r.element_id = sbe.edge_id
--					)
				)
		$$,
		topo.name,                 -- %1
		borderLayerTable::text,    -- %2
		borderLayerTopoGeomColumn, -- %3
		borderLayerIdColumn,       -- %4
		borderLayerID              -- %5
	);
	RAISE DEBUG 'SQL: %', sql;
	EXECUTE sql USING surfaceTopoGeom;

	-- DEBUG
	IF debug THEN
		RAISE DEBUG 'Surface geom: %', ST_AsText(ST_CollectionHomogenize(surfaceTopoGeom));
		RAISE DEBUG 'Surface boundary edges:';
		FOR rec IN SELECT * FROM sbe_borders ORDER BY sbe_id
		LOOP
			RAISE DEBUG ' edge %', rec.sbe_id;
			RAISE DEBUG '   geom: %', ST_AsText(rec.sbe_geom);
			RAISE DEBUG '   covered by topogeom: %', rec.topogeo_id;
			RAISE DEBUG '   covered by border: %', rec.border_id;
		END LOOP;
	END IF;

	-- Sanity check 0: verify that all Surface boundary edges
	-- are covered by at least one Border
	FOR rec IN
		SELECT sbe_id, sbe_geom, count(border_id)
		 FROM pg_temp.sbe_borders
		 GROUP BY sbe_id, sbe_geom
		 HAVING count(border_id) = 0
		ORDER BY sbe_id
	LOOP
		RAISE EXCEPTION 'No Border in layer %.% covers Surface boundary edge % at %',
			borderLayerTable,
			borderLayerTopoGeomColumn,
			rec.sbe_id,
			ST_AsText(
				ST_LineInterpolatePoint(
					rec.sbe_geom, 0.5
				)
			);
	END LOOP;

	-- Sanity check 1: verify that no borders share edges
	-- TODO: run the check in the same step as sanity check 0 ?
	FOR rec IN
		SELECT
			b1.border_id id1,
			b2.border_id id2,
			b1.sbe_id,
			b1.sbe_geom
		FROM
			pg_temp.sbe_borders b1,
			pg_temp.sbe_borders b2
		WHERE
			b1.border_id != b2.border_id
		AND
			b1.sbe_id = b2.sbe_id
		ORDER BY
			sbe_id

	LOOP
		RAISE EXCEPTION 'Borders % and % in layer %.% share edge % at %',
			rec.id1,
			rec.id2,
			borderLayerTable,
			borderLayerTopoGeomColumn,
			rec.sbe_id,
			ST_AsText(
				ST_LineInterpolatePoint(
					rec.sbe_geom, 0.5
				)
			);
	END LOOP;

	-- For each border in sbe_borders extract
	-- its geometry, the set of edges it covers
	-- and the side on which the surface is found
	--
	-- Store the result in a temporary surface_borders
	-- table
	--
	sql := format(
		$$
			CREATE TEMPORARY TABLE surface_borders AS
			WITH
			border_surface_coverage AS (
				SELECT
					sbeb.border_id,
					sbeb.border_tg,
					array_agg(
						sbeb.sbe_id
						ORDER BY sbeb.sbe_id
					) border_surface_edge_ids,
					array_agg(
						sbeb.sbe_geom
						ORDER BY sbeb.sbe_id
					) border_surface_edge_geoms,
					array_agg(
						sbeb.sbe_have_surface_on_left
						ORDER BY sbeb.sbe_id
					) border_surface_edge_have_surface_on_left
				FROM
					sbe_borders sbeb
				GROUP BY
					sbeb.border_id,
					sbeb.border_tg
			),
			border_composition AS (
				SELECT
					bsc.border_id,
					array_agg(
						abs(br.element_id)
						ORDER BY abs(br.element_id)
					) border_edge_ids,
					ST_Collect(
						ed.geom
						ORDER BY abs(br.element_id)
					) border_edge_geoms
				FROM
					border_surface_coverage bsc,
					%1$I.relation br,
					%1$I.edge_data ed
				WHERE
					br.topogeo_id = id(bsc.border_tg)
					AND br.layer_id = layer_id(bsc.border_tg)
					AND br.element_type = 2 -- edge
					AND ed.edge_id IN ( br.element_id, -br.element_id )
				GROUP BY bsc.border_id
			),
			border_info AS (
				SELECT
					bsc.*,
					bc.border_edge_ids,
					ST_LineMerge(
						bc.border_edge_geoms
					) as border_geom,
					ST_LineSubstring(
						ST_RemoveRepeatedPoints(
							bsc.border_surface_edge_geoms[1],
							0
						),
						0.1,
						0.2
					) as first_edge_sub,
					bsc.border_surface_edge_have_surface_on_left[1] as first_edge_has_surface_on_left
				FROM border_surface_coverage bsc
				JOIN border_composition bc ON (bsc.border_id = bc.border_id)
			),
			border_info_with_geomtype_multi AS (
				SELECT
					*,
					GeometryType(border_geom) LIKE 'MULTI%%' as border_geom_is_multi
				FROM border_info
			),
			border_info_with_first_edgeend_location AS (
				SELECT
					*,
					CASE WHEN border_geom_is_multi THEN
						NULL
					ELSE
						ST_LineLocatePoint(
							border_geom,
							ST_StartPoint(first_edge_sub)
						)
					END as first_edge_point1_loc,
					CASE WHEN border_geom_is_multi THEN
						NULL
					ELSE
						ST_LineLocatePoint(
							border_geom,
							ST_EndPoint(first_edge_sub)
						)
					END as first_edge_point2_loc
				FROM border_info_with_geomtype_multi
			)
			SELECT
				*,
				ST_StartPoint(border_geom) as start_point,
				ST_EndPoint(border_geom) as end_point,
				CASE WHEN
					first_edge_point1_loc < first_edge_point2_loc
				THEN
					-- border has face on the same
					-- side of the first edge composing it
					first_edge_has_surface_on_left
				ELSE
					-- border has face on the opposite
					-- side of the first edge composing it
					NOT first_edge_has_surface_on_left
				END as surface_on_left
			FROM border_info_with_first_edgeend_location
		$$,
		topo.name -- %1
	);
	RAISE DEBUG 'SQL: %', sql;
	EXECUTE sql;

	-- DEBUG
	IF debug THEN
		RAISE DEBUG 'Surface boundary borders:';
		FOR rec IN
			SELECT * FROM pg_temp.surface_borders
			ORDER BY border_id
		LOOP
			RAISE DEBUG ' Border %', rec.border_id;
			RAISE DEBUG '   is multigeom ? %', rec.border_geom_is_multi;
			RAISE DEBUG '   covers surface edges: %', rec.border_surface_edge_ids;
			RAISE DEBUG '   composed by    edges: %', rec.border_edge_ids;
			RAISE DEBUG '   starts on point: %', ST_AsText(rec.start_point);
			RAISE DEBUG '   ends  on  point: %', ST_AsText(rec.end_point);
		END LOOP;
	END IF;

	-- Sanity check 2: verify that no borders covers more edges
	-- than the surface boundary ones
	sql := format(
		$$
			WITH extra_edges AS (
				SELECT
					border_id,
					border_surface_edge_ids,
					unnest(border_edge_ids) edge_id
				FROM pg_temp.surface_borders
					EXCEPT
				SELECT
					border_id,
					border_surface_edge_ids,
					unnest(border_surface_edge_ids) edge_id
				FROM pg_temp.surface_borders
			)
			SELECT ee.*, e.geom edge_geom
			FROM extra_edges ee, %1$I.edge_data e
			WHERE ee.edge_id = e.edge_id
		$$,
		topo.name
	);
	RAISE DEBUG 'SQL: %', sql;
	FOR rec IN EXECUTE sql
	LOOP
		RAISE EXCEPTION 'Border % in layer %.% covering surface boundary edges % also extends to edge % at %',
			rec.border_id,
			borderLayerTable,
			borderLayerTopoGeomColumn,
			array_to_string(rec.border_surface_edge_ids, ','),
			rec.edge_id,
			ST_AsText(
				ST_LineInterpolatePoint(
					rec.edge_geom, 0.5
				)
			)
		;
	END LOOP;

	-- Sanity check 3: verify that no borders are multi-line
	FOR rec IN
		SELECT * FROM pg_temp.surface_borders
		WHERE border_geom_is_multi
	LOOP
		RAISE EXCEPTION 'Border % in layer %.% is a multi-geometry: %',
			rec.border_id,
			borderLayerTable,
			borderLayerTopoGeomColumn,
			ST_AsText(
				rec.border_geom
			)
		;
	END LOOP;

	surfaceGeom := ST_BuildArea(ST_Collect(border_geom))
	FROM pg_temp.surface_borders;

	-- Create spatial indices?
	--CREATE INDEX ON pg_temp.surface_borders USING gist(start_point);
	--CREATE INDEX ON pg_temp.surface_borders USING gist(end_point);
	--CREATE INDEX ON pg_temp.surface_borders (ST_XMin(border_geom));
	--CREATE INDEX ON pg_temp.surface_borders (ST_YMin(border_geom));
	--CREATE INDEX ON pg_temp.surface_borders USING gist(border_geom);

	--ANALYZE pg_temp.surface_borders;

	-- Find all border rings
	FOR rec IN SELECT ST_Boundary(geom) surface_ring FROM ST_DumpRings(surfaceGeom)
	LOOP --{

		RAISE DEBUG 'Next ring has % points', ST_NPoints(rec.surface_ring);

		-- Find next ring
		WITH RECURSIVE
		ring_covering_borders AS (
			SELECT *
			FROM pg_temp.surface_borders
			WHERE ST_Covers(rec.surface_ring, border_geom)
		),
		leftmost AS (
			SELECT
				border_id,
				border_geom,
				start_point,
				end_point
			FROM ring_covering_borders
			ORDER BY
				ST_XMin(border_geom),
				ST_YMin(border_geom),
				border_id
			LIMIT 1
		),
		ring AS (

			-- non-recursive term
			SELECT
				sb.border_id,
				sb.surface_on_left,
				sb.start_point,
				sb.end_point,
				sb.border_geom
			FROM ring_covering_borders sb, leftmost
			WHERE sb.border_id = leftmost.border_id

			UNION

			-- recursive term
			SELECT
				sb.border_id,
				sb.surface_on_left,
				sb.start_point,
				sb.end_point,
				sb.border_geom
			FROM ring_covering_borders sb, ring
			WHERE
				CASE WHEN ring.surface_on_left THEN
					-- Current border has surface on the left,
					-- so we want to travel it forward, thus
					-- finding a connection on its end_point
					CASE WHEN sb.surface_on_left THEN
						-- surface border also has surface on left side
						ring.end_point = sb.start_point
					ELSE
						-- surface border has surface on right side
						ring.end_point = sb.end_point
					END
				ELSE
					-- Current border has surface on the right,
					-- so we want to travel it backward, thus
					-- finding a connection on its start_point
					CASE WHEN sb.surface_on_left THEN
						-- surface border has surface on left side
						ring.start_point = sb.start_point
					ELSE
						-- surface border also has surface on right side
						ring.start_point = sb.end_point
					END
				END
		)
		SELECT
			array_agg(ring.border_id),
			array_agg(
				CASE WHEN ring.surface_on_left THEN
					ring.border_id
				ELSE
					'-' || ring.border_id
				END
			),
			array_agg(
				CASE WHEN ring.surface_on_left THEN
					ring.border_geom
				ELSE
					ST_Reverse(ring.border_geom)
				END
			)
		FROM ring
		INTO
			ringBorders,
			signedRingBorders,
			ringBorderGeoms
		;

		-- Exit if no more borders are found
		IF ringBorders IS NULL
		THEN
			RAISE DEBUG '%: no more Surface rings to process', procName;
			EXIT;
		END IF;

		RAISE DEBUG '%: next Surface ring is %', procName, signedRingBorders;

		-- Remove processed ring Borders from the temporary table
		DELETE FROM pg_temp.surface_borders
		WHERE border_id = ANY(ringBorders);

		RETURN NEXT;

	END LOOP; --}

	DROP TABLE pg_temp.surface_borders;
	DROP TABLE pg_temp.sbe_borders;
END;
$BODY$ LANGUAGE 'plpgsql';
-- }
