--
-- Take a JSON object (inp) and a JSON mapping file (map),
-- return a new JSON object with values taken from the input
-- JSON and structure specified by the mapping.
--
-- The mapping file is an array of JSON objects.
-- Each object in the array has a "from" and a "to" key.
-- The values of these keys are both arrays specifying
-- a path to the source and the target value respectively.
--
-- An exception is raised if the given mapping is not
-- an array, or if any object in the array does not
-- have both a "from" and a "to" key, or if values of
-- those keys are not arrays.
--
-- An exception is raised if the referenced "from" path
-- does not exist in the input JSON.
--
-- An exception is raised if the referenced "to" path
-- is a subset of another "to" path (ie: a target must
-- always be a final value, not a full objet).
-- TODO: drop the above limitation ?
--
--
CREATE OR REPLACE FUNCTION topo_update.json_to_json_map(
	inp JSONB,
	map JSONB
)
RETURNS JSONB
AS $BODY$ --{
DECLARE
	ret JSONB;
	val JSONB;
	json_path TEXT[];
	rec RECORD;
	parent_path TEXT[];
	obj JSONB;
	i INT;
BEGIN

	ret := '{}'; -- start with empty object

	IF NOT jsonb_typeof( map ) = 'array' THEN
		RAISE EXCEPTION 'Invalid mapping: map should be an array';
	END IF;

	FOR rec IN SELECT jsonb_array_elements( map ) m
	LOOP

		-- Extract json value from source
		obj := rec.m -> 'from';
		IF obj IS NULL THEN
			RAISE EXCEPTION 'Invalid mapping: "from" key missing';
		END IF;
		IF NOT jsonb_typeof(obj) = 'array' THEN
			RAISE EXCEPTION 'Invalid mapping: "from" value should be array';
		END IF;
		json_path := array_agg(x) from jsonb_array_elements_text( obj ) x;
		RAISE DEBUG 'from json_path: %', json_path;
		val := inp #> json_path;
		IF val IS NULL THEN
			RAISE EXCEPTION 'Unapplicable mapping: key % does not exist in input', json_path;
		END IF;
		RAISE DEBUG 'value extracted from input json path: %', val;

		obj := rec.m -> 'to';
		IF obj IS NULL THEN
			RAISE EXCEPTION 'Invalid mapping: "to" key missing';
		END IF;
		IF NOT jsonb_typeof(obj) = 'array' THEN
			RAISE EXCEPTION 'Invalid mapping: "to" value should be array';
		END IF;
		json_path := array_agg(x) from jsonb_array_elements_text( obj ) x;
		RAISE DEBUG 'to json_path: %', json_path;

		-- Ensure each element in the "to" array, other
		-- than the first, has an object in the returned
		-- JSONB for containing it
		FOR i IN 2..array_upper(json_path, 1)
		LOOP
			parent_path = json_path[1:i-1];
			obj = ret #> parent_path;
			IF obj IS NULL
			THEN
				ret := jsonb_set(ret, parent_path, '{}');
			ELSIF NOT jsonb_typeof(obj) = 'object'
			THEN
				RAISE EXCEPTION 'Invalid mapping: path in "to" exists as both object and non-object';
			END IF;
		END LOOP;

		-- Assign the value to the target
		ret := jsonb_set(ret, json_path, val);

	END LOOP;

	RETURN ret;
END;
$BODY$ --}
LANGUAGE 'plpgsql';


