CREATE OR REPLACE FUNCTION topo_update._app_do_RemovePoint_Dynamic(
	appname NAME,
	appconfig JSONB,
	point_id TEXT
)
RETURNS VOID AS
$BODY$ --{
DECLARE
	cfg_pointLayer JSONB;
	cfg_pointLayerTable JSONB;
	cfg_op JSONB;
	cfg_op_params JSONB;
	cfg JSONB;
	pointLayerTable REGCLASS;
	pointLayerGeomColumn NAME;
	pointLayerIdColumn NAME;
	rec RECORD;
	sql TEXT;
	removedPoint RECORD;
	removePrimitives BOOLEAN := true;
BEGIN

	-- Check if the operation is enabled
	IF NOT appconfig -> 'operations' ? 'RemovePoint' THEN
		RAISE EXCEPTION 'RemovePoint operation not enabled for application %', appname;
	END IF;

	cfg_pointLayer := appconfig -> 'point_layer';
	cfg_op := appconfig -> 'operations' -> 'RemovePoint';

	--RAISE DEBUG 'pointLayer: %', cfg_pointLayer;

	FOR rec IN SELECT jsonb_array_elements( appconfig -> 'tables' ) cfg
	LOOP --{
		IF rec.cfg ->> 'name' = cfg_pointlayer ->> 'table_name' THEN
			cfg_pointLayerTable := rec.cfg;
		END IF;
		IF cfg_pointLayerTable IS NOT NULL
		THEN
			EXIT;
		END IF;
	END LOOP; --}

	--RAISE DEBUG 'pointLayerTable: %', cfg_pointLayerTable;

	pointLayerTable := ( appname || '.' || ( cfg_pointlayer ->> 'table_name' ) );
	pointLayerGeomColumn := cfg_pointlayer ->> 'geo_column';
	pointLayerIdColumn := cfg_pointlayerTable ->> 'primary_key';

	-- TODO: cast the parameter into the type of the primary key, not
	--       vice-versa
	sql := format(
		$$
			WITH deleted AS (
				DELETE FROM %1$s
				WHERE %2$I::text = $1
				RETURNING %3$I as tg
			) SELECT topology.clearTopoGeom(tg) FROM deleted
		$$,
		pointLayerTable,      -- %1
		pointLayerIdColumn,   -- %2
		pointLayerGeomColumn  -- %3
	);

	EXECUTE sql USING point_id INTO removedPoint;

	IF removedPoint IS NULL THEN
		RAISE EXCEPTION 'No Point with id % in table %', point_id, pointLayerTable;
	END IF;

	IF removePrimitives THEN
		-- TODO: pass the bounding box of the removed point ?
		PERFORM topo_update.cleanup_topology(
			( SELECT t FROM topology.topology t
			  WHERE name = appname || '_sysdata_webclient' )
		);
	END IF;


END;
$BODY$ LANGUAGE plpgsql VOLATILE; --}

CREATE OR REPLACE FUNCTION topo_update.app_do_RemovePoint(

	-- Application name, will be used to fetch
	-- configuration from application schema.
	-- See doc/APP_CONFIG.md
	appname NAME,

	-- The identifier of a point to remove
	point_id TEXT

)
RETURNS VOID AS
$BODY$ --{
DECLARE
	sql TEXT;
	appconfig JSONB;
BEGIN

	sql := format('SELECT cfg FROM %I.app_config',
			appname || '_sysdata_webclient_functions');
	EXECUTE sql INTO STRICT appconfig;

	--RAISE WARNING 'APPCONFIG: %', appconfig;

	PERFORM topo_update._app_do_RemovePoint_Dynamic(
		appname,
		appconfig,
		point_id
	);
END;
$BODY$ LANGUAGE plpgsql VOLATILE; --}
