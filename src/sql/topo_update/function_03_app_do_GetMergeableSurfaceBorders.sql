CREATE OR REPLACE FUNCTION topo_update._GetMergeableSurfaceBorders_Query(
	appname NAME, -- topology schema
	cfg_op JSONB,
	borderLayerIdColumn NAME,
	borderLayerTableName NAME,
	borderLayerGeomColumn NAME,
	surfaceLayerTableName NAME,
	surfaceLayerGeomColumn NAME,
	surfaceLayerIdColumn NAME,
	bbox GEOMETRY
)
RETURNS TEXT AS
$BODY$ --{
DECLARE
	filter TEXT := '';
	sql TEXT;
	rec RECORD;

BEGIN

	sql := format(
		$$
SELECT
	ST_Union(distinct b.%5$I) border_visible_geom,
	array_agg(distinct b.%1$I::text) border_ids,
	ARRAY[
		least(sl.%8$I::text, sr.%8$I::text),
		greatest(sl.%8$I::text, sr.%8$I::text)
	] side_faces
FROM
	%2$I.edge e,
	%2$I.face fl,
	%2$I.face fr,
	%2$I.relation ebr,
	topology.layer blayer,
	%3$I.%4$I b,

	%2$I.relation flsr,
	%2$I.relation frsr,
	topology.layer slayer,
	%3$I.%6$I sl,
	%3$I.%6$I sr
WHERE

	-- edges selection
	ST_Intersects(e.geom, $1)

	-- border layer selection
	AND blayer.schema_name = %3$L
	AND blayer.table_name = %4$L
	AND blayer.feature_column = %5$L

	-- border layer relation join
	AND ebr.layer_id = blayer.layer_id
	AND ebr.element_id = e.edge_id
	AND ebr.topogeo_id = id(b.%5$I)

	-- left face selection
	AND e.left_face = fl.face_id

	-- right face selection
	AND e.right_face = fr.face_id

	-- surface layer selection
	AND slayer.schema_name = %3$L
	AND slayer.table_name = %6$L
	AND slayer.feature_column = %7$L

	-- left surface / surface layer relation join
	AND flsr.layer_id = slayer.layer_id
	AND flsr.element_id = e.left_face
	AND flsr.topogeo_id = id(sl.%7$I)

	-- right surface / surface layer relation join
	AND frsr.layer_id = slayer.layer_id
	AND frsr.element_id = e.right_face
	AND frsr.topogeo_id = id(sr.%7$I)

		$$,
		borderLayerIdColumn, ----------------------- %1: border id column
		format('%s_sysdata_webclient', appname), --- %2: topology schema
		appname, ----------------------------------- %3: appname
		borderLayerTableName, ---------------------- %4: border table name
		borderLayerGeomColumn, --------------------- %5: border geom column
		surfaceLayerTableName, --------------------- %6: surface table name
		surfaceLayerGeomColumn, -------------------- %7: surface geom name
		surfaceLayerIdColumn  ---------------------- %8: surface id column
	);

	--RAISE DEBUG 'Op config: %', cfg_op;

	-- Add sharedSurfaceAttributes condition
	FOR rec IN SELECT
		jsonb_array_elements_text(cfg_op -> 'sharedSurfaceAttributes') a
	LOOP
		--RAISE DEBUG 'Adding condition about same value for attribute %', rec.a;
		-- NOTE: we use IS DISTINCT FROM so that nulls are considered equal
		sql := format(E'%1$s\nAND NOT sl.%2$I IS DISTINCT FROM sr.%2$I', sql, rec.a);
	END LOOP;

	-- Add max area condition
	IF cfg_op ? 'maxSurfaceArea' THEN
		sql := format(
			E'%1$s\nAND ( ST_Area(sl.%2$I::%4$s) <= %3$s OR ST_Area(sr.%2$I::%4$s) <= %3$s )',
			sql,
			surfaceLayerGeomColumn,
			cfg_op -> 'maxSurfaceArea' ->> 'units',
			CASE WHEN
				(cfg_op -> 'maxSurfaceArea' -> 'unitsAreMeters')::bool
			THEN
				'geometry::geography'
			ELSE
				'geometry'
			END
		);
	END IF;

	-- Add arbitrary sql filter
	IF cfg_op ? 'surfaceSQLFilter' THEN
		filter = cfg_op ->> 'surfaceSQLFilter';
		-- minimal sanification
		IF position(';' in filter) > 0 THEN
			RAISE EXCEPTION 'Semicolon character is forbidden in surfaceSQLFilter';
		END IF;
		-- TODO: check quotes are balanced ?
		-- TODO: check parens are balanced ?
		filter = REPLACE (filter, ':S1', 'sl');
		filter = REPLACE (filter, ':S2', 'sr');

		sql := format(E'%1$s\nAND ( %2$s )', sql, filter);

	END IF;


	sql := format(E'%1$s\nGROUP BY 3', sql);

	-- RAISE WARNING 'SQL: %', sql;

	RETURN sql;
END;
$BODY$ LANGUAGE plpgsql; --}


-- Changed return type before release
-- TODO: drop (also from extension) if needed
--DROP FUNCTION IF EXISTS topo_update._app_do_GetMergeableSurfaceBorders_Dynamic(
--	appname NAME,
--	appconfig JSONB,
--	bbox GEOMETRY
--);

CREATE OR REPLACE FUNCTION topo_update._app_do_GetMergeableSurfaceBorders_Dynamic(
	appname NAME,
	appconfig JSONB,
	bbox GEOMETRY
)
RETURNS TABLE ( border_ids text[], side_surfaces text[] ) AS
$BODY$ --{
DECLARE
	cfg_surfaceLayer JSONB;
	cfg_surfaceLayerTable JSONB;
	cfg_borderLayer JSONB;
	cfg_borderLayerTable JSONB;
	cfg_op JSONB;
	cfg JSONB;
	surfaceLayerTableName NAME;
	surfaceLayerTableRegclass REGCLASS;
	surfaceLayerGeomColumn NAME;
	surfaceLayerIdColumn NAME;
	borderLayerTableName NAME;
	borderLayerTableRegclass REGCLASS;
	borderLayerGeomColumn NAME;
	borderLayerIdColumn NAME;
	rec RECORD;
	sql TEXT;
BEGIN

	-- Check if the operation is enabled
	IF NOT appconfig -> 'operations' ? 'SurfaceMerge' THEN
		RAISE EXCEPTION 'SurfaceMerge operation not enabled for application %', appname;
	END IF;

	cfg_surfaceLayer := appconfig -> 'surface_layer';
	IF cfg_surfaceLayer IS NULL THEN
		RAISE EXCEPTION 'GetMergeableSurfaceBorders operations requires missing "surface_layer" in appconfig';
	END IF;
	cfg_borderLayer := appconfig -> 'border_layer';
	IF cfg_borderLayer IS NULL THEN
		RAISE EXCEPTION 'GetMergeableSurfaceBorders operations requires missing "border_layer" in appconfig';
	END IF;
	cfg_op := appconfig -> 'operations' -> 'SurfaceMerge';

	--RAISE DEBUG 'cfg_surfacelayer: %', cfg_surfaceLayer;
	--RAISE DEBUG 'cfg_borderLayer: %', cfg_borderLayer;

	FOR rec IN SELECT jsonb_array_elements( appconfig -> 'tables' ) cfg
	LOOP --{
		IF rec.cfg ->> 'name' = cfg_surfacelayer ->> 'table_name' THEN
			cfg_surfaceLayerTable := rec.cfg;
		ELSIF rec.cfg ->> 'name' = cfg_borderlayer ->> 'table_name' THEN
			cfg_borderLayerTable := rec.cfg;
		END IF;
		IF cfg_borderLayerTable IS NOT NULL AND
		   cfg_surfaceLayerTable IS NOT NULL
		THEN
			EXIT;
		END IF;
	END LOOP; --}

	--RAISE DEBUG 'cfg_surfaceLayerTable: %', cfg_surfaceLayerTable;
	--RAISE DEBUG 'cfg_borderLayerTable: %', cfg_borderLayerTable;

	surfaceLayerTableName := cfg_surfaceLayer ->> 'table_name';
	surfaceLayerTableRegclass := appname || '.' || surfaceLayerTableName;
	surfaceLayerGeomColumn := cfg_surfacelayer ->> 'geo_column';
	surfaceLayerIdColumn := cfg_surfacelayerTable ->> 'primary_key';

	borderLayerTableName := cfg_borderlayer ->> 'table_name';
	borderLayerTableRegclass := appname || '.' || borderLayerTableName;
	borderLayerGeomColumn := cfg_borderlayer ->> 'geo_column';
	borderLayerIdColumn := cfg_borderlayerTable ->> 'primary_key';


	sql := topo_update._GetMergeableSurfaceBorders_Query(
		appname,
		cfg_op,
		borderLayerIdColumn,
		borderLayerTableName,
		borderLayerGeomColumn,
		surfaceLayerTableName,
		surfaceLayerGeomColumn,
		surfaceLayerIdColumn,
		bbox
	);

	sql := format('SELECT border_ids, side_faces FROM ( %1$s ) foo', sql);

	-- RAISE WARNING 'SQL: %', sql;

	RETURN QUERY EXECUTE sql USING bbox;

END;
$BODY$ LANGUAGE plpgsql; --}

-- Changed return type before release
-- TODO: drop (also from extension) if needed
--DROP FUNCTION IF EXISTS topo_update._app_do_GetMergeableSurfaceBorders(
--	appname NAME,
--	bbox GEOMETRY
--);

CREATE OR REPLACE FUNCTION topo_update.app_do_GetMergeableSurfaceBorders(

	-- Application name, will be used to fetch
	-- configuration from application schema.
	-- See doc/APP_CONFIG.md
	appname NAME,

	-- A Geometry object whose bounding box will be used
	-- to restrict the Borders that will be returned.
	-- Only intesecting Borders will be considered.
	bbox GEOMETRY
)
RETURNS TABLE ( border_ids text[], side_surfaces text[] ) AS
$BODY$ --{
DECLARE
	sql TEXT;
	appconfig JSONB;
BEGIN

	sql := format('SELECT cfg FROM %I.app_config',
			appname || '_sysdata_webclient_functions');
	EXECUTE sql INTO STRICT appconfig;

	--RAISE WARNING 'APPCONFIG: %', appconfig;

	RETURN QUERY
	SELECT * FROM topo_update._app_do_GetMergeableSurfaceBorders_Dynamic(
		appname,
		appconfig,
		bbox
	);
END;
$BODY$ LANGUAGE plpgsql; --}
