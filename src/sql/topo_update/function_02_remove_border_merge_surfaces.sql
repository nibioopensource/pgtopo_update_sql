-- Replaced before release by the version taking an optional removeEdges parameter
DROP FUNCTION IF EXISTS topo_update.remove_border_merge_surfaces(
	borderLayerTable REGCLASS,
	borderLayerGeomColumn NAME,
	borderLayerIdColumn NAME,
	borderId TEXT,
	surfaceLayerTable REGCLASS,
	surfaceLayerGeomColumn NAME,
	surfaceLayerIdColumn NAME,
	mergePolicyProviderFunc REGPROC,
	mergePolicyProviderParam ANYELEMENT
);
DROP FUNCTION IF EXISTS topo_update.remove_border_merge_surfaces(
	borderLayerTable REGCLASS,
	borderLayerGeomColumn NAME,
	borderLayerIdColumn NAME,
	borderId TEXT,
	surfaceLayerTable REGCLASS,
	surfaceLayerGeomColumn NAME,
	surfaceLayerIdColumn NAME,
	mergePolicyProviderFunc REGPROC,
	mergePolicyProviderParam ANYELEMENT,
	removeEdges BOOL
);

--{
--
-- Remove a border eventually merging surfaces
-- bound by it.
--
-- If two surfaces are bound by the border a callback function
-- is called to determine the attributes of the merged surface.
--
-- Underlying topology is left untouched
--
-- # RETURN VALUE
--
-- The function returns a table containing information about
-- the removed borders and merged surfaces; the
-- returned table has fields:
--
--	- fid text
--
--		Feature identifier (text representation of the value of
--		the specified layer's IdColumn)
--
--	- typ char
--
--		Character representing the type of Feature the record
--		is about, with these possible values:
--
--			S	The Feature is a Surfaces
--
--			B	The Feature is a Border
--
--	- act char
--
--		Character representing the action taken on the Feature,
--		with the following possible values:
--
--			D	The Feature was deleted. This can happen for
--              the given Border, the Surfaces bound by the
--              given Border and any Border that would become
--              dangling due to removal of given Border
--
--			C	The Feature was created. This can only happen
--				for a Surface replacing Surfaces previously
--				bound by the given Border.
--
--  - frm text[]
--
--		Identifiesr of the Feature previously covering the space
--		now covered by the created or modified Feature.
--      This is NULL for deleted features (act=D).
--
-- }{
CREATE OR REPLACE FUNCTION topo_update.remove_border_merge_surfaces(

	-- Border layer table
	borderLayerTable REGCLASS,

	-- Name of Border layer TopoGeometry column
	borderLayerGeomColumn NAME,

	-- Border layer primary key column name
	borderLayerIdColumn NAME,

	-- Border identifier
	borderId TEXT,

	-- Surface layer table
	surfaceLayerTable REGCLASS,

	-- Surface layer TopoGeometry column name
	surfaceLayerGeomColumn NAME,

	-- Surface layer primary key column name
	surfaceLayerIdColumn NAME,

	-- Callback determining which attributes to use
	-- for the Surface replacing the two merged Surfaces.
	--
	-- The function is expected to take the following named
	-- parameters:
	--
	--   surfaceLeftId TEXT
	--		Identifier of the Surface on the left of the Border
	--
	--   surfaceLeftTopoGeom topology.TopoGeometry
	--		TopoGeometry of the Surface on the left of the Border
	--
	--   surfaceLeftProps JSONB
	--		Properties of the Surface on the left of the Border
	--
	--   surfaceRightId TEXT
	--		Identifier of the Surface on the right of the Border
	--
	--   surfaceRightTopoGeom topology.TopoGeometry
	--		TopoGeometry of the Surface on the right of the Border
	--
	--   surfaceRightProps JSONB
	--		Properties of the Surface on the right of the Border
	--
	--   usr ANYELEMENT
	--		User-defined parameter (value of mergePoicyProviderParam)
	--
	-- The function return value is expected to be a JSONB value
	-- containing values for columns of the Surface record that will
	-- replace the left/right Surfaces. Values for columns which are
	-- not found in the returned JSONB object will take the default
	-- value specified for the column.
	--
	mergePolicyProviderFunc REGPROC,

	-- User defined parameter to pass to the
	-- surfaceMergeFunc
	mergePolicyProviderParam ANYELEMENT,

	-- Boolean indicating whether the topology primitives (edges,
	-- nods) used by the removed Borders should also be removed
	removePrimitives BOOL DEFAULT FALSE,

	-- Boolean indicating whether any dangling Border in the Surface
	-- taking the space previously occupied by the given border
	-- should also be removed.
	removeDanglingBorders BOOL DEFAULT FALSE
)
RETURNS TABLE(fid text, typ char, act char, frm text[])
AS $BODY$ --}{
DECLARE
	topo topology.topology;
	borderLayer topology.layer;
	surfaceLayer topology.layer;
	sql TEXT;
	rec RECORD;
	rec2 RECORD;
	borderTopoGeom topology.TopoGeometry;
	surfaceLeftId int;
	surfaceRightId int;
	surfaceLeftTopoGeom topology.TopoGeometry;
	surfaceRightTopoGeom topology.TopoGeometry;
	surfaceLeftKey TEXT;
	surfaceRightKey TEXT;
	surfaceLeftProps JSONB;
	surfaceRightProps JSONB;

	surfaceLayerColMap JSONB;
	mergedSurfaceProps JSONB;
	mergedSurfaceTopoGeom topology.TopoGeometry;
	tmpInt int;
	borderEdges int[];
	borderEdgeEndNodes int[];
	linkedEdges int[];

	procName TEXT := 'topo_update.remove_border_merge_surfaces';

BEGIN
	-- Fetch topology and layer info from border layer references
	SELECT t topo, l layer
		FROM topology.layer l, topology.topology t, pg_class c, pg_namespace n
		WHERE l.schema_name = n.nspname
		AND l.table_name = c.relname
		AND c.oid = borderLayerTable
		AND c.relnamespace = n.oid
		AND l.feature_column = borderLayerGeomColumn
		AND l.topology_id = t.id
		INTO rec;
	topo := rec.topo;
	borderLayer := rec.layer;

	-- Fetch topology and layer info from surface layer references
	SELECT t topo, l layer
		FROM topology.layer l, topology.topology t, pg_class c, pg_namespace n
		WHERE l.schema_name = n.nspname
		AND l.table_name = c.relname
		AND c.oid = surfaceLayerTable
		AND c.relnamespace = n.oid
		AND l.feature_column = surfaceLayerGeomColumn
		AND l.topology_id = t.id
		INTO rec;

	IF id(topo) != id(rec.topo) THEN
		RAISE EXCEPTION 'Surface and Border layers need to be in same topology';
	END IF;
	surfaceLayer := rec.layer;

	-- Fetch border
	sql := format(
		'SELECT %1$I tg FROM %2$s WHERE %3$I = %4$L',
		borderLayerGeomColumn,
		borderLayerTable,
		borderLayerIdColumn,
		borderId
	);
	RAISE DEBUG 'SQL: %', sql;
	EXECUTE sql
	INTO rec;

	IF rec IS NULL THEN
		RAISE EXCEPTION 'No Border with id % in table %', borderId, borderLayerTable;
	END IF;

	borderTopoGeom := rec.tg;

	RAISE DEBUG 'Border % retrieved: %', borderId, borderTopoGeom;

	-- For each edge composing the border, find the surfaces covering
	-- the face on each side, and keep note of them.
	sql := format($$
		SELECT
			e.edge_id,
			e.start_node,
			e.end_node,
			r.element_id,
			e.left_face,
			e.right_face
		FROM %1$I.edge e, %1$I.relation r
		WHERE e.edge_id in (r.element_id, -r.element_id)
		AND r.layer_id = %2$L
		AND r.topogeo_id = $1
		AND r.element_type = 2
	$$, topo.name, borderLayer.layer_id);
	RAISE DEBUG 'SQL: %', sql;
	FOR rec IN EXECUTE sql USING borderTopoGeom.id
	LOOP --{
		RAISE DEBUG 'Border % includes edge % having face % on the left and % on the right', borderId, rec.element_id, rec.left_face, rec.right_face;

		borderEdges := array_append(borderEdges, rec.edge_id);
		borderEdgeEndNodes := borderEdgeEndNodes || ARRAY[rec.start_node, rec.end_node];

		-- Swap left/right if edge_id was negated
		IF rec.element_id < 0 THEN
			tmpInt := rec.left_face;
			rec.left_face := rec.right_face;
			rec.right_face := tmpInt;
		END IF;
		RAISE DEBUG 'Border % has face % on the left and % on the right', borderId, rec.left_face, rec.right_face;


		sql := format($$
			SELECT r.topogeo_id, r.element_id
			FROM %1$I.relation r
			WHERE r.layer_id = %2$L
			AND r.element_type = 3
			AND r.element_id IN ( %3$L, %4$L )
		$$, topo.name, surfaceLayer.layer_id,
			rec.left_face, rec.right_face );
		RAISE DEBUG 'SQL: %', sql;
		FOR rec2 IN EXECUTE sql LOOP --{

			IF rec.left_face = rec2.element_id
			THEN --{
				-- Found one Surface on the left of the edge
				RAISE DEBUG 'Border % at edge % has Surface % on the left', borderId, rec.edge_id, rec2.topogeo_id;
				IF surfaceLeftId IS NULL THEN
					surfaceLeftId := rec2.topogeo_id;
				ELSIF surfaceLeftId != rec2.topogeo_id THEN
					RAISE EXCEPTION
						'Border % at edge % has Surface % on the left but '
						'was previously found to have Surface % on the left',
						borderId, rec.edge_id, rec2.topogeo_id, surfaceLeftId;
				END IF;
			END IF; --}
			IF rec.right_face = rec2.element_id
			THEN --{
				-- Found one Surface on the right of the edge
				RAISE DEBUG 'Border % at edge % has Surface % on the right', borderId, rec.edge_id, rec2.topogeo_id;
				IF surfaceRightId IS NULL THEN
					surfaceRightId := rec2.topogeo_id;
				ELSIF surfaceRightId != rec2.topogeo_id THEN
					RAISE EXCEPTION
						'Border % at edge % has Surface % on the right but '
						'was previously found to have Surface % on the right',
						borderId, rec.edge_id, rec2.topogeo_id, surfaceRightId;
				END IF;
			END IF; --}

		END LOOP; --} Loop over Surfaces composed by given (left/right) faces

	END LOOP; --} Loop over edges composing given Border

	RAISE NOTICE 'Border % has Surfaces % on the left and % on the right', borderId, surfaceLeftId, surfaceRightId;

	IF ( surfaceLeftId IS NULL ) != ( surfaceRightId IS NULL )
	THEN
		RAISE EXCEPTION
			'Only one Surface found defined by faces on either sides'
			' of edge %, included in definition of Border %'
			' (must be a peripherical Border which we cannot remove)',
			rec.edge_id, borderId;
	END IF;

	IF surfaceLeftId != surfaceRightId
	THEN --{

		-- find the surface identifiers and pass them to the policy callback
		sql := format(
			$$
				SELECT
					id(%2$I) i,
					r.%1$I k,
					r.%2$I g,
					to_jsonb(r) - %1$L - %2$L p
				FROM %3$s r
				WHERE id(%2$I) IN ( %4$L, %5$L )
			$$,
			surfaceLayerIdColumn, -- %1
			surfaceLayerGeomColumn, -- %2
			surfaceLayerTable, -- %3
			surfaceLeftId, -- %4
			surfaceRightId -- %5
		);
		RAISE DEBUG 'SQL: %', sql;
		FOR rec IN EXECUTE sql
		LOOP
			IF rec.i = surfaceLeftId THEN
				surfaceLeftTopoGeom := rec.g;
				surfaceLeftKey := rec.k;
				surfaceLeftProps := rec.p;
			ELSE
				surfaceRightTopoGeom := rec.g;
				surfaceRightKey := rec.k;
				surfaceRightProps := rec.p;
			END IF;
		END LOOP;

		RAISE DEBUG 'Border % has Surfaces % on the left and % on the right',
			borderId,
			surfaceLeftKey,
			surfaceRightKey;

		RAISE DEBUG ' Properties of Left Surface: %', surfaceLeftProps;
		RAISE DEBUG ' Properties of Right Surface: %', surfaceRightProps;

		-- Now pass surfaces and properties to the callback to return
		-- the properties of the new surface
		sql := format(
			$$
				SELECT * FROM %1$s(
					surfaceLeftId => %2$L,
					surfaceLeftTopoGeom => %3$L,
					surfaceLeftProps => %4$L,
					surfaceRightId => %5$L,
					surfaceRightTopoGeom => %6$L,
					surfaceRightProps => %7$L,
					usr => $1
				)
			$$,
			mergePolicyProviderFunc, -- %1
			surfaceLeftKey,          -- %2
			surfaceLeftTopoGeom,     -- %3
			surfaceLeftProps,        -- %4
			surfaceRightKey,         -- %5
			surfaceRightTopoGeom,    -- %6
			surfaceRightProps        -- %7
		);
		RAISE DEBUG 'SQL: %', sql;
		EXECUTE sql
			USING mergePolicyProviderParam
			INTO mergedSurfaceProps;
		IF mergedSurfaceProps IS NULL
		THEN
			RAISE DEBUG '%: mergePolicyProviderFunc returned NULL, not merging our surfaces', procName;
			RETURN;
		END IF;

		RAISE DEBUG 'mergePolicyProviderFunc returned %', mergedSurfaceProps;

		-- Create a merged Surface TopoGeometry
		mergedSurfaceTopoGeom := topology.CreateTopoGeom(
			topo.name,
			3, -- area
			surfaceLayer.layer_id
		);
		mergedSurfaceTopoGeom := topology.TopoGeom_addTopoGeom(
			mergedSurfaceTopoGeom,
			surfaceLeftTopoGeom
		);
		mergedSurfaceTopoGeom := topology.TopoGeom_addTopoGeom(
			mergedSurfaceTopoGeom,
			surfaceRightTopoGeom
		);

		RAISE DEBUG 'surfaceLeft TopoGeom: % -- area %', surfaceLeftTopoGeom, ST_Area(surfaceLeftTopoGeom);
		RAISE DEBUG 'surfaceRight TopoGeom: % -- area %', surfaceRightTopoGeom, ST_Area(surfaceRightTopoGeom);
		RAISE DEBUG 'MergedSurface TopoGeom: % -- area %', mergedSurfaceTopoGeom, ST_Area(mergedSurfaceTopoGeom);

		-- TODO: fetch from mergePolicy function ?
		surfaceLayerColMap := topo_update.json_col_map_from_pg(
			surfaceLayerTable,
			ARRAY[
				surfaceLayerGeomColumn::text
			]
		);
		RAISE DEBUG 'surfaceLayerColMap: %', surfaceLayerColMap;

		-- Insert the new TopoGeometry
		fid := topo_update.insert_feature(
			mergedSurfaceTopoGeom,
			mergedSurfaceProps,
			surfaceLayerTable,
			surfaceLayerGeomColumn,
			surfaceLayerIdColumn,
			surfaceLayerColMap
		);
		RAISE DEBUG 'Merge Surface % inserted', fid;
		typ := 'S';
		act := 'C';
		frm := ARRAY[surfaceLeftKey, surfaceRightKey];


		RETURN NEXT;

		-- Delete the two surface records
		sql := format(
			$$
				DELETE FROM %1$s WHERE %2$I IN (%3$L, %4$L)
				RETURNING %2$I as id
			$$,
			surfaceLayerTable,
			surfaceLayerIdColumn,
			surfaceLeftKey,
			surfaceRightKey
		);
		RAISE DEBUG 'SQL: %', sql;
		FOR REC IN EXECUTE sql
		LOOP
			fid := rec.id;
			typ := 'S';
			act := 'D';
			frm := NULL;
			RETURN NEXT;
		END LOOP;

		-- Clear the two Surface object TopoGeometries
		PERFORM topology.clearTopoGeom(surfaceLeftTopoGeom);
		PERFORM topology.clearTopoGeom(surfaceRightTopoGeom);

		RAISE DEBUG 'Old Surface objects % and % deleted and cleared',
			surfaceLeftKey, surfaceRightKey;

	END IF; --}

	-- Delete the border record
	sql := format(
		$$
			DELETE FROM %1$s WHERE %2$I = %3$L
		$$,
		borderLayerTable,
		borderLayerIdColumn,
		borderId
	);
	RAISE DEBUG 'SQL: %', sql;
	EXECUTE sql;
	fid := borderId;
	typ := 'B';
	act := 'D';
	frm := NULL;
	RETURN NEXT;

	-- Clear the Border object TopoGeometry
	PERFORM topology.clearTopoGeom(borderTopoGeom);

	RAISE DEBUG 'Border object % deleted and cleared', borderId;

	-- Remove primitives, if requested
	IF removePrimitives THEN

		-- Remove edges used by the removed borderTopoGeom
		RAISE DEBUG 'Removing border edges: %', borderEdges;
		FOR rec IN SELECT unnest(borderEdges) edge_id
		LOOP
			PERFORM topology.ST_RemEdgeModFace(topo.name, rec.edge_id);
		END LOOP;

		-- Remove removed edges' nodes which left isolated
		sql := format(
			$$
				SELECT n.node_id FROM %1$I.node n
				WHERE n.containing_face IS NOT NULL
				AND n.node_id = ANY ($1) -- $1 == borderEdgeEndNodes
			$$,
			topo.name
		);
		RAISE DEBUG 'SQL: %', sql;
		FOR rec IN EXECUTE sql USING borderEdgeEndNodes
		LOOP
			PERFORM topology.ST_RemIsoNode(topo.name, rec.node_id);
		END LOOP;
	END IF;


	-- Remove dangling requested
	IF removeDanglingBorders THEN --{

		-- Find any dangling border defined by linked
		-- edges and remove them too
		sql := format(
			$$
				WITH dangling_edges AS (
					SELECT
					  rb.topogeo_id tgid,
					  e.edge_id eid,
					  e.left_face fl,
					  e.right_face fr,
					  rsl.topogeo_id sl,
					  rsr.topogeo_id sr
					FROM %1$I.relation rb -- relation for border
					JOIN %1$I.edge_data e
						ON ( e.edge_id in ( rb.element_id, -rb.element_id ) )
					LEFT JOIN %1$I.relation rsl -- relation for surface on the left
						ON ( rsl.layer_id = %2$L AND rsl.element_id = e.left_face )
					LEFT OUTER JOIN %1$I.relation rsr -- relation for surface on the right
						ON ( rsr.layer_id = %2$L AND rsr.element_id = e.right_face )
					WHERE rb.layer_id = %3$L
					AND rsl.topogeo_id IS NOT DISTINCT FROM rsr.topogeo_id
					AND rsl.topogeo_id = %4$L
				)
				SELECT %5$I bid
				FROM %6$s
				WHERE id(%7$I) IN (
					SELECT tgid FROM dangling_edges
				)
			$$,
			topo.name,                 -- %1
			surfaceLayer.layer_id,     -- %2
			borderLayer.layer_id,      -- %3
			-- Surface covering the space previously occupied
			-- by the removed border, which is either a new
			-- merged surface or (if the border was dangling)
			-- the Surface which was on both of its sides
			-- %4
			COALESCE(
				id(mergedSurfaceTopoGeom),
				surfaceLeftId
			),
			borderLayerIdColumn,       -- %5
			borderLayerTable,          -- %6
			borderLayerGeomColumn      -- %7
		);
		RAISE DEBUG 'SQL: %', sql;
		FOR rec IN EXECUTE sql LOOP --{
			RAISE DEBUG 'Found dangling border %', rec.bid;
			-- Recurse
			RETURN QUERY SELECT * FROM topo_update.remove_border_merge_surfaces(
				borderLayerTable,
				borderLayerGeomColumn,
				borderLayerIdColumn,
				rec.bid::text,
				surfaceLayerTable,
				surfaceLayerGeomColumn,
				surfaceLayerIdColumn,
				-- TODO: pass an function that would error out here,
				-- sinc we don't expect any further merge to occur ?
				mergePolicyProviderFunc,
				mergePolicyProviderParam,
				removePrimitives,
				removeDanglingBorders => false -- not needed recursively
			);
		END LOOP; --}


		-- TODO: remove isolated nodes if no surface was merged

	END IF; --}



END;
$BODY$ LANGUAGE plpgsql VOLATILE; --}



