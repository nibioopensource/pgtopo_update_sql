--
-- Update a feature in a table by TopoElementArray/properties pair
--
-- The feature's TopoGeometry will be MODIFIED to only be composed
-- by the given TopoElementArray
--
-- Returns true if the feature was updated, false otherwise.
--
CREATE OR REPLACE FUNCTION topo_update.update_feature(

	-- The elements forming the Feature's TopoGeometry.
	--
	-- The existing TopoGeometry contents will be replaced
	-- by the contents of the given array.
	--
	feature_topoelems topology.TopoElementArray,

	-- The 'properties' component of a GeoJSON Feature object
	-- See https://geojson.org/
	feature_properties JSONB,

	-- Target layer table (where to update the new feature in)
	layerTable REGCLASS,

	-- Name of the TopoGeometry column in the target layer table
	layerGeomColumn NAME,

	-- Name of the primary key column in the target layer table
	layerIdColumn NAME,

	-- Primary key value for the feature to be updated
	id TEXT,

	-- JSON mapping of PG columns and values
	-- from JSON attributes and values
	-- See topo_update.json_props_to_pg_cols() function
	layerColMap JSONB,

	-- True if a record was updated, false otherwise
	OUT updated BOOL,

	-- Feature's TopoGeometry
	OUT topogeom topology.TopoGeometry
)
AS $BODY$ -- {
DECLARE

	colnames TEXT[];
	colvals TEXT[];

	col_updates TEXT[];
	col_upd TEXT;

	sql TEXT;

	rec RECORD;

	topo topology.topology;

	procName TEXT := 'topo_update.update_feature';

	row_count INT;

BEGIN

	SELECT c.colnames, c.colvals
	FROM topo_update.json_props_to_pg_cols(feature_properties, layerColMap) c
	INTO colnames, colvals;

	RAISE DEBUG 'COMPUTED: colnames: %', colnames;
	RAISE DEBUG 'COMPUTED: colvals: %', colvals;

	-- Naive approach: always UPDATE, no matter what

	IF colnames IS NULL THEN
		RAISE WARNING 'NO VALID colnames found using layerColMap % in feature_properties % for layerTable %', layerColMap, feature_properties, layerTable;
		RETURN;
	END IF;

	FOR i IN 1 .. array_upper(colnames, 1)
	LOOP
		col_upd := format('%s = %s', colnames[i], colvals[i]);
		RAISE DEBUG 'col_upd: %', col_upd;
		col_updates := array_append(col_updates, col_upd);
	END LOOP;

	RAISE DEBUG 'col_updates: %', col_updates;

	sql := format($$
UPDATE %1$s t
SET %2$s
FROM %1$s ot
WHERE ot.%3$I = t.%3$I
  AND ot.%3$I = %4$L
RETURNING (ot.%5$I) old_tg
		$$,
		layerTable,
		array_to_string(col_updates, ', '),
		layerIdColumn,
		id,
		layerGeomColumn
	);

	-- TODO: ADD more WHERE clauses to skip setting values which did not
	--			 change ?
	RAISE DEBUG 'SQL: %', sql;

	EXECUTE sql INTO rec;

	-- Early exit if no row was found
	GET DIAGNOSTICS row_count = ROW_COUNT;
	IF row_count = 0 THEN
		updated := FALSE;
		RETURN;
	END IF;

	topogeom := rec.old_tg;

	-- Fetch topology info
	SELECT t topo
		FROM topology.topology t
		WHERE t.id = topology_id(topogeom)
		INTO rec;
	topo := rec.topo;

	-- Query replacing components of topogeom ($2)
	-- with contents of feature_topoelems ($1)
	sql := format(
		$$
			WITH
			newElements AS (
				SELECT e[1][1] element_id, e[1][2] element_type
				FROM (
					SELECT ($1)[x:x][:] e
					FROM generate_series(1, array_upper($1, 1)) x
				) foo
			),
			oldElements AS (
				SELECT element_id, element_type
				FROM %1$I.relation
				WHERE topogeo_id = id($2)
				AND layer_id = layer_id($2)
			),
			insElements AS (
				INSERT INTO %1$I.relation (
					topogeo_id,
					layer_id,
					element_id,
					element_type
				)
				SELECT
					id($2),
					layer_id($2),
					element_id,
					element_type
				FROM newElements
				WHERE ( element_id, element_type )
					NOT IN
					(
						SELECT element_id, element_type
						FROM oldElements
					)
				RETURNING element_id, element_type
			)
			DELETE FROM %1$I.relation
			WHERE topogeo_id = id($2)
			AND layer_id = layer_id($2)
			AND ( element_id, element_type )
				NOT IN
				(
					SELECT element_id, element_type
					FROM newElements
				)
		$$,
		topo.name
	);

	--RAISE DEBUG 'SQL: %', sql;

	EXECUTE sql USING feature_topoelems, topogeom;

	-- or shall we check if anything changed ?
	updated := true;

END;
$BODY$ --}
LANGUAGE plpgsql;


--
-- Update a feature in a table by TopoGeometry/properties pair
--
-- Returns two values:
--	updated - true if the feature was updated, false otherwise
--	old_topogeom - the value of the old TopoGeometry
--	               If modifyExistingTopoGeom is true this will have
--	               the same value as the 'feature_topogeom' input
--	               parameter
--
CREATE OR REPLACE FUNCTION topo_update.update_feature(

	-- A TopoGeometry
	-- If modifyExistingTopoGeom is true the existing
	-- TopoGeometry contents will be replaced by the
	-- contents of the given TopoGeometry.
	--
	-- The caller is responsible to dispose the input
	-- TopoGeometry if needed
	--
	feature_topogeom topology.TopoGeometry,

	-- The 'properties' component of a GeoJSON Feature object
	-- See https://geojson.org/
	feature_properties JSONB,

	-- Target layer table (where to update the new feature in)
	layerTable REGCLASS,

	-- Name of the TopoGeometry column in the target layer table
	layerGeomColumn NAME,

	-- Name of the primary key column in the target layer table
	layerIdColumn NAME,

	-- Primary key value for the feature to be updated
	id TEXT,

	-- JSON mapping of PG columns and values
	-- from JSON attributes and values
	-- See topo_update.json_props_to_pg_cols() function
	layerColMap JSONB,

	-- Wether the TopoGeometry object in an already
	-- existing Feature record will be modified
	-- instead of replaced by the new TopoGeometry
	modifyExistingTopoGeom BOOLEAN DEFAULT TRUE,

	-- True if a record was updated, false otherwise
	OUT updated BOOL,

	-- Feature's old TopoGeometry
	-- If modifyExistingTopoGeom is true this will have
	-- the same value as the 'feature_topogeom' input
	-- parameter
	OUT old_topogeom topology.TopoGeometry

)
AS $BODY$ -- {
DECLARE

	colnames TEXT[];
	colvals TEXT[];

	col_updates TEXT[];
	col_upd TEXT;

	sql TEXT;

	rec RECORD;

	topo topology.topology;

	procName TEXT := 'topo_update.update_feature';

	row_count INT;

BEGIN

	-- Fetch topology info
	SELECT t topo
		FROM topology.topology t
		WHERE t.id = topology_id(feature_topogeom)
		INTO rec;
	topo := rec.topo;

	IF modifyExistingTopoGeom THEN
		SELECT * FROM topo_update.update_feature(
			topology.GetTopoGeomElementArray(
				topo.name,
				layer_id(feature_topogeom),
				id(feature_topogeom)
			),
			feature_properties,
			layerTable,
			layerGeomColumn,
			layerIdColumn,
			id,
			layerColMap
		) INTO rec;
		updated := rec.updated;
		old_topogeom := rec.topogeom;
		RETURN;
	END IF;

	SELECT c.colnames, c.colvals
	FROM topo_update.json_props_to_pg_cols(feature_properties, layerColMap) c
	INTO colnames, colvals;

	colnames := array_append(colnames, format('%I', layerGeomColumn));
	colvals := array_append(colvals, format('%L', feature_topogeom));


	RAISE DEBUG 'COMPUTED: colnames: %', colnames;
	RAISE DEBUG 'COMPUTED: colvals: %', colvals;

	-- Naive approach: always UPDATE, no matter what

	FOR i IN 1 .. array_upper(colnames, 1)
	LOOP
		col_upd := format('%s = %s', colnames[i], colvals[i]);
		RAISE DEBUG 'col_upd: %', col_upd;
		col_updates := array_append(col_updates, col_upd);
	END LOOP;

	RAISE DEBUG 'col_updates: %', col_updates;

	sql := format($$
UPDATE %1$s t
SET %2$s
FROM %1$s ot
WHERE ot.%3$I = t.%3$I
  AND ot.%3$I = %4$L
RETURNING (ot.%5$I) old_tg
		$$,
		layerTable,
		array_to_string(col_updates, ', '),
		layerIdColumn,
		id,
		layerGeomColumn
	);

	-- TODO: ADD more WHERE clauses to skip setting values which did not
	--			 change ?
	RAISE DEBUG 'SQL: %', sql;

	EXECUTE sql INTO rec;

	-- Early exit if no row was found
	GET DIAGNOSTICS row_count = ROW_COUNT;
	IF row_count = 0 THEN
		updated := FALSE;
		RETURN;
	END IF;

	updated := TRUE;
	old_topogeom := rec.old_tg;

END;
$BODY$ --}
LANGUAGE plpgsql;


--
-- Update a feature in a table by Geometry/properties pair
--
-- Returns an 'updated' field telling the caller whether
-- or not the Feature was modified and the new and the
-- old TopoGeometry attributes of it (which may be the same
-- if modifyExistingTopoGeom is TRUE)
--
--
CREATE OR REPLACE FUNCTION topo_update.update_feature(

	-- The 'geometry' component of a GeoJSON Feature object
	feature_geometry GEOMETRY,

	-- The 'properties' component of a GeoJSON Feature object
	-- See https://geojson.org/
	feature_properties JSONB,

	-- Target layer table (where to insert the new feature in)
	layerTable REGCLASS,

	-- Name of the TopoGeometry column in the target layer table
	layerGeomColumn NAME,

	-- Name of the primary key column in the target layer table
	layerIdColumn NAME,

	-- Identifier value for the feature to be updated
	id TEXT,

	-- JSON mapping of PG columns and values
	-- from JSON attributes and values
	-- See topo_update.json_props_to_pg_cols() function
	layerColMap JSONB,

	-- Tolerance to use for insertion of topology primitives
	-- used to represent the feature geometry.
	-- May be NULL to use topology-default tolerance.
	-- The value is to be given in the units of the target topology
	-- projection.
	tolerance FLOAT8,

	-- Wether the TopoGeometry object in an already
	-- existing Feature record will be modified
	-- instead of replaced by the new TopoGeometry
	modifyExistingTopoGeom BOOLEAN DEFAULT TRUE,

	-- True if a record was updated, false otherwise
	OUT updated BOOL,

	-- Feature new TopoGeometry
	OUT topogeom topology.TopoGeometry,

	-- Feature's old TopoGeometry
	-- If modifyExistingTopoGeom is true this will have
	-- the same value as the 'feature_topogeom' input
	-- parameter
	OUT old_topogeom topology.TopoGeometry,

	-- Name of a field that represents the "version" of
	-- the whole record. If the value of such field does
	-- not change, the whole record is expected to not
	-- have changed, which means we don't need to update it
	layerVersionColumn NAME DEFAULT NULL
)
AS $BODY$ -- {
DECLARE
	toponame TEXT;
	topo_srid INT;
	layer_id INT;

	geom GEOMETRY;

	colnames TEXT[];
	colvals TEXT[];

	col_updates TEXT[];
	col_upd TEXT;

	quotedLayerVersionColumn TEXT;
	version_value TEXT;

	rec RECORD;
	sql TEXT;

	i INT;

	row_count INT;
BEGIN

	-- TODO: do this one in the calling function and receive this
	--			 information pre-computed
	SELECT t.name, l.layer_id, t.srid
		FROM topology.layer l, topology.topology t, pg_class c, pg_namespace n
		WHERE l.schema_name = n.nspname
		AND l.table_name = c.relname
		AND c.oid = layerTable
		AND c.relnamespace = n.oid
		AND l.feature_column = layerGeomColumn
		AND l.topology_id = t.id
		INTO toponame, layer_id, topo_srid;

	RAISE DEBUG 'toponame: %', toponame;
	RAISE DEBUG 'layer_id: %', layer_id;

	SELECT c.colnames, c.colvals
	FROM topo_update.json_props_to_pg_cols(feature_properties, layerColMap) c
	INTO colnames, colvals;

	colnames := array_append(colnames, format('%I', layerGeomColumn));

	geom := feature_geometry;
	--RAISE DEBUG 'GEOM: %', ST_AsEWKT(geom);

	IF modifyExistingTopoGeom THEN
		colvals := array_append(
			colvals,
			format(
				'topology.toTopoGeom($1, topology.clearTopoGeom(t.%I), %L)',
				layerGeomColumn,
				tolerance
			)
		);
	ELSE
		colvals := array_append(
			colvals,
			format(
				'topology.toTopoGeom($1, %L, %L, %L)',
				toponame,
				layer_id,
				tolerance
			)
		);
	END IF;

	RAISE DEBUG 'COMPUTED: colnames: %', colnames;
	RAISE DEBUG 'COMPUTED: colvals: %', colvals;

	-- Naive approach: always UPDATE, no matter what

	quotedLayerVersionColumn = quote_ident(layerVersionColumn);
	FOR i IN 1 .. array_upper(colnames, 1)
	LOOP
		col_upd := format('%s = %s', colnames[i], colvals[i]);
		RAISE DEBUG 'col_upd: %', col_upd;
		col_updates := array_append(col_updates, col_upd);
		IF quotedLayerVersionColumn = colnames[i] THEN
			version_value = colvals[i];
			RAISE DEBUG 'VERSION VALUE: %', version_value;
		END IF;
	END LOOP;

	RAISE DEBUG 'col_updates: %', col_updates;

	sql := format(
		$$
			UPDATE %1$s t
			SET %2$s
			FROM %1$s ot
			WHERE ot.%3$I = t.%3$I
			  AND ot.%3$I = %4$L
			  AND ( %6$s )
			RETURNING (ot.%5$I) old_tg, (t.%5$I) new_tg
		$$,
		layerTable,
		array_to_string(col_updates, ', '),
		layerIdColumn,
		id,
		layerGeomColumn,
		CASE
		WHEN layerVersionColumn IS NULL THEN
			'true'
		WHEN version_value IS NULL THEN
			format('ot.%1$I IS NOT NULL', layerVersionColumn)
		ELSE
			format('ot.%1$I IS NULL OR ot.%1$I != %2$s', layerVersionColumn, version_value)
		END
	);
	-- TODO: ADD more WHERE clauses to skip setting values which did not
	--			 change ?
	RAISE DEBUG 'SQL: %', sql;

	EXECUTE sql USING geom INTO rec;

	GET DIAGNOSTICS row_count = ROW_COUNT;
	IF row_count = 0 THEN
		RAISE DEBUG 'Row for feature with id % was NOT updated', id;
		updated := FALSE;
	ELSE
		updated := TRUE;
	END IF;

	topogeom := rec.new_tg;
	old_topogeom := rec.old_tg;

END;
$BODY$ --}
LANGUAGE plpgsql;


--
-- Update a feature in a table by GeoJSON
--
-- Returns an 'updated' field telling the caller whether
-- or not the Feature was modified and the new and the
-- old TopoGeometry attributes of it (which may be the same
-- if modifyExistingTopoGeom is TRUE)
--
CREATE OR REPLACE FUNCTION topo_update.update_feature(

	-- A GeoJSON Feature object
	-- See https://geojson.org/
	feature JSONB,

	-- SRID of geometry, or NULL for leaving it as unknown
	srid INT,

	-- Target layer table (where to update the new feature in)
	layerTable REGCLASS,

	-- Name of the TopoGeometry column in the target layer table
	layerGeomColumn NAME,

	-- Name of the primary key column in the target layer table
	layerIdColumn NAME,

	-- Identifier value for the feature to be updated
	id TEXT,

	-- JSON mapping of PG columns and values
	-- from JSON attributes and values
	-- See topo_update.json_props_to_pg_cols() function
	layerColMap JSONB,

	-- Tolerance to use for insertion of topology primitives
	-- used to represent the feature geometry.
	-- May be NULL to use topology-default tolerance.
	-- The value is to be given in the units of the target topology
	-- projection.
	tolerance FLOAT8,

	-- Wether the TopoGeometry object in an already
	-- existing Feature record will be modified
	-- instead of replaced by the new TopoGeometry
	modifyExistingTopoGeom BOOLEAN DEFAULT TRUE,

	-- True if a record was updated, false otherwise
	OUT updated BOOL,

	-- Feature new TopoGeometry
	OUT topogeom topology.TopoGeometry,

	-- Feature's old TopoGeometry
	-- If modifyExistingTopoGeom is true this will have
	-- the same value as the 'feature_topogeom' input
	-- parameter
	OUT old_topogeom topology.TopoGeometry
)
AS $BODY$ -- {
DECLARE
	topo_srid INT;
	geom GEOMETRY;
	rec RECORD;
BEGIN

	geom := ST_GeomFromGeoJSON(feature -> 'geometry');
	--RAISE DEBUG 'GEOM: %', ST_AsEWKT(geom);

	-- TODO: do this one in the calling function and receive this
	--			 information pre-computed
	SELECT t.srid
		FROM topology.layer l, topology.topology t, pg_class c, pg_namespace n
		WHERE l.schema_name = n.nspname
		AND l.table_name = c.relname
		AND c.oid = layerTable
		AND c.relnamespace = n.oid
		AND l.feature_column = layerGeomColumn
		AND l.topology_id = t.id
		INTO topo_srid;

	-- TODO: use the deprecated "crs" member in GeoJSON, if found ?
	RAISE DEBUG 'SRID: %', srid;
	RAISE DEBUG 'TOPO_SRID: %', topo_srid;
	IF srid IS NOT NULL THEN
		geom := ST_SetSRID(geom, srid);
		IF topo_srid IS NOT NULL THEN
			geom := ST_Transform(geom, topo_srid);
		END IF;
		RAISE DEBUG 'REPROJECTED GEOM: %', ST_AsEWKT(geom);
	END IF;

	SELECT * FROM topo_update.update_feature(
		geom,
		feature -> 'properties',
		layerTable,
		layerGeomColumn,
		layerIdColumn,
		id,
		layerColMap,
		tolerance,
		modifyExistingTopoGeom
	) INTO rec;

	RAISE DEBUG 'update_feature(geom) returned rec: %', rec;

	updated := rec.updated;
	topogeom := rec.topogeom;
	old_topogeom := rec.old_topogeom;
END;
$BODY$ --}
LANGUAGE plpgsql;
