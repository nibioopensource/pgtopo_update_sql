CREATE OR REPLACE FUNCTION topo_update._app_do_MovePoint_Dynamic(
	appname NAME,
	appconfig JSONB,
	point_id TEXT,
	point JSONB
)
RETURNS VOID AS
$BODY$ --{
DECLARE
	cfg_pointLayer JSONB;
	cfg_pointLayerTable JSONB;
	cfg_op JSONB;
	cfg_op_params JSONB;
	cfg JSONB;
	pointLayerTable REGCLASS;
	pointLayerGeomColumn NAME;
	pointLayerIdColumn NAME;
	snapTolerance FLOAT8;
	point_geom GEOMETRY;
	rec RECORD;
	sql TEXT;
	removePrimitives BOOLEAN := true;
BEGIN

	-- Check if the operation is enabled
	IF NOT appconfig -> 'operations' ? 'MovePoint' THEN
		RAISE EXCEPTION 'MovePoint operation not enabled for application %', appname;
	END IF;

	cfg_pointLayer := appconfig -> 'point_layer';

	--RAISE DEBUG 'pointLayer: %', cfg_pointLayer;

	FOR rec IN SELECT jsonb_array_elements( appconfig -> 'tables' ) cfg
	LOOP --{
		IF rec.cfg ->> 'name' = cfg_pointlayer ->> 'table_name' THEN
			cfg_pointLayerTable := rec.cfg;
			EXIT;
		END IF;
	END LOOP; --}

	--RAISE DEBUG 'pointLayerTable: %', cfg_pointLayerTable;

	pointLayerTable := ( appname || '.' || ( cfg_pointlayer ->> 'table_name' ) );
	pointLayerGeomColumn := cfg_pointlayer ->> 'geo_column';
	pointLayerIdColumn := cfg_pointlayerTable ->> 'primary_key';

	-- Validate point geometry,
	point_geom := ST_GeomFromGeoJSON(point -> 'geometry');
	IF point_geom IS NULL THEN
		RAISE EXCEPTION 'Missing geometry in point';
	END IF;

	IF ST_GeometryType(point_geom) != 'ST_Point' THEN
		RAISE EXCEPTION 'Point must be Point, is % instead', ST_GeometryType(point_geom);
	END IF;

	snapTolerance := 0; -- TODO: read from config ?

	-- update topogeometry of record with new point

	-- TODO: cast user argument to id type
	sql := format(
		$$
			UPDATE %1$s SET %2$I = topology.toTopoGeom($1, topology.clearTopoGeom(%2$I), $2)
			WHERE %3$I::text = $3
			RETURNING %3$I::text id
		$$,
		pointLayerTable,      -- %1
		pointLayerGeomColumn, -- %2
		pointLayerIdColumn    -- %3
	);

	RAISE DEBUG 'SQL: %', sql;

	EXECUTE sql USING point_geom, snapTolerance, point_id
	INTO rec;

	IF rec IS NULL THEN
		RAISE EXCEPTION 'No Point with id % in table %', point_id, pointLayerTable;
	END IF;

	-- Cleanup old node if not needed anymore
	IF removePrimitives THEN
		-- TODO: pass the bounding box of the removed point ?
		PERFORM topo_update.cleanup_topology(
			( SELECT t FROM topology.topology t
			  WHERE name = appname || '_sysdata_webclient' )
		);
	END IF;

END;
$BODY$ LANGUAGE plpgsql; --}

CREATE OR REPLACE FUNCTION topo_update.app_do_MovePoint(

	-- Application name, will be used to fetch
	-- configuration from application schema.
	-- See doc/APP_CONFIG.md
	appname NAME,

	point_id TEXT,

	-- A GeoJSON Feature object representing
	-- a single Points (as a Point)
	-- to be used to create a new Point object
	--
	point JSONB

)
RETURNS VOID AS
$BODY$ --{
DECLARE
	sql TEXT;
	appconfig JSONB;
BEGIN

	sql := format('SELECT cfg FROM %I.app_config',
			appname || '_sysdata_webclient_functions');
	EXECUTE sql INTO STRICT appconfig;

	--RAISE DEBUG 'APPCONFIG: %', appconfig;

	PERFORM topo_update._app_do_MovePoint_Dynamic(
		appname,
		appconfig,
		point_id,
		point
	);
END;
$BODY$ LANGUAGE plpgsql; --}
