--
-- Take a JSON object (prop - properties) and a JSON mapping file
-- (col_map) and return 2 arrays:
--
--   - colnames will contain the name of PostgreSQL columns
--   - colvals will contain canonical text values for those columns
--
-- The mapping file is a JSON object having PostgreSQL column names
-- as keys (either in "simple" or "compo.site" form) and paths into
-- the payload properties object as JSON arrays
-- ( [ "path", "to", "value" ] )
--
-- When the "strict" parameter is false (default) then keys in the
-- mapping file which reference non-existing elements in the properties
-- (prop) object will be considered as not given and thus will NOT result
-- in the PostgreSQL column name being output. When "strict" is true,
-- such invalid mapping will result in an EXCEPTION being raised.
--
CREATE OR REPLACE FUNCTION topo_update.json_props_to_pg_cols(
	props JSONB,
	col_map JSONB,
	strict BOOLEAN DEFAULT false,
	OUT colnames TEXT[],
	OUT colvals TEXT[]
)
AS $BODY$
DECLARE
	geom GEOMETRY;
	rec RECORD;
	nam TEXT;
	val TEXT;
	val_json JSONB;
	prop_array_json JSONB;
	prop_array TEXT[];
BEGIN

	FOR rec IN SELECT jsonb_object_keys( col_map ) k
	LOOP
		prop_array_json := col_map -> rec.k;
		RAISE DEBUG 'prop_array_json: %', prop_array_json;

		-- Compute column value
		IF NOT jsonb_typeof(prop_array_json) = 'array' THEN
			RAISE EXCEPTION 'Invalid mapping: values should be arrays';
		END IF;

		-- TODO: improve performance of this
		prop_array := array_agg(x) from jsonb_array_elements_text( prop_array_json ) x;
		RAISE DEBUG 'prop_array: %', prop_array;

		val_json := props #> prop_array;
		IF val_json IS NULL
		THEN
			IF strict THEN
				RAISE EXCEPTION 'Mapping file % references '
					'non-existent key % in properties object %',
					col_map, prop_array,
					props
				;
			END IF;
			CONTINUE;
		END IF;
		val := format('%L', props #>> prop_array);
		colvals := array_append(colvals, val);

		-- Compute column name
		nam = rec.k;
		IF nam LIKE '%.%' THEN
			-- Protect from SQL injection
			-- TODO: improve the checking !
			nam := regexp_replace(regexp_replace(nam, '(["\\])', '\\\1', 'g'), '([^.]+)', '"\1"', 'g');
		ELSE
			nam := format('%I', nam);
		END IF;
		colnames := array_append(colnames, nam);

	END LOOP;

	--RETURN ( colnames, colvals );
END;
$BODY$ LANGUAGE 'plpgsql';


