
-- {
-- This function will be called from the client when the user
-- draws a line to create a new path
--
--   1. The line, with associated attributes, is added into
--      the border layer, using json attributes mapped using
--      the colMapProviderFunc provided mapping file.
--
--   3. Paths split by the drawn line are recognized and split
--      in the Path layer, by removing all but one edge
--      ending at any node also covered by the drawn line
--      from their definition and creating new Paths covering
--      their lost spatial portions.
--
-- # RETURN VALUE
--
-- The function returns a table containing information about
-- the created and splitted paths; the
-- returned table has fields:
--
--	- fid text
--
--		Path identifier (text representation of the value of
--		the specified layer's IdColumn)
--
--	- act char
--
--		Character representing the action taken on the Feature,
--		with the following possible values:
--
--			M	The Feature was modified loosing space
--				due to a split.
--
--			C	The Feature was created independently from
--				previously existing Features.
--
--			S	The Feature was created by taking up space
--				space previosuly taken by previosly existing
--				Features. When such a Feature is created, there
--				should also be at least another Feature of the
--				same type which is returned as modified (M).
--
--  - frm text
--
--		Identifier of the Feature previously covering the space
--		now covered by the split Path. This is NULL for all but
--		splitted (act=S) features.
--
-- }{
CREATE OR REPLACE FUNCTION topo_update.add_path(

	-- A GeoJSON Feature object representing the
	-- line drawn by the user and the holding the
	-- attributes to use with the paths
	feature JSONB,

	-- Path layer table
	pathLayerTable REGCLASS,

	-- Name of Path layer TopoGeometry column
	pathLayerGeomColumn NAME,

	-- Path layer primary key column name
	pathLayerIdColumn NAME,

	-- Function returning mapping file for layer features
	-- based on action being performed.
	--
	-- The function is expected to take 3 named parameters:
	--
	--	act
	--		Action performed on Feature (see RETURN VALUE section)
	--
	--	usr
	--		User-defined parameter (value of colMapProviderParam)
	--
	--
	-- The function is expected to return a single JSONB value
	-- representing the mapping between the Path's properties
	-- and the PostgreSQL columns for the target layer.
	-- See json_props_to_pg_cols for documentation of the
	-- format of this mapping file
	--
	colMapProviderFunc REGPROC,

	-- User defined parameter to pass to the colMapProvider function
	colMapProviderParam ANYELEMENT,

	-- Snap tolerance to use when
	-- inserting the new line
	-- in the topology
	tolerance float8 DEFAULT 0.0,

	-- Minimum length of newly created Paths, in topology units
	minToleratedPathLength FLOAT8 DEFAULT NULL
)
RETURNS TABLE(fid text, act char, frm text) AS $BODY$
DECLARE

	rec RECORD;
	rec2 RECORD;

	featureGeom JSONB;

	payloadProps JSONB;

	newPath topology.topogeometry;
	modPathGeom GEOMETRY;
	newPathId text;
	oldPathProps JSONB;
	newPathProps JSONB;

	-- holds dynamic sql
	sql text;

	proc_name text = 'topo_update.add_path';

	inputLine GEOMETRY;
	overlapPoints GEOMETRY;

	topo topology.topology;

	candidateInputLines GEOMETRY;
	dropInputLines INT[];

	inputLineEdgeIds INT[];
	inputLineNodeIds INT[];

	pathLayer topology.layer;

	-- Holds dynamic sql to execute
	-- by passing $1=colMapProviderParam,
	-- $2=act to extract the
	-- appropriate column mapping
	colMapExtractSQL TEXT;

	-- Column mapping file for modified paths
	-- as provided by colMapProvider
	colMapForModPath JSONB;

	-- Column mapping file for paths
	-- as provided by colMapProvider
	colMapForNewPath JSONB;

	-- Column mapping file for paths
	-- replacing space previously covered
	-- by existing paths as provided by
	-- colMapProvider
	colMapForSplitPath JSONB;

	-- Column mapping file for split paths
	-- inheriting attributes from existing Path
	inhSplitPathColMap JSONB;

	-- Path as split by incoming line
	splitPathGeom GEOMETRY;
	splitPathGeomComp GEOMETRY;
	splitPathStartPoint GEOMETRY;
	splitPathStartPointNodeDegree INT;
	splitPathMergeOnStartPoint BOOLEAN;

	-- Number of elements a path was split into
	splitPathCount INT;

	i INT;

	tea topology.TopoElementArray;

BEGIN

	-- Fetch topology and layer info from path layer references
	SELECT t topo, l layer
		FROM topology.layer l, topology.topology t, pg_class c, pg_namespace n
		WHERE l.schema_name = n.nspname
		AND l.table_name = c.relname
		AND c.oid = pathLayerTable
		AND c.relnamespace = n.oid
		AND l.feature_column = pathLayerGeomColumn
		AND l.topology_id = t.id
	--	FOR UPDATE, 2 problems, user needs SUPERUSER (can be fixed),
	--  ms used from 8601 -> 113606 for ((14.355156130507279 67.27980828078813, 14.354976710179235 67.28648792270606, 14.395077865039152 67.28664388346324, 14.395246132501715 67.27996419068565, 14.355156130507279 67.27980828078813))
		INTO rec;
	topo := rec.topo;
	pathLayer := rec.layer;

	--	RAISE NOTICE 'Lock on topology % obtained', topo.name;

	featureGeom = feature->'geometry';
	IF featureGeom IS NULL
	THEN
		RAISE EXCEPTION 'Input feature lacks a "geometry" element: %',
			feature;
	END IF;

	IF NOT featureGeom ? 'crs'
	THEN
		RAISE EXCEPTION 'Input feature geometry lacks a "crs" element: %',
			featureGeom;
	END IF;

	-- Read input line geometry from GeoJSON
	inputLine := ST_GeomFromGeoJSON(featureGeom);

	IF topo.srid != ST_Srid(inputLine) THEN
		inputLine := ST_Transform(inputLine, topo.srid);
		--RAISE DEBUG 'REPROJECTED GEOM: %', ST_AsEWKT(geom);
	END IF;

	RAISE DEBUG 'Input line: %', ST_AsText(inputLine);

	-- Write SQL to extract colMaps
	-- $1: usr
	-- $2: act
	colMapExtractSQL := format(
		'SELECT * FROM %s(usr => $1, act => $2)',
		colMapProviderFunc
	);

	payloadProps := feature -> 'properties';

	candidateInputLines := ST_ForceCollection(inputLine);

	-- We only tolerate paths having a length of at least
	-- the given amoun of topology units
	RAISE NOTICE 'Minimum tolerated path length: %', minToleratedPathLength;

	-- Insert candidate input lines and iterate if any of
	-- them has to be refused
	LOOP --{

		--RAISE DEBUG 'Candidate input lines: %', ST_AsText(candidateInputLines);

		BEGIN --{

			-- Insert the inputLine to the topology
			-- and gather list of edges forming it
			SELECT array_agg(DISTINCT e)
			FROM ST_Dump(candidateInputLines) d,
			LATERAL topology.TopoGeo_addLineString(
					topo.name,
					d.geom,
					tolerance
				) e
			INTO inputLineEdgeIds;
			RAISE DEBUG '%: input line edges: %',
				proc_name, array_to_string(inputLineEdgeIds, ',');

			-- Remove any edge already used by existing paths
			-- from the the list of edges forming the new line
			sql := format(
				$$
		SELECT array_agg(x)
		FROM (
			SELECT x FROM (
				SELECT unnest($1) x
			) foo
			WHERE abs(x) NOT IN (
				SELECT abs(element_id)
				FROM %1$I.relation
				WHERE layer_id = %2$L
				AND element_type = 2 -- edge
			)
		) foo
				$$,
				topo.name,           -- %1
				pathLayer.layer_id -- %2
			);
			RAISE DEBUG '%: sql: %', proc_name, sql;
			EXECUTE sql
			USING inputLineEdgeIds
			INTO inputLineEdgeIds;

			RAISE DEBUG '%: input line edges not already used by existing paths: %',
				proc_name, array_to_string(inputLineEdgeIds, ',');

			-- Check which new edges should be dropped
			dropInputLines := NULL;

			-- Remove edge below min tolerated length
			IF minToleratedPathLength > 0 THEN
				-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/261
				sql := format(
					$$
						SELECT array_agg(edge_id)
						FROM %1$I.edge_data
						WHERE edge_id = ANY($1)
						AND ST_Length(geom) < $2
					$$,
					topo.name
				);
				EXECUTE sql USING inputLineEdgeIds, minToleratedPathLength
				INTO dropInputLines;
				IF dropInputLines IS NOT NULL THEN
					RAISE NOTICE 'New edges % are shorter than tolerated, dropping', dropInputLines;
				END IF;

				IF dropInputLines IS NOT NULL
				THEN --{
					RAISE NOTICE 'All input lines edges: %', inputLineEdgeIds;
					RAISE NOTICE 'Dropped input lines edges: %', dropInputLines;
					SELECT array_agg(x) FROM (
						SELECT abs(unnest(inputLineEdgeIds)) x
							EXCEPT
						SELECT unnest(dropInputLines)
					) foo
					INTO inputLineEdgeIds;
					-- Re-write candidate input lines as the list
					-- of edges we keep
					sql := format(
						$$
							SELECT ST_Collect(e.geom ORDER BY e.edge_id)
							FROM
								%1$I.edge e
							WHERE edge_id = ANY($1)
						$$,
						topo.name
					);
					EXECUTE sql USING inputLineEdgeIds
					INTO candidateInputLines;
					--RAISE DEBUG 'New candidate input lines num: %', ST_NumGeometries(candidateInputLines);

					IF candidateInputLines IS NULL THEN
						RAISE EXCEPTION 'No usable input lines found for inputline %' , ST_AsText(inputLine);
					END IF;

					-- Some input lines were refused,
					-- rollback the subtransaction
					RAISE USING ERRCODE = '22901'; -- custom error code
				END IF; --}

			END IF;

			EXIT; -- Exits the loop, no input lines were drop

		EXCEPTION WHEN SQLSTATE '22901' THEN --}{
			NULL; -- continue the loop
		END; --} exception handling transaction

	END LOOP; --}

	RAISE DEBUG '%: nodes collecting phase start', proc_name;

	-- Collect all nodes covered by the inputLineEdgeIds
	-- Param $1 is the input line edge ids
	sql := format(
		$$
			WITH coveredNodes AS (
				SELECT start_node node_id
				FROM %1$I.edge_data
				WHERE edge_id = ANY($1)
					UNION
				SELECT end_node
				FROM %1$I.edge_data
				WHERE edge_id = ANY($1)
			)
			SELECT array_agg(node_id)
			FROM coveredNodes
		$$,
		topo.name
	);
	RAISE DEBUG '%: sql: %', proc_name, sql;
	EXECUTE sql
		USING inputLineEdgeIds
		INTO inputLineNodeIds
	;
	RAISE DEBUG '%: input line covers nodes %',
		proc_name,
		array_to_string(inputLineNodeIds, ',')
	;

	-- Query to extract Paths split by nodes
	-- covering edges forming the input line
	--
	-- Param $1 is the list of inputLineNodeIds
	--
	sql := format(
		$$
WITH pathSplit AS (
		SELECT
			DISTINCT
			b path_record,
			b.%1$I::text path_id,
			b.%4$I path_tg,
			n.node_id,
			e.edge_id,
			n.geom node_geom
		FROM
			%3$s b,
			%2$I.relation r,
			%2$I.edge_data e,
			%2$I.node n
		WHERE
			r.topogeo_id = id(b.%4$I)
			AND r.layer_id = layer_id(b.%4$I)
			AND r.element_type = 2 -- edge
			AND r.element_id IN ( e.edge_id, -e.edge_id )
			AND
			(
				(
					e.start_node = ANY($1)
					AND
					e.start_node = n.node_id
				)
				OR
				(
					e.end_node = ANY($1)
					AND
					e.end_node = n.node_id
				)
			)
)
SELECT
	path_record,
	path_id,
	ST_CollectionHomogenize(
			path_tg::geometry
	) path_geom,
	ST_Collect(node_geom) node_geoms,
	array_agg(node_id) node_ids
FROM pathSplit
GROUP BY path_record, path_id, path_tg
HAVING count(edge_id) > 1
		$$,
		pathLayerIdColumn,  -- %1
		topo.name,            -- %2
		pathLayerTable,     -- %3
		pathLayerGeomColumn -- %4
	);
	RAISE DEBUG '%: sql: %', proc_name, sql;
	FOR rec IN
		EXECUTE sql
		USING inputLineNodeIds
	LOOP --{
		RAISE DEBUG 'Path % was split on nodes %',
			rec.path_id,
			array_to_string(rec.node_ids, ',')
		;

		RAISE DEBUG 'Path geom was: %',
			ST_AsText(rec.path_geom)
		;

		splitPathStartPoint := ST_StartPoint(rec.path_geom);
		-- If the path is a closed path
		-- and its start/end point is not
		-- one of the "splitting nodes",
		-- then we will want to merge components
		-- of the split path that end on such
		-- point.
		splitPathMergeOnStartPoint := FALSE;
		IF ST_IsClosed(rec.path_geom)
		AND NOT ST_Intersects(
				splitPathStartPoint,
				rec.node_geoms
			)
		THEN
			RAISE DEBUG 'Path is closed and its endpoint % is not a split node',
				ST_AsText(splitPathStartPoint);
			-- Check if the node has degree > 2
			-- in which case we cannot merge back on it.
			-- TODO: only count edges taking part of the definition of
			--       other TopoGeometries in the Path layer
			-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/275
			sql := format(
				$SQL$
					SELECT count(e.edge_id)
					FROM %1$I.edge e, %1$I.node n
					WHERE ST_Equals(n.geom, $1)
					AND ( e.start_node = n.node_id OR e.end_node = n.node_id )
				$SQL$,
				topo.name
			);
			EXECUTE sql USING splitPathStartPoint INTO splitPathStartPointNodeDegree;
			RAISE DEBUG 'Path endpoint node degree=%', splitPathStartPointNodeDegree;
			IF splitPathStartPointNodeDegree < 3 THEN
				splitPathMergeOnStartPoint := TRUE;
			END IF;
		END IF;

		-- Split the path geometry
		splitPathGeom := ST_Split(
			rec.path_geom,
			rec.node_geoms
		);
		RAISE DEBUG 'Path was split to: %', ST_AsText(splitPathGeom);

		splitPathCount := ST_NumGeometries(splitPathGeom);

		-- (Sanity check: split path must have been split in at least 2 components.)
		-- The above is not true when we use snapTo because then we get the nodes from the server
		-- TODO do we need another sanity check her, or do we need this check at all ?
		IF splitPathCount < 1
		THEN
			RAISE EXCEPTION 'Path % (%) split by nodes % (%) is still a single geometry',
				ST_AsText(rec.path_geom),
				rec.path_id,
				ST_AsText(rec.node_geoms),
				array_to_string(rec.node_ids, ',')
			;
		END IF;

		-- Get column map for modified Paths
		IF colMapForModPath IS NULL
		THEN
			EXECUTE colMapExtractSQL
				USING colMapProviderParam, 'M'
				INTO colMapForModPath;
			IF colMapForModPath IS NULL
			THEN
				RAISE EXCEPTION '%: colMapProvider did not provide a map for modified path act=%',
					proc_name, 'M';
			END IF;
			RAISE DEBUG 'colMapForModPath: %', colMapForModPath;
		END IF;

		-- Get column map for split Paths
		IF colMapForSplitPath IS NULL
		THEN
			EXECUTE colMapExtractSQL
				USING colMapProviderParam, 'S'
				INTO colMapForSplitPath;
			IF colMapForSplitPath IS NULL
			THEN
				RAISE EXCEPTION '%: colMapProvider did not provide a map for split path act=%',
					proc_name, 'S';
			END IF;
			RAISE DEBUG 'colMapForSplitPath: %', colMapForSplitPath;

			-- Build a column map for attributes-inheriting split Path objects
			-- Write new path mapping taking properties
			-- from payload when specified or from old path
			-- when not specified
			inhSplitPathColMap := topo_update._json_col_map_override(
				ARRAY[
					topo_update.json_col_map_from_pg(
						pathLayerTable,
						ARRAY[
							-- omit TopoGeometry column from mapping
							pathLayerGeomColumn::text,
							-- omit ID column from props
							pathLayerIdColumn::text
						]
					),
					colMapForSplitPath
				]
			);
			RAISE DEBUG '%: inheriting split Path ColMap: %', proc_name, inhSplitPathColMap;
		END IF;

		-- Create split paths for subsequent
		-- components of the splitted geometry

		-- Extract properties of old path
		oldPathProps := to_jsonb(rec.path_record)
			- pathLayerGeomColumn -- drop TopoGeometry column from props
			- pathLayerIdColumn   -- drop ID column from props
		;
		RAISE DEBUG 'Old Path props: %', oldPathProps;

		-- Create new path props with
		-- old attributes in an 'old'-keyed field
		-- and payload attributes in a 'new'-keyed field
		newPathProps := jsonb_build_object(
			'p0', oldPathProps,
			'p1', payloadProps
		);
		RAISE DEBUG 'New Path props: %', newPathProps;

		modPathGeom := NULL;
		FOR i IN 1 .. splitPathCount
		LOOP -- {
			splitPathGeomComp = ST_GeometryN(splitPathGeom, i);
			RAISE DEBUG 'Split path geom component: %',
				ST_AsText(splitPathGeomComp);

			IF NOT splitPathMergeOnStartPoint
			THEN --{

				-- If we don't need to merge components
				-- then we take component having the
				-- same start point as the original
				-- path as the one which will retain
				-- its path id
				IF modPathGeom IS NULL
				THEN
					IF ST_Equals(
						ST_StartPoint(splitPathGeomComp),
						splitPathStartPoint
					)
					THEN
						RAISE DEBUG 'Component starts from  original path start point';
						modPathGeom := splitPathGeomComp;
						CONTINUE;
					END IF;
				END IF;

			ELSE -- }{

				-- If we do need to merge components
				-- then we take all components (2 expected)
				-- ending on the splitPathMergeOnStartPoint
				-- to build the new versino of the path
				-- which will retain its id
				IF ST_Equals(
						ST_StartPoint(splitPathGeomComp),
						splitPathStartPoint
					) OR
					ST_Equals(
						ST_EndPoint(splitPathGeomComp),
						splitPathStartPoint
					)
				THEN --{
					RAISE DEBUG 'Component ends on original path endpoint';
					IF modPathGeom IS NULL
					THEN
						modPathGeom := splitPathGeomComp;
					ELSE
						modPathGeom := ST_LineMerge(
							ST_Collect(
								modPathGeom,
								splitPathGeomComp
							)
						);
					END IF;
					CONTINUE;

				END IF; --}

			END IF; --}

			rec2 := topo_update.insert_feature(
				splitPathGeomComp,
				newPathProps,
				pathLayerTable,
				pathLayerGeomColumn,
				pathLayerIdColumn,
				inhSplitPathColMap,
				-- use 0 tolerance as we're only using
				-- existing topological elements
				0.0 -- tolerance
			);

			fid := rec2.id;
			act := 'S';
			frm := rec.path_id;
			RAISE DEBUG 'Path % split from % - returning %|%|%',
				fid, frm, fid,act,frm;
			RETURN NEXT;

		END LOOP; --}

		-- Update the modified path to only be composed
		-- by the first geometry returned by the split
		RAISE DEBUG 'Modified path geom: %', ST_AsText(modPathGeom);

		fid := rec.path_id;
		act := 'M';
		frm := null;
		RAISE DEBUG 'Path % modified - returning %|%|%',
			fid, fid,act,frm;
		RETURN NEXT;

		rec2 := topo_update.update_feature(
			modPathGeom,
			payloadProps,
			pathLayerTable,
			pathLayerGeomColumn,
			pathLayerIdColumn,
			fid,
			colMapForModPath,
			-- use 0 tolerance as we're only using
			-- existing topological elements
			0.0, -- tolerance
			true -- modify existing TopoGeometry
		);

		-- Sanity check
		IF NOT (
			id(rec2.old_topogeom) = newPath.id
		) THEN
			RAISE EXCEPTION 'topo_update.update_feature'
				' unexpectedly found the old TopoGeometry'
				' to be different from the new one (path)';
		END IF;


	END LOOP; --}

	-- Get column map for new Paths
	EXECUTE colMapExtractSQL
		USING colMapProviderParam, 'C'
		INTO colMapForNewPath;
	IF colMapForNewPath IS NULL
	THEN
		RAISE EXCEPTION '%: colMapProvider did not provide a map for new path act=%',
			proc_name, 'C';
	END IF;
	RAISE DEBUG 'colMapForNewPath: %', colMapForNewPath;

	-- Create a new Path for each edge in
	-- inputLineEdgeIds
	-- TODO: join edges togheter if they are not
	--       ending on inputLineNodeIds !
	FOR rec IN
		SELECT unnest(inputLineEdgeIds) edge_id
	LOOP

		act := 'C';
		frm := null;

		-- Create the new Path TopoGeometry
		newPath := topology.CreateTopoGeom(
			topo.name,
			2, -- lineal
			pathLayer.layer_id,
			topology.TopoElementArray_agg(ARRAY[rec.edge_id,2])
		)
		;
		--RAISE DEBUG 'newPath: %', newPath;

		newPathId := topo_update.insert_feature(
			newPath,
			payloadProps,
			pathLayerTable,
			pathLayerGeomColumn,
			pathLayerIdColumn,
			colMapForNewPath
		);

		RAISE DEBUG '%: Path % created to cover input line edge %',
			proc_name, newPathId, rec.edge_id;

		-- Return info about the created Path
		fid := newPathId;
		RETURN NEXT;
	END LOOP;

END;
$BODY$ LANGUAGE plpgsql; --}

