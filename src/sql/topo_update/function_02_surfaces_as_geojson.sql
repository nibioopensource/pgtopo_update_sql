--
-- This function takes a Surface (id, TopoGeometry and properties)
-- and reference to Border table and returns a set of GeoJSON
-- features in JSONB format that are needed to represent it.
--
-- Surface features spatial will contain a
-- "geometry_properties" element describing Border rings
-- forming the exterior ("exterior") and the interior ("interiors")
-- rings of the Surface. Rings are specified as arrays of
-- Border identifiers in the order they would be encountered by
-- walking against them from inside the Surface area and are prefixed
-- with a "-" character when they are walked from the end point to the
-- start point. This means the Surface area will be always found on
-- the left hand of the walk, thus Borders forming the shell will be
-- listed in CCW order and Borders forming the holes will be listed
-- in CW order.
--
-- Border features will only be returned if not already found
-- in the given 'visitedBorders' table, expected to contain
-- a single 'fid' field containing the border identifier
--
--
CREATE OR REPLACE FUNCTION topo_update._surface_as_geojson_features(
	-- The topology
	topo topology.topology,
	-- The Surface layer
	surfaceLayer topology.layer,
	-- The Surface ID value
	surfaceID TEXT,
	-- The Surface TopoGeometry
	surfaceTopoGeom topology.TopoGeometry,
	-- The Surface properties (pre-mapped)
	surfaceProps JSONB,
	-- Border layer table
	borderLayerTable regclass,
	-- Border layer TopoGeometry column name
	borderLayerTopoGeomColumn name,
	-- Border layer ID column name
	borderLayerIdColumn name,
	-- Mapping file for border layer
	borderLayerColMap JSONB,
	-- Table holding "visited" borders, expected
	-- to contain a single 'fid' TEXT field containing
	-- the identifiers of visited Border objects
	visitedBorders REGCLASS,
	-- Feature JSON modifier, for added (non-visited) borders
	-- (the added surface will be modified by caller)
	featureJsonModifier REGPROC DEFAULT NULL,
	-- User argument to featureJsonModifier function
	featureJsonModifierArg ANYELEMENT DEFAULT NULL::int,

	-- Override projection
	--
	-- If value of this parameter is not-null use the
	-- specified CRS for the output GeoJSON, reprojecting
	-- to that system all the geometries.
	--
	outSRID INT DEFAULT NULL,


	-- Specify max number of decimal digits to use in GeoJSON output
	--
	-- The maxdecimaldigits argument may be used when calling
	-- ST_AsGeoJSON to change the number of decimal digits used
	-- in output
	--
	maxdecimaldigits INT DEFAULT 9

)
RETURNS SETOF JSONB AS
$BODY$ -- {
DECLARE
	sql TEXT;
	borderRewriteSQL TEXT;
	surfaceRewriteSQL TEXT;
	rec RECORD;
	surfaceBorders TEXT[];
	feature JSONB;
	surfaceGeom GEOMETRY;
	borderGeom GEOMETRY;
	surfacePosition GEOMETRY;
	exteriorRing JSONB;
	exteriorRingGeom GEOMETRY;
	interiorRings JSONB[];
	interiorRingGeoms GEOMETRY[];
	procName TEXT := 'topo_update._surface_as_geojson_features';
	borderProps JSONB;
	prec FLOAT8 := pow(10, -maxdecimaldigits);
BEGIN


	--------------------------------------------------------
	-- Find the Surface's exterior and interior rings
	--------------------------------------------------------

	-- Create a temporary "surface_borders" table
	FOR rec IN
		SELECT * FROM topo_update.get_surface_borders(
			topo,
			surfaceTopoGeom,
			borderLayerTable,
			borderLayerTopoGeomColumn,
			borderLayerIdColumn
		)
	LOOP

		IF exteriorRing IS NULL
		THEN
			exteriorRing := to_jsonb(rec.signedRingBorders);
			exteriorRingGeom := ST_LineMerge(ST_Collect(rec.ringBorderGeoms));
		ELSE
			interiorRings := array_append(interiorRings, to_jsonb(rec.signedRingBorders));
			interiorRingGeoms := array_append(interiorRingGeoms, ST_LineMerge(ST_Collect(rec.ringBorderGeoms)));
		END IF;

		surfaceBorders := surfaceBorders || rec.ringBorders;

	END LOOP;


	RAISE DEBUG '%: Surface % exteriorRing: %', procName, surfaceID, exteriorRing;
	RAISE DEBUG '%: Surface % interiorRings: %', procName, surfaceID, interiorRings;

	-------------------------------------------------
	-- Write and return the unvisited Border objects
	-------------------------------------------------

	-- Query to:
	--   - extract unvisited Borders data
	--   - mark unvisited Borders as visited
	--
	-- Parameters:
	--   $1 - Surface Border identifiers as a TEXT[]
	sql := format(
		$$
			WITH unvisitedBorders AS (
				INSERT INTO %4$s
				SELECT b FROM unnest($1) b
				WHERE b NOT IN ( SELECT fid FROM %4$s )
				RETURNING fid
			)
			SELECT
				b border_record,
				b.%1$I::text border_id,
				b.%2$I border_topogeom
			FROM %3s b, unvisitedBorders ub
			WHERE b.%1$I::TEXT = ub.fid
			ORDER BY b.%1$I
		$$,
		borderLayerIdColumn,        -- %1
		borderLayerTopoGeomColumn,  -- %2
		borderLayerTable,           -- %3
		visitedBorders              -- %4
	);
	RAISE DEBUG '%: SQL: %', procName, sql;

	-- Write and return each unvisited Border
	FOR rec IN EXECUTE sql USING surfaceBorders
	LOOP --{

		-- Build properties of the Border
		borderProps := topo_update.record_to_jsonb(
			rec.border_record,
			borderLayerColMap
		);

		IF borderProps IS NULL THEN
			RAISE EXCEPTION 'Properties for border % as extracted by colMap % are NULL',
				to_jsonb(rec.border_record), borderLayerColMap;
		END IF;

		-- Write and return the unvisited Border Feature object

		borderGeom = ST_CollectionHomogenize(
			rec.border_topogeom::geometry
		);

		IF outSRID IS NOT NULL
		THEN
			borderGeom := ST_Transform(borderGeom, outSRID);
		END IF;

		-- Remove points that will be seen as repeated
		-- when precision is reduced due to number of
		-- decimal digits
		borderGeom := ST_RemoveRepeatedPoints(borderGeom, prec);

		feature := jsonb_build_object(
			'type', 'Feature',
			'geometry', ST_AsGeoJSON(
				borderGeom,
				maxdecimaldigits => maxdecimaldigits,
				options => 0
			)::jsonb,
			'properties', borderProps
		);

		IF featureJsonModifier IS NOT NULL
		THEN
			-- TODO: take borderRewriteSQL as parameter
			IF borderRewriteSQL IS NULL
			THEN
				IF featureJsonModifierArg IS NULL
				THEN
					borderRewriteSQL := format(
						$$
							SELECT * FROM %s(feat => $1, role => 'B')
						$$,
						featureJsonModifier::text
					);
				ELSE
					borderRewriteSQL := format(
						$$
							SELECT * FROM %s(feat => $1, role => 'B', usr => $2)
						$$,
						featureJsonModifier::text
					);
				END IF;
				RAISE DEBUG 'Border JSON Modifier SQL: %', borderRewriteSQL;
			END IF;
			-- invoke borderRewriteSQL
			EXECUTE borderRewriteSQL
				USING feature, featureJsonModifierArg
				INTO feature;
		END IF;

		RETURN NEXT feature;

	END LOOP; -- }

	-----------------------------------------------
	-- Write and return the Surface Feature object
	-----------------------------------------------

	-- Build the Surface geometry, to be used
	-- for writing the "geometry" GeoJSON Feature
	-- attribute
	--
	IF interiorRingGeoms IS NULL
	THEN
		surfaceGeom := ST_MakePolygon(
			exteriorRingGeom
		);
	ELSE
		surfaceGeom := ST_MakePolygon(
			exteriorRingGeom,
			interiorRingGeoms
		);
	END IF;

	-- Reproject to output SRID, if requested
	IF outSRID IS NOT NULL
	THEN
		-- TODO: drop this, ensure borders are transformed first
		surfaceGeom := ST_Transform(surfaceGeom, outSRID);
	END IF;

	-- Remove points that will be seen as repeated
	-- when precision is reduced due to number of
	-- decimal digits
	-- TODO: do this at the border level, no need to do it again
	surfaceGeom := ST_RemoveRepeatedPoints(surfaceGeom, prec);

	-- Sanity check
	IF ST_NumGeometries(surfaceGeom) > 1 THEN
		RAISE EXCEPTION '%: Surface % is unexpectedly a multi-geometry: %',
			procName, surfaceID, ST_Summary(surfaceGeom);
	END IF;

	-- Find a point on the Surface
	-- TODO: get point on surface directly from the TopoGeometry
	--       see https://trac.osgeo.org/postgis/ticket/4865
	surfacePosition := ST_PointOnSurface(surfaceGeom);

	feature := jsonb_build_object(
		'type', 'Feature',
		'geometry', ST_AsGeoJSON(
			surfaceGeom,
			maxdecimaldigits => maxdecimaldigits,
			options => 0
		)::jsonb,
		'properties', surfaceProps,
		'geometry_properties', jsonb_build_object(
			'position', jsonb_build_array(
				round(ST_X(surfacePosition)::numeric, maxdecimaldigits),
				round(ST_Y(surfacePosition)::numeric, maxdecimaldigits)
			),
			'exterior', exteriorRing
		)
	);

	IF interiorRings IS NOT NULL THEN
		feature := jsonb_set(
			feature,
			'{geometry_properties,interiors}',
			to_jsonb(interiorRings)
		);
	END IF;

	IF featureJsonModifier IS NOT NULL
	THEN
		-- TODO: take surfaceRewriteSQL as parameter
		IF surfaceRewriteSQL IS NULL
		THEN
			IF featureJsonModifierArg IS NULL
			THEN
				surfaceRewriteSQL := format(
					$$
						SELECT * FROM %s(feat => $1, role => 'S')
					$$,
					featureJsonModifier::text
				);
			ELSE
				surfaceRewriteSQL := format(
					$$
						SELECT * FROM %s(feat => $1, role => 'S', usr => $2)
					$$,
					featureJsonModifier::text
				);
			END IF;
			RAISE DEBUG 'Surface JSON Modifier SQL: %', surfaceRewriteSQL;
		END IF;
		-- invoke surfaceRewriteSQL
		EXECUTE surfaceRewriteSQL
			USING feature, featureJsonModifierArg
			INTO feature;
	END IF;

	RETURN NEXT feature;

END;
$BODY$ --}
LANGUAGE plpgsql;

--
-- This function takes Surface objects from a topological layer
-- and encodes them and the Border objects required to build
-- them into a topological GeoJSON JSONB format.
--
-- Attributes for the Surface features and Border features
-- will be included in the returned GeoJSON based on mapping files
--
-- Surface features will contain a "geometry_properties" element
-- (at the same level as the "geometry" and "properties" elements)
-- being an object with a "position" element being an array of X,Y
-- coordinates of a point on the surface, an "exterior" element
-- and an optional "interiors" element being arrays of Border
-- identifiers forming the rings of the Surface.
-- Border identifiers are prefixed with an optional "-" sign when
-- the Surface area will be on their right side rather than on their
-- left side.
--
--
CREATE OR REPLACE FUNCTION topo_update.surfaces_as_geojson(

	surfaceLayerTable regclass,
	surfaceLayerTopoGeomColumn name,
	surfaceLayerIDColumn name,
	surfaceLayerColMap JSONB, -- See json_props_to_pg_cols

	borderLayerTable regclass,
	borderLayerTopoGeomColumn name,
	borderLayerIdColumn name,
	borderLayerColMap JSONB, -- See json_props_to_pg_cols

	-- Array of surface identifiers values
	surfaceIds TEXT[],

	-- If this parameter is given, use the given
	-- function to modify JSONB Features written
	-- for extracted Surfaces or their required Borders
	--
	-- The function is required to accept the following parameters:
	--
	--	o INOUT feat JSONB
	--		(mandatory)
	--		Represent the GeoJSON feature as built using the given
	--		column mamppings (surfaceLayerColMap and borderLayerColMap)
	--
	--  o role CHAR
	--		(mandatory)
	--		Represents the feauture role: 'S' for Surface and 'B' for Border
	--
	--  o usr ANYELEMENT
	--		(optional)
	--		The value of the featureJsonModifierArg parameter passed
	--		to this function.
	--
	-- The function's optionally modified INOUT `feat` parameter
	-- will be used in the GeoJSON feature collection returned by
	-- this function.
	--
	-- If not given, the GeoJSON feature will solely be built
	-- using the provided column maps.
	--
	featureJsonModifier REGPROC DEFAULT NULL,

	-- User argument to be passed to featureJsonModifier.
	-- Can be omitted to not pass such parameter.
	featureJsonModifierArg ANYELEMENT DEFAULT NULL::int,

	-- Override projection
	--
	-- If value of this parameter is not-null use the
	-- specified CRS for the output GeoJSON, reprojecting
	-- to that system all the geometries.
	--
	outSRID INT DEFAULT NULL,

	-- Specify max number of decimal digits to use in GeoJSON output
	--
	-- The maxdecimaldigits argument may be used when calling
	-- ST_AsGeoJSON to change the number of decimal digits used
	-- in output
	--
	maxdecimaldigits INT DEFAULT 9

)
RETURNS JSONB AS
$BODY$ -- {
DECLARE
	rec RECORD;
	sql TEXT;
	crs TEXT;
	crsSRID INT;
	geojson JSONB;
	topo topology.topology;
	surfaceLayer topology.layer;
	surfaceProps JSONB;
	surfaceTopoGeo topology.TopoGeometry;
	surfaceFeatures JSONB[];
	geojsonFeatures JSONB[] = ARRAY[]::JSONB[];
	procName TEXT := 'topo_update.surfaces_as_geojson';
BEGIN

	-- Create a table to keep track of visited borders
	CREATE TEMPORARY TABLE visitedBorders(fid TEXT PRIMARY KEY);

	sql := format(
		$$
			SELECT
			s as surface_record,
			s.%3$I as surface_topogeom,
			s.%2$I::TEXT AS surface_id
			FROM %1$s s
			WHERE %2$I::TEXT = ANY($1)
			ORDER BY %2$I
		$$,
		surfaceLayerTable,
		surfaceLayerIDColumn,
		surfaceLayerTopoGeomColumn
	);
	RAISE DEBUG '%: SQL": %', procName, sql;
	FOR rec IN EXECUTE sql USING surfaceIds
	LOOP --{

		IF topo IS NULL THEN
			-- Fetch topology info from Surface TopoGeometry
			SELECT *
				FROM topology.topology
				WHERE id = topology_id(rec.surface_topogeom)
				INTO topo;
			IF NOT FOUND THEN
				RAISE EXCEPTION
					'Cannot find topology (id=%) '
					'referenced by TopoGeometry of Surface %',
					topology_id(rec.surface_topogeom),
					rec.surface_id
				;
			END IF;

			-- Fetch layer info from Surface TopoGeometry
			SELECT *
				FROM topology.layer
				WHERE topology_id = topo.id
				AND layer_id = layer_id(rec.surface_topogeom)
				INTO surfaceLayer;
			IF NOT FOUND THEN
				RAISE EXCEPTION
					'Cannot find layer (id=%) of topology % '
					'referenced by TopoGeometry of Surface %',
					layer_id(rec.surface_topogeom),
					topo.name,
					rec.surface_id
				;
			END IF;
		ELSE
			-- Sanity check: all Surface TopoGeometry objects are defined
			-- against the SAME topology
			IF topology_id(rec.surface_topogeom) != topo.id
			THEN
				RAISE EXCEPTION 'TopoGeometry of Surface % is defined on'
					' Topology % instead of % (%)',
					rec.surface_id,
					topology_id(rec.surface_topogeom),
					topo.id,
					topo.name
				;
			END IF;
			-- Sanity check: all Surface TopoGeometry objects are defined
			-- against the SAME topology layer
			IF layer_id(rec.surface_topogeom) != surfaceLayer.layer_id
			THEN
				RAISE EXCEPTION 'TopoGeometry of Surface % is defined on'
					' layer % instead of % (%.%.%)',
					rec.surface_id,
					layer_id(rec.surface_topogeom),
					surfaceLayer.layer_id,
					surfaceLayer.schema_name,
					surfaceLayer.table_name,
					surfaceLayer.feature_column
				;
			END IF;
		END IF;

		-- Build properties of the Surface
		surfaceProps := topo_update.record_to_jsonb(
			rec.surface_record,
			surfaceLayerColMap
		);
		IF surfaceProps IS NULL THEN
			RAISE EXCEPTION 'Properties for surface % as extracted by colMap % are NULL',
				to_jsonb(rec.surface_record), surfaceLayerColMap;
		END IF;

		RAISE DEBUG '%: surface % has props %', procName, rec.surface_id, surfaceProps;

		SELECT array_agg(f) FILTER (WHERE f IS NOT NULL)
		FROM topo_update._surface_as_geojson_features(
			topo,
			surfaceLayer,
			rec.surface_id,
			rec.surface_topogeom,
			surfaceProps,
			borderLayerTable,
			borderLayerTopoGeomColumn,
			borderLayerIdColumn,
			borderLayerColMap,
			'pg_temp.visitedBorders'::regclass,
			featureJsonModifier,
			featureJsonModifierArg,
			outSRID,
			maxdecimaldigits
		) f
		INTO surfaceFeatures;

		RAISE DEBUG '%: surface features: %', procName, surfaceFeatures;

		-- Append surface features to the global features array
		geojsonFeatures := geojsonFeatures || surfaceFeatures;

	END LOOP; --}

	DROP TABLE pg_temp.visitedBorders;

	IF outSRID IS NOT NULL
	THEN
		IF outSRID = 0 THEN
			RAISE EXCEPTION 'If an output SRID is given, it cannot be 0';
		END IF;
		crsSRID = outSRID;
	ELSE
		crsSRID = topo.srid;
	END IF;

	-- Initialize geojson with CRS member, if SRID is known
	IF crsSRID = 0 THEN
		geojson := jsonb_build_object();
	ELSE
		SELECT auth_name || ':' || auth_srid
		FROM public.spatial_ref_sys
		WHERE srid = crsSRID
		INTO crs;

		IF FOUND THEN
			geojson := jsonb_build_object(
				'crs', jsonb_build_object(
					'properties', jsonb_build_object('name', crs),
					'type', 'name'
				)
			);
		ELSE
			RAISE EXCEPTION 'Could not found spatial_ref_sys entry for SRID %', crsSRID;
		END IF;
	END IF;

	RAISE DEBUG '%: GeoJSON header: %', procName, geojson;

	-- Add features and type to the GsoJSON
	geojson := geojson || jsonb_build_object(
		'type', 'FeatureCollection',
		'features', to_jsonb(geojsonFeatures)
	);

	RAISE DEBUG '%: GeoJSON being returned: %', procName, geojson;

	RETURN geojson;
END;
$BODY$ --}
LANGUAGE plpgsql;
