CREATE OR REPLACE FUNCTION topo_update._app_do_GetMergeableSurfaceBordersAsGeoJSON_Dynamic(
	appname NAME,
	appconfig JSONB,
	bbox GEOMETRY,
	out_srid INTEGER,
	max_decimal_digits INTEGER
)
RETURNS JSONB AS
$BODY$ --{
DECLARE
	cfg_borderLayer JSONB;
	cfg_borderLayerTable JSONB;
	cfg_surfaceLayer JSONB;
	cfg_surfaceLayerTable JSONB;
	cfg_op JSONB;
	cfg_op_params JSONB;
	cfg JSONB;
	surfaceLayerTableName NAME;
	surfaceLayerTableRegclass REGCLASS;
	surfaceLayerGeomColumn NAME;
	surfaceLayerIdColumn NAME;
	borderLayerTableName NAME;
	borderLayerTableRegclass REGCLASS;
	borderLayerGeomColumn NAME;
	borderLayerIdColumn NAME;
	rec RECORD;
	sql TEXT;
	geojsonFeatures JSONB;
	out JSONB;
	borderIds TEXT[];
	procName TEXT := 'app_do_GetMergeableSurfaceBordersAsGeoJSON_Dynamic';
	crsSRID INT;
	crs TEXT;
BEGIN

	-- Check if the operation is enabled
	IF NOT appconfig -> 'operations' ? 'SurfaceMerge' THEN
		RAISE EXCEPTION 'SurfaceMerge operation not enabled for application %', appname;
	END IF;

	cfg_surfaceLayer := appconfig -> 'surface_layer';
	IF cfg_surfaceLayer IS NULL THEN
		RAISE EXCEPTION 'GetMergeableSurfaceBordersAsGeoJSON operations requires missing "surface_layer" in appconfig';
	END IF;
	cfg_borderLayer := appconfig -> 'border_layer';
	IF cfg_borderLayer IS NULL THEN
		RAISE EXCEPTION 'GetMergeableSurfaceBordersAsGeoJSON operations requires missing "border_layer" in appconfig';
	END IF;
	cfg_op := appconfig -> 'operations' -> 'SurfaceMerge';

	--RAISE DEBUG 'cfg_surfacelayer: %', cfg_surfaceLayer;
	--RAISE DEBUG 'cfg_borderLayer: %', cfg_borderLayer;

	FOR rec IN SELECT jsonb_array_elements( appconfig -> 'tables' ) cfg
	LOOP --{
		IF rec.cfg ->> 'name' = cfg_surfacelayer ->> 'table_name' THEN
			cfg_surfaceLayerTable := rec.cfg;
		ELSIF rec.cfg ->> 'name' = cfg_borderlayer ->> 'table_name' THEN
			cfg_borderLayerTable := rec.cfg;
		END IF;
		IF cfg_borderLayerTable IS NOT NULL AND
		   cfg_surfaceLayerTable IS NOT NULL
		THEN
			EXIT;
		END IF;
	END LOOP; --}

	--RAISE DEBUG 'cfg_surfaceLayerTable: %', cfg_surfaceLayerTable;
	--RAISE DEBUG 'cfg_borderLayerTable: %', cfg_borderLayerTable;

	surfaceLayerTableName := cfg_surfaceLayer ->> 'table_name';
	surfaceLayerTableRegclass := appname || '.' || surfaceLayerTableName;
	surfaceLayerGeomColumn := cfg_surfacelayer ->> 'geo_column';
	surfaceLayerIdColumn := cfg_surfacelayerTable ->> 'primary_key';

	borderLayerTableName := cfg_borderlayer ->> 'table_name';
	borderLayerTableRegclass := appname || '.' || borderLayerTableName;
	borderLayerGeomColumn := cfg_borderlayer ->> 'geo_column';
	borderLayerIdColumn := cfg_borderlayerTable ->> 'primary_key';

	IF out_srid IS NOT NULL
	THEN
		IF out_srid = 0 THEN
			RAISE EXCEPTION 'If an output SRID is given, it cannot be 0';
		END IF;
		crsSRID = out_srid;
	ELSE
		crsSRID = COALESCE( ( appconfig -> 'topology' -> 'srid' )::int, 0 );
	END IF;

	RAISE DEBUG '%: GeoJSON header: %', procName, out;

	sql := topo_update._GetMergeableSurfaceBorders_Query(
		appname,
		cfg_op,
		borderLayerIdColumn,
		borderLayerTableName,
		borderLayerGeomColumn,
		surfaceLayerTableName,
		surfaceLayerGeomColumn,
		surfaceLayerIdColumn,
		bbox
	);

	sql := format(
		$$
			SELECT to_jsonb(array_agg(ST_AsGeoJSON(bar, 'geom', %1$L)::jsonb))
			FROM (
				SELECT
					ST_Transform(
						border_visible_geom,
						COALESCE(
							%2$L,
							ST_Srid(border_visible_geom)
						)
					) geom,
					border_ids,
					side_faces
				FROM ( %3$s ) foo
				ORDER BY border_ids -- make predictable
			) bar
		$$,
		max_decimal_digits,    -- %1
		out_srid,              -- %2
		sql                    -- %3
	);

	-- RAISE DEBUG 'SQL: %', sql;

	EXECUTE sql USING bbox INTO geojsonFeatures;

	IF geojsonFeatures IS NULL THEN
		RETURN NULL; -- TODO: return something nicer here, maybe
	END IF;

	-- Initialize geojson with CRS member
	IF crsSRID = 0 THEN
		out := jsonb_build_object();
	ELSE
		SELECT auth_name || ':' || auth_srid
		FROM public.spatial_ref_sys
		WHERE srid = crsSRID
		INTO crs;

		IF NOT FOUND THEN
			RAISE EXCEPTION 'Could not found spatial_ref_sys entry for SRID %', out_srid;
		END IF;

		-- Initialize geojson with CRS member, if SRID is known
		out := jsonb_build_object(
			'crs', jsonb_build_object(
				'properties', jsonb_build_object('name', crs),
				'type', 'name'
			)
		);
	END IF;

	-- Add features and type to the GsoJSON
	out := out || jsonb_build_object(
		'type', 'FeatureCollection',
		'features', geojsonFeatures
	);

	RETURN out;

END;
$BODY$ LANGUAGE plpgsql; --}

CREATE OR REPLACE FUNCTION topo_update.app_do_GetMergeableSurfaceBordersAsGeoJSON(

	-- Application name, will be used to fetch
	-- configuration from application schema.
	-- See doc/APP_CONFIG.md
	appname NAME,

	-- A Geometry object whose bounding box will be used
	-- to restrict the Borders that will be returned.
	-- Only intesecting Borders will be considered.
	bbox GEOMETRY,

	-- Identifier of the spatial reference system to use for the
	-- output arc coordinates. If NULL (the default), it will use the app topology SRID.
	out_srid INTEGER DEFAULT NULL,

	-- Maximum number of decimal digits to use for the arc coordinates.
	-- Defaults to 9
	max_decimal_digits INTEGER DEFAULT 9
)
RETURNS JSONB AS
--
-- Returns Border features in GeoJSON format, see
-- https://datatracker.ietf.org/doc/html/rfc7946
--
$BODY$ --{
DECLARE
	sql TEXT;
	appconfig JSONB;
	out JSONB;
BEGIN

	sql := format('SELECT cfg FROM %I.app_config',
			appname || '_sysdata_webclient_functions');
	EXECUTE sql INTO STRICT appconfig;

	--RAISE WARNING 'APPCONFIG: %', appconfig;

	out := topo_update._app_do_GetMergeableSurfaceBordersAsGeoJSON_Dynamic(
		appname,
		appconfig,
		bbox,
		out_srid,
		max_decimal_digits
	);

	RETURN out;
END;
$BODY$ LANGUAGE plpgsql; --}
