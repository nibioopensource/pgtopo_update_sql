-- {
-- Build an overriding pgcol_map
--
-- The returned map will use keys found in the first
-- mapping file of the given array values taken from
-- the last mapping file containing such key, prefixing
-- such values with element key p# where # is the 0-based
-- index of the mapping file used.
--
-- Callers will need to take care of building the appropriate
-- payload to match the expected prefixing.
--
-- }{
CREATE OR REPLACE FUNCTION topo_update._json_col_map_override(
	-- Column mapping files
	maps JSONB[]
)
RETURNS JSONB
AS $BODY$
DECLARE
	rec RECORD;
	map JSONB;
BEGIN

	IF array_upper(maps, 1) != 2 THEN
		RAISE EXCEPTION '_json_col_map_override only supports 2 maps at the moment';
	END IF;

	map := '{}';
	FOR rec IN SELECT jsonb_object_keys(maps[1]) k
	LOOP
		-- Find right-most map containing the key

		map := jsonb_set(
			map,
			ARRAY[rec.k], -- pg col name (check composite!)
			CASE WHEN maps[2] ? rec.k THEN
				-- Take col value from payload
				to_jsonb(ARRAY['p' || 1]) || (
					maps[2] -> rec.k
				)
			ELSE
				-- Take col value from exiting Surface
				to_jsonb(ARRAY['p' || 0]) || (
					maps[1] -> rec.k
				)
			END
		);
	END LOOP;
	return map;
END;
$BODY$ LANGUAGE plpgsql; --}

-- {
--
-- Find a Surface which shares the longest boundary with the given set of Faces
--
-- }{
CREATE OR REPLACE FUNCTION topo_update._most_adjacent_surface(

	-- Topology record
	topo topology.topology,

	-- Faces on which we want to check adjacency from
	faces INT[],

	-- Surface layer
	surfaceLayer topology.layer,

	-- Surface layer primary key column name
	-- needed ? (maybe in case of same-length, to choose "highest" id?)
	surfaceLayerIdColumn NAME,

	-- Identifier of the "best" adjiacent surface found
	-- or NULL if no adjiacent surface is found
	OUT bestSurfaceId TEXT,

	-- Properties of the "best" adjiacent surface found,
	-- encoded in a JSONB (or NULL if no adjacent surface is found)
	OUT bestSurfaceProps JSONB
) -- }{
AS $BODY$
DECLARE
	--adjFaces INT[];
	rec RECORD;
	sql TEXT;
	procName TEXT := 'topo_update._most_adjacent_surface';
BEGIN
	--
	-- Find Surface TopoGeometry object containing any
	-- adjacent faces and pick the one with the longest
	-- shared boundary
	--
	sql := format(
		$$
WITH adjacentFaces AS (
	-- Find adjacent faces
	SELECT face_id, sum(len) len
	FROM (
		SELECT
			CASE WHEN left_face = ANY($1) THEN right_face
			ELSE left_face END face_id,
			ST_Length(geom) len
		FROM %1$s.edge
		WHERE ( left_face = ANY($1) ) <> ( right_face = ANY($1) )
		AND left_face > 0
		AND right_face > 0
	) foo
	GROUP BY face_id
), adjacentSurfaces AS (
	SELECT
		surface.%2$I surface_id,
		sum(adjFace.len) len,
		surface surface_record
	FROM
		%3$I.%4$I surface,
		adjacentFaces adjFace,
		%1$I.relation
	WHERE
		-- TopoGeometry contains any of the
		-- adjacentFaces
		relation.topogeo_id = id(%5$I)
		AND relation.layer_id = layer_id(%5$I)
		AND relation.element_id = adjFace.face_id
	GROUP BY surface_id
)
SELECT surface_id, surface_record, len border_len
FROM adjacentSurfaces
ORDER BY
	len DESC,
	-- in case of same shared-edges len, pick Surface with highest id
	surface_id DESC
LIMIT 1
		$$,
		topo.name,
		surfaceLayerIdColumn,
		surfaceLayer.schema_name,
		surfaceLayer.table_name,
		surfaceLayer.feature_column
	);
	RAISE DEBUG '%: SQL %', procName, sql;
	EXECUTE sql USING faces INTO rec;
	IF rec.surface_id IS NULL THEN
		RETURN;
	END IF;

	RAISE DEBUG '%: Surface % has shared border length %',
		procName, rec.surface_id, rec.border_len;

	-- Extract id of best surface
	bestSurfaceId = rec.surface_id;

	-- Extract properties of best surface
	bestSurfaceProps := to_jsonb(rec.surface_record)
		- surfaceLayer.feature_column -- drop TopoGeometry column from props
		- surfaceLayerIdColumn -- drop ID column from props
	;
	RAISE DEBUG '%: best Surface props: %', procName, bestSurfaceProps;

END;
$BODY$ LANGUAGE plpgsql; --}

-- Given an array of face identifiers, form groups of
-- adjacent faces that are not separated by Border features
-- {
CREATE OR REPLACE FUNCTION topo_update._add_border_split_surface_mergeable_faces(
	-- The topology
	topo topology.topology,
	-- The border layer
	borderLayer topology.layer,
	-- Array of face identifiers
	face_ids int[]
)
RETURNS SETOF topology.TopoElementArray -- return groups of face TopoElement arrays
AS $BODY$ --}{
DECLARE
	id INT;
	sql TEXT;
	mergeable_ids INT[];
	teary topology.TopoElementArray;
BEGIN

	LOOP --{
		id := face_ids[1];
		IF id IS NULL THEN
			EXIT;
		END IF;

		face_ids := array_remove(face_ids, id);

		IF cardinality(face_ids) = 0 THEN
			-- Shortcut (TODO: drop?)
			RAISE DEBUG 'No more faces to find!';
			RETURN NEXT ARRAY[ARRAY[id, 3]];
			EXIT;
		END IF;

		RAISE DEBUG 'More than a face to evaluate for merge';

		CREATE TEMPORARY TABLE face_set(fid INT unique, checked BOOL DEFAULT FALSE);
		INSERT INTO pg_temp.face_set(fid, checked) VALUES (id, true);

		LOOP --{

			sql := format(
				$$
SELECT
	array_agg(
		DISTINCT
		CASE WHEN $1 = e.left_face THEN
			e.right_face
		ELSE
			e.left_face
		END
	)
FROM %1$I.edge e
WHERE ( e.left_face = $1 OR e.right_face = $1 )
  AND e.left_face != e.right_face
  -- TODO: this part should be optimized
  AND e.edge_id NOT IN (
	SELECT abs(r.element_id)
	FROM %1$I.relation r
	WHERE r.layer_id = %2$L
  )
				$$,
				topo.name, -- %1
				borderLayer.layer_id -- %2
			);
			RAISE DEBUG 'SQL: %', sql;
			EXECUTE sql
			INTO mergeable_ids
			USING id;

			RAISE DEBUG 'Faces mergeable with %: %', id, mergeable_ids;

			INSERT INTO pg_temp.face_set(fid)
			SELECT unnest(mergeable_ids)
			ON CONFLICT DO NOTHING;

			SELECT fid FROM pg_temp.face_set
			WHERE NOT checked
			ORDER BY fid LIMIT 1
			INTO id;

			IF id IS NULL THEN
				EXIT;
			END IF;

			UPDATE pg_temp.face_set
			SET checked = true
			WHERE fid = id;

		END LOOP; --}

		-- Remove each of the mergeable faces from face_ids
		SELECT array_agg(x) FROM (
			SELECT unnest(face_ids) x
				EXCEPT
			SELECT fid FROM pg_temp.face_set
		) foo
		INTO face_ids;

		-- Build a TopoElementArray with all
		-- all mergeable ids
		SELECT array_agg(ARRAY[fid,3])
		FROM pg_temp.face_set
		INTO teary;
		RETURN NEXT teary;

		DROP TABLE pg_temp.face_set;

	END LOOP; --}

END;
$BODY$ LANGUAGE plpgsql; --}

-- {
-- This function will be called from the client when the user
-- draws a line to either create a new surface or extend an
-- existing one.
--
-- If the line will not form a new surface or modify
-- an existing one, an exception is thrown, otherwise:
--
--   1. The line, with associated attributes, is added into
--      the border layer, using json attributes mapped using
--      the colMapProviderFunc provided mapping file.
--
--   2. Surfaces split by the Border are recognized and split
--      in the Surface layer, by removing newly formed faces
--      from their definition and creating new surfaces covering
--      their lost spatial portions. Newly formed faces which
--      are not covered by any existing surface will result in
--      a new surface being created by "eroding" the space from
--      the implicit "universal surface".
--
--      Attributes for new Surfaces will default to attributes
--		of the Surfaces they are most closely related to:
--		Surfaces previosuly covering the new Surface area or
--		Surfaces sharing the longest border with the new Surface.
--
--      Attributes for both new surfaces and update surfaces (those
--      "shrinked" by the split) will be subsequently overridden with
--      attributes found in the JSON payload, using the corresponding
--      mapping file provided by colMapProviderFunc callback.
--
--   3. Borders split by the drawn line are recognized and split
--      in the Border layer, by removing all but one edge
--      ending at any node also covered by the drawn line
--      from their definition and creating new borders covering
--      their lost spatial portions.
--
-- # RETURN VALUE
--
-- The function returns a table containig information about
-- the created and splitted borders and surfaces; the
-- returned table has fields:
--
--	- fid text
--
--		Feature identifier (text representation of the value of
--		the specified layer's IdColumn)
--
--	- typ char
--
--		Character representing the type of Feature the record
--		is about, with these possible values:
--
--			S	The Feature is a Surfaces
--
--			B	The Feature is a Border
--
--	- act char
--
--		Character representing the action taken on the Feature,
--		with the following possible values:
--
--			M	The Feature was modified loosing space
--				due to a split.
--
--			C	The Feature was created independently from
--				previously existing Features.
--
--			S	The Feature was created by taking up space
--				space previosuly taken by previosly existing
--				Features. When such a Feature is created, there
--				should also be at least another Feature of the
--				same type which is returned as modified (M).
--
--			D	The Feature was deleted. This can currently
--				only happen for Surface features if `keepModifiedSurface`
--				parameter is set to false.
--
--  - frm text
--
--		Identifier of the Feature previously covering the space
--		now covered by the new Feature. This is NULL for all but
--		splitted (act=S) features.
--
-- }{
CREATE OR REPLACE FUNCTION topo_update.add_border_split_surface(

	-- A GeoJSON Feature object representing the
	-- line drawn by the user and the holding the
	-- attributes to use with the border and the
	-- surface layers
	feature JSONB,

	-- Surface layer table
	surfaceLayerTable REGCLASS,

	-- Surface layer TopoGeometry column name
	surfaceLayerGeomColumn NAME,

	-- Surface layer primary key column name
	surfaceLayerIdColumn NAME,

	-- Border layer table
	borderLayerTable REGCLASS,

	-- Name of Border layer TopoGeometry column
	borderLayerGeomColumn NAME,

	-- Border layer primary key column name
	borderLayerIdColumn NAME,

	-- Snap tolerance to use when
	-- inserting the new line
	-- in the topology
	tolerance float8,

	-- Function returning mapping file for layer features
	-- based on action being performed.
	--
	-- The function is expected to take 3 named parameters:
	--
	--	typ
	--		Type of Feature layer we're mapping for: 'B' for Border
	--		and 'S' for Surface
	--
	--	act
	--		Action performed on Feature (see RETURN VALUE section)
	--
	--	usr
	--		User-defined parameter (value of colMapProviderParam)
	--
	--
	-- The function is expected to return a single JSONB value
	-- representing the mapping between the Feature's properties
	-- and the PostgreSQL columns for the target layer.
	-- See json_props_to_pg_cols for documentation of the
	-- format of this mapping file
	--
	colMapProviderFunc REGPROC,

	-- User defined parameter to pass to the colMapProvider function
	colMapProviderParam ANYELEMENT,

	-- Minimum area of newly created face, in squared topology units
	minToleratedFaceArea FLOAT8 DEFAULT NULL,

	-- Maximum number of surface splits allowed, defaults to 5
	-- This is the max number of new surfaces an input operation may changed spatialy. If the area has changed the surface has changed.
	-- Other surfaces may be changed because of new vertex/node, but this are not relevant for this case
	maxAllowedSurfaceSplitCount INT DEFAULT 5,

	-- Two this is the max number of surfaces with new area or changed surface area after the splitt.
	-- but with multline we should accept 3 changed/new surfaces (1 changed and two new ones)
	maxNewSurfacesOrChangedAreaSurfaces INT DEFAULT 10,

	-- Retain the record of surfaces split by only updating their
	-- spatial component. If set to false those surfaces will be
	-- deleted and a new one will be created to replace them.
	keepModifiedSurface BOOLEAN DEFAULT true
)
RETURNS TABLE(fid text, typ char, act char, frm text) AS $BODY$
DECLARE

	rec RECORD;
	rec2 RECORD;

	featureGeom JSONB;

	payloadProps JSONB;

	newBorder topology.topogeometry;
	modBorderGeom GEOMETRY;
	newBorderId text;
	oldBorderProps JSONB;
	newBorderProps JSONB;

	newSurface topology.topogeometry;
	newSurfaceId text;
	oldSurfaceProps JSONB;
	newSurfaceProps JSONB;

	-- holds dynamic sql
	sql text;

	proc_name text = 'topo_update.add_border_split_surface';

	inputLine GEOMETRY;
	overlapPoints GEOMETRY;

	topo topology.topology;

	candidateInputLines GEOMETRY;
	dropInputLines INT[];

	inputLineEdgeIds INT[];
	inputLineNodeIds INT[];

	maxFaceBefore INT;
	newFacesCount INT;
	newFaces INT[];

	coveredNewFaces INT[];
	uncoveredNewFaces INT[];

	borderLayer topology.layer;
	surfaceLayer topology.layer;

	-- Holds dynamic sql to execute
	-- by passing $1=colMapProviderParam,
	-- $2=typ, $3=act to extract the
	-- appropriate column mapping
	colMapExtractSQL TEXT;

	-- Column mapping file for modified surfaces
	-- as provided by colMapProvider
	colMapForModSurface JSONB;

	-- Column mapping file for independent surfaces
	-- as provided by colMapProvider
	colMapForNewSurface JSONB;

	-- Column mapping file for surfaces replacing
	-- space previously covered by existing surfaces
	-- as provided by colMapProvider
	colMapForSplitSurface JSONB;

	-- Column mapping file for independent surfaces
	-- inheriting attributes from adjacent Surface
	inhNewSurfaceColMap JSONB;

	-- Column mapping file for split surfaces
	-- inheriting attributes from adjacent Surface
	inhSplitSurfaceColMap JSONB;

	-- Column mapping used for new surfaces
	-- (pointer to one of the *SurfaceColMap above)
	surfaceColMap JSONB;

	-- Column mapping file for modified borders
	-- as provided by colMapProvider
	colMapForModBorder JSONB;

	-- Column mapping file for borders
	-- as provided by colMapProvider
	colMapForNewBorder JSONB;

	-- Column mapping file for borders
	-- replacing space previously covered
	-- by existing borders as provided by
	-- colMapProvider
	colMapForSplitBorder JSONB;

	-- Column mapping file for split borders
	-- inheriting attributes from existing Border
	inhSplitBorderColMap JSONB;

	-- Border as split by incoming line
	splitBorderGeom GEOMETRY;
	splitBorderGeomComp GEOMETRY;
	splitBorderStartPoint GEOMETRY;
	splitBorderStartPointNodeDegree INT;
	splitBorderMergeOnStartPoint BOOLEAN;

	-- Number of elements a border was split into
	splitBorderCount INT;

	-- Number of surfaces being split
	splitSurfaceCount INT := 0;

	i INT;

	tea topology.TopoElementArray;

BEGIN

	-- Fetch topology and layer info from border layer references
	SELECT t topo, l layer
		FROM topology.layer l, topology.topology t, pg_class c, pg_namespace n
		WHERE l.schema_name = n.nspname
		AND l.table_name = c.relname
		AND c.oid = borderLayerTable
		AND c.relnamespace = n.oid
		AND l.feature_column = borderLayerGeomColumn
		AND l.topology_id = t.id
	--	FOR UPDATE, 2 problems, user needs SUPERUSER (can be fixed),
	--  ms used from 8601 -> 113606 for ((14.355156130507279 67.27980828078813, 14.354976710179235 67.28648792270606, 14.395077865039152 67.28664388346324, 14.395246132501715 67.27996419068565, 14.355156130507279 67.27980828078813))
		INTO rec;
	topo := rec.topo;
	borderLayer := rec.layer;

	--	RAISE NOTICE 'Lock on topology % obtained', topo.name;

	-- Fetch topology and layer info from surface layer references
	SELECT t topo, l layer
		FROM topology.layer l, topology.topology t, pg_class c, pg_namespace n
		WHERE l.schema_name = n.nspname
		AND l.table_name = c.relname
		AND c.oid = surfaceLayerTable
		AND c.relnamespace = n.oid
		AND l.feature_column = surfaceLayerGeomColumn
		AND l.topology_id = t.id
		INTO rec;

	IF id(topo) != id(rec.topo) THEN
		RAISE EXCEPTION 'Surface and Border layers need to be in same topology';
	END IF;
	surfaceLayer := rec.layer;

	featureGeom = feature->'geometry';
	IF featureGeom IS NULL
	THEN
		RAISE EXCEPTION 'Input feature lacks a "geometry" element: %',
			feature;
	END IF;

	IF NOT featureGeom ? 'crs'
	THEN
		RAISE EXCEPTION 'Input feature geometry lacks a "crs" element: %',
			featureGeom;
	END IF;

	-- Read input line geometry from GeoJSON
	inputLine := ST_GeomFromGeoJSON(featureGeom);

	IF topo.srid != ST_Srid(inputLine) THEN
		inputLine := ST_Transform(inputLine, topo.srid);
		--RAISE DEBUG 'REPROJECTED GEOM: %', ST_AsEWKT(geom);
	END IF;

	-- "Massage" input line
	RAISE DEBUG 'Input line: %', ST_AsText(inputLine);
	inputLine := topo_update.prepare_split_border(
		inputLine,
		topo,
		tolerance
	);
	RAISE DEBUG 'Prepared input line: %', ST_AsText(inputLine);

	-- Keep track of max face id in the topology
	sql := format('SELECT max(face_id) FROM %I.face', topo.name);
	RAISE DEBUG '%: sql: %', proc_name, sql;
	EXECUTE sql INTO maxFaceBefore;
	RAISE DEBUG '%: max face_id before inserting border: %', proc_name, maxFaceBefore;

	-- Write SQL to extract colMaps
	-- $1: usr
	-- $2: typ
	-- $3: act
	colMapExtractSQL := format(
		'SELECT * FROM %s(usr => $1, typ => $2, act => $3)',
		colMapProviderFunc
	);

	payloadProps := feature -> 'properties';

	candidateInputLines := ST_ForceCollection(inputLine);

	-- We only tolerate faces having an area of at least
	-- the given amoun of square topology units
	RAISE NOTICE 'Minimum tolerated face area: %', minToleratedFaceArea;

	-- Insert candidate input lines and iterate if any of
	-- them has to be refused
	LOOP --{

		--RAISE DEBUG 'Candidate input lines: %', ST_AsText(candidateInputLines);

		BEGIN --{

			-- Insert the inputLine to the topology
			-- and gather list of edges forming it
			SELECT array_agg(DISTINCT e)
			FROM ST_Dump(candidateInputLines) d,
			LATERAL topology.TopoGeo_addLineString(
					topo.name,
					d.geom,
					tolerance
				) e
			INTO inputLineEdgeIds;
			RAISE DEBUG '%: input line edges: %',
				proc_name, array_to_string(inputLineEdgeIds, ',');

			-- Remove any edge already used by existing borders
			-- from the the list of edges forming the new line
			sql := format(
				$$
		SELECT array_agg(x)
		FROM (
			SELECT x FROM (
				SELECT unnest($1) x
			) foo
			WHERE abs(x) NOT IN (
				SELECT abs(element_id)
				FROM %1$I.relation
				WHERE layer_id = %2$L
				AND element_type = 2 -- edge
			)
		) foo
				$$,
				topo.name,           -- %1
				borderLayer.layer_id -- %2
			);
			RAISE DEBUG '%: sql: %', proc_name, sql;
			EXECUTE sql
			USING inputLineEdgeIds
			INTO inputLineEdgeIds;

			RAISE DEBUG '%: input line edges not already used by existing borders: %',
				proc_name, array_to_string(inputLineEdgeIds, ',');

			-- Check which new edges should be dropped
			dropInputLines := NULL;

			-- Remove dangling edge
			-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/261
			sql := format(
				$$
					SELECT array_agg(edge_id)
					FROM %1$I.edge_data
					WHERE edge_id = ANY($1)
					AND left_face = right_face
				$$,
				topo.name
			);
			EXECUTE sql USING inputLineEdgeIds
			INTO dropInputLines;
			IF dropInputLines IS NOT NULL THEN
				RAISE NOTICE 'New edges % are dangling, dropping', dropInputLines;
			END IF;

			-- Remove any edge binding faces having an area
			-- below given optional area tolerance
			-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/157
			IF minToleratedFaceArea IS NOT NULL
			THEN --{

				sql := format(
					$$
			WITH newEdges AS (
				SELECT edge_id, left_face, right_face
				FROM %1$I.edge_data
				WHERE edge_id = ANY($1)
			), newBoundFaces AS (
				SELECT face_id, ST_Area(topology.ST_GetFaceGeometry(%1$L, f.face_id)) face_area
				FROM %1$I.face f
				WHERE face_id > 0
				AND face_id IN (
					SELECT left_face FROM newEdges
					UNION
					SELECT right_face FROM newEdges
				)
			)
			SELECT array_agg(DISTINCT e.edge_id) edge_ids, f.face_id, f.face_area
			FROM newBoundFaces f, newEdges e
			WHERE ( f.face_id = e.left_face OR f.face_id = e.right_face )
			AND face_area < $2
			GROUP BY f.face_id, f.face_area
					$$,
					topo.name
				);
				FOR rec IN EXECUTE sql USING inputLineEdgeIds, minToleratedFaceArea
				LOOP
					-- Collect IDs of edges to be dropped
					RAISE NOTICE 'New edges % binding too small face % (area %), dropping',
						rec.edge_ids, rec.face_id, rec.face_area;
					dropInputLines := array_cat(dropInputLines, rec.edge_ids);
				END LOOP;

			END IF; --}

			IF dropInputLines IS NOT NULL
			THEN --{
				RAISE NOTICE 'All input lines edges: %', inputLineEdgeIds;
				RAISE NOTICE 'Dropped input lines edges: %', dropInputLines;
				SELECT array_agg(x) FROM (
					SELECT abs(unnest(inputLineEdgeIds)) x
						EXCEPT
					SELECT unnest(dropInputLines)
				) foo
				INTO inputLineEdgeIds;
				-- Re-write candidate input lines as the list
				-- of edges we keep
				sql := format(
					$$
						SELECT ST_Collect(e.geom ORDER BY e.edge_id)
						FROM
							%1$I.edge e
						WHERE edge_id = ANY($1)
					$$,
					topo.name
				);
				EXECUTE sql USING inputLineEdgeIds
				INTO candidateInputLines;
				--RAISE DEBUG 'New candidate input lines num: %', ST_NumGeometries(candidateInputLines);

				IF candidateInputLines IS NULL THEN
				    IF minToleratedFaceArea IS NOT NULL THEN
					    RAISE EXCEPTION 'No usable input lines found with minimum tolerated face area: % for inputline % where isClosed = %' ,
					    minToleratedFaceArea , ST_AsText(inputLine),  ST_IsClosed(inputLine) ;
				    ELSE
					    RAISE EXCEPTION 'No usable input lines found for inputline % where isClosed = %' , ST_AsText(inputLine),  ST_IsClosed(inputLine) ;
				    END IF;
				END IF;

				-- Some input lines were refused,
				-- rollback the subtransaction
				RAISE USING ERRCODE = '22901'; -- custom error code
			END IF; --}

			EXIT; -- Exits the loop, no input lines were drop

		EXCEPTION WHEN SQLSTATE '22901' THEN --}{
			NULL; -- continue the loop
		END; --} exception handling transaction

	END LOOP; --}

	-- Collect all nodes covered by the inputLineEdgeIds
	-- Param $1 is the input line edge ids
	sql := format(
		$$
			WITH coveredNodes AS (
				SELECT start_node node_id
				FROM %1$I.edge_data
				WHERE edge_id = ANY($1)
					UNION
				SELECT end_node
				FROM %1$I.edge_data
				WHERE edge_id = ANY($1)
			)
			SELECT array_agg(node_id)
			FROM coveredNodes
		$$,
		topo.name
	);
	RAISE DEBUG '%: sql: %', proc_name, sql;
	EXECUTE sql
		USING inputLineEdgeIds
		INTO inputLineNodeIds
	;
	RAISE DEBUG '%: input line covers nodes %',
		proc_name,
		array_to_string(inputLineNodeIds, ',')
	;

	-- Query to extract Borders split by nodes
	-- covering edges forming the input line
	--
	-- Param $1 is the list of inputLineNodeIds
	--
	sql := format(
		$$
WITH borderSplit AS (
		SELECT
			DISTINCT
			b border_record,
			b.%1$I::text border_id,
			b.%4$I border_tg,
			n.node_id,
			e.edge_id,
			n.geom node_geom
		FROM
			%3$s b,
			%2$I.relation r,
			%2$I.edge_data e,
			%2$I.node n
		WHERE
			r.topogeo_id = id(b.%4$I)
			AND r.layer_id = layer_id(b.%4$I)
			AND r.element_type = 2 -- edge
			AND r.element_id IN ( e.edge_id, -e.edge_id )
			AND
			(
				(
					e.start_node = ANY($1)
					AND
					e.start_node = n.node_id
				)
				OR
				(
					e.end_node = ANY($1)
					AND
					e.end_node = n.node_id
				)
			)
)
SELECT
	border_record,
	border_id,
	ST_CollectionHomogenize(
			border_tg::geometry
	) border_geom,
	ST_Collect(node_geom) node_geoms,
	array_agg(node_id) node_ids
FROM borderSplit
GROUP BY border_record, border_id, border_tg
HAVING count(edge_id) > 1
		$$,
		borderLayerIdColumn,  -- %1
		topo.name,            -- %2
		borderLayerTable,     -- %3
		borderLayerGeomColumn -- %4
	);
	RAISE DEBUG '%: sql: %', proc_name, sql;
	FOR rec IN
		EXECUTE sql
		USING inputLineNodeIds
	LOOP --{
		RAISE DEBUG 'Border % was split on nodes %',
			rec.border_id,
			array_to_string(rec.node_ids, ',')
		;

		RAISE DEBUG 'Border geom was: %',
			ST_AsText(rec.border_geom)
		;

		splitBorderStartPoint := ST_StartPoint(rec.border_geom);
		-- If the border is a closed border
		-- and its start/end point is not
		-- one of the "splitting nodes",
		-- then we will want to merge components
		-- of the split border that end on such
		-- point.
		splitBorderMergeOnStartPoint := FALSE;
		IF ST_IsClosed(rec.border_geom)
		AND NOT ST_Intersects(
				splitBorderStartPoint,
				rec.node_geoms
			)
		THEN
			RAISE DEBUG 'Border is closed and its endpoint % is not a split node',
				ST_AsText(splitBorderStartPoint);
			-- Check if the node has degree > 2
			-- in which case we cannot merge back on it.
			-- TODO: only count edges taking part of the definition of
			--       other TopoGeometries in the Border layer
			-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/275
			sql := format(
				$SQL$
					SELECT count(e.edge_id)
					FROM %1$I.edge e, %1$I.node n
					WHERE ST_Equals(n.geom, $1)
					AND ( e.start_node = n.node_id OR e.end_node = n.node_id )
				$SQL$,
				topo.name
			);
			EXECUTE sql USING splitBorderStartPoint INTO splitBorderStartPointNodeDegree;
			RAISE DEBUG 'Border endpoint node degree=%', splitBorderStartPointNodeDegree;
			IF splitBorderStartPointNodeDegree < 3 THEN
				splitBorderMergeOnStartPoint := TRUE;
			END IF;
		END IF;

		-- Split the border geometry
		splitBorderGeom := ST_Split(
			rec.border_geom,
			rec.node_geoms
		);
		RAISE DEBUG 'Border was split to: %', ST_AsText(splitBorderGeom);

		splitBorderCount := ST_NumGeometries(splitBorderGeom);

		-- (Sanity check: split border must have been split in at least 2 components.)
		-- The above is not true when we use snapTo because then we get the nodes from the server
		-- TODO do we need another sanity check her, or do we need this check at all ?
		IF splitBorderCount < 1
		THEN
			RAISE EXCEPTION 'Border % (%) split by nodes % (%) is still a single geometry',
				ST_AsText(rec.border_geom),
				rec.border_id,
				ST_AsText(rec.node_geoms),
				array_to_string(rec.node_ids, ',')
			;
		END IF;

		-- Get column map for modified Borders
		IF colMapForModBorder IS NULL
		THEN
			EXECUTE colMapExtractSQL
				USING colMapProviderParam, 'B', 'M'
				INTO colMapForModBorder;
			IF colMapForModBorder IS NULL
			THEN
				RAISE EXCEPTION '%: colMapProvider did not provide a map for modified border typ=% and act=%',
					proc_name, 'B', 'M';
			END IF;
			RAISE DEBUG 'colMapForModBorder: %', colMapForModBorder;
		END IF;

		-- Get column map for split Borders
		IF colMapForSplitBorder IS NULL
		THEN
			EXECUTE colMapExtractSQL
				USING colMapProviderParam, 'B', 'S'
				INTO colMapForSplitBorder;
			IF colMapForSplitBorder IS NULL
			THEN
				RAISE EXCEPTION '%: colMapProvider did not provide a map for split border typ=% and act=%',
					proc_name, 'B', 'S';
			END IF;
			RAISE DEBUG 'colMapForSplitBorder: %', colMapForSplitBorder;

			-- Build a column map for attributes-inheriting split Border objects
			-- Write new border mapping taking properties
			-- from payload when specified or from old border
			-- when not specified
			inhSplitBorderColMap := topo_update._json_col_map_override(
				ARRAY[
					topo_update.json_col_map_from_pg(
						borderLayerTable,
						ARRAY[
							-- omit TopoGeometry column from mapping
							borderLayerGeomColumn::text,
							-- omit ID column from props
							borderLayerIdColumn::text
						]
					),
					colMapForSplitBorder
				]
			);
			RAISE DEBUG '%: inheriting split Border ColMap: %', proc_name, inhSplitBorderColMap;
		END IF;

		-- Create split borders for subsequent
		-- components of the splitted geometry

		-- Extract properties of old border
		oldBorderProps := to_jsonb(rec.border_record)
			- borderLayerGeomColumn -- drop TopoGeometry column from props
			- borderLayerIdColumn   -- drop ID column from props
		;
		RAISE DEBUG 'Old Border props: %', oldBorderProps;

		-- Create new border props with
		-- old attributes in an 'old'-keyed field
		-- and payload attributes in a 'new'-keyed field
		newBorderProps := jsonb_build_object(
			'p0', oldBorderProps,
			'p1', payloadProps
		);
		RAISE DEBUG 'New Border props: %', newBorderProps;

		modBorderGeom := NULL;
		FOR i IN 1 .. splitBorderCount
		LOOP -- {
			splitBorderGeomComp = ST_GeometryN(splitBorderGeom, i);
			RAISE DEBUG 'Split border geom component: %',
				ST_AsText(splitBorderGeomComp);

			IF NOT splitBorderMergeOnStartPoint
			THEN --{

				-- If we don't need to merge components
				-- then we take component having the
				-- same start point as the original
				-- border as the one which will retain
				-- its border id
				IF modBorderGeom IS NULL
				THEN
					IF ST_Equals(
						ST_StartPoint(splitBorderGeomComp),
						splitBorderStartPoint
					)
					THEN
						RAISE DEBUG 'Component starts from  original border start point';
						modBorderGeom := splitBorderGeomComp;
						CONTINUE;
					END IF;
				END IF;

			ELSE -- }{

				-- If we do need to merge components
				-- then we take all components (2 expected)
				-- ending on the splitBorderMergeOnStartPoint
				-- to build the new versino of the border
				-- which will retain its id
				IF ST_Equals(
						ST_StartPoint(splitBorderGeomComp),
						splitBorderStartPoint
					) OR
					ST_Equals(
						ST_EndPoint(splitBorderGeomComp),
						splitBorderStartPoint
					)
				THEN --{
					RAISE DEBUG 'Component ends on original border endpoint';
					IF modBorderGeom IS NULL
					THEN
						modBorderGeom := splitBorderGeomComp;
					ELSE
						modBorderGeom := ST_LineMerge(
							ST_Collect(
								modBorderGeom,
								splitBorderGeomComp
							)
						);
					END IF;
					CONTINUE;

				END IF; --}

			END IF; --}

			rec2 := topo_update.insert_feature(
				splitBorderGeomComp,
				newBorderProps,
				borderLayerTable,
				borderLayerGeomColumn,
				borderLayerIdColumn,
				inhSplitBorderColMap,
				-- use 0 tolerance as we're only using
				-- existing topological elements
				0.0 -- tolerance
			);

			fid := rec2.id;
			typ := 'B';
			act := 'S';
			frm := rec.border_id;
			RAISE DEBUG 'Border % split from % - returning %|%|%|%',
				fid, frm, fid,typ,act,frm;
			RETURN NEXT;

		END LOOP; --}

		-- Update the modified border to only be composed
		-- by the first geometry returned by the split
		RAISE DEBUG 'Modified border geom: %', ST_AsText(modBorderGeom);

		fid := rec.border_id;
		typ := 'B';
		act := 'M';
		frm := null;
		RAISE DEBUG 'Border % modified - returning %|%|%|%',
			fid, fid,typ,act,frm;
		RETURN NEXT;

		rec2 := topo_update.update_feature(
			modBorderGeom,
			payloadProps,
			borderLayerTable,
			borderLayerGeomColumn,
			borderLayerIdColumn,
			fid,
			colMapForModBorder,
			-- use 0 tolerance as we're only using
			-- existing topological elements
			0.0, -- tolerance
			true -- modify existing TopoGeometry
		);

		-- Sanity check
		IF NOT (
			id(rec2.old_topogeom) = newBorder.id
		) THEN
			RAISE EXCEPTION 'topo_update.update_feature'
				' unexpectedly found the old TopoGeometry'
				' to be different from the new one (border)';
		END IF;


	END LOOP; --}

	-- Get column map for new Borders
	EXECUTE colMapExtractSQL
		USING colMapProviderParam, 'B', 'C'
		INTO colMapForNewBorder;
	IF colMapForNewBorder IS NULL
	THEN
		RAISE EXCEPTION '%: colMapProvider did not provide a map for new border typ=% and act=%',
			procName, 'B', 'C';
	END IF;
	RAISE DEBUG 'colMapForNewBorder: %', colMapForNewBorder;

	-- Create a new Border for each edge in
	-- inputLineEdgeIds
	-- TODO: join edges togheter if they are not
	--       ending on inputLineNodeIds !
	FOR rec IN
		SELECT unnest(inputLineEdgeIds) edge_id
	LOOP

		typ := 'B';
		act := 'C';
		frm := null;

		-- Create the new Border TopoGeometry
		newBorder := topology.CreateTopoGeom(
			topo.name,
			2, -- lineal
			borderLayer.layer_id,
			topology.TopoElementArray_agg(ARRAY[rec.edge_id,2])
		)
		;
		--RAISE DEBUG 'newBorder: %', newBorder;

		newBorderId := topo_update.insert_feature(
			newBorder,
			payloadProps,
			borderLayerTable,
			borderLayerGeomColumn,
			borderLayerIdColumn,
			colMapForNewBorder
		);

		RAISE DEBUG '%: Border % created to cover input line edge %',
			proc_name, newBorderId, rec.edge_id;

		-- Return info about the created Border
		fid := newBorderId;
		RETURN NEXT;
	END LOOP;

	--------------------------------
	--
	-- Handling Surface split here
	--
	--------------------------------

	-- Fetch identifiers of new face created by adding the border
	sql := format(
		$$
SELECT array_agg(face_id)
FROM %I.face
WHERE face_id > %L
		$$,
		topo.name, maxFaceBefore
	);
	RAISE DEBUG '%: sql: %', proc_name, sql;
	EXECUTE sql INTO newFaces;

	newFacesCount := COALESCE(array_upper(newFaces, 1), 0);

	RAISE DEBUG '%: % new faces created by inserting border', proc_name, newFacesCount;

	-- Check that at least one face was split by the new edge
	IF newFacesCount = 0 THEN
		RAISE EXCEPTION '%: no face was split by border', proc_name;
	END IF;

	-- Check that the number of new faces is acceptable
	IF newFacesCount > maxNewSurfacesOrChangedAreaSurfaces THEN
		RAISE EXCEPTION '%: newFacesCount % is bigger than maxNewSurfacesOrChangedAreaSurfaces % when split by border', proc_name, newFacesCount, maxNewSurfacesOrChangedAreaSurfaces;
	END IF;



	-- Get column map for split Surface
	EXECUTE colMapExtractSQL
		USING colMapProviderParam, 'S', 'S'
		INTO colMapForSplitSurface;
	IF colMapForSplitSurface IS NULL
	THEN
		RAISE EXCEPTION '%: colMapProvider did not provide a map for split surface typ=% and act=%',
			procName, 'S', 'S';
	END IF;

	RAISE DEBUG '%: split Surface ColMap: %', proc_name, colMapForSplitSurface;

	-- Get column map for modified Surface
	EXECUTE colMapExtractSQL
		USING colMapProviderParam, 'S', 'M'
		INTO colMapForModSurface;
	IF colMapForModSurface IS NULL
	THEN
		RAISE EXCEPTION '%: colMapProvider did not provide a map for modified surface typ=% and act=%',
			procName, 'S', 'M';
	END IF;

	RAISE DEBUG '%: modified Surface ColMap: %', proc_name, colMapForModSurface;

	-- Build a column map for attributes-inheriting split Surface objects
	-- Write new surface mapping taking properties
	-- from payload when specified or from old surface
	-- when not specified
	inhSplitSurfaceColMap := topo_update._json_col_map_override(
		ARRAY[
			topo_update.json_col_map_from_pg(
				surfaceLayerTable,
				ARRAY[
					-- omit TopoGeometry column from mapping
					surfaceLayerGeomColumn::text,
					-- omit ID column from props
					surfaceLayerIdColumn::text
				]
			),
			colMapForSplitSurface
		]
	);
	RAISE DEBUG '%: inheriting split Surface ColMap: %', proc_name, inhSplitSurfaceColMap;

	-- Find all Surface objects which were
	-- split by the new border, with their
	-- definition
	--
	-- $1 is newFaces
	--
	sql := format(
		$$
	SELECT
		DISTINCT ON (%1$I)
		%1$I::text id,
		%2$I tg,
		array_agg( r_to.element_id ) over (
			partition by %1$I
		) covered_new_faces,
		fa_to surface_record
	FROM
		%3$s fa_to,
		%4$s.relation r_to
	WHERE
		r_to.layer_id = %5$s AND
		r_to.element_id = ANY($1) AND
		r_to.topogeo_id = id(fa_to.%2$s)
	ORDER BY %1$I
		$$,
		surfaceLayerIdColumn,   -- %1
		surfaceLayerGeomColumn, -- %2
		surfaceLayerTable,      -- %3
		topo.name,				-- %4
		layer_id(surfaceLayer)	-- %5
	);
	RAISE DEBUG '%: sql: %', proc_name, sql;

	-- Iterate over all Surface objects which were
	-- split by the new border
	FOR rec IN EXECUTE sql
	USING newFaces
	LOOP --{
		RAISE DEBUG 'Affected surface % covers new faces %',
			rec.id, rec.covered_new_faces;

		splitSurfaceCount := splitSurfaceCount + 1;
		IF splitSurfaceCount > maxAllowedSurfaceSplitCount THEN
			RAISE EXCEPTION 'Surface split count exceeded limit of %', maxAllowedSurfaceSplitCount;
		END IF;

		-- SANITY CHECK
		-- Check that the new faces covered by
		-- this surface were not previously found
		-- covered by another surface
		IF coveredNewFaces && rec.covered_new_faces -- overlap operator
		THEN
			-- This query could be improved by exposing a
			-- PointOnFace in PostGIS Topology:
			-- with: https://trac.osgeo.org/postgis/ticket/4861
			SELECT
				ST_Union(
					ST_PointOnSurface(
						topology.ST_GetFaceGeometry(
							topo.name,
							id
						)
					)
				)
			FROM unnest(rec.covered_new_faces) id
			INTO overlapPoints;
			RAISE EXCEPTION 'Surfaces overlap detected in %',
				ST_AsEWKT(overlapPoints);
		END IF;

		-- Extract properties of old surface
		oldSurfaceProps := to_jsonb(rec.surface_record)
			- surfaceLayerGeomColumn -- drop TopoGeometry column from props
			- surfaceLayerIdColumn   -- drop ID column from props
		;
		RAISE DEBUG 'Old Surface props: %', oldSurfaceProps;

		-- Create new surface props with
		-- old attributes in an 'old'-keyed field
		-- and payload attributes in a 'new'-keyed field
		newSurfaceProps := jsonb_build_object(
			'p0', oldSurfaceProps,
			'p1', payloadProps
		);
		RAISE DEBUG 'New Surface props: %', newSurfaceProps;

		-- Update the set of new faces found covered
		coveredNewFaces := array(
			SELECT unnest(coveredNewFaces)
				UNION
			SELECT unnest(rec.covered_new_faces)
		);

--		RAISE DEBUG 'Surface TopoGeom BEFORE: id(%), elems(%)',
--			id(rec.tg),
--			topology.GetTopoGeomElementArray(rec.tg);

		-- Delete covered new faces from the
		-- split surface TopoGeometry object
		--
		-- FIXME: Single-part Surfaces defined by multiple faces may become
		--        multi-part when removing from them faces effectively
		--        splitting them. When multi-part Surfaces are not
		--        wanted we'll need to deal with this scenario.
		--        See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/263
		--
		--
		newSurface := rec.tg;
		sql := format(
			$$
DELETE FROM %1$I.relation
WHERE layer_id = %2$L
AND topogeo_id = %3$L
AND element_type = 3
AND element_id = ANY($1)
			$$,
			topo.name,
			layer_id(surfaceLayer),
			id(rec.tg)
		);
		RAISE DEBUG '%: sql: %', proc_name, sql;
		EXECUTE sql USING rec.covered_new_faces;

--		RAISE DEBUG 'Surface TopoGeom AFTER: id(%), elems(%)',
--			id(rec.tg),
--			topology.GetTopoGeomElementArray(rec.tg);

		IF keepModifiedSurface
		THEN -- {

			rec2 := topo_update.update_feature(
				newSurface,
				payloadProps,
				surfaceLayerTable,
				surfaceLayerGeomColumn,
				surfaceLayerIdColumn,
				rec.id,
				colMapForModSurface
			);

			RAISE DEBUG 'Surface % modified to drop faces %',
				rec.id, rec.covered_new_faces;

			-- Sanity check
			IF NOT (
				id(rec2.old_topogeom) = newSurface.id
			) THEN
				RAISE EXCEPTION 'topo_update.update_feature'
					' unexpectedly found the old TopoGeometry'
					' to be different from the new one (surface)';
			END IF;

			fid := rec.id;
			typ := 'S';
			act := 'M';
			frm := null;
			RETURN NEXT;
		ELSE -- }{

			-- DELETE old surface
			sql := format(
				$$
					DELETE FROM %1$s WHERE %2$I::text = $1
				$$,
				surfaceLayerTable,
				surfaceLayerIdColumn
			);
			RAISE DEBUG '%: sql: %', proc_name, sql;
			EXECUTE sql USING rec.id;
			fid := rec.id;
			typ := 'S';
			act := 'D';
			frm := null;
			RETURN NEXT;

			-- CREATE new surface
			newSurfaceId := topo_update.insert_feature(
				newSurface,
				newSurfaceProps,
				surfaceLayerTable,
				surfaceLayerGeomColumn,
				surfaceLayerIdColumn,
				inhSplitSurfaceColMap
			);

			RAISE DEBUG 'New Surface % created, with props %',
				newSurfaceId, newSurfaceProps;

			fid := newSurfaceId;
			typ := 'S';
			act := 'S';
			frm := rec.id;
			RETURN NEXT;

		END IF; --}


		-- Create a new surface TopoGeometry object
		-- covering each group of faces, from
		-- the set of the faces which we removed
		-- from the definition of the split surface
		-- ("rec.covered_new_faces"),
		-- that have at least one common edge not used
		-- by any Border feature

		-- Loop over every group of mergeable covered faces
		FOR tea IN
			SELECT topo_update._add_border_split_surface_mergeable_faces(
				topo,
				borderLayer,
				rec.covered_new_faces
			) tea
			ORDER BY tea
		LOOP --{

			newSurface := topology.CreateTopoGeom(
				topo.name,
				3, -- areal type
				layer_id(surfaceLayer), -- layer id
				tea -- topoelement_array
			);
			--RAISE DEBUG 'TopoGeom covering faces eroded by split Surface: %',
			--	ST_AsText(newSurface);


			-- Insert the new Surface feature with
			-- attribute values taken from the split surface
			-- and updated with values from the payload
			newSurfaceId := topo_update.insert_feature(
				newSurface,
				newSurfaceProps,
				surfaceLayerTable,
				surfaceLayerGeomColumn,
				surfaceLayerIdColumn,
				inhSplitSurfaceColMap
			);

			RAISE DEBUG 'New Surface % created, with props %',
				newSurfaceId, newSurfaceProps;

			fid := newSurfaceId;
			typ := 'S';
			act := 'S';
			frm := rec.id;
			RETURN NEXT;

		END LOOP; --}


	END LOOP; --}

	uncoveredNewFaces := array(
		SELECT unnest(newFaces) face_id
			EXCEPT
		SELECT unnest(coveredNewFaces)
	);

	RAISE DEBUG '%: for uncovered new faces are %',
		proc_name, uncoveredNewFaces;

	IF uncoveredNewFaces = '{}' THEN
		RETURN; -- nothing more to do here
	END IF;

	-- Get column map for new Surface
	EXECUTE colMapExtractSQL
		USING colMapProviderParam, 'S', 'C'
		INTO colMapForNewSurface;
	IF colMapForNewSurface IS NULL
	THEN
		RAISE EXCEPTION '%: colMapProvider did not provide a map for new surface typ=% and act=%',
			procName, 'S', 'C';
	END IF;

	-- Build a column map for attributes-inheriting new Surface objects
	-- Write new surface mapping taking properties
	-- from payload when specified or from old surface
	-- when not specified
	inhNewSurfaceColMap := topo_update._json_col_map_override(
		ARRAY[
			topo_update.json_col_map_from_pg(
				surfaceLayerTable,
				ARRAY[
					-- omit TopoGeometry column from mapping
					surfaceLayerGeomColumn::text,
					-- omit ID column from props
					surfaceLayerIdColumn::text
				]
			),
			colMapForNewSurface
		]
	);
	RAISE DEBUG '%: inheriting split Surface ColMap: %', proc_name, inhSplitSurfaceColMap;

	-- Create a new surface TopoGeometry object
	-- covering each group of faces, from
	-- the set of the faces not previously covered
	-- by existing Srufaces ("uncoveredNewFaces"),
	-- that have at least one common edge not used
	-- by any Border feature
	FOR tea IN
		SELECT topo_update._add_border_split_surface_mergeable_faces(
			topo,
			borderLayer,
			uncoveredNewFaces
		) tea
		--ORDER BY tea -- TODO: order to make predictable ?
	LOOP
		newSurface := topology.CreateTopoGeom(
			topo.name,
			3, -- areal type
			layer_id(surfaceLayer), -- layer id
			tea -- topoelement_array
		);

		RAISE DEBUG '%: new surface for uncovered new faces % is %',
			proc_name, tea, newSurface;

		-- Select a pre-existing Surface having the
		-- "most" adjacency to this new Surface
		SELECT bestSurfaceId, bestSurfaceProps
		FROM topo_update._most_adjacent_surface(
			topo,
			uncoveredNewFaces,
			surfaceLayer,
			surfaceLayerIdColumn -- needed ?
		)
		INTO rec;

		IF rec.bestSurfaceId IS NOT NULL
		THEN
			RAISE DEBUG '%: most adjacent surface is %, with props %',
				proc_name, rec.bestSurfaceId, rec.bestSurfaceProps;
			newSurfaceProps := jsonb_build_object(
				'p0', rec.bestSurfaceProps,
				'p1', payloadProps
			);
			surfaceColMap := inhNewSurfaceColMap;
		ELSE
			RAISE DEBUG '%: no adjacent surface found, not inheriting attributes',
				proc_name;
			newSurfaceProps := payloadProps;
			surfaceColMap := colMapForNewSurface;
		END IF;


		newSurfaceId := topo_update.insert_feature(
			newSurface,
			newSurfaceProps,
			surfaceLayerTable,
			surfaceLayerGeomColumn,
			surfaceLayerIdColumn,
			surfaceColMap
		);

		RAISE DEBUG '%: new surface feature id is %',
			proc_name, newSurfaceId;

		-- Add new item to the returned JSONB for the created surface
		-- Return info about the created Surface
		fid := newSurfaceId;
		typ := 'S';
		act := 'C';
		frm := NULL;
		RETURN NEXT;

	END LOOP;

END;
$BODY$ LANGUAGE plpgsql; --}


-----------------------------------------------------
--
-- Functions defined for backward compatibility
--
-----------------------------------------------------

--{
CREATE OR REPLACE FUNCTION topo_update._add_border_split_surface_get_map_from_array(
	usr JSONB[], typ char, act char
) RETURNS JSONB AS $BODY$
DECLARE
	procName TEXT := 'topo_update._add_border_split_surface_get_map_from_array';
BEGIN
	IF typ = 'B' AND act IN ( 'M', 'C', 'S' )
	THEN
		RETURN usr[1];
	ELSIF typ = 'S' AND act IN ( 'M', 'C', 'S' )
	THEN
		RETURN usr[2];
	ELSE
		RAISE EXCEPTION '%: unsupported typ/act combination: %/%',
			procName, typ, act;
	END IF;
END;
$BODY$ LANGUAGE 'plpgsql'; --}

-- Replaced in v1.1.0 by the version taking an optional max number
-- of surface splits and a boolean value to express willingness
-- to retain modified surface
DROP FUNCTION IF EXISTS topo_update.add_border_split_surface(
	feature JSONB,
	surfaceLayerTable REGCLASS,
	surfaceLayerGeomColumn NAME,
	surfaceLayerIdColumn NAME,
	surfaceLayerColMap JSONB,
	borderLayerTable REGCLASS,
	borderLayerGeomColumn NAME,
	borderLayerIdColumn NAME,
	borderLayerColMap JSONB,
	tolerance float8,
	colMapProviderFunc REGPROC,
	colMapProviderParam ANYELEMENT,
	minToleratedFaceArea FLOAT8
);


--{
CREATE OR REPLACE FUNCTION topo_update.add_border_split_surface(
	feature JSONB,
	surfaceLayerTable REGCLASS,
	surfaceLayerGeomColumn NAME,
	surfaceLayerIdColumn NAME,
	surfaceLayerColMap JSONB,
	borderLayerTable REGCLASS,
	borderLayerGeomColumn NAME,
	borderLayerIdColumn NAME,
	borderLayerColMap JSONB,
	tolerance float8
)
RETURNS TABLE(fid text, typ char, act char) AS $BODY$
BEGIN
	RAISE WARNING 'add_border_split_surface with colMap params is deprecated';
	RETURN QUERY
	SELECT o.fid, o.typ, o.act FROM topo_update.add_border_split_surface(
		feature,
		surfaceLayerTable,
		surfaceLayerGeomColumn,
		surfaceLayerIdColumn,
		borderLayerTable,
		borderLayerGeomColumn,
		borderLayerIdColumn,
		tolerance,
		'topo_update._add_border_split_surface_get_map_from_array',
		ARRAY[borderLayerColMap, surfaceLayerColMap]
	) o;

END;
$BODY$ LANGUAGE plpgsql; --}
