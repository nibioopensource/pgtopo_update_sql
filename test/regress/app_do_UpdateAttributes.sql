SET client_min_messages TO WARNING;
SET extra_float_digits TO -3;

------------------------------------------------------------
-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
-------------------------------------------------------------

\i :regdir/../../src/sql/topo_update/function_03_app_do_UpdateAttributes.sql

-------------------------------------------
-- Utility schema (data and functions)
-------------------------------------------

\i :regdir/utils/app_test_utils.sql

-------------------------------------------
-- Save minimal app schema into a table
-------------------------------------------

CREATE TABLE util.minimal_app_schema AS
SELECT $$
	{
		"version": "1.0",
		"topology": { "srid": 4326 },
		"tables": [ {
				"name": "nokey_tab",
				"attributes": [
					{ "name": "k", "type": "serial" },
					{ "name": "label", "type": "text" }
				]
			}, {
				"name": "r1",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" },
					{ "name": "label", "type": "text" }
				]
			}
		],
		"operations": {
			"UpdateAttributes": { }
		}
	}
$$::jsonb AS cfg;

SELECT * FROM topo_update.app_CreateSchema( 'test',
	( SELECT cfg FROM util.minimal_app_schema )
);

-- te1: unexisting app
SELECT 'te1', * FROM topo_update.app_do_UpdateAttributes(
	'non-existing-app',
	'r1', '1', '{ "label": "te1" }'
);

-- te2: unexisting table in app
SELECT 'te2', * FROM topo_update.app_do_UpdateAttributes(
	'test',
	'r1-not-there', '1', '{ "label": "te2" }'
);

-- te3: table does not have primary key
SELECT 'te3', * FROM topo_update.app_do_UpdateAttributes(
	'test',
	'nokey_tab', '1', '{ "label": "te3" }'
);

-- te4: table does not exist in db
BEGIN;
DROP TABLE test.r1;
SELECT 'te4', * FROM topo_update.app_do_UpdateAttributes(
	'test',
	'r1', '1', '{ "label": "te4" }'
);
ROLLBACK;

-- te5: operation is not enabled
BEGIN;
UPDATE
	test_sysdata_webclient_functions.app_config
SET cfg = jsonb_concat(
		cfg - 'operations',
		jsonb_build_object('operations', jsonb_build_object())
	);
SELECT 'te5', * FROM topo_update.app_do_UpdateAttributes(
	'test',
	'r1', '1', '{ "label": "te5" }'
);
ROLLBACK;

-- t1: record not found
SELECT 't1', 'r1-count', count(*) FROM test.r1;
SELECT 't1', * FROM topo_update.app_do_UpdateAttributes(
	'test',
	'r1', '1', '{ "label": "t1" }'
);

-- t2: existing record, set value
INSERT INTO test.r1 (label) values ('initial');
SELECT 't2', 'r1', * FROM test.r1 ORDER BY k;
SELECT 't2', * FROM topo_update.app_do_UpdateAttributes(
	'test',
	'r1', '1', '{ "label": "t2" }'
);
SELECT 't2', 'r1', * FROM test.r1 ORDER BY k;

-- t3: existing record, set to null
SELECT 't3', 'r1', * FROM test.r1 ORDER BY k;
SELECT 't3', * FROM topo_update.app_do_UpdateAttributes(
	'test',
	'r1', '1', '{ "label": null }'
);
SELECT 't3', 'r1', * FROM test.r1 ORDER BY k;

-- t4: existing record, nothing set (no-op)
SELECT 't4', 'r1', * FROM test.r1 ORDER BY k;
SELECT 't4', * FROM topo_update.app_do_UpdateAttributes(
	'test',
	'r1', '1', '{}'
);
SELECT 't4', 'r1', * FROM test.r1 ORDER BY k;

-- TODO: test setting column to default value (unsupported)
-- TODO: test columns which are forbidden from being updated


-------------------------------------------
-- Cleanup
-------------------------------------------

SELECT * FROM util.drop_generated_schema('test');

SET extra_float_digits TO 0;
DROP SCHEMA util CASCADE;
