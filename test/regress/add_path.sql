
set client_min_messages to WARNING;

-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
\i :regdir/../../src/sql/topo_update/function_02_add_path.sql
\i :regdir/../../src/sql/topo_update/function_01_json_props_to_pg_cols.sql

-------------------------------------------
-- Utility includes
-------------------------------------------

\i :regdir/utils/normalized_geom.sql

-------------------------------------
-- Create a topology 'topo'
-------------------------------------


SELECT NULL FROM CreateTopology('topo', 4326);

-------------------------------------
-- Create Path layer
-------------------------------------

CREATE TABLE topo.path (
	k SERIAL PRIMARY KEY,
	lbl TEXT,
	lbl2 TEXT DEFAULT 'lbl2'
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo', 'path', 'g',
	'LINE'
);

-------------------------------------
-- Create colMapProvider function
-------------------------------------

--{
CREATE FUNCTION topo.testColMapProvider(act char, usr JSONB)
RETURNS JSONB AS $BODY$
DECLARE
	procName TEXT := 'testColMapProvider';
	map JSONB;
BEGIN
	map := usr -> format('%s', act);

	--RAISE WARNING '%: map for act:% is %', procName, act, map;

	RETURN map;
END;
$BODY$ LANGUAGE 'plpgsql'; --}



------------------------------------------------------
-- Test the add_path function
------------------------------------------------------

CREATE TABLE topo.add_path_results(
	test TEXT,
	id TEXT,
	act CHAR,
	frm TEXT
);

-- {
CREATE FUNCTION check_add_path_effects(
	testname TEXT
)
RETURNS TABLE(o TEXT)
AS $BODY$
DECLARE
  rec RECORD;
  sql TEXT;
  attrs TEXT[];
BEGIN

	--RAISE WARNING 'Hello from check_add_path_effects, testname %', testname;

	RETURN QUERY
	SELECT array_to_string(ARRAY[
			testname,
			'act',
			id,
			act,
			frm
		], '|')
	FROM topo.add_path_results
	WHERE test = testname
	ORDER BY id::int;

	RETURN QUERY
	SELECT array_to_string(ARRAY[
			testname,
			'path',
			k::text,
			lbl,
			COALESCE(lbl2, ''),
			ST_AsText(
				normalized_geom(
					ST_Simplify(
						ST_CollectionHomogenize(g),
						0
					)
				)
			)
		], '|')
	FROM topo.path
	WHERE k in (
		SELECT id::int
		FROM topo.add_path_results
		WHERE test = testname
	)
	ORDER BY k;

END;
$BODY$ LANGUAGE 'plpgsql';
--}

--
-- t1.e1: add a path which is too short to be used
--
--   . . . . . .
--
SELECT 't1.e1', * FROM topo_update.add_path(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [ [ 0, 0 ], [ 1, 0 ] ],
			"type" : "LineString"
		},
		"properties" : {},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.path', 'g', 'k', -- path layer identifier
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	'{}'::jsonb, -- colMapProviderParam
	minToleratedPathLength => 10 -- needs be at least 10 units to be accepted
);


--
-- t1.e2: add a path with missing CRS indication
--

SELECT 't1.e2', * FROM topo_update.add_path(
	$$
	{
		"geometry" : {
			"coordinates" : [ [ 0, 0 ], [ 10, 0 ] ],
			"type" : "LineString"
		},
		"properties" : {
			"lbl": "t1"
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.path', 'g', 'k', -- path layer identifier
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	'{}'::jsonb -- colMapProviderParam
)
ORDER BY fid::int
;

--
-- t1: add an accepted path
--
--
--       P1
--  o-----------o
--

INSERT INTO topo.add_path_results
SELECT 't1', * FROM topo_update.add_path(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [ [ 0, 0 ], [ 10, 0 ] ],
			"type" : "LineString"
		},
		"properties" : { "lbl": "t1" },
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.path', 'g', 'k', -- path layer identifier
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	'{ "C": { "lbl": [ "lbl" ], "lbl2": [ "lbl" ] } }'::jsonb -- colMapProviderParam
);
SELECT * FROM check_add_path_effects('t1');

--
-- t2: add path intersecting existing path
--
--
--         o
--         |
--         P4
--         |
--  o--P1--*--P2--o
--         |
--         P3
--         |
--         o
--
--

INSERT INTO topo.add_path_results
SELECT 't2', * FROM topo_update.add_path(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [ [ 5, -5 ], [ 5, 5 ] ],
			"type" : "LineString"
		},
		"properties" : { "lbl": "t2" },
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.path', 'g', 'k', -- path layer identifier
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	'{ "C": { "lbl": [ "lbl" ] }, "M": { "lbl": [ "lbl" ]}, "S": { "lbl": [ "lbl" ] } }'::jsonb -- colMapProviderParam
);
SELECT * FROM check_add_path_effects('t2');

-----------
-- Cleanup
-----------

SELECT NULL FROM DropTopology('topo');
DROP FUNCTION check_add_path_effects(TEXT);
DROP FUNCTION normalized_geom;
