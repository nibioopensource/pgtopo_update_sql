--BEGIN;

set client_min_messages to WARNING;

-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
\i :regdir/../../src/sql/topo_update/function_02_find_interiors_intersect.sql
\i :regdir/../../src/sql/topo_update/function_02_check_unique_topo_primitive.sql

---------------------
-- Initialize schema
---------------------

SELECT NULL FROM CreateTopology('topo');

CREATE TABLE topo.border (
  "id" TEXT PRIMARY KEY,
  "live" BOOLEAN DEFAULT FALSE
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo',
	'border',
	'tg',
	'LINEAL'
);

CREATE TABLE topo.surface (
  "id" TEXT PRIMARY KEY,
  "live" BOOLEAN DEFAULT FALSE
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo',
	'surface',
	'tg',
	'AREAL'
);

CREATE TABLE topo.constraint (
	tab REGCLASS,
	lbl TEXT,
	trg TEXT
);


-------------------------------------
-- Add constraint on Borders layer
-- only checking for "live" features
-------------------------------------

INSERT INTO topo.constraint(tab,lbl,trg)
SELECT
	'topo.border',
	'live',
	trigname
FROM topo_update.add_unique_topo_primitive_constraint(
	'topo.border',
	'tg',
	'id',
	'live'
) trigname;


-------------------------------------
-- Add constraint on Surface layer
-- only checking for "live" features
-------------------------------------

INSERT INTO topo.constraint(tab,lbl,trg)
SELECT
	'topo.surface',
	'live',
	trigname
FROM topo_update.add_unique_topo_primitive_constraint(
	'topo.surface',
	'tg',
	'id',
	'live'
) trigname;


--------------------------------
-- Add Borders (non-overlapping)
--------------------------------

INSERT INTO topo.border (id, live, tg) VALUES
(
  'b1', true,
  --
  --   ,----b1----.
  --   |          |
  --   |  (F1)    |
  --   |          |
  --   |          |
  --   |          |
  --   |          |
  --   |          |
  --   v          |
  --   o--(E1)----'
  --
  toTopoGeom(
    'LINESTRING(0 0,10 0,10 10,0 10,0 0)',
    'topo',
    1,
    0
  )
),
(
  'b2', true,
  --
  --   ,----(b1)---.
  --   |           |
  --   |   (F2)    |
  --   |           |
  --   | o-b2---.  |
  --   | ^      |  |
  --   | | (F1) |  |
  --   | |      |  |
  --   | `-(E2)-'  |
  --   v           |
  --   o---(E1)----'
  --
  toTopoGeom(
    'LINESTRING(2 8,8 8,8 2,2 2, 2 8)',
    'topo',
    1,
    0
  )
);

--------------------------------
-- Add Borders (overlapping)
--------------------------------

INSERT INTO topo.border (id, live, tg) VALUES
(
  'b1+b2', true,
  --
  --   ,-----b1----.
  --   |           |
  --   |   (F2)    |
  --   |           |
  --   | o-b2---.  |
  --   | ^      |  |
  --   | | (F1) |  |
  --   | |      |  |
  --   | `-(E2)-'  |
  --   v           |
  --   o---(E1)----'
  --
  CreateTopoGeom(
    'topo',
	2, -- lineal
	1, -- layer_id
    '{{1,2},{2,2}}'
  )
)
;

-- Insert a border using the *same* TopoGeometry
-- as another border
INSERT INTO topo.border (id, live, tg)
SELECT 'b1dup', live, tg FROM topo.border
WHERE id = 'b1';

-------------------------------------------
-- Add Borders (overlapping) but unchecked
-------------------------------------------

INSERT INTO topo.border (id, live, tg) VALUES
(
  'b1+b2.dead', false,
  --
  --   ,-----b1----.
  --   |           |
  --   |   (F2)    |
  --   |           |
  --   | o-b2---.  |
  --   | ^      |  |
  --   | | (F1) |  |
  --   | |      |  |
  --   | `-(E2)-'  |
  --   v           |
  --   o---(E1)----'
  --
  CreateTopoGeom(
    'topo',
	2, -- lineal
	1, -- layer_id
    '{{1,2},{2,2}}'
  )
)
RETURNING id, 'not live, not checked';
;

-- Insert a border using the *same* TopoGeometry
-- as another border
INSERT INTO topo.border (id, live, tg)
SELECT 'b1dup.dead', not live, tg FROM topo.border
WHERE id = 'b1'
RETURNING id, 'not live, not checked';

---------------------------------
-- Add Surfaces (non-overlapping)
---------------------------------

INSERT INTO topo.surface (id, live, tg) VALUES
(
  's1', true,
  --
  --   ,----(b1)---.
  --   | (F2)      |
  --   |           |
  --   |           |
  --   | o-(b2)-.  |
  --   | ^      |  |
  --   | |  s1  |  |
  --   | |      |  |
  --   | | (F1) |  |
  --   | |      |  |
  --   | `-(E2)-'  |
  --   v           |
  --   o---(E1)----'
  --
  CreateTopoGeom(
    'topo',
	3, -- areal
	2, -- layer_id
    '{{2,3}}'
  )
),
(
  's2', true,
  --
  --   ,----(b1)---.
  --   | (F2)      |
  --   |       s2  |
  --   |           |
  --   | o-(b2)-.  |
  --   | ^      |  |
  --   | | (F1) |  |
  --   | |      |  |
  --   | `-(E2)-'  |
  --   v           |
  --   o---(E1)----'
  --
  CreateTopoGeom(
    'topo',
	3, -- areal
	2, -- layer_id
    '{{1,3}}'
  )
)
;

---------------------------------
-- Add Surfaces (overlapping)
---------------------------------

INSERT INTO topo.surface (id, live, tg) VALUES
(
  's1+s2', true,
  --
  --   ,----(b1)---.
  --   | (F2)      |
  --   |       s2  |
  --   |           |
  --   | o-(b2)-.  |
  --   | ^      |  |
  --   | |  s2  |  |
  --   | |      |  |
  --   | `-(E2)-'  |
  --   v           |
  --   o---(E1)----'
  --
  CreateTopoGeom(
    'topo',
	3, -- areal
	2, -- layer_id
    '{{1,3},{2,3}}'
  )
)
;

-- Insert a Surface using the *same* TopoGeometry
-- as another Surface
INSERT INTO topo.surface (id, live, tg)
SELECT 's1dup', live, tg FROM topo.surface
WHERE id = 's1';

-------------------------------------------
-- Add Surfaces (overlapping) but unchecked
-------------------------------------------

INSERT INTO topo.surface (id, live, tg) VALUES
(
  's1+s2.dead', false,
  --
  --   ,----(b1)---.
  --   | (F2)      |
  --   |       s2  |
  --   |           |
  --   | o-(b2)-.  |
  --   | ^      |  |
  --   | |  s2  |  |
  --   | |      |  |
  --   | `-(E2)-'  |
  --   v           |
  --   o---(E1)----'
  --
  CreateTopoGeom(
    'topo',
	3, -- areal
	2, -- layer_id
    '{{1,3},{2,3}}'
  )
)
RETURNING id, 'not live, not checked';
;

-- Insert a Surface using the *same* TopoGeometry
-- as another Surface
INSERT INTO topo.surface (id, live, tg)
SELECT 's1dup.dead', not live, tg FROM topo.surface
WHERE id = 's1'
RETURNING id, 'not live, not checked';

----------------------------------------
-- Re-create constraint on Borders layer
-- checking for ALL features
----------------------------------------

DROP TRIGGER unique_primitives_in_layer_1
ON topo.border;

INSERT INTO topo.constraint(tab,lbl,trg)
SELECT
	'topo.border',
	'all',
	trigname
FROM topo_update.add_unique_topo_primitive_constraint(
	'topo.border',
	'tg',
	'id'
) trigname;

----------------------------------------
-- Re-create constraint on Surface layer
-- only checking for "live" features
----------------------------------------

DROP TRIGGER unique_primitives_in_layer_2
ON topo.surface;

INSERT INTO topo.constraint(tab,lbl,trg)
SELECT
	'topo.surface',
	'all',
	trigname
FROM topo_update.add_unique_topo_primitive_constraint(
	'topo.surface',
	'tg',
	'id'
) trigname;


---------------------------------
-- Cleanup
---------------------------------

SELECT NULL FROM DropTopology('topo');
