set client_min_messages to WARNING;

-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
\i :regdir/../../src/sql/topo_update/function_02_find_gaps.sql
\i :regdir/../../src/sql/topo_update/function_02_check_topo_primitives_all_used.sql

-------------------------------------
-- Create a topology 'topo'
-------------------------------------

SELECT NULL FROM CreateTopology('topo');

-------------------------------------
-- Create puntual layer (1)
-------------------------------------

CREATE TABLE topo.puntal_features (
	k SERIAL PRIMARY KEY,
	live BOOLEAN default true
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo', 'puntal_features', 'g',
	'POINT'
);

INSERT INTO topo.puntal_features(g) VALUES (
	toTopoGeom('POINT(0 0)', 'topo', 1)
);
INSERT INTO topo.puntal_features(g) VALUES (
	toTopoGeom('POINT(1 1)', 'topo', 1)
);

----------------------------------------------------------------------------
-- t1: try to add no-gaps constraint to a table already containing node gaps
----------------------------------------------------------------------------

UPDATE topo.puntal_features SET live = false WHERE k = 1;
SELECT 't1', 'attempt to constraint table with gaps (filtered)';
-- This should fail
SELECT 't1', 'add_constraint_filtered', topo_update.add_all_topo_primitive_used_constraint(
	'topo.puntal_features', 'g', 'live'
);

CREATE TABLE topo.puntal_features_deleted
AS WITH deleted AS (
	DELETE FROM topo.puntal_features WHERE NOT live
	RETURNING *
) SELECT * FROM deleted;
SELECT 't1', 'attempt to constraint table with gaps (unfiltered)';
-- This should fail
SELECT 't1', 'add_constraint_unfiltered', topo_update.add_all_topo_primitive_used_constraint(
	'topo.puntal_features', 'g'
);
INSERT INTO topo.puntal_features SELECT *
FROM topo.puntal_features_deleted;
DROP TABLE topo.puntal_features_deleted;

-----------------------------------------------------
-- t2: add unique topo primitive constraint
--     to a point table with no gaps (filtered)
-----------------------------------------------------

UPDATE topo.puntal_features SET live = true WHERE k = 1;
SELECT 't2', 'add_constraint', topo_update.add_all_topo_primitive_used_constraint(
	'topo.puntal_features', 'g', 'live'
);

SELECT 't2', 'attempt to update object to create gap in point layer';
-- This should fail
WITH updated AS (
	UPDATE topo.puntal_features
	SET live = false
	WHERE k = 1
	RETURNING k
)
SELECT 't2', 'updated', k FROM updated;

SELECT 't2', 'attempt to delete object to create gap in point layer';
-- This should fail
WITH deleted AS (
	DELETE FROM topo.puntal_features
	WHERE k = 1
	RETURNING k
)
SELECT 't2', 'deleted', k FROM deleted;

-----------------------------------------------------
-- t3: add unique topo primitive constraint
--     to a point table with no gaps (unfiltered)
-----------------------------------------------------

DROP TRIGGER primitives_all_used_in_layer_1
ON topo.puntal_features;

CREATE TABLE topo.puntal_features_deleted
AS WITH deleted AS (
	DELETE FROM topo.puntal_features WHERE NOT live
	RETURNING *
) SELECT * FROM deleted;
SELECT 't3', 'add_constraint', topo_update.add_all_topo_primitive_used_constraint(
	'topo.puntal_features', 'g'
);
SELECT 't3', 'attempt to delete object to create gap in point layer';
-- This should fail
WITH deleted AS (
	DELETE FROM topo.puntal_features
	RETURNING k
)
SELECT 't3', 'deleted', k FROM deleted;
SELECT 't3', 'attempt to update object to create gap in point layer';
-- This should fail
WITH updated AS (
	UPDATE topo.puntal_features
	SET g = ClearTopoGeom(g)
	RETURNING k
)
SELECT 't3', 'updated', k FROM updated;

-- TODO: test modifying a TopoGeometry by directly acting on the relation table

--------------------------------------------------------
-- CLEANUP
--------------------------------------------------------

-- FIXME: Dropping puntal_features is a workaround
-- to bug https://trac.osgeo.org/postgis/ticket/5026
-- Should not be required with PostGIS >= 3.0.5, 3.1.5, 3.2.0
DROP TABLE topo.puntal_features;
SELECT NULL FROM DropTopology('topo');
