
set client_min_messages to WARNING;

-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
\i :regdir/../../src/sql/topo_update/function_02_add_border_split_surface.sql
\i :regdir/../../src/sql/topo_update/function_01_json_props_to_pg_cols.sql
\i :regdir/../../src/sql/topo_update/function_01_prepare_split_border.sql

-------------------------------------------
-- Utility includes
-------------------------------------------

\i :regdir/utils/normalized_geom.sql

-------------------------------------
-- Create a topology 'topo'
-------------------------------------


SELECT NULL FROM CreateTopology('topo', 4326);

-------------------------------------
-- Create Surface and Border layers
-------------------------------------

CREATE TABLE topo.surface (
	k SERIAL PRIMARY KEY,
	lbl TEXT,
	lbl2 TEXT DEFAULT 'lbl2'
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo', 'surface', 'g',
	'POLYGON'
);

CREATE TABLE topo.border (
	k SERIAL PRIMARY KEY,
	lbl TEXT,
	lbl2 TEXT DEFAULT 'lbl2'
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo', 'border', 'g',
	'LINE'
);

-------------------------------------
-- Create colMapProvider function
-------------------------------------

--{
CREATE FUNCTION topo.testColMapProvider(typ char, act char, usr JSONB)
RETURNS JSONB AS $BODY$
DECLARE
	procName TEXT := 'testColMapProvider';
	map JSONB;
BEGIN
	map := usr -> format('%s%s', typ, act);

	RAISE DEBUG '%: map for typ:% act:% is %', procName, typ, act, map;

	RETURN map;
END;
$BODY$ LANGUAGE 'plpgsql'; --}



------------------------------------------------------
-- Test the add_border_split_surface function
------------------------------------------------------

CREATE TABLE topo.add_border_results(
	test TEXT,
	id TEXT,
	typ CHAR,
	act CHAR,
	frm TEXT
);

-- {
CREATE FUNCTION check_add_border_effects(
	testname TEXT
)
RETURNS TABLE(o TEXT)
AS $BODY$
DECLARE
  rec RECORD;
  sql TEXT;
  attrs TEXT[];
BEGIN

	RETURN QUERY
	SELECT array_to_string(ARRAY[
			testname,
			'act',
			id,
			typ,
			act,
			frm
		], '|')
	FROM topo.add_border_results
	WHERE test = testname
	ORDER BY typ, id::int;

	RETURN QUERY
	SELECT array_to_string(ARRAY[
			testname,
			'brd',
			k::text,
			lbl,
			COALESCE(lbl2, ''),
			ST_AsText(
				normalized_geom(
					ST_Simplify(
						ST_CollectionHomogenize(g),
						0
					)
				)
			)
		], '|')
	FROM topo.border
	WHERE k in (
		SELECT id::int
		FROM topo.add_border_results
		WHERE test = testname
		  AND typ = 'B'
	)
	ORDER BY k;

	RETURN QUERY
	SELECT array_to_string(ARRAY[
			testname,
			'srf',
			k::text,
			lbl,
			COALESCE(lbl2, ''),
			ST_AsText(
				ST_Simplify(
					normalized_geom(
						ST_CollectionHomogenize(g)
					),
					0
				)
			)
		], '|')
	FROM topo.surface
	WHERE k in (
		SELECT id::int
		FROM topo.add_border_results
		WHERE test = testname
		  AND typ = 'S'
	)
	ORDER BY k;

END;
$BODY$ LANGUAGE 'plpgsql';
--}

--
-- t1.e1: add a non-closed border splitting no face
--
--   . . . . . .
--
SELECT 't1.e1', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [ [ 0, 0 ], [ 1, 0 ] ],
			"type" : "LineString"
		},
		"properties" : {},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.border', 'g', 'k', -- border layer identifier
	0.0, -- tolerance
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	'{}'::jsonb -- colMapProviderParam
);

--
-- t1.e2: add a closed border, with missing CRS indication
--
--    ,---------.
--    |         |
--    |         |
--    |         |
--    |      S1 |
--    `-B1------'
--

INSERT INTO topo.add_border_results
SELECT 't1.e1', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"coordinates" : [
				[ 0, 0 ],
				[ 10, 0 ],
				[ 10, 10 ],
				[ 0, 10 ],
				[ 0, 0 ]
			],
			"type" : "LineString"
		},
		"properties" : {
			"srf" : { "lbl": "t1-srf" },
			"brd" : { "lbl": "t1-brd" }
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'{
		"lbl": [ "srf", "lbl" ],
		"lbl2": [ "srf", "lbl" ]
	}', -- surface layer colMap
	'topo.border', 'g', 'k', -- border layer identifier
	'{
		"lbl": [ "brd", "lbl" ],
		"lbl2": [ "brd", "lbl" ]
	}', -- border layer colMap
	0.0 -- tolerance
)
ORDER BY typ, fid::int
;


--
-- t1: add a closed border, creating a new square surface
--
--    ,---------.
--    |         |
--    |         |
--    |         |
--    |      S1 |
--    `-B1------'
--

INSERT INTO topo.add_border_results
SELECT 't1', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [
				[ 0, 0 ],
				[ 10, 0 ],
				[ 10, 10 ],
				[ 0, 10 ],
				[ 0, 0 ]
			],
			"type" : "LineString"
		},
		"properties" : {
			"srf" : { "lbl": "t1-srf" },
			"brd" : { "lbl": "t1-brd" }
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'{
		"lbl": [ "srf", "lbl" ],
		"lbl2": [ "srf", "lbl" ]
	}', -- surface layer colMap
	'topo.border', 'g', 'k', -- border layer identifier
	'{
		"lbl": [ "brd", "lbl" ],
		"lbl2": [ "brd", "lbl" ]
	}', -- border layer colMap
	0.0 -- tolerance
)
ORDER BY typ, fid::int
;

SELECT * FROM check_add_border_effects('t1');

--
-- t2.e1: add a non-closed border splitting no face
--
--
--
--    ,-----------.
--    |  S1       |
--    |           |
--    |     .     |
--    |     .     |
--    `-B1--------'
--          .
--          .
--
SELECT 't2.e1', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [ [ 5, -5 ], [ 5, 5 ] ],
			"type" : "LineString"
		},
		"properties" : {},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.border', 'g', 'k', -- border layer identifier
	0.0, -- tolerance
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	'{}'::jsonb -- colMapProviderParam
);

--
-- t2.e2: add a non-closed border crossing the existing
--        border more than twice
--
--
--
--    ,-----------.
--    |  S1     . | . .
--    |           |   .
--    |     . . . | . .
--    |     .     |
--    `-B1--------'
--          .
--          .
--
SELECT 't2.e2', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [ [ 5, -5 ], [ 5, 5 ], [ 15, 5 ], [ 15, 8 ], [ 8, 8 ] ],
			"type" : "LineString"
		},
		"properties" : {},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.border', 'g', 'k', -- border layer identifier
	0.0, -- tolerance
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	'{}'::jsonb -- colMapProviderParam
);

--
-- t2: add a vertical border splitting the surface created by 't1';
--     the border exceed the area covered by the existing surface
--     (we expect it to be cut)
--
--          ^
--          .
--    ,-----+-----.
--    |     |     |
--    |    B3     |
--    | S2  |  S1 |
--    |     |     |
--    `-B1--+--B2-'
--          .
--          .
--

INSERT INTO topo.add_border_results
SELECT 't2', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [
				[ 5, -2 ],
				[ 5, 12 ]
			],
			"type" : "LineString"
		},
		"properties" : {
			"srf" : { "lbl": "t2-srf" },
			"brd" : { "lbl": "t2-brd" }
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'{ "lbl": [ "srf", "lbl" ] }', -- surface layer colMap
	'topo.border', 'g', 'k', -- border layer identifier
	'{ "lbl": [ "brd", "lbl" ] }', -- border layer colMap
	0.0 -- tolerance
)
ORDER BY typ, fid::int
;

SELECT * FROM check_add_border_effects('t2');


--
-- t3: add a border forming an extrusion from the surfaces
--     created by 't1' (S1) and 't2' (S2), sharing the
--     longest border with S1.
--
--
--       ,--B6--------.
--       |      S3    |
--    ,--+-B4-+--B5---+
--    |  .    |       |
--    |      B3       |
--    | S2    |  S1   |
--    |       |       |
--    `-B1----+--B2---'
--
--
--

-- Update the lbl2 value for the surface on the right
-- to be able to verify the new surface inherits from it
UPDATE topo.surface
SET lbl2 = 't1-srf-R'
WHERE k = 1;
INSERT INTO topo.add_border_results VALUES ('t3.prep', 1, 'S', 'M');
SELECT * FROM check_add_border_effects('t3.prep');

INSERT INTO topo.add_border_results
SELECT 't3', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [
				[ 4, 8 ],
				[ 4, 15 ],
				[ 10, 15 ],
				[ 10, 10 ]
			],
			"type" : "LineString"
		},
		"properties" : {
			"srf" : { "lbl": "t3-srf" },
			"brd" : { "lbl": "t3-brd" }
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'{ "lbl": [ "srf", "lbl" ] }', -- surface layer colMap
	'topo.border', 'g', 'k', -- border layer identifier
	'{ "lbl": [ "brd", "lbl" ] }', -- border layer colMap
	0.0 -- tolerance
)
ORDER BY typ, fid::int
;

SELECT * FROM check_add_border_effects('t3');

--
-- t4: add a border splitting surface S1
--     and using a colMapProvider function
--     (new signature) to use different label
--     for the modified and the split surfaces
--
--
--       ,--B6--------.
--       |      S3    |
--    ,--+-B4-+--B5---+
--    |       |       |
--    |      B8  S4  B7
--    |       |       |
--    | S2    +--B9---+ - -  -
--    |       |       |
--    |      B3  S1   |
--    |       |       |
--    `-B1----+--B2---'
--
--

-- Update the lbl2 value for the borders being split
-- to be able to verify the split borders inherits from it
UPDATE topo.border
SET lbl2 = 't2-origin'
WHERE k = 3;
INSERT INTO topo.add_border_results VALUES ('t4.prep', 3, 'B', 'M');
SELECT * FROM check_add_border_effects('t4.prep');

INSERT INTO topo.add_border_results
SELECT 't4', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [
				[ 5, 5 ],
				[ 15, 5 ]
			],
			"type" : "LineString"
		},
		"properties" : {
			"lbl.srf.mod" : "t4-srf-mod",
			"lbl.srf.spl" : "t4-srf-spl",
			"lbl.srf.new" : "t4-srf-new",
			"lbl.brd.mod" : "t4-brd-mod",
			"lbl.brd.new" : "t4-brd-new",
			"lbl.brd.spl" : "t4-brd-spl"
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.border', 'g', 'k', -- border layer identifier
	0.0,
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	$$ {
		"BM": { "lbl": [ "lbl.brd.mod" ] },
		"BC": { "lbl": [ "lbl.brd.new" ] },
		"BS": { "lbl": [ "lbl.brd.spl" ] },
		"SM": { "lbl": [ "lbl.srf.mod" ] },
		"SC": { "lbl": [ "lbl.srf.new" ] },
		"SS": { "lbl": [ "lbl.srf.spl" ] }
	} $$::jsonb -- colMapProviderParam
)
ORDER BY typ, fid::int
;

SELECT * FROM check_add_border_effects('t4');

--------------------------------------------------
-- t5: add a border creating a new surface
--     and using a colMapProvider function
--     (new signature) with the _same_ parameter
--     as in in t4 to show newly created Surface
--     objects have a different col map due
--     to the colMapProvider.
--
--
--       ,--B6--------.
--       |      S3    |
--    ,--+-B4-+--B5---+
--    |       |       |
--    |      B8  S4  B7
--    |       |       |
--    | S2    +--B9---+-(B11)--.
--    |       |       |        |
--    |      B3  S1 [B10] (S5) |
--    |       |       |        |
--    `-B1----+--B2---+--------'
--
--

INSERT INTO topo.add_border_results
SELECT 't5', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [
				[ 10, 0 ],
				[ 15, 0 ],
				[ 15, 5 ],
				[ 10, 5 ]
			],
			"type" : "LineString"
		},
		"properties" : {
			"lbl.srf.mod" : "t5-srf-mod",
			"lbl.srf.spl" : "t5-srf-spl",
			"lbl.srf.new" : "t5-srf-new",
			"lbl.brd.mod" : "t5-brd-mod",
			"lbl.brd.spl" : "t5-brd-spl",
			"lbl.brd.new" : "t5-brd-new"
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.border', 'g', 'k', -- border layer identifier
	0.0,
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	$$ {
		"BM": { "lbl": [ "lbl.brd.mod" ] },
		"BC": { "lbl": [ "lbl.brd.new" ] },
		"BS": { "lbl": [ "lbl.brd.spl" ] },
		"SM": { "lbl": [ "lbl.srf.mod" ] },
		"SC": { "lbl": [ "lbl.srf.new" ] },
		"SS": { "lbl": [ "lbl.srf.spl" ] }
	} $$::jsonb -- colMapProviderParam
)
ORDER BY typ, fid::int
;

SELECT * FROM check_add_border_effects('t5');

-----------------------------------------------------
-- t6: add an closed border splitting a border twice
--
-- TODO: avoid creating disjoint B14 and B16 but prefer
--       a single new border
--
--
--                 ,--B6-----------.
--                 |      S3       |
--         ,-------+-B4----+--B5---+
--         |               |       |
--       [B1]             B8  S4  B7
--         |               |       |
--         |     [S6]      +--B9---+---B11-.
--         |               |       |       |
-- o-(B14)-+-(B15)-.      B3  S1  B10  S5  |
-- |       |       |       |       |       |
-- |       |  [S2] |       |       |       |
-- |       |       |       |       |       |
-- |       `-[B13]-+-[B12]-+--B2---+-------'
-- | (S7)          |
-- |               |
-- `----(B16)------'
--

--select 'EDGE', edge_id, ST_AsText(geom) FROM topo.edge oRDER BY edge_id;
--set client_min_messages to DEBUG;

INSERT INTO topo.add_border_results
SELECT 't6', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [
				[ -3, 3 ],
				[ 3, 3 ],
				[ 3, -3 ],
				[ -3, -3 ],
				[ -3, 3 ]
			],
			"type" : "LineString"
		},
		"properties" : {
			"lbl.srf.mod" : "t6-srf-mod",
			"lbl.srf.spl" : "t6-srf-spl",
			"lbl.srf.new" : "t6-srf-new",
			"lbl.brd.mod" : "t6-brd-mod",
			"lbl.brd.spl" : "t6-brd-spl",
			"lbl.brd.new" : "t6-brd-new"
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.border', 'g', 'k', -- border layer identifier
	0.0,
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	$$ {
		"BM": { "lbl": [ "lbl.brd.mod" ] },
		"BC": { "lbl": [ "lbl.brd.new" ] },
		"BS": { "lbl": [ "lbl.brd.spl" ] },
		"SM": { "lbl": [ "lbl.srf.mod" ] },
		"SC": { "lbl": [ "lbl.srf.new" ] },
		"SS": { "lbl": [ "lbl.srf.spl" ] }
	} $$::jsonb -- colMapProviderParam
)
ORDER BY typ, fid::int
;

SELECT * FROM check_add_border_effects('t6');

---------------------------------------------------------------------
-- t7: properly split a multi-edge Border
--
-- Reference:
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/100
--
--
--                 ,--B6-----------.
--                 |      S3       |
--         ,-------+-B4----+--B5---+
--         |               |       |
--        B1              B8  S4  B7
--         |               |       |
--         |      S6       +--B9---+--------x {15,5}
--         |               |       |        |
--         |               |       |  [S8]  |
--         |               |       |        |
-- +--B14--+--B15--.      B3      B17     [B18]
-- |       |       |       |       |        |
-- |       |       |       |  S1   +--(B19)-+ {15,3}
-- |       |       |       |       |        |
-- |       |   S2  |       |       | [S5]   |
-- |       |       |       |       |        |
-- |       |       |       |     [B10]    [B11]
-- |       |       |       |       |        |
-- |       `--B13--+--B12--+--B2---+--------'
-- |  S7           |            {10,0}
-- |               |
-- `-----B16-------'
--

-- Turn B11 into a multi-edge border
SELECT NULL FROM topology.TopoGeo_addPoint('topo', 'SRID=4326;POINT(15 5)');

INSERT INTO topo.add_border_results
SELECT 't7', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [
				[ 10, 3 ],
				[ 15, 3 ]
			],
			"type" : "LineString"
		},
		"properties" : {
			"lbl.srf.mod" : "t7-srf-mod",
			"lbl.srf.spl" : "t7-srf-spl",
			"lbl.srf.new" : "t7-srf-new",
			"lbl.brd.mod" : "t7-brd-mod",
			"lbl.brd.spl" : "t7-brd-spl",
			"lbl.brd.new" : "t7-brd-new"
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.border', 'g', 'k', -- border layer identifier
	0.0, -- tolerance
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	$$ {
		"BM": { "lbl": [ "lbl.brd.mod" ] },
		"BC": { "lbl": [ "lbl.brd.new" ] },
		"BS": { "lbl": [ "lbl.brd.spl" ] },
		"SM": { "lbl": [ "lbl.srf.mod" ] },
		"SC": { "lbl": [ "lbl.srf.new" ] },
		"SS": { "lbl": [ "lbl.srf.spl" ] }
	} $$::jsonb -- colMapProviderParam
)
ORDER BY typ, fid::int
;

SELECT * FROM check_add_border_effects('t7');

---------------------------------------------------------------------
-- t8: do not create multipolygon surfaces
--
-- Reference:
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/86
--
--
--                                   ,--B6-----------.
--                                   |      S3       |
--                           ,-------+-B4----+--B5---+
--                           |               |       |
--                          B1              B8  S4  B7
--                           |               |       |
--                           |      S6       +--B9---+--------.
--                           |               |       |        |
--           ,-(B26)-.       |               |       |   S8   |
--           |       |       |               |       |        |
--           | (S11) |       |               |       |        |
--           |       |       |               |       |        |
--   +-[B14]-+-(B21)-+-(B20)-+--B15--.      B3      B17      B18
--   |       |       |       |       |       |       |        |
--   |       |       |       |       |       |  S1   +---B19--+
--   |       | (S10) |       |       |       |       |        |
-- [B16]     |       |       |   S2  |       |       |  S5    |
--   |       |       | [S7]  |       |       |       |        |
--   |     (B27)     |       |       |       |      B10      B11
--   |       |     (B25)     |       |       |       |        |
--   | (S9)  |       |       `--B13--+--B12--+--B2---+--------'
--   |       |       |     {0,0}     |
--   |       |       |               |
--   `-------+-(B23)-+----(B22)------'
--   {-2,-3} |       |
--           |       |
--           | (S12) |
--           |       |
--         (B28)     |
--           |       |
--  {-2,-5}  +-(B24)-' {-1,-5}


INSERT INTO topo.add_border_results
SELECT 't8', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [
				[ -2, -5 ],
				[ -1, -5 ],
				[ -1, 5 ],
				[ -2, 5 ],
				[ -2, -5 ]
			],
			"type" : "LineString"
		},
		"properties" : {
			"lbl.srf.mod" : "t8-srf-mod",
			"lbl.srf.spl" : "t8-srf-spl",
			"lbl.srf.new" : "t8-srf-new",
			"lbl.brd.mod" : "t8-brd-mod",
			"lbl.brd.spl" : "t8-brd-spl",
			"lbl.brd.new" : "t8-brd-new"
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.border', 'g', 'k', -- border layer identifier
	0.0, -- tolerance
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	$$ {
		"BM": { "lbl": [ "lbl.brd.mod" ] },
		"BC": { "lbl": [ "lbl.brd.new" ] },
		"BS": { "lbl": [ "lbl.brd.spl" ] },
		"SM": { "lbl": [ "lbl.srf.mod" ] },
		"SC": { "lbl": [ "lbl.srf.new" ] },
		"SS": { "lbl": [ "lbl.srf.spl" ] }
	} $$::jsonb -- colMapProviderParam
)
ORDER BY typ, fid::int
;

SELECT * FROM check_add_border_effects('t8');

---------------------------------------------------------------------
-- t9: do not return same modified border multiple times
--
-- Reference:
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/134
--
--
--                                   ,--B6-----------.
--                                   |      S3       |
--                           ,-------+-B4----+--B5---+
--                           |               |       |
--                          B1              B8  S4  B7
--                           |               |       |
--                           |      S6       +--B9---+--------.
--                           |               |       |        |
--           ,--B26--.       |               |       |   S8   |
--           |       |       |               |       |        |
--           |  S11  |       |               |       |        |
--           |       |       |               |       |        |
--   +--B14--+--B21--+--B20--+--B15--.      B3      B17      B18
--   |       |       |       |       |       |       |        |
--   |       |       |       |       |       |  S1   +---B19--+
--   |       |  S10  |       |       |       |       |        |
--  B16      |       |       |   S2  |       |       |  S5    |
--   |       |       |  S7   |       |       |       |        |
--   |      B27      |       |       |       |      B10      B11
--   |       |      B25      |       |       |       |        |
--   |  S9   |       |       `--B13--+--B12--+--B2---+--------'
--   |       |       |     {0,0}     |
--   |       |       |               |
--   `-------+--B23--+-----B22-------'
--  {-2,-3}  |       | {-1,-3}
--           |       |
--         [B28]   (B29)
--           |       |
--           | (S13) |
--           |       |
--  {-2,-4}  +-(B32)-+ {-1,-4}
--           |       |
--         (B31)   (B30)
--           |       |
--           | [S12] |
--           |       |
--  {-2,-5}  +-[B24]-+ {-1,-5}
--           |       |
--           | (S14) |
--           |       |
--  {-2,-8}  `-(B33)-' {-1,-8}

INSERT INTO topo.add_border_results
SELECT 't9', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [
				[ -2, -4 ],
				[ -1, -4 ],
				[ -1, -8 ],
				[ -2, -8 ],
				[ -2, -4 ]
			],
			"type" : "LineString"
		},
		"properties" : {
			"lbl.srf.mod" : "t9-srf-mod",
			"lbl.srf.spl" : "t9-srf-spl",
			"lbl.srf.new" : "t9-srf-new",
			"lbl.brd.mod" : "t9-brd-mod",
			"lbl.brd.spl" : "t9-brd-spl",
			"lbl.brd.new" : "t9-brd-new"
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.border', 'g', 'k', -- border layer identifier
	0.0, -- tolerance
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	$$ {
		"BM": { "lbl": [ "lbl.brd.mod" ] },
		"BC": { "lbl": [ "lbl.brd.new" ] },
		"BS": { "lbl": [ "lbl.brd.spl" ] },
		"SM": { "lbl": [ "lbl.srf.mod" ] },
		"SC": { "lbl": [ "lbl.srf.new" ] },
		"SS": { "lbl": [ "lbl.srf.spl" ] }
	} $$::jsonb -- colMapProviderParam
)
ORDER BY typ, fid::int
;

SELECT * FROM check_add_border_effects('t9');

---------------------------------------------------------------------
-- t10: add an hole in an existing surface
--
-- Reference:
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/152
--
--
--  {-2,-5}  +----B24----+ {-1,-5}
--           |           |
--           | +-------. |
--           | |       | |
--           | | (S14) | |
--           | |       | |
--           | `-[B34]-' |
--           |           |
--           |   [S15]   |
--           |           |
--  {-2,-8}  `----B33----' {-1,-8}

INSERT INTO topo.add_border_results
SELECT 't10', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [
				[ -1.8, -7 ],
				[ -1.8, -6 ],
				[ -1.2, -6 ],
				[ -1.2, -7 ],
				[ -1.8, -7 ]
			],
			"type" : "LineString"
		},
		"properties" : {
			"lbl.srf.mod" : "t10-srf-mod",
			"lbl.srf.spl" : "t10-srf-spl",
			"lbl.srf.new" : "t10-srf-new",
			"lbl.brd.mod" : "t10-brd-mod",
			"lbl.brd.spl" : "t10-brd-spl",
			"lbl.brd.new" : "t10-brd-new"
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.border', 'g', 'k', -- border layer identifier
	0.0, -- tolerance
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	$$ {
		"BM": { "lbl": [ "lbl.brd.mod" ] },
		"BC": { "lbl": [ "lbl.brd.new" ] },
		"BS": { "lbl": [ "lbl.brd.spl" ] },
		"SM": { "lbl": [ "lbl.srf.mod" ] },
		"SC": { "lbl": [ "lbl.srf.new" ] },
		"SS": { "lbl": [ "lbl.srf.spl" ] }
	} $$::jsonb -- colMapProviderParam
)
ORDER BY typ, fid::int
;

SELECT * FROM check_add_border_effects('t10');

-----------------------------------------------------------------------
-- t11: add an line from an hole to the exterior boundary of a Surface
--
-- No new face is created, so we expect an exception
--
-- Reference:
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/156
--
--
--  {-2,-5}  +----B24--------+ {-1,-5}
--           |              /|
--           |             / |
--           |   {-1.2,-6}/  |
--           | +---------.   |
--           | |         |   |
--           | | (S14)   |   |
--           | |         |   |
--           | `-[B34]---'   |
--           |               |
--           |   [S15]       |
--           |               |
--  {-2,-8}  `----B33--------' {-1,-8}

INSERT INTO topo.add_border_results
SELECT 't11', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [
				[ -1.2, -6 ],
				[ -1, -5 ]
			],
			"type" : "LineString"
		},
		"properties" : {
			"lbl.srf.mod" : "t11-srf-mod",
			"lbl.srf.spl" : "t11-srf-spl",
			"lbl.srf.new" : "t11-srf-new",
			"lbl.brd.mod" : "t11-brd-mod",
			"lbl.brd.spl" : "t11-brd-spl",
			"lbl.brd.new" : "t11-brd-new"
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.border', 'g', 'k', -- border layer identifier
	0.0, -- tolerance
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	$$ {
		"BM": { "lbl": [ "lbl.brd.mod" ] },
		"BC": { "lbl": [ "lbl.brd.new" ] },
		"BS": { "lbl": [ "lbl.brd.spl" ] },
		"SM": { "lbl": [ "lbl.srf.mod" ] },
		"SC": { "lbl": [ "lbl.srf.new" ] },
		"SS": { "lbl": [ "lbl.srf.spl" ] }
	} $$::jsonb -- colMapProviderParam
)
ORDER BY typ, fid::int
;

SELECT * FROM check_add_border_effects('t11');

-----------------------------------------------------------------------
-- t12: add an closed line forming 2 tiny faces and an
-- acceptabily big one
--
-- Only the acceptabily big one is retained and no additional
-- vertices are created in the border not affected by the actually
-- used input line components (B34)
--
-- Reference:
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/157
--
--
--  {-2,-5}  +-------B24--------+ {-1,-5}
--           |                  |
--           |                [B33]
--           |                  |
--           |      {-1.2,-6}   |
--           | +------------.   |
--           | |            |   |
--           | |          , | - +----(B37)---. {2, -6.4}
--           | |            |   |            |
--           | |  S14     | | (B36)  (S16)   |
--           | |            |   |            |
--           | |          ' | - +--(B38)-----+ {2, -6.5}
--           | |            |   |
--           | `--B34-------'   |
--           |                  |
--           |    S15           |
--           |                  |
--  {-2,-8}  `----(B35)---------' {-1,-8}

INSERT INTO topo.add_border_results
SELECT 't12', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [
				[ 2, -6.5 ],
				[ 2, -6.4 ],
				[ -1.21, -6.4 ],
				[ -1.21, -6.5 ],
				[ 2, -6.5 ]
			],
			"type" : "LineString"
		},
		"properties" : {
			"lbl.srf.mod" : "t12-srf-mod",
			"lbl.srf.spl" : "t12-srf-spl",
			"lbl.srf.new" : "t12-srf-new",
			"lbl.brd.mod" : "t12-brd-mod",
			"lbl.brd.spl" : "t12-brd-spl",
			"lbl.brd.new" : "t12-brd-new"
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.border', 'g', 'k', -- border layer identifier
	0.0, -- tolerance
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	$$ {
		"BM": { "lbl": [ "lbl.brd.mod" ] },
		"BC": { "lbl": [ "lbl.brd.new" ] },
		"BS": { "lbl": [ "lbl.brd.spl" ] },
		"SM": { "lbl": [ "lbl.srf.mod" ] },
		"SC": { "lbl": [ "lbl.srf.new" ] },
		"SS": { "lbl": [ "lbl.srf.spl" ] }
	} $$::jsonb, -- colMapProviderParam
	0.2 -- minToleratedFaceArea
)
ORDER BY typ, fid::int
;

SELECT * FROM check_add_border_effects('t12');

-- Check no vertices have been added to
-- the edges forming border B34
SELECT 't12', 'B34-vertices', sum(ST_NumPoints(geom))
FROM topo.edge
WHERE edge_id IN (
	SELECT abs(r.element_id)
	FROM
		topo.relation r,
		topo.border b
	WHERE layer_id = layer_id(b.g)
	AND topogeo_id = id(b.g)
	AND b.k = '34'
);

-----------------------------------------------------------------------
-- t13: add a multiline to split a polygon without splitting its hole
--
-- Reference:
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/168
--
--
--     {-2,-5}       {-1.7,-5}          {-1,-5}
--           +-[B24]------+---(B39)-------+
--           |            |               |
--           |          (B42)  (S17)     B33
--           |            |               |
--           |  {-1.8,-6} |               |
--           |    +-------+----(B40)--.   |
--           |    |   {-1.7,-6}       |   |
--           |    |                   |   +
--           |    |                   |   |
--           |    |        S14        |  B36
--           |    |                   |   |
--           |    |                   |   + {-1,-6.5}
--           |    |         {-1.3,-7} |   |
--           |    `---[B34]-------+---'   |
--           |                    |       |
--           |       [S15]      (B43)     |
--           |                    |       |
--           `--------(B41)-------+-[B35]-'
--      {-2,-8}              {-1.3,-8}    {-1,-8}

INSERT INTO topo.add_border_results
SELECT 't13', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [
				[
					[-1.7,-5],
					[-1.7,-6]
				],
				[
					[-1.3,-7],
					[-1.3,-8]
				]
			],
			"type":"MultiLineString"
		},
		"properties" : {
			"lbl.srf.mod" : "t13-srf-mod",
			"lbl.srf.spl" : "t13-srf-spl",
			"lbl.srf.new" : "t13-srf-new",
			"lbl.brd.mod" : "t13-brd-mod",
			"lbl.brd.spl" : "t13-brd-spl",
			"lbl.brd.new" : "t13-brd-new"
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.border', 'g', 'k', -- border layer identifier
	0.0, -- tolerance
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	$$ {
		"BM": { "lbl": [ "lbl.brd.mod" ] },
		"BC": { "lbl": [ "lbl.brd.new" ] },
		"BS": { "lbl": [ "lbl.brd.spl" ] },
		"SM": { "lbl": [ "lbl.srf.mod" ] },
		"SC": { "lbl": [ "lbl.srf.new" ] },
		"SS": { "lbl": [ "lbl.srf.spl" ] }
	} $$::jsonb -- colMapProviderParam
)
ORDER BY typ, fid::int
;

SELECT * FROM check_add_border_effects('t13');

-----------------------------------------------------------------------
-- t14: add a multiline to split a polygon without splitting its hole
--
-- Reference:
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/168
--
--
--
--        {-1.8,-6}                   {-1.2,-6}
--                +-------+----[B40]--.
--                |                   |
--              (B44)   (S19)         |
--                |                   |
--   {-1.8,-6.5}  +-----(B47)---------+ {-1.2,-6.5)
--                |                   |
--                |      (S18)      (B45)
--                |                   |
--                `---[B34]-------+---'
--        {-1.8,-7}                   {-1.2,-7}
--
--

INSERT INTO topo.add_border_results
SELECT 't14', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [
				[-1.8,-6.5],
				[-1.2,-6.5]
			],
			"type":"LineString"
		},
		"properties" : {
			"lbl.srf.mod" : "t14-srf-mod",
			"lbl.srf.spl" : "t14-srf-spl",
			"lbl.srf.new" : "t14-srf-new",
			"lbl.brd.mod" : "t14-brd-mod",
			"lbl.brd.spl" : "t14-brd-spl",
			"lbl.brd.new" : "t14-brd-new"
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.border', 'g', 'k', -- border layer identifier
	0.0, -- tolerance
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	$$ {
		"BM": { "lbl": [ "lbl.brd.mod" ] },
		"BC": { "lbl": [ "lbl.brd.new" ] },
		"BS": { "lbl": [ "lbl.brd.spl" ] },
		"SM": { "lbl": [ "lbl.srf.mod" ] },
		"SC": { "lbl": [ "lbl.srf.new" ] },
		"SS": { "lbl": [ "lbl.srf.spl" ] }
	} $$::jsonb, -- colMapProviderParam
	maxAllowedSurfaceSplitCount => 5,
	keepModifiedSurface => false
)
ORDER BY typ, fid::int
;

SELECT * FROM check_add_border_effects('t14');

-----------------------------------------------------------------------
-- t15: split a Surface defined by two faces (synthetic edge!)
--
-- Reference:
--
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/258
--
--
--   {-1.8,-6.5}                  {-1.2,-6.5)
--        +-----[B47]-----+--(B47)--+
--        |               |    .    |
--        |               |    .    |
--        |             (B49)  .   B45
--        |               |    .    |
--        |               |    .    |
--        |  . (S20) . .  +  . .    |
--        |  .            |         |
--        |  .            |  [S18]  |
--        |  .            |         |
--        |  .          (B48)       |
--        |  .            |         |
--        `----B34--------+---------'
--   {-1.8,-7}       {-1.3,-7}   {-1.2,-7}
--
--

-- Add an edge (not used in the definition of any Border),
-- making Surface S18 defined by 2 faces
SELECT NULL FROM topology.TopoGeo_addLineString(
	'topo',
	'SRID=4326;LINESTRING(-1.7 -7, -1.7 -6.8, -1.25 -6.8, -1.25 -6.5)'
);

-- Add a vertical border splitting Surface S18
-- and both faces defining it
INSERT INTO topo.add_border_results
SELECT 't15', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [
				[-1.3,-7],
				[-1.3,-6.5]
			],
			"type":"LineString"
		},
		"properties" : {
			"lbl.srf.mod" : "t15-srf-mod",
			"lbl.srf.spl" : "t15-srf-spl",
			"lbl.srf.new" : "t15-srf-new",
			"lbl.brd.mod" : "t15-brd-mod",
			"lbl.brd.spl" : "t15-brd-spl",
			"lbl.brd.new" : "t15-brd-new"
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.border', 'g', 'k', -- border layer identifier
	0.0, -- tolerance
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	$$ {
		"BM": { "lbl": [ "lbl.brd.mod" ] },
		"BC": { "lbl": [ "lbl.brd.new" ] },
		"BS": { "lbl": [ "lbl.brd.spl" ] },
		"SM": { "lbl": [ "lbl.srf.mod" ] },
		"SC": { "lbl": [ "lbl.srf.new" ] },
		"SS": { "lbl": [ "lbl.srf.spl" ] }
	} $$::jsonb, -- colMapProviderParam
	maxAllowedSurfaceSplitCount => 5,
	keepModifiedSurface => true
)
ORDER BY typ, fid::int
;

SELECT * FROM check_add_border_effects('t15');

-----------------------------------------------------------------------
-- t16: create a Surface defined by three faces (synthetic edge!)
--
-- Reference:
--
-- https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/258
--
--
--
--             {0,-5} ,-----(B54)-------. {1, -5}
--                    |                 |
--                    |                 |
--                    |                 |
--          {0,-5.5}  + . . . . . . . . +
--                    |                 |
--                  (B53)             (B55)
--                    |                 |
--            {0,-6}  + . .  (S21)  . . +
--                    |                 |
--                    |                 |
--                  (B52)             (B56)
--                    |                 |
--  {-1, -6.4}        |                 |
--     +----(B50)-----+-----(B51)-------+--[B37]--. {2, -6.4}
--     |                                          |
--    B36    S16                                  |
--     |                                          |
--     +---B38------------------------------------+ {2, -6.5}
--  {-1, -6.5}
--
--

-- Add edges (not used in the definition of any Border),
-- making Surface S21 defined by 3 faces
SELECT NULL FROM topology.TopoGeo_addLineString(
	'topo',
	'SRID=4326;LINESTRING(-0 -5.5, 1 -5.5)'
);
SELECT NULL FROM topology.TopoGeo_addLineString(
	'topo',
	'SRID=4326;LINESTRING(-0 -6, 1 -6)'
);

-- Add a vertical border splitting Surface S18
-- and both faces defining it
INSERT INTO topo.add_border_results
SELECT 't16', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [
				[0,-6.4],
				[0,-5],
				[1,-5],
				[1,-6.4]
			],
			"type":"LineString"
		},
		"properties" : {
			"lbl.srf.mod" : "t16-srf-mod",
			"lbl.srf.spl" : "t16-srf-spl",
			"lbl.srf.new" : "t16-srf-new",
			"lbl.brd.mod" : "t16-brd-mod",
			"lbl.brd.spl" : "t16-brd-spl",
			"lbl.brd.new" : "t16-brd-new"
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.border', 'g', 'k', -- border layer identifier
	0.0, -- tolerance
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	$$ {
		"BM": { "lbl": [ "lbl.brd.mod" ] },
		"BC": { "lbl": [ "lbl.brd.new" ] },
		"BS": { "lbl": [ "lbl.brd.spl" ] },
		"SM": { "lbl": [ "lbl.srf.mod" ] },
		"SC": { "lbl": [ "lbl.srf.new" ] },
		"SS": { "lbl": [ "lbl.srf.spl" ] }
	} $$::jsonb, -- colMapProviderParam
	maxAllowedSurfaceSplitCount => 5,
	keepModifiedSurface => true
)
ORDER BY typ, fid::int
;

SELECT * FROM check_add_border_effects('t16');

-----------------------------------------------------------------------
-- t17: add a closed border creating an hole in an existing Surface
--
--
--
--             {0,-5} ,------------- B54 -----------. {1, -5}
--                    |                             |
--                    |  {0.2,-5.1}                 |
--                    |      +<------------.        |
--                    |      |             |        |
--                    |      |    (S22)    |        |
--                    |      |             |        |
--                    |      `---(B57)-----'        |
--                    |                {0.8,-5.4}   |
--                    |                             |
--          {0,-5.5}  +   .   .   .   .   .   .   . +
--                    |                             |
--                   B53                           B55
--                    |                             |
--                    |                             |
--            {0,-6}  +   .   [S21]   .   .   .   . +
--                    |                             |
--                    |                             |
--                   B52                           B56
--                    |                             |
--                    |                             |
--         {0, -6.4}  +------------- B51 -----------+ {1, -6.4}
--
--
-- Add border creating an hole in Surface S21
-- NOTE: we add it counter-clockwise to avoid issue #263
INSERT INTO topo.add_border_results
SELECT 't17', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [
				[0.2,-5.1],
				[0.2,-5.4],
				[0.8,-5.4],
				[0.8,-5.1],
				[0.2,-5.1]
			],
			"type":"LineString"
		},
		"properties" : {
			"lbl.srf.mod" : "t17-srf-mod",
			"lbl.srf.spl" : "t17-srf-spl",
			"lbl.srf.new" : "t17-srf-new",
			"lbl.brd.mod" : "t17-brd-mod",
			"lbl.brd.spl" : "t17-brd-spl",
			"lbl.brd.new" : "t17-brd-new"
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.border', 'g', 'k', -- border layer identifier
	0.0, -- tolerance
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	$$ {
		"BM": { "lbl": [ "lbl.brd.mod" ] },
		"BC": { "lbl": [ "lbl.brd.new" ] },
		"BS": { "lbl": [ "lbl.brd.spl" ] },
		"SM": { "lbl": [ "lbl.srf.mod" ] },
		"SC": { "lbl": [ "lbl.srf.new" ] },
		"SS": { "lbl": [ "lbl.srf.spl" ] }
	} $$::jsonb, -- colMapProviderParam
	maxAllowedSurfaceSplitCount => 5,
	keepModifiedSurface => true
)
ORDER BY typ, fid::int;
SELECT * FROM check_add_border_effects('t17');

-----------------------------------------------------------------------
-- t18: add a line splitting hole and crossing exterior ring
--
-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/261
--
--
--
--        {0,-5} ,------------- B54 -----------. {1, -5}
--               |                             |
--               |  {0.2,-5.1}                 |
--               |      +---(B58)-----.        |
--               |      |             |        |
--               |      |   (S23)     |        |
--               |      |             |        |
--    {0,-5.25}  |    - +----(B59)----+ - - - -|- - <-- incoming
--               |      |             |        |
--               |      |   [S22]     |        |
--               |      |             |        |
--               |      `---[B57]-----'        |
--               |                {0.8,-5.4}   |
--               |                             |
--     {0,-5.5}  +   .   .   .   .   .   .   . +
--               |                             |
--              B53                           B55
--               |                             |
--               |                             |
--       {0,-6}  +   .    S21    .   .   .   . +
--               |                             |
--               |                             |
--              B52                           B56
--               |                             |
--               |                             |
--    {0, -6.4}  +------------- B51 -----------+ {1, -6.4}
--
--
--
-- Add border crossing S22 (hole) and part of S21 (outer polygon)
-- The part crossing outer polygon is not retained as it does not
-- split the Surface.
--
INSERT INTO topo.add_border_results
SELECT 't18', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [
				[0.1,-5.25],
				[1.2,-5.25]
			],
			"type":"LineString"
		},
		"properties" : {
			"lbl.srf.mod" : "t18-srf-mod",
			"lbl.srf.spl" : "t18-srf-spl",
			"lbl.srf.new" : "t18-srf-new",
			"lbl.brd.mod" : "t18-brd-mod",
			"lbl.brd.spl" : "t18-brd-spl",
			"lbl.brd.new" : "t18-brd-new"
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.border', 'g', 'k', -- border layer identifier
	0.0, -- tolerance
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	$$ {
		"BM": { "lbl": [ "lbl.brd.mod" ] },
		"BC": { "lbl": [ "lbl.brd.new" ] },
		"BS": { "lbl": [ "lbl.brd.spl" ] },
		"SM": { "lbl": [ "lbl.srf.mod" ] },
		"SC": { "lbl": [ "lbl.srf.new" ] },
		"SS": { "lbl": [ "lbl.srf.spl" ] }
	} $$::jsonb, -- colMapProviderParam
	maxAllowedSurfaceSplitCount => 5,
	keepModifiedSurface => true
)
ORDER BY typ, fid::int;
SELECT * FROM check_add_border_effects('t18');

-----------------------------------------------------------------------
-- t19: add a closed line starting and ending on an existing node
--
--                  {10,15}
--      ,--B6-----------.        . {15,15}
--      |               |      .'|
--      |      S3       |    .'  |
--      |               |  .'   (B60)
--      |               |.'(S24) |
--      +-B4----+--B5---+--------' {15,10}
--              |       |
--              |       |
--             B8  S4  B7
--              |       |
--              |       |
--              |       |
--              +--B9---+
--
--
INSERT INTO topo.add_border_results
SELECT 't19', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [
				[10,10],
				[15,15],
				[15,10],
				[10,10]
			],
			"type":"LineString"
		},
		"properties" : {
			"lbl.srf.mod" : "t19-srf-mod",
			"lbl.srf.spl" : "t19-srf-spl",
			"lbl.srf.new" : "t19-srf-new",
			"lbl.brd.mod" : "t19-brd-mod",
			"lbl.brd.spl" : "t19-brd-spl",
			"lbl.brd.new" : "t19-brd-new"
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.border', 'g', 'k', -- border layer identifier
	0.0, -- tolerance
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	$$ {
		"BM": { "lbl": [ "lbl.brd.mod" ] },
		"BC": { "lbl": [ "lbl.brd.new" ] },
		"BS": { "lbl": [ "lbl.brd.spl" ] },
		"SM": { "lbl": [ "lbl.srf.mod" ] },
		"SC": { "lbl": [ "lbl.srf.new" ] },
		"SS": { "lbl": [ "lbl.srf.spl" ] }
	} $$::jsonb, -- colMapProviderParam
	maxAllowedSurfaceSplitCount => 5,
	keepModifiedSurface => true
)
ORDER BY typ, fid::int;
SELECT * FROM check_add_border_effects('t19');

-----------------------------------------------------------------------
-- t20: add a border splitting a closed edge incident to node of
--      degree > 2
--
--                  {10,15}   {15,15}     {20,15}
--      ,--B6-----------.        +---------,
--      |               |      .'|         |
--      |      S3       |    .'  |         |
--      |               |  .'   B60        |
--      |               |.' S24  |         |
--      +-B4----+--B5---+--------+---------'
--              |       |     {15,10}    {20,10}
--              |       |
--             B8  S4  B7
--              |       |
--              |       |
--              |       |
--              +--B9---+
--
--
INSERT INTO topo.add_border_results
SELECT 't20', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [
				[15,15],
				[20,15],
				[20,10],
				[15,10]
			],
			"type":"LineString"
		},
		"properties" : {
			"lbl.srf.mod" : "t20-srf-mod",
			"lbl.srf.spl" : "t20-srf-spl",
			"lbl.srf.new" : "t20-srf-new",
			"lbl.brd.mod" : "t20-brd-mod",
			"lbl.brd.spl" : "t20-brd-spl",
			"lbl.brd.new" : "t20-brd-new"
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.border', 'g', 'k', -- border layer identifier
	0.0, -- tolerance
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	$$ {
		"BM": { "lbl": [ "lbl.brd.mod" ] },
		"BC": { "lbl": [ "lbl.brd.new" ] },
		"BS": { "lbl": [ "lbl.brd.spl" ] },
		"SM": { "lbl": [ "lbl.srf.mod" ] },
		"SC": { "lbl": [ "lbl.srf.new" ] },
		"SS": { "lbl": [ "lbl.srf.spl" ] }
	} $$::jsonb, -- colMapProviderParam
	maxAllowedSurfaceSplitCount => 5,
	keepModifiedSurface => true
)
ORDER BY typ, fid::int;
SELECT * FROM check_add_border_effects('t20');

---------------------------------------------------------------------------
-- t21: add border which slightly misses vertices (but can snap to them)
--
-- see: https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/287
--
--          {15,15}     {20,15}
--          \  +---------,
--            /|         |
--           / \   S25   |
--          /  |         |
--         /   | \       |
--        /    |         |
--       /    B60        |
--      /      |     \   |
--     /  S24  |         |
--    +--------+--------\'
--          {15,10}    {20,10}
--
--

--
-- First we try with snap tolerance of 0.1 which fails to snap and
-- thus also splits surfaces S24, while we limit to 1 surface split
--
SELECT 't21.unexpected_success', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [
				[19,10],
				[14,15]
			],
			"type":"LineString"
		},
		"properties" : {
			"x" : "t21-whatever"
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.border', 'g', 'k', -- border layer identifier
	0.1, -- tolerance (does not snap, thus splits 2 surfaces)
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	$$ {
		"BM": { "lbl": [ "x" ] },
		"BC": { "lbl": [ "x" ] },
		"BS": { "lbl": [ "x" ] },
		"SM": { "lbl": [ "x" ] },
		"SC": { "lbl": [ "x" ] },
		"SS": { "lbl": [ "x" ] }
	} $$::jsonb, -- colMapProviderParam
	maxAllowedSurfaceSplitCount => 1
);

--
-- Then we try with snap tolerance of 1 which results
-- to only split surfaces S25, honouring the limit of 1 surface split
--
INSERT INTO topo.add_border_results
SELECT 't21', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [
				[20,10],
				[14,15]
			],
			"type":"LineString"
		},
		"properties" : {
			"lbl.srf.mod" : "t21-srf-mod",
			"lbl.srf.spl" : "t21-srf-spl",
			"lbl.srf.new" : "t21-srf-new",
			"lbl.brd.mod" : "t21-brd-mod",
			"lbl.brd.spl" : "t21-brd-spl",
			"lbl.brd.new" : "t21-brd-new"
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.border', 'g', 'k', -- border layer identifier
	1, -- tolerance (enough to snap, thus splitting a single Surface)
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	$$ {
		"BM": { "lbl": [ "lbl.brd.mod" ] },
		"BC": { "lbl": [ "lbl.brd.new" ] },
		"BS": { "lbl": [ "lbl.brd.spl" ] },
		"SM": { "lbl": [ "lbl.srf.mod" ] },
		"SC": { "lbl": [ "lbl.srf.new" ] },
		"SS": { "lbl": [ "lbl.srf.spl" ] }
	} $$::jsonb, -- colMapProviderParam
	maxAllowedSurfaceSplitCount => 1,
	keepModifiedSurface => true
)
ORDER BY typ, fid::int;
SELECT * FROM check_add_border_effects('t21');

---------------------------------------------------------------------------
-- t22: add border with self-intersecting multiline input
--
-- see: https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/288
--
--                    .
--                 . . . .
--                    .
--          +--[B19]--+--(B72)--+ {15,3}
--          |         |         |
--          | (S28)   |  [S5]   |
--          |         |         |
--          |       (B73)       |
--         B10        |       (B71)
--          |         |         |
--   {10,0} +--[B11]--+---------'
--                    .
--
INSERT INTO topo.add_border_results
SELECT 't22', * FROM topo_update.add_border_split_surface(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [
				[ [13,-1], [13,4] ],
				[ [12,4], [14,4] ]
			],
			"type":"MultiLineString"
		},
		"properties" : {
			"lbl.srf.mod" : "t22-srf-mod",
			"lbl.srf.spl" : "t22-srf-spl",
			"lbl.srf.new" : "t22-srf-new",
			"lbl.brd.mod" : "t22-brd-mod",
			"lbl.brd.spl" : "t22-brd-spl",
			"lbl.brd.new" : "t22-brd-new"
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.border', 'g', 'k', -- border layer identifier
	1, -- tolerance (enough to snap, thus splitting a single Surface)
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	$$ {
		"BM": { "lbl": [ "lbl.brd.mod" ] },
		"BC": { "lbl": [ "lbl.brd.new" ] },
		"BS": { "lbl": [ "lbl.brd.spl" ] },
		"SM": { "lbl": [ "lbl.srf.mod" ] },
		"SC": { "lbl": [ "lbl.srf.new" ] },
		"SS": { "lbl": [ "lbl.srf.spl" ] }
	} $$::jsonb, -- colMapProviderParam
	maxAllowedSurfaceSplitCount => 1,
	keepModifiedSurface => true
)
ORDER BY typ, fid::int;
SELECT * FROM check_add_border_effects('t22');



-----------
-- Cleanup
-----------

SELECT NULL FROM DropTopology('topo');
DROP FUNCTION check_add_border_effects(TEXT);
DROP FUNCTION normalized_geom;
