SET client_min_messages TO WARNING;
SET extra_float_digits TO -3;

------------------------------------------------------------
-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
-------------------------------------------------------------

\i :regdir/../../src/sql/topo_update/function_03_app_CreateSchema.sql
\i :regdir/../../src/sql/topo_update/function_03_app_do_AddPath.sql
\i :regdir/../../src/sql/topo_update/function_02_add_path.sql

-------------------------------------------
-- Utility schema (data and functions)
-------------------------------------------

\i :regdir/utils/app_test_utils.sql

CREATE TABLE util.add_path_results(
	test TEXT,
	id TEXT,
	act CHAR,
	frm TEXT
);

-- {
CREATE FUNCTION util.check_add_path_effects(
	testname TEXT
)
RETURNS TABLE(o TEXT)
AS $BODY$
DECLARE
  rec RECORD;
  sql TEXT;
  attrs TEXT[];
BEGIN

	RETURN QUERY
	SELECT array_to_string(ARRAY[
			testname,
			'act',
			id,
			act,
			frm
		], '|')
	FROM util.add_path_results
	WHERE test = testname
	ORDER BY id::int;

	RETURN QUERY
	SELECT array_to_string(ARRAY[
			testname,
			'pth',
			identifier::text,
			COALESCE(created_by, ''),
			COALESCE(last_modified_by, ''),
			COALESCE(kind, ''),
			ST_AsText(
				ST_Normalize(
					ST_Simplify(
						ST_CollectionHomogenize(g),
						0
					)
				)
			)
		], '|')
	FROM myproduct.path
	WHERE identifier in (
		SELECT id::int
		FROM util.add_path_results
		WHERE test = testname
	)
	ORDER BY identifier;

END;
$BODY$ LANGUAGE 'plpgsql';
--}


-------------------------------------
-- Create the app schema
-------------------------------------

SELECT * FROM topo_update.app_CreateSchema(
	'myproduct', '{
		"version": "1.0",
		"topology": {
			"srid": "4326"
		},
		"tables": [
			{
				"name": "path",
				"primary_key": "identifier",
				"attributes": [
					{ "name": "identifier", "type": "serial" },
					{ "name": "created_by", "type": "text", "default": "unknown-creator"},
					{ "name": "last_modified_by", "type": "text", "default": "unmodified" },
					{ "name": "kind", "type": "text", "default": "unknown-path-kind" }
				],
				"topogeom_columns": [
					{ "name": "g", "type": "lineal" }
				]
			}
		],
		"path_layer": {
			"table_name": "path",
			"geo_column": "g"
		},
		"operations": {
			"AddPath": {
				"parameters": {
					"snapTolerance": {
						"units": 1e-10,
						"unitsAreMeters": false
					},
					"minAllowedPathLength": {
						"units": 2,
						"unitsAreMeters": true
					}
				}
			}
		}

	}'
);

-------------------------------------
-- Add paths
-------------------------------------

DO $BODY$
BEGIN
	INSERT INTO util.add_path_results
	SELECT 't1.e1', * FROM topo_update.app_do_AddPath('myproduct',
		$$
		{
			"geometry" : {
				"crs" : {
					"properties" : { "name" : "EPSG:4326" },
					"type" : "name"
				},
				"coordinates" : [
					[ 0, 0 ], [ 1e-6, 0 ]
				],
				"type" : "LineString"
			},
			"properties" : {
				"new": { "created_by": "t1-new" },
				"modified": { "last_modified_by": "t1-mod" },
				"split": { "created_by": "t1-spl" }
			},
			"type" : "Feature"
		}
		$$
	);
EXCEPTION WHEN OTHERS THEN
	RAISE EXCEPTION '%', substring(SQLERRM,1,70);
END;
$BODY$ LANGUAGE 'plpgsql';

-- t1: draw a line
INSERT INTO util.add_path_results
SELECT 't1', * FROM topo_update.app_do_AddPath('myproduct',
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : { "name" : "EPSG:4326" },
				"type" : "name"
			},
			"coordinates" : [
				[ 0, 0 ], [ 10, 0 ]
			],
			"type" : "LineString"
		},
		"properties" : {
			"new": { "created_by": "t1-new", "last_modified_by": "t1-new", "kind": "t1-kind" },
			"modified": { "last_modified_by": "t1-mod" },
			"split": { "created_by": "t1-spl" }
		},
		"type" : "Feature"
	}
	$$
);
SELECT * FROM util.check_add_path_effects('t1');


-- t2: draw a line crossing first line
INSERT INTO util.add_path_results
SELECT 't2', * FROM topo_update.app_do_AddPath('myproduct',
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : { "name" : "EPSG:4326" },
				"type" : "name"
			},
			"coordinates" : [
				[ 5, -5 ], [ 5, 5 ]
			],
			"type" : "LineString"
		},
		"properties" : {
			"new": { "created_by": "t2-new", "last_modified_by": "t2-new", "kind": "t2-kind" },
			"modified": { "last_modified_by": "t2-mod" },
			"split": { "created_by": "t2-spl" }
		},
		"type" : "Feature"
	}
	$$
);
SELECT * FROM util.check_add_path_effects('t2');

-----------
-- Cleanup
-----------

SET extra_float_digits TO 0;
SELECT * FROM util.drop_generated_schema('myproduct');
DROP SCHEMA util CASCADE;

