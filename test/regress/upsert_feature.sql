BEGIN;

set client_min_messages to WARNING;

\i :regdir/utils/check_topology_changes.sql
\i :regdir/utils/check_layer_features.sql
\i :regdir/utils/topogeom_summary.sql

-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
\i :regdir/../../src/sql/topo_update/function_02_update_feature.sql
\i :regdir/../../src/sql/topo_update/function_02_insert_feature.sql
\i :regdir/../../src/sql/topo_update/function_02_upsert_feature.sql

---------------------
-- Initialize schema
---------------------

SELECT NULL FROM CreateTopology('topo', 4326);

CREATE TABLE topo.poi (
    id INT PRIMARY KEY,
	lbl TEXT
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo',
	'poi',
	'tg',
	'POINT');

-- Start of operations
SELECT 'start', 'topo', * FROM check_topology_changes('topo');
SELECT 'start', 'poi', * FROM check_layer_features('topo.poi'::regclass, 'tg'::name);

CREATE TEMP TABLE upsert_results (
	test TEXT,
	id TEXT,
	new_tg topology.TopoGeometry,
	old_tg topology.TopoGeometry
);

CREATE TEMP TABLE tmp_tg (
	test TEXT,
	tg topology.TopoGeometry
);

---------------------------------------------------------
-- ups1: insert new feature (point 1), pass GeoJSON
---------------------------------------------------------

SELECT 'ups1', u.id, topogeom_summary(u.topogeom)
FROM topo_update.upsert_feature(
	$${
		"geometry": { "coordinates": [ 1,0 ], "type": "Point" },
		"properties": { "id": "1", "lbl": "new" },
		"type": "Feature"
	}$$, -- feature
	4326, -- srid
    'topo.poi', -- layerTable
    'tg', -- layerGeomColumn
    'id', -- layerIdColumn
	'{ "id": [ "id" ], "lbl": [ "lbl" ] }', -- layerColMap
	0 -- tolerance
) u;

SELECT 'ups1', 'topo', * FROM check_topology_changes('topo');
SELECT 'ups1', 'poi', * FROM check_layer_features('topo.poi'::regclass, 'tg'::name);

---------------------------------------------------------------
-- ups2: update existing feature (point 1), pass GeoJSON
---------------------------------------------------------------

SELECT 'ups2', u.id, topogeom_summary(u.topogeom)
FROM topo_update.upsert_feature(
	$${
		"geometry": { "coordinates": [ 1,1 ], "type": "Point" },
		"properties": { "id": "1", "lbl": "upd" },
		"type": "Feature"
	}$$, -- feature
	4326, -- srid
    'topo.poi', -- layerTable
    'tg', -- layerGeomColumn
    'id', -- layerIdColumn
	'{ "id": [ "id" ], "lbl": [ "lbl" ] }', -- layerColMap
	0 -- tolerance
) u;

SELECT 'ups2', 'topo', * FROM check_topology_changes('topo');
SELECT 'ups2', 'poi', * FROM check_layer_features('topo.poi'::regclass, 'tg'::name);

---------------------------------------------------------------
-- ups3: insert new feature (point 2), pass GeoJSON
---------------------------------------------------------------

SELECT 'ups3', u.id, topogeom_summary(u.topogeom)
FROM topo_update.upsert_feature(
	$${
		"geometry": { "coordinates": [ 2,0 ], "type": "Point" },
		"properties": { "id": "2", "lbl": "new" },
		"type": "Feature"
	}$$, -- feature
	4326, -- srid
    'topo.poi', -- layerTable
    'tg', -- layerGeomColumn
    'id', -- layerIdColumn
	'{ "id": [ "id" ], "lbl": [ "lbl" ] }', -- layerColMap
	0 -- tolerance
) u;

SELECT 'ups3', 'topo', * FROM check_topology_changes('topo');
SELECT 'ups3', 'poi', * FROM check_layer_features('topo.poi'::regclass, 'tg'::name);

---------------------------------------------------------------
-- ups4: update existing feature (point 2), pass geometry
---------------------------------------------------------------

SELECT 'ups4', u.id, topogeom_summary(u.topogeom)
FROM topo_update.upsert_feature(
	'SRID=4326;POINT(2 1)'::geometry, -- feature_geometry
	$$ { "id": "2", "lbl": "upd" } $$, -- feature_properties
    'topo.poi', -- layerTable
    'tg', -- layerGeomColumn
    'id', -- layerIdColumn
	'{ "id": [ "id" ], "lbl": [ "lbl" ] }', -- layerColMap
	0 -- tolerance
) u;

SELECT 'ups4', 'topo', * FROM check_topology_changes('topo');
SELECT 'ups4', 'poi', * FROM check_layer_features('topo.poi'::regclass, 'tg'::name);

--------------------------------------------------------------------------
-- ups5: update existing feature (point 2), pass geometry, keep old alive
--------------------------------------------------------------------------

INSERT INTO upsert_results(test, id, new_tg, old_tg)
SELECT 'ups5', u.id, topogeom, old_topogeom
-- topogeom_summary(u.topogeom)
FROM topo_update.upsert_feature(
	'SRID=4326;POINT(2 1)'::geometry, -- feature_geometry
	$$ { "id": "2", "lbl": "upd.geom.ka" } $$, -- feature_properties
    'topo.poi', -- layerTable
    'tg', -- layerGeomColumn
    'id', -- layerIdColumn
	'{ "id": [ "id" ], "lbl": [ "lbl" ] }', -- layerColMap
	0, -- tolerance
	false -- keep alive the old TopoGeometry
) u;

SELECT test, 'ups', id, ST_AsEWKT(old_tg)
FROM upsert_results
WHERE test = 'ups5';

SELECT 'ups5', 'topo', * FROM check_topology_changes('topo');
SELECT 'ups5', 'poi', * FROM check_layer_features('topo.poi'::regclass, 'tg'::name, true);

SELECT test, 'clean', topogeom_summary(clearTopoGeom(old_tg))
FROM upsert_results
WHERE test = 'ups5';

--------------------------------------------------------------------------
-- ups6: update existing feature (point 1), pass GeoJSON, keep old alive
--------------------------------------------------------------------------

INSERT INTO upsert_results(test, id, new_tg, old_tg)
SELECT 'ups6', u.id, topogeom, old_topogeom
-- topogeom_summary(u.topogeom)
FROM topo_update.upsert_feature(
	$${
		"geometry": { "coordinates": [ 1,0 ], "type": "Point" },
		"properties": { "id": "1", "lbl": "upd.json.ka" },
		"type": "Feature"
	}$$, -- feature
	4326, -- srid
    'topo.poi', -- layerTable
    'tg', -- layerGeomColumn
    'id', -- layerIdColumn
	'{ "id": [ "id" ], "lbl": [ "lbl" ] }', -- layerColMap
	0, -- tolerance
	false -- keep alive the old TopoGeometry
) u;

SELECT test, 'ups', id, ST_AsEWKT(old_tg)
FROM upsert_results
WHERE test = 'ups6';

SELECT 'ups6', 'topo', * FROM check_topology_changes('topo');
SELECT 'ups6', 'poi', * FROM check_layer_features('topo.poi'::regclass, 'tg'::name, true);

SELECT test, 'clean', topogeom_summary(clearTopoGeom(old_tg))
FROM upsert_results
WHERE test = 'ups6';

--------------------------------------------------------------------------
-- ups7: update existing feature (point 1), pass TopoGeomElementArray
--------------------------------------------------------------------------

INSERT INTO upsert_results(test, id, new_tg, old_tg)
SELECT 'ups7', u.id, topogeom, topogeom
-- topogeom_summary(u.topogeom)
FROM topo_update.upsert_feature(
	'{{1,1}}',
	$${
		"id": "1", "lbl": "upd.elems"
	}$$, -- feature_properties
    'topo.poi', -- layerTable
    'tg', -- layerGeomColumn
    'id', -- layerIdColumn
	'{ "id": [ "id" ], "lbl": [ "lbl" ] }' -- layerColMap
) u;

SELECT test, 'ups', id, ST_AsEWKT(old_tg), id(old_tg) = id(new_tg)
FROM upsert_results
WHERE test = 'ups7';

SELECT 'ups7', 'topo', * FROM check_topology_changes('topo');
SELECT 'ups7', 'poi', * FROM check_layer_features('topo.poi'::regclass, 'tg'::name, true);

--------------------------------------------------------------------------
-- ups8: add new feature (point 3), pass TopoGeomElementArray
--------------------------------------------------------------------------

INSERT INTO upsert_results(test, id, new_tg, old_tg)
SELECT 'ups8', u.id, topogeom, topogeom
-- topogeom_summary(u.topogeom)
FROM topo_update.upsert_feature(
	'{{3,1}}',
	$${
		"id": "3", "lbl": "ins.elems"
	}$$, -- feature_properties
    'topo.poi', -- layerTable
    'tg', -- layerGeomColumn
    'id', -- layerIdColumn
	'{ "id": [ "id" ], "lbl": [ "lbl" ] }' -- layerColMap
) u;

SELECT test, 'ups', id, ST_AsEWKT(new_tg), id(old_tg) = id(new_tg)
FROM upsert_results
WHERE test = 'ups8';

SELECT 'ups8', 'topo', * FROM check_topology_changes('topo');
SELECT 'ups8', 'poi', * FROM check_layer_features('topo.poi'::regclass, 'tg'::name, true);

--------------------------------------------------------------------------
-- Cleanup
--------------------------------------------------------------------------


ROLLBACK;

