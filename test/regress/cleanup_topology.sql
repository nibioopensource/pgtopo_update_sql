
BEGIN;

set client_min_messages to ERROR;

\i :regdir/utils/check_topology.sql
\i :regdir/utils/check_layer_features.sql
\i :regdir/utils/topogeom_summary.sql

-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
\i :regdir/../../src/sql/topo_update/function_02_cleanup_topology.sql

---------------------
-- Initialize schema
---------------------

SELECT NULL FROM CreateTopology('topo');

CREATE TABLE topo.poi (
    id INT PRIMARY KEY,
	lbl TEXT
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo',
	'poi',
	'tg',
	'POINT');

CREATE TABLE topo.lin (
    id INT PRIMARY KEY,
	lbl TEXT
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo',
	'lin',
	'tg',
	'LINE');

CREATE TABLE topo.are (
    id INT PRIMARY KEY,
	lbl TEXT
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo',
	'are',
	'tg',
	'POLYGON');

SELECT 'start', 'topo', * FROM check_topology('topo');

INSERT INTO topo.poi (id, lbl, tg) VALUES
( 1, 'orig', toTopoGeom('POINT(0 0)', 'topo', 1, 0) ),
( 2, 'orig', toTopoGeom('POINT(10 10)', 'topo', 1, 0) )
;

SELECT 't1', 'topo-before-clean', * FROM check_topology('topo');

SELECT 't1', 'clean', * FROM topo_update.cleanup_topology(
	( SELECT t FROM topology.topology t WHERE name = 'topo' ),
	ST_MakeEnvelope(0,0,20,20)
);

SELECT NULL FROM (
	SELECT clearTopoGeom(tg) FROM topo.poi WHERE id = 1
) x;

SELECT 't2', 'topo-before-clean', * FROM check_topology('topo');

-- Only node 1 is removed because nodes 2 and 3 are used by poi
-- features
SELECT 't2', 'clean', * FROM topo_update.cleanup_topology(
	( SELECT t FROM topology.topology t WHERE name = 'topo' ),
	ST_MakeEnvelope(0,0,20,20)
);

SELECT 't2', 'topo-after-clean', * FROM check_topology('topo', true);


-- Unreference all point features (leaves the nodes behind)
WITH deleted AS (
	DELETE FROM topo.poi
	RETURNING tg
), clear AS (
	SELECT clearTopoGeom(tg) FROM deleted
) SELECT NULL FROM clear;

-- Add lines forming a square and its diagonal
-- NOTE: the diagonal line will be split by existing node at 10,10
INSERT INTO topo.lin (id, lbl, tg) VALUES
( 1, 'orig', toTopoGeom('LINESTRING(0 0, 20 0, 20 20)', 'topo', 2, 0) ),
( 2, 'orig', toTopoGeom('LINESTRING(0 0, 0 20, 20 20)', 'topo', 2, 0)),
( 3, 'orig', toTopoGeom('LINESTRING(0 0, 20 20)', 'topo', 2, 0) )
;

-- Add polygon bound by the square
INSERT INTO topo.are (id, lbl, tg) VALUES
( 1, 'orig', toTopoGeom('POLYGON((0 0, 20 0, 20 20,0 20,0 0))', 'topo', 3, 0) )
;

-- Unreference the 2 edges forming the diagonal line
WITH deleted AS (
	DELETE FROM topo.lin WHERE id = 3
	RETURNING tg
), clear AS (
	SELECT clearTopoGeom(tg) FROM deleted
) SELECT NULL FROM clear;

SELECT 't3', 'topo-before-clean', * FROM check_topology('topo');

SELECT 't3', 'clean', * FROM topo_update.cleanup_topology(
	( SELECT t FROM topology.topology t WHERE name = 'topo' ),
	ST_MakeEnvelope(0,0,20,20)
);

SELECT 't3', 'topo-after-clean', * FROM check_topology('topo', true);


--SELECT edge_id, st_astext(geom) from topo.edge order by edge_id;
-- +2|LINESTRING(0 0,0 20,20 20)
-- +1|LINESTRING(0 0,20 0,20 20)
--SELECT id, ST_AsText(tg) FROM topo.lin order by id;
-- +1|MULTILINESTRING((0 0,20 0,20 20))
-- +2|MULTILINESTRING((0 0,0 20,20 20))

-- Add nodes which will become internal
-- to topo.lin id 1 and 2
SELECT NULL FROM (
	SELECT TopoGeo_AddPoint('topo', x.geom)
	FROM ST_DumpPoints('MULTIPOINT(0 10, 0 20, 10 20,10 0,20 0,20 10)') x
) foo;

SELECT 't4', 'topo-before-clean', * FROM check_topology('topo');

-- Cleaning topology now will result in all
-- internal nodes being removed
SELECT 't4', 'clean', * FROM topo_update.cleanup_topology(
	( SELECT t FROM topology.topology t WHERE name = 'topo' ),
	ST_MakeEnvelope(0,0,20,20)
);

SELECT 't4', 'topo-after-clean', * FROM check_topology('topo', true);

-- SELECT edge_id, st_astext(geom) from topo.edge order by edge_id;
-- +2|LINESTRING(0 0,0 10,0 20,10 20,20 20)
-- +10|LINESTRING(0 0,10 0,20 0,20 10,20 20)
-- SELECT id, ST_AsText(tg) FROM topo.lin order by id;
-- +1|MULTILINESTRING((0 0,10 0,20 0,20 10,20 20))
-- +2|MULTILINESTRING((0 0,0 10,0 20,10 20,20 20))

-- Add a vertical and an horizontal edge splitting the
-- (0 0,20 20) box into 4 faces
SELECT NULL FROM (
	SELECT TopoGeo_AddLineString('topo', x.geom)
	FROM ( VALUES
		('LINESTRING(5 0, 5 20)'),
		('LINESTRING(0 5, 20 5)')
	) x(geom)
) foo;

SELECT 't5', 'topo-before-clean', * FROM check_topology('topo');

-- Cleaning topology now should result in
-- going back to 1 face only
SELECT 't5', 'clean', * FROM topo_update.cleanup_topology(
	( SELECT t FROM topology.topology t WHERE name = 'topo' ),
	ST_MakeEnvelope(0,0,20,20)
);

SELECT 't5', 'topo-after-clean', * FROM check_topology('topo', true);

-- SELECT edge_id, st_astext(geom) from topo.edge order by edge_id;
-- +2|LINESTRING(0 0,0 5,0 10,0 20,5 20,10 20,20 20)
-- +11|LINESTRING(0 0,5 0,10 0,20 0,20 5,20 10,20 20)
-- SELECT id, ST_AsText(tg) FROM topo.lin order by id;
-- +1|MULTILINESTRING((0 0,5 0,10 0,20 0,20 5,20 10,20 20))
-- +2|MULTILINESTRING((0 0,0 5,0 10,0 20,5 20,10 20,20 20))
-- SELECT id, ST_AsText(tg) FROM topo.are order by id;
-- 1|MULTIPOLYGON(((20 20,20 10,20 5,20 0,10 0,5 0,0 0,0 5,0 10,0 20,5 20,10 20,20 20)))

-- Unreference the top-left edge
WITH deleted_line AS (
	DELETE FROM topo.lin WHERE id = 2
	RETURNING tg
), deleted_surface AS (
	DELETE FROM topo.are WHERE id = 1
	RETURNING tg
), clear AS (
	SELECT clearTopoGeom(tg) FROM deleted_line
		UNION
	SELECT clearTopoGeom(tg) FROM deleted_surface
) SELECT NULL FROM clear;

SELECT 't6', 'topo-before-clean', * FROM check_topology('topo');

-- Cleaning topology now should result in
-- going down to no faces at all and a single edge
SELECT 't6', 'clean', * FROM topo_update.cleanup_topology(
	( SELECT t FROM topology.topology t WHERE name = 'topo' ),
	ST_MakeEnvelope(0,0,20,20)
);

SELECT 't6', 'topo-after-clean', * FROM check_topology('topo', true);

-- SELECT edge_id, st_astext(geom) from topo.edge order by edge_id;
-- +10|LINESTRING(0 0,5 0,10 0,20 0,20 5,20 10,20 20)
-- SELECT id, ST_AsText(tg) FROM topo.lin order by id;
-- +1|MULTILINESTRING((0 0,5 0,10 0,20 0,20 5,20 10,20 20))
-- SELECT id, ST_AsText(tg) FROM topo.are order by id;
-- (none)

-- Drop linear TopoGeometry reference to the bottom-right edge
WITH deleted_line AS (
	DELETE FROM topo.lin WHERE id = 1
	RETURNING tg
), clear AS (
	SELECT clearTopoGeom(tg) FROM deleted_line
) SELECT NULL FROM clear;

-- Simplify the left-over edge
SELECT NULL FROM (
	SELECT ST_ChangeEdgeGeom('topo',
		edge_id,
		ST_Simplify(geom, 0)
	) FROM topo.edge
) foo;

-- Add a closing edge and an areal TopoGeometry reference
-- to resulting triangle face
INSERT INTO topo.are (id, lbl, tg) VALUES
( 1, 't7', toTopoGeom('POLYGON((0 0,20 0,20 20,0 0))', 'topo', 3, 0) )
;

-- Add another edge creating a new unreferenced face
SELECT NULL FROM TopoGeo_AddLineString('topo', 'LINESTRING(0 0,0 20,20 20)' );

SELECT 't7', 'topo-before-clean', * FROM check_topology('topo');

-- Cleaning should clean NOTHING, as all 2 edges are needed
-- to retain definition of the triangle face
SELECT 't7', 'clean', * FROM topo_update.cleanup_topology(
	( SELECT t FROM topology.topology t WHERE name = 'topo' ),
	ST_MakeEnvelope(0,0,20,20)
);

SELECT 't7', 'topo-after-clean', * FROM check_topology('topo', true);

SET CONSTRAINTS ALL IMMEDIATE;

ROLLBACK;
