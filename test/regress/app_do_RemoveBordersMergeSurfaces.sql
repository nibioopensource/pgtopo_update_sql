SET client_min_messages TO WARNING;

------------------------------------------------------------
-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
-------------------------------------------------------------

\i :regdir/../../src/sql/topo_update/function_03_app_CreateSchema.sql
\i :regdir/../../src/sql/topo_update/function_03_app_do_RemoveBordersMergeSurfaces.sql
\i :regdir/../../src/sql/topo_update/function_02_find_dangling_borders.sql
\i :regdir/../../src/sql/topo_update/function_02_remove_border_merge_surfaces.sql

-------------------------------------------
-- Utility schema (data and functions)
-------------------------------------------

\i :regdir/utils/app_test_utils.sql
\i :regdir/utils/check_layer_features.sql

CREATE TABLE util.remove_border_results(
	test TEXT,
	id TEXT,
	typ CHAR,
	act CHAR,
	frm TEXT
);

-- {
CREATE FUNCTION util.check_remove_border_effects(
	testname TEXT
)
RETURNS TABLE(o TEXT)
AS $BODY$
DECLARE
  rec RECORD;
  sql TEXT;
  attrs TEXT[];
BEGIN

	RETURN QUERY
	SELECT array_to_string(ARRAY[
			testname,
			'act',
			--id, removed id since number vary from system to system
			typ,
			act,
			frm
		], '|')
	FROM util.remove_border_results
	WHERE test = testname
	ORDER BY typ, id::int;

	RETURN QUERY
	SELECT array_to_string(ARRAY[
			testname,
			'brd',
			k::text,
			COALESCE(l, ''),
			ST_AsText(
				ST_Normalize(
					ST_Simplify(
						ST_CollectionHomogenize(g),
						0
					)
				)
			)
		], '|')
	FROM test.border
	WHERE k in (
		SELECT id::int
		FROM util.remove_border_results
		WHERE test = testname
		  AND typ = 'B'
	)
	ORDER BY k;

	RETURN QUERY
	SELECT array_to_string(ARRAY[
			testname,
			'srf',
			k::text,
			COALESCE(l, ''),
			ST_AsText(
				ST_Simplify(
					ST_Normalize(
						ST_CollectionHomogenize(g)
					),
					0
				)
			)
		], '|')
	FROM test.surface
	WHERE k in (
		SELECT id::int
		FROM util.remove_border_results
		WHERE test = testname
		  AND typ = 'S'
	)
	ORDER BY k;

END;
$BODY$ LANGUAGE 'plpgsql';
--}

-------------------------------------
-- Create the app schema
-------------------------------------

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"topology": {
			"srid": "4326"
		},
		"tables": [
			{
				"name": "surface",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" },
					{ "name": "l", "type": "text", "default": "surface" }
				],
				"topogeom_columns": [
					{ "name": "g", "type": "areal" }
				]
			},
			{
				"name": "border",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" },
					{ "name": "l", "type": "text", "default": "border" }
				],
				"topogeom_columns": [
					{ "name": "g", "type": "lineal" }
				]
			}
		],
		"surface_layer": {
			"table_name": "surface",
			"geo_column": "g"
		},
		"border_layer": {
			"table_name": "border",
			"geo_column": "g"
		},
		"operations": {
			"SurfaceMerge": {
			}
		}

	}'
);

--------------------------------------------
-- Create a 3x3 grid of borders/surfaces
-- with the middle row taller than the other
-- two (twice the height)
--------------------------------------------

-- Create surfaces
WITH rects AS (
	SELECT
		x, y,
		ST_SetSRID(
			ST_MakeEnvelope(
				x * 10,
				y * 10 + CASE WHEN y = 2 THEN 10 ELSE 0 END,
				x * 10
					+ 10,
				y * 10 + CASE WHEN y = 2 THEN 10 ELSE 0 END
					+ CASE WHEN y % 2 = 0 THEN 10 ELSE 20 END
			),
			4326
		) g
	FROM
		generate_series(0,2) x,
		generate_series(0,2) y
)
INSERT INTO test.surface(l,g)
SELECT
	format('%s%s', x, y),
	topology.toTopoGeom(
		CASE WHEN x % 2 = y % 2 THEN
			g
		ELSE
			ST_Reverse(g)
		END,
		'test_sysdata_webclient',
		(
			SELECT l.layer_id
			FROM topology.topology t
				JOIN topology.layer l ON (l.topology_id = t.id)
			WHERE l.table_name = 'surface'
		)
	)
FROM rects;

-- Check there are no overlaps, we want none
SELECT 'init', 'interior_intersects', *
FROM topo_update.find_interiors_intersect(
	'test.surface',
	'g',
	'k'
);

-- Heal the bottom-left edges
WITH bl AS (
		SELECT edge_id i
		FROM test_sysdata_webclient.edge
		WHERE ST_Xmin(geom) = 0 AND ST_YMin(geom) = 0
		ORDER BY ST_Xmin(geom), ST_YMin(geom)
		LIMIT 2
)
SELECT NULL FROM topology.ST_ModEdgeHeal(
	'test_sysdata_webclient',
	( SELECT i FROM bl ORDER BY i LIMIT 1),
	( SELECT i FROM bl ORDER BY i LIMIT 1 OFFSET 1)
);

-- Create borders (one border per edge)
-- Create some in the same direction of underlying
-- edge and some in the opposite direction
INSERT INTO test.border (l, g)
SELECT
	format(
		'%s%s%s',
		CASE
		WHEN ST_Xmin(geom) = ST_Xmax(geom) THEN
			'V'
		WHEN ST_Ymin(geom) = ST_Ymax(geom) THEN
			'H'
		ELSE
			'M'
		END,
		ST_XMin(geom)/10,
		ST_YMin(geom)/10
	),
	topology.toTopoGeom(
		CASE WHEN edge_id % 2 = 0 THEN
			geom
		ELSE
			ST_Reverse(geom)
		END,
		'test_sysdata_webclient',
		(
			SELECT l.layer_id
			FROM topology.topology t
				JOIN topology.layer l ON (l.topology_id = t.id)
			WHERE l.table_name = 'border'
		)
	)
FROM test_sysdata_webclient.edge;

--   .---M03---o---H14-->o<--M23---.
--   |         |         |         |
--   |        V12       V22        |
--   | 7:02    |  8:12   |  9:22   |
--   v         v         v         |
--   o---H03-->o---H13-->o---H23-->o
--   |         ^         ^         |
--   |         |         |         |
--  V01       V11       V21       V31
--   |         |         |         |
--   |  4:01   |  5:11   |  6:21   |
--   |         |         |         |
--   v         |         |         v
--   o<--H01---o---H11-->o---H21-->o
--   |         |         |         ^
--   |        V10       V20        |
--   |  1:00   | 2:10    |  3:20   |
--   |         v         v         |
--   `---M00-->o<--H10---o---M20---'

SELECT 'init', 'srf', substring(o::text,POSITION(',' in o::text)) AS o FROM check_layer_features('test.surface', 'g') ORDER BY substring(o::text,POSITION(',' in o::text));
SELECT 'init', 'brd', substring(o::text,POSITION(',' in o::text)) AS o FROM check_layer_features('test.border', 'g') ORDER BY substring(o::text,POSITION(',' in o::text));

------------------------------------------------------
-- Remove border H11, merging surfaces 11 and 10
-- Props will be taken from surface 5 (label 11)
-- because it is bigger
------------------------------------------------------

--   .---M03---o---H14-->o<--M23---.
--   |         |         |         |
--   |        V12       V22        |
--   | 7:02    |  8:12   |  9:22   |
--   v         v         v         |
--   o---H03-->o---H13-->o---H23-->o
--   |         ^         ^         |
--   |         |         |         |
--  V01       V11       V21       V31
--   |         |         |         |
--   |  4:01   |  10:11  |  6:21   |
--   |         |         |         |
--   v         |         |         v
--   o<--H01---o         o---H21-->o
--   |         |         |         ^
--   |        V10       V20        |
--   |  1:00   |         |  3:20   |
--   |         v         v         |
--   `---M00-->o<--H10---o---M20---'

INSERT INTO util.remove_border_results
SELECT 't1', * FROM
topo_update.app_do_RemoveBordersMergeSurfaces(
	'test',
	( SELECT k FROM test.border WHERE l = 'H11' )::text
);
SELECT * FROM util.check_remove_border_effects('t1');
SELECT 't1', 'topo', 'invalidity', * FROM ValidateTopology('test_sysdata_webclient');

------------------------------------------------------
-- Remove border H03, merging surfaces 02 and 01
-- Props will be taken from surface 11 (label 01)
-- which is bigger
------------------------------------------------------

--   .---M03---o---H14-->o<--M23---.
--   |         |         |         |
--   |        V12       V22        |
--   |         |  8:12   |  9:22   |
--   v         v         v         |
--   o         o---H13-->o---H23-->o
--   |         ^         ^         |
--   |         |         |         |
--  V01       V11       V21       V31
--   |         |         |         |
--   | 11:01   |  10:11  |  6:21   |
--   |         |         |         |
--   v         |         |         v
--   o<--H01---o         o---H21-->o
--   |         |         |         ^
--   |        V10       V20        |
--   |  1:00   |         |  3:20   |
--   |         v         v         |
--   `---M00-->o<--H10---o---M20---'

INSERT INTO util.remove_border_results
SELECT 't2', * FROM
topo_update.app_do_RemoveBordersMergeSurfaces(
	'test',
	( SELECT k FROM test.border WHERE l = 'H03' )::text
);
SELECT * FROM util.check_remove_border_effects('t2');
SELECT 't2', 'topo', 'invalidity', * FROM ValidateTopology('test_sysdata_webclient');

------------------------------------------------------
-- Remove border H01, merging surfaces 00 and 01
-- Props will be taken from surface 01 which is bigger
------------------------------------------------------

--   .---M03---o---H14-->o<--M23---.
--   |         |         |         |
--   |        V12       V22        |
--   |         |  8:12   |  9:22   |
--   v         v         v         |
--   o         o---H13-->o---H23-->o
--   |         ^         ^         |
--   |         |         |         |
--  V01       V11       V21       V31
--   |         |         |         |
--   | 12:01   |  10:11  |  6:21   |
--   |         |         |         |
--   v         |         |         v
--   o         o         o---H21-->o
--   |         |         |         ^
--   |        V10       V20        |
--   |         |         |  3:20   |
--   |         v         v         |
--   `---M00-->o<--H10---o---M20---'

INSERT INTO util.remove_border_results
SELECT 't3', * FROM
topo_update.app_do_RemoveBordersMergeSurfaces(
	'test',
	( SELECT k FROM test.border WHERE l = 'H01' )::text
);
SELECT * FROM util.check_remove_border_effects('t3');
SELECT 't3', 'topo', 'invalidity', * FROM ValidateTopology('test_sysdata_webclient');

----------------------------------------------------------
-- Remove border V11, merging surfaces 11 and 01
-- and cascading to removal of border V10 as a consequence.
--
-- Props will be taken from surface 01 which is bigger
---------------------------------------------------------------------------

--   .---M03---o---H14-->o<--M23---.
--   |         |         |         |
--   |        V12       V22        |
--   |         |  8:12   |  9:22   |
--   v         v         v         |
--   o         o---H13-->o---H23-->o
--   |                   ^         |
--   |                   |         |
--  V01                 V21       V31
--   |                   |         |
--   | 13:01             |  6:21   |
--   |                   |         |
--   v                   |         v
--   o                   o---H21-->o
--   |                   |         ^
--   |                  V20        |
--   |                   |  3:20   |
--   |                   v         |
--   `---M00-->o<--H10---o---M20---'

INSERT INTO util.remove_border_results
SELECT 't4', * FROM
topo_update.app_do_RemoveBordersMergeSurfaces(
	'test',
	( SELECT k FROM test.border WHERE l = 'V11' )::text
);
SELECT * FROM util.check_remove_border_effects('t4');
SELECT 't4', 'topo', 'invalidity', * FROM ValidateTopology('test_sysdata_webclient');



-----------
-- Cleanup
-----------

SELECT * FROM util.drop_generated_schema('test');
DROP SCHEMA util CASCADE;
DROP FUNCTION check_layer_features(regclass, name, bool);

