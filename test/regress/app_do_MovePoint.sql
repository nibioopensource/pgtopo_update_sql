SET client_min_messages TO WARNING;
SET extra_float_digits TO -3;

------------------------------------------------------------
-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
-------------------------------------------------------------

\i :regdir/../../src/sql/topo_update/function_03_app_CreateSchema.sql
\i :regdir/../../src/sql/topo_update/function_03_app_do_MovePoint.sql

-------------------------------------------
-- Utility schema (data and functions)
-------------------------------------------

\i :regdir/utils/app_test_utils.sql

-------------------------------------
-- Create the app schema
-------------------------------------

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"topology": {
			"srid": "4326"
		},
		"tables": [
			{
				"name": "point",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" }
				],
				"topogeom_columns": [
					{ "name": "g", "type": "puntal" }
				]
			}
		],
		"point_layer": {
			"table_name": "point",
			"geo_column": "g"
		},
		"operations": {
			"MovePoint": {
			}
		}

	}'
);

-- Create points

INSERT INTO test.point(g)
SELECT
	topology.toTopoGeom(
		ST_SetSRID(ST_MakePoint(n, n), 4326),
		'test_sysdata_webclient',
		(
			SELECT l.layer_id
			FROM topology.topology t
				JOIN topology.layer l ON (l.topology_id = t.id)
			WHERE l.table_name = 'point'
		)
	)
FROM generate_series(0, 1) n;

SELECT 'start', 'extent', ST_Extent(geom) FROM test_sysdata_webclient.node;
SELECT 'start', 'point', k, ST_AsEWKT(g) FROM test.point ORDER BY k;

-----------------------------------------------------------
-- te1: Test MovePoint on unexistent point
-----------------------------------------------------------

SELECT topo_update.app_do_MovePoint('test',
	'non-existent',
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : { "name" : "EPSG:4326" },
				"type" : "name"
			},
			"coordinates" : [ 10, 10 ],
			"type" : "Point"
		},
		"type" : "Feature",
		"properties" : { }
	}
	$$
);

-----------------------------------------------------------
-- t1: Test MovePoint
-----------------------------------------------------------

SELECT topo_update.app_do_MovePoint('test',
	1::text,
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : { "name" : "EPSG:4326" },
				"type" : "name"
			},
			"coordinates" : [ 10, 10 ],
			"type" : "Point"
		},
		"type" : "Feature",
		"properties" : { }
	}
	$$
);
SELECT 't1', 'point', k, ST_AsEWKT(g) FROM test.point ORDER BY k;
SELECT 'after t1', 'extent', ST_Extent(geom) FROM test_sysdata_webclient.node;



-------------------------------------------
-- Cleanup
-------------------------------------------

SELECT * FROM util.drop_generated_schema('test');

SET extra_float_digits TO 0;
DROP SCHEMA util CASCADE;
