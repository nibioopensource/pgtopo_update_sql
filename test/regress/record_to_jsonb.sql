BEGIN;

set client_min_messages to NOTICE;

-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
\i :regdir/../../src/sql/topo_update/function_01_record_to_jsonb.sql

CREATE TABLE cmp(a int, b text);
CREATE TABLE t1(k int primary key, c cmp);

INSERT INTO t1(k, c) VALUES (1,(10,null));

SELECT 't1', topo_update.record_to_jsonb(
  ( SELECT t1 FROM t1 WHERE k = 1 ),
  '{ "k": [ "id" ] }'
);

SELECT 't2', topo_update.record_to_jsonb(
  ( SELECT t1 FROM t1 WHERE k = 1 ),
  '{ "k": [ "c", "k" ], "c.a": [ "c", "a" ] }'
);

SELECT 't3', topo_update.record_to_jsonb(
  ( SELECT t1 FROM t1 WHERE k = 1 ),
  '{ "k": [ "k" ], "c.b": [ "b" ] }'
);


INSERT INTO t1(k, c) VALUES (2,null);

SELECT 't4', topo_update.record_to_jsonb(
  ( SELECT t1 FROM t1 WHERE k = 2 ),
  '{ "k": [ "k" ], "c.b": [ "c", "b" ] }'
);
