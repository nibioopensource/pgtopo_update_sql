set client_min_messages to WARNING;

-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
\i :regdir/../../src/sql/topo_update/function_02_get_surface_borders.sql

\i :regdir/utils/normalized_geom.sql
\i :regdir/utils/check_topology_changes.sql

---------------------
-- Initialize schema
---------------------

SELECT NULL FROM CreateTopology('topo');

CREATE TABLE topo.border (
	"ser" SERIAL,
    "id" TEXT PRIMARY KEY,
    "deleted" BOOLEAN DEFAULT false
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo',
	'border',
	'tg',
	'LINEAL'
);

CREATE VIEW topo.live_border
AS SELECT * FROM topo.border
WHERE NOT deleted;

CREATE TABLE topo.surface (
	"ser" SERIAL,
    "id" TEXT PRIMARY KEY
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo',
	'surface',
	'tg',
	'AREAL'
);

SELECT 'start', 'topo', * FROM check_topology_changes('topo');

---------------------
-- Add Borders
---------------------

INSERT INTO topo.border (id, tg) VALUES
(
  'b1.BR',
  --
  --                   ^
  --                   |
  --                   |
  --                   |
  --                   |
  --                   |
  --                   |
  --                   |
  --                   |
  --   o---(b1.BR)-----'
  --
  toTopoGeom(
    'LINESTRING(0 0,10 0,10 10)',
    'topo',
    1,
    0
  )
),
(
  'b1.LT',
  --
  --   ,----b1.LT----->o
  --   |               ^
  --   |               |
  --   |               |
  --   |               |
  --   |               |
  --   |               |
  --   |               |
  --   |               |
  --   |               |
  --   o---(b1.BR)-----'
  --
  toTopoGeom(
    'LINESTRING(0 0,0 10,10 10)',
    'topo',
    1,
    0
  )
),
(
  'b2.TR',
  --
  --   ,---(b1.LT)---->o
  --   |               ^
  --   |               |
  --   |               |
  --   |  o--b2.TR--.  |
  --   |            |  |
  --   |            |  |
  --   |            v  |
  --   |               |
  --   |               |
  --   o---(b1.BR)-----'
  --
  toTopoGeom(
    'LINESTRING(2 8,8 8,8 2)',
    'topo',
    1,
    0
  )
),
(
  'b2.LB',
  --
  --   ,---(b1.LT)---->o
  --   |               ^
  --   |               |
  --   |               |
  --   |  o-(b2.TR)-.  |
  --   |  |         |  |
  --   |  |         |  |
  --   |  |         v  |
  --   |  `-b2.LB-->o  |
  --   |               |
  --   o---(b1.BR)-----'
  --
  toTopoGeom(
    'LINESTRING(2 8,2 2,8 2)',
    'topo',
    1,
    0
  )
)
;

---------------------
-- Add Surfaces
---------------------

INSERT INTO topo.surface (id, tg) VALUES
(
  's1',
  --
  --   ,---(b1.LT)---->o
  --   |               ^
  --   |  s1           |
  --   |               |
  --   |  o-(b2.TR)-.  |
  --   |  |         |  |
  --   |  |         |  |
  --   |  |         v  |
  --   |  `-(b2.LB)>o  |
  --   |               |
  --   o---(b1.BR)-----'
  --
  toTopoGeom(
    'POLYGON((0 0,10 0,10 10,0 10,0 0),(2 2,8 2,8 8,2 8,2 2))',
    'topo',
    2,
    0
  )
)
;

SELECT 'prep', 'topo', * FROM check_topology_changes('topo');

-----------------------
-- Test error handling
-----------------------

-- [te1]
--
-- If a Surface's boundary edges are used by more
-- than a single Border we get an exception.

BEGIN; --{

INSERT INTO topo.border (id, tg) VALUES
(
  'bo', -- (b)order (o)verlap
  --
  --   ,---(b1.LT)---->o
  --   |               ^
  --   |               |
  --   |               |
  --   |               |
  --   |               |
  --   |               |
  --   |               |
  --   |               |
  --   v               |
  --   o-(b1.BR + bo)--'
  --
  toTopoGeom(
    'LINESTRING(0 0,10 0)',
    'topo',
    1,
    0
  )
);

--  not needed in this test
UPDATE topo.border SET deleted = true WHERE id = 'b2';

SELECT 'te1', 'bord', id, ST_AsText(normalized_geom(tg))
FROM topo.live_border ORDER BY id;

SELECT 'te1', 'surf', id, ST_AsText(normalized_geom(tg))
FROM topo.surface ORDER BY id;

SELECT 'te1', 'topo', * FROM check_topology_changes('topo');

SELECT
  'te1.exception-expected',
  (topo_update.get_surface_borders(
    --findTopology('topo'), -- not available until PostGIS-3.2.0
    ( select t from topology.topology t where t.name = 'topo' ),
    tg,
    'topo.live_border',
    'tg',
    'id'
  )).signedRingBorders
FROM topo.surface
WHERE id = 's1';

ROLLBACK; --}


-- [te2]
--
-- If a Surface's boundary edges are not
-- fully covered by existing Borders
-- we get an exception.

BEGIN; -- {

-- Add a node splitting Border's b1.BR underlying edge
SELECT NULL FROM topology.TopoGeo_addPoint('topo', 'POINT(10 0)');

-- Remove newest edge from the definition of Border b1.BR
WITH tg AS (
	SELECT b.tg, max(r.element_id) as max_edge
	FROM
		topo.border b,
		topo.relation r
	WHERE
		b.id = 'b1.BR'
		AND r.topogeo_id = id(b.tg)
		AND r.layer_id = layer_id(b.tg)
	GROUP BY b.tg
)
DELETE FROM topo.relation r
USING  tg
WHERE topogeo_id = id(tg.tg)
AND layer_id = layer_id(tg.tg)
AND element_id = tg.max_edge
--RETURNING ('te2', 'relation', 'deleted', tg.max_edge, r)
;

SELECT 'te2', 'bord',
       id, ST_AsText(normalized_geom(tg))
FROM topo.live_border ORDER BY id;

SELECT 'te2', 'surf', id, ST_AsText(normalized_geom(tg))
FROM topo.surface ORDER BY id;

SELECT 'te2', 'topo', * FROM check_topology_changes('topo');

-- Try calling get_surface_borders again, expect an exception
SELECT
  'te2', 'surface_borders', 's1',
  (topo_update.get_surface_borders(
	--findTopology('topo'), -- not available until PostGIS-3.2.0
	( select t from topology.topology t where t.name = 'topo' ),
	tg,
	'topo.live_border',
	'tg',
	'id'
  )).signedRingBorders
FROM topo.surface
WHERE id = 's1';

ROLLBACK; --}

-- [te3]
--
-- If a Border covers extraneous edges in addition
-- to Surface's boundary edges we get an exception.

BEGIN; -- {

-- Add a new edge to the definition of Border b1.BR
WITH tg AS (
	SELECT b.tg
	FROM topo.border b
	WHERE b.id = 'b1.BR'
)
SELECT
	'te3',
	'updated-border', 'b1.BR',
	ST_AsText(
		ST_CollectionHomogenize(
			topology.TopoGeom_addElement(
				tg,
				ARRAY[
					topology.TopoGeo_addLineString(
						'topo',
						'LINESTRING(10 10, 10 20)'
					),
					2 -- edge
				]
			)
		)
	)
FROM tg
;

SELECT 'te3', 'bord',
       id, ST_AsText(normalized_geom(tg))
FROM topo.live_border ORDER BY id;

SELECT 'te3', 'surf', id, ST_AsText(normalized_geom(tg))
FROM topo.surface ORDER BY id;

SELECT 'te3', 'topo', * FROM check_topology_changes('topo');


-- Try calling get_surface_borders again, expect an exception
SELECT
  'te3', 'surface_borders', 's1',
  (topo_update.get_surface_borders(
	--findTopology('topo'), -- not available until PostGIS-3.2.0
	( select t from topology.topology t where t.name = 'topo' ),
	tg,
	'topo.live_border',
	'tg',
	'id'
  )).signedRingBorders
FROM topo.surface
WHERE id = 's1';

ROLLBACK; --}

-- [te4]
--
-- A Surface has NO borders covering any of its
-- boundary edges

BEGIN; -- {

-- Delete all borders

DELETE FROM topo.border;

SELECT 'te4', 'bord',
       id, ST_AsText(normalized_geom(tg))
FROM topo.live_border ORDER BY id;

SELECT 'te4', 'surf', id, ST_AsText(normalized_geom(tg))
FROM topo.surface
WHERE id = 'te4.surf';

SELECT 'te4', 'topo', * FROM check_topology_changes('topo');

-- Try calling get_surface_borders again, expect an exception
SELECT
  'te4', 'surface_borders', 's1',
  (topo_update.get_surface_borders(
	--findTopology('topo'), -- not available until PostGIS-3.2.0
	( select t from topology.topology t where t.name = 'topo' ),
	tg,
	'topo.live_border',
	'tg',
	'id'
  )).signedRingBorders
FROM topo.surface
WHERE id = 's1';

ROLLBACK; --}

-- [te5]
--
-- A Surface boundary is covered by multiline borders
--
-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/96

BEGIN; -- {

-- DELETE b1.LT and b1.BR
DELETE FROM topo.border WHERE id IN ( 'b1.LT', 'b1.BR' );

-- Use multiline borders to describe the horizontals
-- and the vertical elements of s1 exterior ring
INSERT INTO topo.border (id, tg) VALUES
(
  'b1.BT',
  --
  --   o---b1.BT------>o
  --
  --
  --
  --
  --
  --
  --
  --
  --   o---b1.BT------>o
  --
  toTopoGeom(
    'MULTILINESTRING((0 0,10 0),(0 10,10 10))',
    'topo',
    1,
    0
  )
),
(
  'b1.LR',
  --
  --   o---(b1.BT)---->o
  --   ^               ^
  --   |               |
  --   |               |
  --  b2.LR           b2.LR
  --   |               |
  --   |               |
  --   |               |
  --   |               |
  --   |               |
  --   o---(b1.BT)---->o
  --
  toTopoGeom(
    'MULTILINESTRING((0 0,0 10),(10 0,10 10))',
    'topo',
    1,
    0
  )
);

SELECT 'te5', 'bord',
       id, ST_AsText(normalized_geom(tg))
FROM topo.live_border
WHERE ser >= (SELECT ser FROM topo.live_border WHERE id = 'b1.BT')
ORDER BY id;

SELECT 'te5', 'surf', id, ST_AsText(normalized_geom(tg))
FROM topo.surface
WHERE id = 's1';

SELECT 'te5', 'topo', * FROM check_topology_changes('topo');

-- Try calling get_surface_borders again, expect an exception
SELECT
  'te5', 'surface_borders', 's1',
  (topo_update.get_surface_borders(
	--findTopology('topo'), -- not available until PostGIS-3.2.0
	( select t from topology.topology t where t.name = 'topo' ),
	tg,
	'topo.live_border',
	'tg',
	'id'
  )).signedRingBorders
FROM topo.surface
WHERE id = 's1';

ROLLBACK; --}

-----------------------
-- Test valid calls
-----------------------

--
-- [t1]
--
--  Two rings each formed by
--  2 borders in opposite direction
--

SELECT 't1', 'bord',
       id, ST_AsText(normalized_geom(tg))
FROM topo.live_border ORDER BY id;

SELECT 't1',
	'surf', id,
	ST_AsText(normalized_geom(tg))
FROM topo.surface ORDER BY id;

SELECT 't1', 'topo', * FROM check_topology_changes('topo');

SELECT
  't1', 'surface_borders', 's1',
  (topo_update.get_surface_borders(
    --findTopology('topo'), -- not available until PostGIS-3.2.0
    ( select t from topology.topology t where t.name = 'topo' ),
    tg,
    'topo.live_border',
    'tg',
    'id'
  )).signedRingBorders
FROM topo.surface
WHERE id = 's1';

--
-- [t2]
--
--  A ring formed by 3 borders
--	See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/83#note_546758551
--

INSERT INTO topo.border (id, tg) VALUES
(
  'b3.L',
  --
  --   ,---(b1.LT)---->o   o
  --   |               ^   ^
  --   |  (s1)         |   |
  --   |               |   |
  --   |  o-(b2.TR)-.  |  b3.L
  --   |  |         |  |   |
  --   |  |         |  |   |
  --   |  |         v  |   |
  --   |  `-(b2.LB)>o  |   |
  --   |               |   |
  --   o---(b1.BR)-----'   o
  --
  toTopoGeom(
    'LINESTRING(12 0,12 10)',
    'topo',
    1,
    0
  )
),
(
  'b4.T',
  --
  --   ,---(b1.LT)---->o   o-----b4.T------>o
  --   |               ^   ^
  --   |  (s1)         |   |
  --   |               |   |
  --   |  o-(b2.TR)-.  | (b3.L)
  --   |  |         |  |   |
  --   |  |         |  |   |
  --   |  |         v  |   |
  --   |  `-(b2.LB)>o  |   |
  --   |               |   |
  --   o---(b1.BR)-----'   o
  --
  toTopoGeom(
    'LINESTRING(12 10,22 10)',
    'topo',
    1,
    0
  )
),
(
  'b5.BR',
  --
  --   ,---(b1.LT)---->o   o----(b4.T)----->o
  --   |               ^   ^                ^
  --   |  (s1)         |   |                |
  --   |               |   |                |
  --   |  o-(b2.TR)-.  | (b3.L)             |
  --   |  |         |  |   |                |
  --   |  |         |  |   |                |
  --   |  |         v  |   |                |
  --   |  `-(b2.LB)>o  |   |                |
  --   |               |   |                |
  --   o---(b1.BR)-----'   o----b5.BR-------'
  --
  toTopoGeom(
    'LINESTRING(12 0,22 0,22 10)',
    'topo',
    1,
    0
  )
)
;

INSERT INTO topo.surface (id, tg) VALUES
(
  's2',
  --
  --   ,---(b1.LT)---->o   o----(b4.T)----->o
  --   |               ^   ^                ^
  --   |  (s1)         |   |                |
  --   |               |   |                |
  --   |  o-(b2.TR)-.  | (b3.L)             |
  --   |  |         |  |   |       s2       |
  --   |  |         |  |   |                |
  --   |  |         v  |   |                |
  --   |  `-(b2.LB)>o  |   |                |
  --   |               |   |                |
  --   o---(b1.BR)-----'   o----(b5.BR)-----'
  --
  toTopoGeom(
    'POLYGON((12 0,12 10,22 10,22 0,12 0))',
    'topo',
    2,
    0
  )
)
;

SELECT 't2', 'topo', * FROM check_topology_changes('topo');

SELECT 't2', 'bord',
       id, ST_AsText(normalized_geom(tg))
FROM topo.live_border
WHERE ser >= (SELECT ser FROM topo.live_border WHERE id = 'b3.L')
ORDER BY id;

SELECT 't2', 'surf',
		id, ST_AsText(normalized_geom(tg))
FROM topo.surface
WHERE ser >= (SELECT ser FROM topo.surface WHERE id = 's2')
ORDER BY id;

SELECT
  't2', 'surface_borders', 's2',
  (topo_update.get_surface_borders(
    --findTopology('topo'), -- not available until PostGIS-3.2.0
    ( select t from topology.topology t where t.name = 'topo' ),
    tg,
    'topo.live_border',
    'tg',
    'id'
  )).signedRingBorders
FROM topo.surface
WHERE id = 's2';

--
-- [t3]
--
--  Surface using borders shared with other surfaces
--	See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/97
--

INSERT INTO topo.border (id, tg) VALUES
(
  'b6.T',
  --
  --   ,---(b1.LT)---->o--b6.T-->o----(b4.T)----->o
  --   |               ^         ^                ^
  --   |  (s1)         |         |                |
  --   |               |         |                |
  --   |  o-(b2.TR)-.  |       (b3.L)             |
  --   |  |         |  |         |      (s2)      |
  --   |  |         |  |         |                |
  --   |  |         v  |         |                |
  --   |  `-(b2.LB)>o  |         |                |
  --   |               |         |                |
  --   o---(b1.BR)-----'         o----(b5.BR)-----'
  --
  toTopoGeom(
    'LINESTRING(10 10,12 10)',
    'topo',
    1,
    0
  )
),
(
  'b7.D',
  --
  --   ,---(b1.LT)---->o-(b6.T)->o----(b4.T)----->o
  --   |               ^\        ^                ^
  --   |  (s1)         | \       |                |
  --   |               |  \      |                |
  --   |  o-(b2.TR)-.  |   \   (b3.L)             |
  --   |  |         |  |    \    |      (s2)      |
  --   |  |         |  |   b7.D  |                |
  --   |  |         v  |      \  |                |
  --   |  `-(b2.LB)>o  |       \ |                |
  --   |               |        \|                |
  --   o---(b1.BR)-----'         o----(b5.BR)-----'
  --
  toTopoGeom(
    'LINESTRING(10 10,12 0)',
    'topo',
    1,
    0
  )
)
;

INSERT INTO topo.surface (id, tg) VALUES
(
  's3',
  --
  --   ,---(b1.LT)---->o-(b6.T)->o----(b4.T)----->o
  --   |               ^\        ^                ^
  --   |  (s1)         | \  s3   |                |
  --   |               |  \      |                |
  --   |  o-(b2.TR)-.  |   \   (b3.L)             |
  --   |  |         |  |    \    |      (s2)      |
  --   |  |         |  |  (b7.D) |                |
  --   |  |         v  |      \  |                |
  --   |  `-(b2.LB)>o  |       \ |                |
  --   |               |        \|                |
  --   o---(b1.BR)-----'       '-o----(b5.BR)-----'
  --
  toTopoGeom(
    'POLYGON((12 0,12 10,10 10,12 0))',
    'topo',
    2,
    0
  )
)
;

SELECT 't3', 'topo', * FROM check_topology_changes('topo');

SELECT 't3', 'bord',
       id, ST_AsText(normalized_geom(tg))
FROM topo.live_border
WHERE ser >= (SELECT ser FROM topo.live_border WHERE id = 'b6.T')
 OR id = 'b3.L'
ORDER BY id;

SELECT 't3', 'surf',
		id, ST_AsText(normalized_geom(tg))
FROM topo.surface
WHERE ser >= (SELECT ser FROM topo.surface WHERE id = 's3')
ORDER BY id;

SELECT
  't3', 'surface_borders', id,
  (topo_update.get_surface_borders(
    --findTopology('topo'), -- not available until PostGIS-3.2.0
    ( select t from topology.topology t where t.name = 'topo' ),
    tg,
    'topo.live_border',
    'tg',
    'id'
  )).signedRingBorders
FROM topo.surface
WHERE id IN ( 's2', 's3' )
ORDER BY id;

--
-- [t4]
--
--  Surface border composed by 2 edges, first of which ends
--	on the Border start point
--
-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/123
--

-- Reset the topology
DELETE FROM topo.border;
DELETE FROM topo.surface;
DELETE FROM topo.relation;
DELETE FROM topo.edge_data;
DELETE FROM topo.node;
DELETE FROM topo.face WHERE face_id > 0;
SELECT NULL FROM setval('topo.edge_data_edge_id_seq', 1, false);
SELECT NULL FROM setval('topo.node_node_id_seq', 1, false);
SELECT NULL FROM setval('topo.face_face_id_seq', 1, false);

-- Add edge1
--
--   o
--    \
--  E1 \
--      \
--       x
--
SELECT 't4', 'E', topology.TopoGeo_addLineString(
	'topo',
	'LINESTRING(10 0, 0 10)',
	0
);

-- Add border 'b', will be composed
-- by 2 edges
--
--   o---,
--    \  | E2
--  E1 \ |
--      \|
--       '
--
INSERT INTO topo.border (id, tg) VALUES
(
  'b',
  toTopoGeom(
    'LINESTRING(0 10,10 10,10 0,0 10)',
    'topo',
    1,
    0
  )
);

-- Add surface 's'
INSERT INTO topo.surface (id, tg) VALUES
(
  's',
  --   o---,
  --    \  |
  --     \ |
  --      \|
  --       '
  --
  toTopoGeom(
    'POLYGON((0 10,10 10,10 0,0 10))',
    'topo',
    2,
    0
  )
);

SELECT 't4', 'bord', id,
	ST_AsText(ST_CollectionHomogenize(tg::geometry))
FROM topo.border;

--set client_min_messages TO DEBUG;
SELECT
  't4', 'surface_borders', id,
  (topo_update.get_surface_borders(
    --findTopology('topo'), -- not available until PostGIS-3.2.0
    ( select t from topology.topology t where t.name = 'topo' ),
    tg,
    'topo.border',
    'tg',
    'id'
  )).signedRingBorders
FROM topo.surface
WHERE id = 's';
--set client_min_messages TO WARNING;

--
-- [t5]
--
--  Surface border composed by 3 edges, one of which forms
--	an hole touching the shell
--
-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/149
--

-- Reset the topology
DELETE FROM topo.border;
DELETE FROM topo.surface;
DELETE FROM topo.relation;
DELETE FROM topo.edge_data;
DELETE FROM topo.node;
DELETE FROM topo.face WHERE face_id > 0;
SELECT NULL FROM setval('topo.edge_data_edge_id_seq', 1, false);
SELECT NULL FROM setval('topo.node_node_id_seq', 1, false);
SELECT NULL FROM setval('topo.face_face_id_seq', 1, false);

--
--           o
--          / \
--         /   \
--        /     \
--       /       \
--      / .-BH--. \
--     /  \     /  \
--    /    \   /    \
--   /      \ /      \
--   '--BL---o---BR--'
--
INSERT INTO topo.border (id, tg) VALUES
(
  'BL', toTopoGeom(
    'LINESTRING(10 0,0 0,10 10)',
    'topo', 1, 0)
),
(
  'BR', toTopoGeom(
    'LINESTRING(10 0,20 0,10 10)',
    'topo', 1, 0)
),
(
  'BH',
  toTopoGeom(
    'LINESTRING(10 0,8 2,12 2,10 0)',
    'topo', 1, 0)
)
;

INSERT INTO topo.surface (id, tg) SELECT
	'S',
	CreateTopoGeom(
		'topo', 3, 2,
		TopoElementArray_agg(ARRAY[x,3])
	) FROM GetFaceByPoint('topo', 'POINT(1 1)', 0) x
;

SELECT 't5', 'surface_borders', signedRingBorders
FROM topo_update.get_surface_borders(
    --findTopology('topo'), -- not available until PostGIS-3.2.0
    ( select t from topology.topology t where t.name = 'topo' ),
	(select tg from topo.surface where id = 'S'),
	'topo.border',
	'tg',
	'id'
);

--
-- [t6]
--
--  Multi-face Surface
--
-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/260
--

-- Reset the topology
DELETE FROM topo.border;
DELETE FROM topo.surface;
DELETE FROM topo.relation;
DELETE FROM topo.edge_data;
DELETE FROM topo.node;
DELETE FROM topo.face WHERE face_id > 0;
SELECT NULL FROM setval('topo.edge_data_edge_id_seq', 1, false);
SELECT NULL FROM setval('topo.node_node_id_seq', 1, false);
SELECT NULL FROM setval('topo.face_face_id_seq', 1, false);

--
--           o
--          /.\
--         / . \
--        /  .  \
--       /   .   \
--      /    .    \
--     /     .     \
--    /      .      \
--   /       .       \
--   '--BL---o---BR--'
--
INSERT INTO topo.border (id, tg) VALUES
(
  'BL', toTopoGeom(
    'LINESTRING(10 0,0 0,10 10)',
    'topo', 1, 0)
),
(
  'BR', toTopoGeom(
    'LINESTRING(10 0,20 0,10 10)',
    'topo', 1, 0)
)
;

INSERT INTO topo.surface (id, tg) SELECT
	'S',
	CreateTopoGeom(
		'topo', 3, 2,
		TopoElementArray_agg(ARRAY[x,3])
	) FROM GetFaceByPoint('topo', 'POINT(5 2)', 0) x
;

-- Add splitting edge
SELECT 't6', 'syn', topology.TopoGeo_addLinestring(
	'topo',
    'LINESTRING(10 0,8 2,12 2,10 0)'
);

SELECT 't6', 'surface_borders', signedRingBorders
FROM topo_update.get_surface_borders(
    --findTopology('topo'), -- not available until PostGIS-3.2.0
    ( select t from topology.topology t where t.name = 'topo' ),
	(select tg from topo.surface where id = 'S'),
	'topo.border',
	'tg',
	'id'
)
ORDER BY 3;

---------------------
-- Cleanup
---------------------

SELECT NULL FROM DropTopology('topo');
DROP FUNCTION check_topology_changes(text);
DROP FUNCTION normalized_geom(geometry);
