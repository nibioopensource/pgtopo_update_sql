BEGIN;

set client_min_messages to NOTICE;

-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
\i :regdir/../../src/sql/topo_update/function_02_features_as_geojson.sql

---------------------
-- Initialize schema
---------------------

SELECT NULL FROM CreateTopology('topo', 4326);

CREATE TABLE topo.features (
  "id" TEXT PRIMARY KEY
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo',
	'features',
	'tg',
	'LINEAL'
);

---------------------
-- Add features
---------------------

INSERT INTO topo.features (id, tg)
SELECT x, toTopoGeom(
    ST_SetSRID(ST_MakeLine(ST_Point(0,x),ST_Point(10,x)), 4326),
    'topo',
    1,
    0
  )
FROM generate_series(0, 4) x;

---------------------
-- t1: simple call
---------------------

SELECT '--- t1 ---', E'\n' || jsonb_pretty(
	topo_update.features_as_geojson(
		'topo.features',
		'tg',
		'id',
		ARRAY['0', '1']
	)
)
;


ROLLBACK;
