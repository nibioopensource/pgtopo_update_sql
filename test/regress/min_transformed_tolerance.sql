\i :regdir/../../src/sql/topo_update/function_01_min_transformed_tolerance.sql

set client_min_messages to WARNING;

SELECT
	ST_AsEWKT(g), t, s,
	round(topo_update.min_transformed_tolerance(
		g,
		t,
		s
	)::numeric, 10)
FROM ( VALUES

( 'SRID=4326;POINT(0 0)', 1, 4326 ),
( 'SRID=4326;POINT(0 0)', 1, 3857 ),
( 'SRID=4326;POINT(0 60)', 1, 3857 ),
( 'SRID=4326;LINESTRING(0 0, 0 60)', 1, 3857 ),
( 'SRID=25835;POINT(1221960 6722800)', 1, 4326 )

) as x(g,t,s);
