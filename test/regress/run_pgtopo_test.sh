#!/bin/sh

REGDIR=$(dirname $0)
TOPDIR=${REGDIR}/../..

export POSTGIS_REGRESS_DB=${POSTGIS_REGRESS_DB-nibio_reg}
export PGTZ=utc

echo ARGS: $@

OPTS="--topology --extension"

if echo $@ | grep -q -- '--nocreate'; then
	:
else
	if echo $@ | grep -q -- '--extension'; then
		OPTS="${OPTS} --after-create-script ${REGDIR}/load_extension.sql"
	else
		OPTS="${OPTS} --after-create-script ${TOPDIR}/topo_update.sql"
	fi
fi

OPTS="${OPTS} --after-create-script ${REGDIR}/print_version.sql"

if echo $@ | grep -q -- '--nodrop'; then
	:
else
	OPTS="${OPTS} --before-uninstall-script ${TOPDIR}/topo_update_uninstall.sql"
fi

${REGDIR}/run_test.pl ${OPTS} $@

