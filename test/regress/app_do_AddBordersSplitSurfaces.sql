SET client_min_messages TO WARNING;
SET extra_float_digits TO -3;

------------------------------------------------------------
-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
-------------------------------------------------------------

\i :regdir/../../src/sql/topo_update/function_03_app_CreateSchema.sql
\i :regdir/../../src/sql/topo_update/function_03_app_do_AddBordersSplitSurfaces.sql
\i :regdir/../../src/sql/topo_update/function_02_add_border_split_surface.sql

-------------------------------------------
-- Utility schema (data and functions)
-------------------------------------------

\i :regdir/utils/app_test_utils.sql

CREATE TABLE util.add_border_results(
	test TEXT,
	id TEXT,
	typ CHAR,
	act CHAR,
	frm TEXT
);

-- {
CREATE FUNCTION util.check_add_border_effects(
	testname TEXT
)
RETURNS TABLE(o TEXT)
AS $BODY$
DECLARE
  rec RECORD;
  sql TEXT;
  attrs TEXT[];
BEGIN

	RETURN QUERY
	SELECT array_to_string(ARRAY[
			testname,
			'act',
			id,
			typ,
			act,
			frm
		], '|')
	FROM util.add_border_results
	WHERE test = testname
	ORDER BY typ, id::int;

	RETURN QUERY
	SELECT array_to_string(ARRAY[
			testname,
			'brd',
			identifier::text,
			COALESCE(created_by, ''),
			COALESCE(last_modified_by, ''),
			COALESCE(kind, ''),
			ST_AsText(
				ST_Normalize(
					ST_Simplify(
						ST_CollectionHomogenize(g),
						0
					)
				)
			)
		], '|')
	FROM myproduct.border
	WHERE identifier in (
		SELECT id::int
		FROM util.add_border_results
		WHERE test = testname
		  AND typ = 'B'
	)
	ORDER BY identifier;

	RETURN QUERY
	SELECT array_to_string(ARRAY[
			testname,
			'srf',
			identifier::text,
			COALESCE(created_by, ''),
			COALESCE(last_modified_by, ''),
			COALESCE(kind, ''),
			ST_AsText(
				ST_Simplify(
					ST_Normalize(
						ST_CollectionHomogenize(g)
					),
					0
				)
			)
		], '|')
	FROM myproduct.surface
	WHERE identifier in (
		SELECT id::int
		FROM util.add_border_results
		WHERE test = testname
		  AND typ = 'S'
	)
	ORDER BY identifier;

END;
$BODY$ LANGUAGE 'plpgsql';
--}


-------------------------------------
-- Create the app schema
-------------------------------------

SELECT * FROM topo_update.app_CreateSchema(
	'myproduct', '{
		"version": "1.0",
		"topology": {
			"srid": "4326"
		},
		"tables": [
			{
				"name": "surface",
				"primary_key": "identifier",
				"attributes": [
					{ "name": "identifier", "type": "serial" },
					{ "name": "created_by", "type": "text", "default": "unknown-creator"},
					{ "name": "last_modified_by", "type": "text", "default": "unmodified" },
					{ "name": "kind", "type": "text", "default": "unknown-surface-kind" }
				],
				"topogeom_columns": [
					{ "name": "g", "type": "areal" }
				]
			},
			{
				"name": "border",
				"primary_key": "identifier",
				"attributes": [
					{ "name": "identifier", "type": "serial" },
					{ "name": "created_by", "type": "text", "default": "unknown-creator"},
					{ "name": "last_modified_by", "type": "text", "default": "unmodified" },
					{ "name": "kind", "type": "text", "default": "unknown-border-kind" }
				],
				"topogeom_columns": [
					{ "name": "g", "type": "lineal" }
				]
			}
		],
		"surface_layer": {
			"table_name": "surface",
			"geo_column": "g"
		},
		"border_layer": {
			"table_name": "border",
			"geo_column": "g"
		},
		"operations": {
			"AddBordersSplitSurfaces": {
				"parameters": {
					"default": {
						"snapTolerance": {
							"units": 1e-10,
							"unitsAreMeters": false
						},
						"minAllowedNewSurfacesArea": {
							"units": 200,
							"unitsAreMeters": true
						},
						"maxAllowedSplitSurfacesCount": 0
					},
					"forOpenLines": {
						"minAllowedNewSurfacesArea": {
							"units": 10,
							"unitsAreMeters": true
						},
						"maxAllowedSplitSurfacesCount": 2
					},
					"forClosedLines": {
						"maxAllowedSplitSurfacesCount": 1
					}
				}
			}
		}

	}'
);

-------------------------------------
-- Add borders
-------------------------------------

DO $BODY$
BEGIN
	SELECT 't1.e1', * FROM topo_update.app_do_AddBordersSplitSurfaces('myproduct',
		$$
		{
			"geometry" : {
				"crs" : {
					"properties" : { "name" : "EPSG:4326" },
					"type" : "name"
				},
				"coordinates" : [
					[ 0, 0 ], [ 1e-4, 0 ], [ 1e-4, 1e-4 ], [ 0, 1e-4 ], [ 0, 0 ]
				],
				"type" : "LineString"
			},
			"properties" : {
				"Surface": {
					"new": { "created_by": "t1-new-srf" },
					"modified": { "last_modified_by": "t1-mod-srf" },
					"split": { "created_by": "t1-spl-srf" }
				},
				"Border": {
					"new": { "created_by": "t1-new-brd" },
					"modified": { "last_modified_by": "t1-mod-brd" },
					"split": { "created_by": "t1-spl-brd" }
				}
			},
			"type" : "Feature"
		}
		$$
	);
EXCEPTION WHEN OTHERS THEN
	RAISE EXCEPTION '%', substring(SQLERRM,1,70);
END;
$BODY$ LANGUAGE 'plpgsql';

-- t1: draw a square
INSERT INTO util.add_border_results
SELECT 't1', * FROM topo_update.app_do_AddBordersSplitSurfaces('myproduct',
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : { "name" : "EPSG:4326" },
				"type" : "name"
			},
			"coordinates" : [
				[ 0, 0 ], [ 10, 0 ], [ 10, 10 ], [ 0, 10 ], [ 0, 0 ]
			],
			"type" : "LineString"
		},
		"properties" : {
			"Surface": {
				"new": { "created_by": "t1-new-srf", "last_modified_by": "t1-new-srf", "kind": "t1-srf-kind" },
				"modified": { "last_modified_by": "t1-mod-srf" },
				"split": { "created_by": "t1-spl-srf" }
			},
			"Border": {
				"new": { "created_by": "t1-new-brd", "last_modified_by": "t1-new-brd", "kind": "t1-brd-kind" },
				"modified": { "last_modified_by": "t1-mod-brd" },
				"split": { "created_by": "t1-spl-brd" }
			}
		},
		"type" : "Feature"
	}
	$$
);
SELECT * FROM util.check_add_border_effects('t1');


-- t2: draw a square overlapping the one in t1
INSERT INTO util.add_border_results
SELECT 't2', * FROM topo_update.app_do_AddBordersSplitSurfaces('myproduct',
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : { "name" : "EPSG:4326" },
				"type" : "name"
			},
			"coordinates" : [
				[ 5, 10 ], [ 5, 5 ], [ 15, 5 ], [ 15, 15 ], [ 5, 15 ], [ 5, 5 ]
			],
			"type" : "LineString"
		},
		"properties" : {
			"Surface": {
				"new": { "created_by": "t2-new-srf", "last_modified_by": "t2-new-srf" },
				"modified": { "last_modified_by": "t2-mod-srf" },
				"split": { "created_by": "t2-spl-srf" }
			},
			"Border": {
				"new": { "created_by": "t2-new-brd", "last_modified_by": "t2-new-brd" },
				"modified": { "last_modified_by": "t2-mod-brd" },
				"split": { "created_by": "t2-spl-brd" }
			}
		},
		"type" : "Feature"
	}
	$$
);
SELECT * FROM util.check_add_border_effects('t2');

-- t3.e1: draw 2 open lines forming a ring
SELECT 't3.e1', * FROM topo_update.app_do_AddBordersSplitSurfaces('myproduct',
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : { "name" : "EPSG:4326" },
				"type" : "name"
			},
			"coordinates" : [
				[ [ -1, 0 ], [ 10, 0 ], [ 10, 11 ] ],
				[ [ 0, -1 ], [ 0, 10 ], [ 11, 10 ] ]
			],
			"type" : "MultiLineString"
		},
		"properties" : {
			"Surface": {
				"new": { "created_by": "t3-new-srf", "last_modified_by": "t3-new-srf" },
				"modified": { "last_modified_by": "t3-mod-srf" },
				"split": { "created_by": "t3-spl-srf" }
			},
			"Border": {
				"new": { "created_by": "t3-new-brd", "last_modified_by": "t3-new-brd" },
				"modified": { "last_modified_by": "t3-mod-brd" },
				"split": { "created_by": "t3-spl-brd" }
			}
		},
		"type" : "Feature"
	}
	$$
);

-- t3.e2: draw 3 open lines splitting 3 surfaces
SELECT 't3.e2', * FROM topo_update.app_do_AddBordersSplitSurfaces('myproduct',
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : { "name" : "EPSG:4326" },
				"type" : "name"
			},
			"coordinates" : [
				[ [ -1, 2 ], [ 11, 2 ] ],
				[ [ 4, 8 ], [ 11, 8 ] ],
				[ [ 4, 13 ], [ 16, 13 ] ]
			],
			"type" : "MultiLineString"
		},
		"properties" : {
			"Surface": {
				"new": { "created_by": "t3-new-srf", "last_modified_by": "t3-new-srf" },
				"modified": { "last_modified_by": "t3-mod-srf" },
				"split": { "created_by": "t3-spl-srf" }
			},
			"Border": {
				"new": { "created_by": "t3-new-brd", "last_modified_by": "t3-new-brd" },
				"modified": { "last_modified_by": "t3-mod-brd" },
				"split": { "created_by": "t3-spl-brd" }
			}
		},
		"type" : "Feature"
	}
	$$
);

-- t3: draw 2 open lines splitting 2 surfaces
INSERT INTO util.add_border_results
SELECT 't3', * FROM topo_update.app_do_AddBordersSplitSurfaces('myproduct',
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : { "name" : "EPSG:4326" },
				"type" : "name"
			},
			"coordinates" : [
				[ [ -1, 2 ], [ 11, 2 ] ],
				[ [ 4, 13 ], [ 16, 13 ] ]
			],
			"type" : "MultiLineString"
		},
		"properties" : {
			"Surface": {
				"new": { "created_by": "t3-new-srf", "last_modified_by": "t3-new-srf" },
				"modified": { "last_modified_by": "t3-mod-srf" },
				"split": { "created_by": "t3-spl-srf" }
			},
			"Border": {
				"new": { "created_by": "t3-new-brd", "last_modified_by": "t3-new-brd" },
				"modified": { "last_modified_by": "t3-mod-brd" },
				"split": { "created_by": "t3-spl-brd" }
			}
		},
		"type" : "Feature"
	}
	$$
);
SELECT * FROM util.check_add_border_effects('t3');


-----------
-- Cleanup
-----------

SET extra_float_digits TO 0;
SELECT * FROM util.drop_generated_schema('myproduct');
DROP SCHEMA util CASCADE;

