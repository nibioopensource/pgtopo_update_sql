SET client_min_messages TO WARNING;

------------------------------------------------------------
-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
-------------------------------------------------------------

\i :regdir/../../src/sql/topo_update/function_03_app_do_GetMergeableSurfaceBordersAsGeoJSON.sql

-------------------------------------------
-- Utility schema (data and functions)
-------------------------------------------

\i :regdir/utils/app_test_utils.sql

-------------------------------------------
-- Save minimal app schema into a table
-------------------------------------------

--		"surface_layer": {
--			"table": "surface",
--			"geo_column": "tg"
--		},

CREATE TABLE util.minimal_app_schema AS
SELECT $$
	{
		"version": "1.0",
		"topology": { "srid": 4326 },
		"tables": [ {
				"name": "surface",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "text" },
					{ "name": "a1", "type": "text" },
					{ "name": "a2", "type": "text" }
				],
				"topogeom_columns": [
					{
						"name": "g",
						"type": "areal"
					}
				]
			}, {
				"name": "border",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "text" }
				],
				"topogeom_columns": [
					{
						"name": "g",
						"type": "lineal"
					}
				]
			}
		],
		"border_layer": {
			"table_name": "border",
			"geo_column": "g"
		},
		"surface_layer": {
			"table_name": "surface",
			"geo_column": "g"
		},
		"operations": {
			"SurfaceMerge": {
				"sharedSurfaceAttributes": [ "a1", "a2" ],
				"maxSurfaceArea": { "units": 22 }
			}
		}
	}
$$::jsonb AS cfg;

-- Create the schema
SELECT * FROM topo_update.app_CreateSchema( 'test',
	( SELECT cfg FROM util.minimal_app_schema )
);

-- Build this topology:
--
--   y=8               ,-----------------------.
--                     |                       |
--                     |                       |
--   y=6   ,--B1-------o----B2----.            |
--         |           |          |            |
--         |   S1      B3         |            |
--         |           |    S2    |            |
--   y=4   |     ,-B6--o          |            |
--         |     |     |          |            |
--         |     | S3  B4         |   S4       |
--         |     |     |          |            |
--   y=2   |     *-----o          |            |
--         |           |          |            |
--         |           B5         |            |
--         |           |          |            |
--   y=0   `-----------o----------'            |
--        x=0   x=2   x=4        x=8         x=12
--                     |                       |
--   y=-2              `-----------------------'
--
-- Add B1
INSERT INTO test.border(k, g) VALUES ( 'b1', topology.toTopoGeom(
	'SRID=4326;LINESTRING(4 0,0 0,0 6,4 6)',
	'test_sysdata_webclient',
	( select layer_id from topology.layer where table_name = 'border' )
) );
-- Add B2
INSERT INTO test.border(k, g) VALUES ( 'b2', topology.toTopoGeom(
	'SRID=4326;LINESTRING(4 6,8 6,8 0,4 0)',
	'test_sysdata_webclient',
	( select layer_id from topology.layer where table_name = 'border' )
) );
-- Add B3
INSERT INTO test.border(k, g) VALUES ( 'b3', topology.toTopoGeom(
	'SRID=4326;LINESTRING(4 6,4 4)',
	'test_sysdata_webclient',
	( select layer_id from topology.layer where table_name = 'border' )
) );
-- Add B4
INSERT INTO test.border(k, g) VALUES ( 'b4', topology.toTopoGeom(
	'SRID=4326;LINESTRING(4 4,4 2)',
	'test_sysdata_webclient',
	( select layer_id from topology.layer where table_name = 'border' )
) );
-- Add B5
INSERT INTO test.border(k, g) VALUES ( 'b5', topology.toTopoGeom(
	'SRID=4326;LINESTRING(4 2,4 0)',
	'test_sysdata_webclient',
	( select layer_id from topology.layer where table_name = 'border' )
) );
-- Add B6
INSERT INTO test.border(k, g) VALUES ( 'b6', topology.toTopoGeom(
	'SRID=4326;LINESTRING(4 2,2 2,2 4,4 4)',
	'test_sysdata_webclient',
	( select layer_id from topology.layer where table_name = 'border' )
) );
-- Add B7
INSERT INTO test.border(k, g) VALUES ( 'b7', topology.toTopoGeom(
	'SRID=4326;LINESTRING(4 0,4 -2,12 -2,12 8,4 8,4 6)',
	'test_sysdata_webclient',
	( select layer_id from topology.layer where table_name = 'border' )
) );
-- Add S1
INSERT INTO test.surface(k, g) VALUES ( 's1', topology.toTopoGeom(
	'SRID=4326;POLYGON((4 0,0 0,0 6,4 6,4 4,2 4,2 2,4 2,4 0))',
	'test_sysdata_webclient',
	( select layer_id from topology.layer where table_name = 'surface' )
) );
-- Add S2
INSERT INTO test.surface(k, g) VALUES ( 's2', topology.toTopoGeom(
	'SRID=4326;POLYGON((4 0,4 6,8 6,8 0,4 0))',
	'test_sysdata_webclient',
	( select layer_id from topology.layer where table_name = 'surface' )
) );
-- Add S3
INSERT INTO test.surface(k, g) VALUES ( 's3', topology.toTopoGeom(
	'SRID=4326;POLYGON((4 2,2 2,2 4,4 4,4 2))',
	'test_sysdata_webclient',
	( select layer_id from topology.layer where table_name = 'surface' )
) );
-- Add S4
INSERT INTO test.surface(k, g) VALUES ( 's4', topology.toTopoGeom(
	'SRID=4326;POLYGON((4 0,8 0,8 6,4 6,4 8,12 8,12 -2,4 -2,4 0))',
	'test_sysdata_webclient',
	( select layer_id from topology.layer where table_name = 'surface' )
) );
-- Split B6 into two edges by adding a node
SELECT NULL FROM topology.TopoGeo_addPoint('test_sysdata_webclient',
	'SRID=4326;POINT(2 2)');


-- te1: unexisting app
SELECT 'te1', * FROM topo_update.app_do_GetMergeableSurfaceBordersAsGeoJSON(
	'non-existing-app',
	ST_MakeEnvelope(-180, -90, 180, 90, 4326)
);

-- te2: operation is not enabled
BEGIN;
UPDATE test_sysdata_webclient_functions.app_config
SET cfg = jsonb_concat(
		cfg - 'operations',
		jsonb_build_object('operations', jsonb_build_object())
	);
SELECT 'te2', * FROM topo_update.app_do_GetMergeableSurfaceBordersAsGeoJSON(
	'test',
	ST_MakeEnvelope(-180, -90, 180, 90, 4326)
);
ROLLBACK;

-- te3: layers not found in appconfig
-- This should be already prevented at app creation time
BEGIN;
UPDATE test_sysdata_webclient_functions.app_config
SET cfg = cfg - 'border_layer' - 'surface_layer';
SELECT 'te3', * FROM topo_update.app_do_GetMergeableSurfaceBordersAsGeoJSON(
	'test',
	ST_MakeEnvelope(-180, -90, 180, 90, 4326)
);
ROLLBACK;

-- t1: bounding box intersects no border
SELECT 't1', 'count', count(x) FROM topo_update.app_do_GetMergeableSurfaceBordersAsGeoJSON(
	'test',
	ST_MakeEnvelope(5, 1, 6, 2, 4326)
) x;

-- t2: bounding box intersects all features
SELECT 't2', topo_update.app_do_GetMergeableSurfaceBordersAsGeoJSON(
	'test',
	ST_MakeEnvelope(-10, -10, 10, 10, 4326)
) ORDER BY 2;

-- t3: bounding box intersects single border
SELECT 't3', topo_update.app_do_GetMergeableSurfaceBordersAsGeoJSON(
	'test',
	ST_MakeEnvelope(3, 1, 5, 1, 4326)
) ORDER BY 2;

-- t4: bounding box intersects all features but S3 has unique attribute a1
BEGIN;
UPDATE test.surface SET a1 = 'unique' WHERE k = 's3';
SELECT 't4', topo_update.app_do_GetMergeableSurfaceBordersAsGeoJSON(
	'test',
	ST_MakeEnvelope(-10, -10, 10, 10, 4326)
) ORDER BY 2;
ROLLBACK;

-- t5: bounding box intersects all features but S2 has unique attribute a2
BEGIN;
UPDATE test.surface SET a2 = 'unique' WHERE k = 's2';
SELECT 't5', topo_update.app_do_GetMergeableSurfaceBordersAsGeoJSON(
	'test',
	ST_MakeEnvelope(-10, -10, 10, 10, 4326)
) ORDER BY 2;
ROLLBACK;


-------------------------------------------
-- Cleanup
-------------------------------------------

SELECT * FROM util.drop_generated_schema('test');
DROP SCHEMA util CASCADE;
