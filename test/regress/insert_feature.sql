BEGIN;

set client_min_messages to WARNING;

\i :regdir/utils/check_topology_changes.sql
\i :regdir/utils/check_layer_features.sql
\i :regdir/utils/topogeom_summary.sql

SELECT NULL FROM CreateTopology('topo', 4326);

CREATE TABLE topo.poi (
    id SERIAL PRIMARY KEY,
	lbl TEXT
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo',
	'poi',
	'tg',
	'POINT');

-- Start of operations
SELECT 'start', 'topo', * FROM check_topology_changes('topo');
SELECT 'start', 'poi', * FROM check_layer_features('topo.poi'::regclass, 'tg'::name);

SELECT 'ins1', id, topogeom_summary(topogeom)
FROM topo_update.insert_feature(
	$${
		"geometry": { "coordinates": [ 1,0 ], "type": "Point" },
		"properties": { "id": "1", "lbl": "new" },
		"type": "Feature"
	}$$, -- feature
	4326, -- srid
    'topo.poi', -- layerTable
    'tg', -- layerGeomColumn
    'id', -- layerIDColumn
	'{ "id": [ "id" ], "lbl": [ "lbl" ] }', -- layerColMap
	0 -- tolerance
);

SELECT 'ins1', 'topo', * FROM check_topology_changes('topo');
SELECT 'ins1', 'poi', * FROM check_layer_features('topo.poi'::regclass, 'tg'::name);

SELECT 'ins2', id, topogeom_summary(topogeom)
FROM topo_update.insert_feature(
	'SRID=4326;POINT(10 10)'::geometry, -- feature_geometry
	$$ { "id": "2", "lbl": "bygeom" } $$, -- feature_properties
    'topo.poi', -- layerTable
    'tg', -- layerGeomColumn
    'id', -- layerIDColumn
	'{ "id": [ "id" ], "lbl": [ "lbl" ] }', -- layerColMap
	0 -- tolerance
);

SELECT 'ins2', 'topo', * FROM check_topology_changes('topo');
SELECT 'ins2', 'poi', * FROM check_layer_features('topo.poi'::regclass, 'tg'::name);

SELECT 'ins3', id
FROM topo_update.insert_feature(
	toTopoGeom('SRID=4326;POINT(20 20)', 'topo', 1, 0), -- feature_topogeom
	$$ { "id": "3", "lbl": "bytopogeom" } $$, -- feature_properties
    'topo.poi', -- layerTable
    'tg', -- layerGeomColumn
    'id', -- layerIDColumn
	'{ "id": [ "id" ], "lbl": [ "lbl" ] }' -- layerColMap
);

SELECT 'ins3', 'topo', * FROM check_topology_changes('topo');
SELECT 'ins3', 'poi', * FROM check_layer_features('topo.poi'::regclass, 'tg'::name);

SELECT 'ins4', id
FROM topo_update.insert_feature(
	toTopoGeom('SRID=4326;POINT(30 30)', 'topo', 1, 0), -- feature_topogeom
	$$ { "id": "4", "lbl_not_exist": "bytopogeom_" } $$, -- feature_properties
    'topo.poi', -- layerTable
    'tg', -- layerGeomColumn
    'id', -- layerIDColumn
	'{ "id": [ "id" ], "lbl": [ "lbl" ] }' -- layerColMap
);

SELECT 'ins4', 'topo', * FROM check_topology_changes('topo');
SELECT 'ins4', 'poi', * FROM check_layer_features('topo.poi'::regclass, 'tg'::name);

ROLLBACK;

