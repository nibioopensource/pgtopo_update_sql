set client_min_messages to WARNING;

-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
\i :regdir/../../src/sql/topo_update/function_02_remove_border_merge_surfaces.sql
\i :regdir/../../src/sql/topo_update/function_02_insert_feature.sql

\i :regdir/utils/check_topology_changes.sql
\i :regdir/utils/check_layer_features.sql

-------------------------------------
-- Create a topology 'topo'
-------------------------------------

SELECT NULL FROM CreateTopology('topo', 4326);

-------------------------------------
-- Create Surface and Border layers
-------------------------------------

CREATE TABLE topo.surface (
	k SERIAL PRIMARY KEY,
	lbl TEXT,
	lbl2 TEXT DEFAULT 'lbl2'
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo', 'surface', 'g',
	'POLYGON'
);

CREATE TABLE topo.border (
	k SERIAL PRIMARY KEY,
	lbl TEXT,
	lbl2 TEXT DEFAULT 'lbl2'
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo', 'border', 'g',
	'LINE'
);

-------------------------------------
-- Create mergePolicyProviderFunc function
-------------------------------------

--{
CREATE FUNCTION topo.mergePolicyProvider(
	surfaceLeftId TEXT,
	surfaceLeftTopoGeom topology.TopoGeometry,
	surfaceLeftProps JSONB,
	surfaceRightId TEXT,
	surfaceRightTopoGeom topology.TopoGeometry,
	surfaceRightProps JSONB,
	usr JSONB
)
RETURNS JSONB AS $BODY$
DECLARE
	procName TEXT := 'mergePolicyProvider';
	props JSONB;
	att TEXT;
	attValLeft TEXT;
	attValRight TEXT;
BEGIN
	RAISE DEBUG '%: surfaceLeft: %', procName, surfaceLeftId;
	RAISE DEBUG '%: surfaceRight: %', procName, surfaceRightId;

	RAISE DEBUG '%: surfaceLeftProps: %', procName, surfaceLeftProps;
	RAISE DEBUG '%: surfaceRightProps: %', procName, surfaceRightProps;

	FOR att IN SELECT jsonb_array_elements_text(usr -> 'compare_attributes')
	LOOP
		attValLeft := surfaceLeftProps ->> att;
		attValRight := surfaceRightProps ->> att;

		RAISE DEBUG '%: checking for attribute %: left=% right=%', procName, att, attValLeft, attValRight;

		IF attValLeft != attValRight
		THEN
			RAISE WARNING '%(%): attribute % is different between surfaces % (%) and % (%)',
				procName,
				(usr -> 'test_name'),
				att,
				surfaceLeftId,
				attValLeft,
				surfaceRightId,
				attValRight
			;
			RETURN NULL;
		END IF;
	END LOOP;

	IF ST_Area(surfaceLeftTopoGeom) > ST_Area(surfaceRightTopoGeom)
	THEN
		props := surfaceRightProps || surfaceLeftProps;
	ELSE
		props := surfaceLeftProps || surfaceRightProps;
	END IF;

	props := props || ( usr -> 'override_props' );

	RETURN props;
END;
$BODY$ LANGUAGE 'plpgsql'; --}

------------------------------------------------------
-- Test the add_border_split_surface function
------------------------------------------------------

CREATE TABLE topo.rem_border_results(
	test TEXT,
	id TEXT,
	typ CHAR,
	act CHAR,
	frm TEXT
);

--
-- te0: remove non-existing Border
--
SELECT 'te0', * FROM topo_update.remove_border_merge_surfaces(
	'topo.border', 'g', 'k', -- border layer identifier
	'-1', -- borderId
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.mergePolicyProvider'::regproc, -- mergePolicyProviderFunc
	'{}'::jsonb -- mergePolicyProviderParam
);

--
-- Create a topology like the following:
--
--     ,------>o<------.
--     |       |       |
--     |       |   F2  |
--     |       E4      |
--     |       |       |
--     |       v       |
--     |       o       |
--     |       |       |
--     E1      E5      E2
--     |       |       |
--     |       v       |
--     |       o       |
--     |   F1  ^       |
--     |       |       |
--     |       E3      |
--     |       |       |
--     `-------o-------'


-- E1
SELECT 'E' || TopoGeo_addLineString(
	'topo',
	'SRID=4326;LINESTRING(5 0,0 0,0 10,5 10)',
	0
);

-- E2
SELECT 'E' || TopoGeo_addLineString(
	'topo',
	'SRID=4326;LINESTRING(5 0,10 0,10 10,5 10)',
	0
);

-- E3
SELECT 'E' || TopoGeo_addLineString(
	'topo',
	'SRID=4326;LINESTRING(5 0,5 2)',
	0
);

-- E4
SELECT 'E' || TopoGeo_addLineString(
	'topo',
	'SRID=4326;LINESTRING(5 10,5 8)',
	0
);

-- E5
SELECT 'E' || TopoGeo_addLineString(
	'topo',
	'SRID=4326;LINESTRING(5 8,5 2)',
	0
);

--
-- Define Border/Surface like the following:
--
--     ,------>o<------.
--     |       |       |
--     |       |   F2  |
--     |      (B3)     |
--     |       E4      |
--     |       |       |
--     |       v       |
--    (B1)     o      (B2)
--     |       |       |
--     |      (B3)     |
--     E1      E5      E2
--     |       |       |
--     |       v       |
--     |       o       |
--     |   F1  ^       |
--     |       |       |
--     |      (B3)     |
--     |       E3      |
--     |       |       |
--     `-------o-------'
--

-- S1 (F1)
INSERT INTO topo.surface (lbl, g)
SELECT 'S1', topology.CreateTopoGeom(
	'topo', 3, 1,
	'{{1,3}}'
) RETURNING 'S'||k;

-- S2 (F2)
INSERT INTO topo.surface (lbl, g)
SELECT 'S2', topology.CreateTopoGeom(
	'topo', 3, 1,
	'{{2,3}}'
) RETURNING 'S'||k;

-- B1 (E1)
INSERT INTO topo.border (lbl, g)
SELECT 'B1', topology.CreateTopoGeom(
	'topo', 2, 2,
	'{{1,2}}'
) RETURNING 'B'||k;

-- B2 (E2)
INSERT INTO topo.border (lbl, g)
SELECT 'B2', topology.CreateTopoGeom(
	'topo', 2, 2,
	'{{2,2}}'
) RETURNING 'B'||k;

-- B3 (E3+E4+E5)
INSERT INTO topo.border (lbl, g)
SELECT 'B3', topology.CreateTopoGeom(
	'topo', 2, 2,
	'{{3,2},{-4,2},{-5,2}}'
) RETURNING 'B'||k;

SELECT 't0', 'border.features', check_layer_features('topo.border', 'g', true);
SELECT 't0', 'surface.features', check_layer_features('topo.surface', 'g', true);
SELECT 't0', 'topo.changes', * FROM check_topology_changes('topo');

--
-- te1: remove external Border
--
SELECT 'te1', * FROM topo_update.remove_border_merge_surfaces(
	'topo.border', 'g', 'k', -- border layer identifier
	'1', -- borderId
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.mergePolicyProvider'::regproc, -- mergePolicyProviderFunc
	'{}'::jsonb -- mergePolicyProviderParam
);

--
-- t1: remove central border but refuse to do so by mergePolicyProvider
--     (mismatching attributes)
--
SELECT 't1', 'merge', * FROM topo_update.remove_border_merge_surfaces(
	'topo.border', 'g', 'k', -- border layer identifier
	'3', -- borderId
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.mergePolicyProvider'::regproc, -- mergePolicyProviderFunc
	$$
		{
			"test_name": "t1",
			"compare_attributes": [ "lbl" ]
		}
	$$::jsonb, -- mergePolicyProviderParam
	removePrimitives => false,
	removeDanglingBorders => false

) ORDER BY 4,3;

SELECT 't1', 'border.features', check_layer_features('topo.border', 'g', true);
SELECT 't1', 'surface.features', check_layer_features('topo.surface', 'g', true);
SELECT 't1', 'topo.changes', * FROM check_topology_changes('topo');

--
-- t2: remove central border and succeed by mergePolicyProvider
--     (matching attributes)
--
SELECT 't2', 'merge', * FROM topo_update.remove_border_merge_surfaces(
	'topo.border', 'g', 'k', -- border layer identifier
	'3', -- borderId
	'topo.surface', 'g', 'k', -- surface layer identifier
	'topo.mergePolicyProvider'::regproc, -- mergePolicyProviderFunc
	$$
		{
			"test_name": "t2",
			"override_props": { "lbl2": "merged-by-t2", "lbl": "S3" },
			"compare_attributes": [ "lbl2" ]
		}
	$$::jsonb, -- mergePolicyProviderParam
	removePrimitives => false,
	removeDanglingBorders => false

) ORDER BY 4,3;

SELECT 't2', 'border.features', check_layer_features('topo.border', 'g', true);
SELECT 't2', 'surface.features', check_layer_features('topo.surface', 'g', true);
SELECT 't2', 'topo.changes', * FROM check_topology_changes('topo');


-- Situation is now as follows:
--
--     ,-------->o<--------.
--     |         |    F2   |
--     |         |   (S3)  |
--     |         E4        |
--     |         |         |
--     |         v         |
--    (B1)       o        (B2)
--     |         |         |
--     |         |         |
--     E1        E5        E2
--     |         |         |
--     |         v         |
--     |         o         |
--     |   F1    ^         |
--     |  (S3)   |         |
--     |         E3        |
--     |         |         |
--     `---------o---------'

--
-- t3: remove dangling border

-- We define a new border but not let it split
-- Surfaces
--
-- B4 (E3+E4+E5)
INSERT INTO topo.border (lbl, g)
SELECT 'B4', topology.toTopoGeom(
	'SRID=4326;LINESTRING(5 0,5 2,5 8,5 10)'::geometry,
	'topo', 2, 0
) RETURNING 't3', 'init', 'B'||k;

-- Situation is now as follows:
--
--     ,-------->o<--------.
--     |         |    F2   |
--     |         |   (S3)  |
--     |        (B4)       |
--     |         E4        |
--     |         |         |
--     |         v         |
--    (B1)       o        (B2)
--     |         |         |
--     |        (B4)       |
--     E1        E5        E2
--     |         |         |
--     |         v         |
--     |         o         |
--     |   F1    ^         |
--     |  (S3)   |         |
--     |        (B4)       |
--     |         E3        |
--     |         |         |
--     `---------o---------'

SELECT 't3', * FROM topo_update.remove_border_merge_surfaces(
	'topo.border', 'g', 'k', -- border layer identifier
	'4', -- borderId
	'topo.surface', 'g', 'k', -- surface layer identifier
	NULL::regproc, -- mergePolicyProviderFunc
	NULL::text, -- mergePolicyProviderParam
	removePrimitives => false,
	removeDanglingBorders => false
) ORDER BY 4,3;
SELECT 't3', 'border.features', check_layer_features('topo.border', 'g', true);
SELECT 't3', 'surface.features', check_layer_features('topo.surface', 'g', true);
SELECT 't3', 'topo.changes', * FROM check_topology_changes('topo');


-- t4: remove a border having no Surface on either sides

-- B5 (E6)
INSERT INTO topo.border (lbl, g)
SELECT 'B5', topology.toTopoGeom(
	'SRID=4326;LINESTRING(10 5,20 5)'::geometry,
	'topo', 2, 0
) RETURNING 't4.0', 'init', 'B'||k;

-- Situation is now as follows:
--
--     ,-------->o<--------.
--     |         |    F2   |
--     |         |   (S3)  |
--     |         |         |
--     |         E4        |
--     |         |         |
--     |         v        (B2)
--    (B1)       o         E6
--     |         |         |
--     |         |         |
--     E1        E5        o-----------E7-(B5)------->o
--     |         |         | {10,5}                {20,5}
--     |         v         |
--     |         o         |
--     |   F1    ^         |
--     |  (S3)   |        (B2)
--     |         |         E2
--     |         E3        |
--     |         |         |
--     `---------o---------'

SELECT 't4.0', 'border.features', check_layer_features('topo.border', 'g', true);
SELECT 't4.0', 'surface.features', check_layer_features('topo.surface', 'g', true);
SELECT 't4.0', 'topo.changes', check_topology_changes('topo');

SELECT 't4', 'rem', * FROM topo_update.remove_border_merge_surfaces(
	'topo.border', 'g', 'k', -- border layer identifier
	'5', -- borderId
	'topo.surface', 'g', 'k', -- surface layer identifier
	NULL::regproc, -- mergePolicyProviderFunc
	NULL::text, -- mergePolicyProviderParam
	removePrimitives => false,
	removeDanglingBorders => false
) ORDER BY 4,3;
SELECT 't4.1', 'border.features', check_layer_features('topo.border', 'g', true);
SELECT 't4.1', 'surface.features', check_layer_features('topo.surface', 'g', true);
SELECT 't4.1', 'topo.changes', * FROM check_topology_changes('topo');

-- t5: remove a border and cleanup primitives

-- B6 (E4+E5)
INSERT INTO topo.border (lbl, g)
SELECT 'B6', topology.toTopoGeom(
	'SRID=4326;LINESTRING(5 10,5 2)'::geometry,
	'topo', 2, 0
) RETURNING 't5.0', 'init', 'B'||k;

-- Situation is now as follows:
--
--             {5,10}
--     ,-------->o<--------.
--     |         |    F2   |
--     |         |   (S3)  |
--     |        (B6)       |
--     |         E4        |
--     |         |         |
--     |         v        (B2)
--    (B1)       o{5,8}    E6
--     |         |         |
--     |        (B6)       |
--     E1        E5        o-----------E7------------>o
--     |         |         | {10,5}                {20,5}
--     |         v         |
--     |         o{5,2}    |
--     |   F1    ^         |
--     |  (S3)   |        (B2)
--     |         |         E2
--     |         E3        |
--     |         |         |
--     `---------o---------'
--             {5,0}

SELECT 't5.0', 'border.features', check_layer_features('topo.border', 'g', true);
SELECT 't5.0', 'surface.features', check_layer_features('topo.surface', 'g', true);
SELECT 't5.0', 'topo.changes', check_topology_changes('topo');

SELECT 't5', 'rem', * FROM topo_update.remove_border_merge_surfaces(
	'topo.border', 'g', 'k', -- border layer identifier
	'6', -- borderId
	'topo.surface', 'g', 'k', -- surface layer identifier
	NULL::regproc, -- mergePolicyProviderFunc
	NULL::text, -- mergePolicyProviderParam
	removePrimitives => true,
	removeDanglingBorders => false
) ORDER BY 4,3;
SELECT 't5.1', 'border.features', check_layer_features('topo.border', 'g', true);
SELECT 't5.1', 'surface.features', check_layer_features('topo.surface', 'g', true);
SELECT 't5.1', 'topo.changes', * FROM check_topology_changes('topo');

-- t6: remove a border and borders left dangling by its removal

-- B7 (E8)
INSERT INTO topo.border (lbl, g)
SELECT 'B7', topology.toTopoGeom(
	'SRID=4326;LINESTRING(5 0,5 2)'::geometry,
	'topo', 2, 0
) RETURNING 't6.0', 'init', 'B'||k;
-- B8 (E9)
INSERT INTO topo.border (lbl, g)
SELECT 'B8', topology.toTopoGeom(
	'SRID=4326;LINESTRING(5 5,5 2)'::geometry,
	'topo', 2, 0
) RETURNING 't6.0', 'init', 'B'||k;
-- B9 (E10)
INSERT INTO topo.border (lbl, g)
SELECT 'B9', topology.toTopoGeom(
	'SRID=4326;LINESTRING(5 5,5 8)'::geometry,
	'topo', 2, 0
) RETURNING 't6.0', 'init', 'B'||k;
-- B10 (E4)
INSERT INTO topo.border (lbl, g)
SELECT 'B10', topology.toTopoGeom(
	'SRID=4326;LINESTRING(5 10,5 8)'::geometry,
	'topo', 2, 0
) RETURNING 't6.0', 'init', 'B'||k;

-- B11 (E11) -- splits F3 creating F4 on its left (bottom)
INSERT INTO topo.border (lbl, g)
SELECT 'B11', topology.toTopoGeom(
	'SRID=4326;LINESTRING(10 5,5 5)'::geometry,
	'topo', 2, 0
) RETURNING 't6.0', 'init', 'B'||k;

-- Situation is now as follows:
--
--             {5,10}
--     ,-------->o<--------.
--     |         |         |
--     |         |    F3   |
--     |         |   (S4)  |
--     |         |         |
--     |       (B10)       |
--     |         E4        |
--     |         |         |
--     |         v        (B2)
--    (B1)       o{5,8}    E6
--    E1         ^         |
--     |         |         |
--     |        (B9)       |
--     |        E10        |
--     |         |         |
--     |         |  (B11)  |
--     |    {5,5}o<--E11---o-----------E7------------>o
--     |         |         | {10,5}                {20,5}
--     |         |         |
--     |        (B8)       |
--     |         E9   F4   |
--     |         |   (S3)  |
--     |         v         |
--     |         o{5,2}    |
--     |   F1    ^         |
--     |  (S3)   |        (B2)
--     |        (B7)       E2
--     |         E8        |
--     |         |         |
--     `---------o---------'
--             {5,0}
--
SELECT 't6.0', 'border.features', check_layer_features('topo.border', 'g', true);
SELECT 't6.0', 'surface.features', check_layer_features('topo.surface', 'g', true);
SELECT 't6.0', 'topo.changes', check_topology_changes('topo');

BEGIN;
--set client_min_messages to debug;
SELECT 't6.1', 'rem', 'noclean', * FROM topo_update.remove_border_merge_surfaces(
	'topo.border', 'g', 'k', -- border layer identifier
	'7', -- borderId
	'topo.surface', 'g', 'k', -- surface layer identifier
	NULL::regproc, -- mergePolicyProviderFunc
	NULL::text -- mergePolicyProviderParam
	,removePrimitives => false
	,removeDanglingBorders => true
) ORDER BY 5,4;
SELECT 't6.1', 'border.features', check_layer_features('topo.border', 'g', true);
SELECT 't6.1', 'surface.features', check_layer_features('topo.surface', 'g', true);
SELECT 't6.1', 'topo.changes', * FROM check_topology_changes('topo');
ROLLBACK;

BEGIN;
--set client_min_messages to debug;
SELECT 't6.2', 'rem', 'clean', * FROM topo_update.remove_border_merge_surfaces(
	'topo.border', 'g', 'k', -- border layer identifier
	'7', -- borderId
	'topo.surface', 'g', 'k', -- surface layer identifier
	NULL::regproc, -- mergePolicyProviderFunc
	NULL::text, -- mergePolicyProviderParam
	removePrimitives => true,
	removeDanglingBorders => true
) ORDER BY 5,4;
SELECT 't6.2', 'border.features', check_layer_features('topo.border', 'g', true);
SELECT 't6.2', 'surface.features', check_layer_features('topo.surface', 'g', true);
SELECT 't6.2', 'topo.changes', * FROM check_topology_changes('topo');
ROLLBACK;

-----------
-- Cleanup
-----------

SELECT NULL FROM DropTopology('topo');
DROP FUNCTION check_topology_changes(text);
DROP FUNCTION check_layer_features(regclass, name, bool);
