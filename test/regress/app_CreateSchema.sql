SET client_min_messages TO WARNING;

------------------------------------------------------------
-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
-------------------------------------------------------------

\i :regdir/../../src/sql/topo_update/function_03_app_CreateSchema.sql
\i :regdir/../../src/sql/topo_update/function_02_check_topo_primitives_all_used.sql
\i :regdir/../../src/sql/topo_update/function_02_find_gaps.sql
\i :regdir/../../src/sql/topo_update/function_02_check_unique_topo_primitive.sql

-------------------------------------------
-- Utility tester function
-------------------------------------------

\i :regdir/utils/app_test_utils.sql

-------------------------------------------
-- Invalid app schemas
-------------------------------------------

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{}'
);

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{ "version": "0" }'
);

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{ "version": "1.0" }'
);

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"tables": 1
	}'
);

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"tables": []
	}'
);

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"tables": [],
		"operations": {}
	}'
);

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"tables": [],
		"operations": {
			"someUnknownOperation": {}
		}
	}'
);

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"tables": [],
		"operations": {
			"AddBordersSplitSurfaces": {}
		}
	}'
);

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"tables": [],
		"surface_layer": 1,
		"operations": {
			"AddBordersSplitSurfaces": {}
		}
	}'
);

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"tables": [],
		"surface_layer": {},
		"operations": {
			"AddBordersSplitSurfaces": {}
		}
	}'
);

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"tables": [],
		"surface_layer": {
			"table_name": "surface"
		},
		"operations": {
			"AddBordersSplitSurfaces": {}
		}
	}'
);

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"tables": [],
		"surface_layer": {
			"table_name": "surface",
			"geo_column": "g"
		},
		"operations": {
			"AddBordersSplitSurfaces": {}
		}
	}'
);

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"tables": [
			{
				"name": "surface"
			}
		],
		"surface_layer": {
			"table_name": "surface",
			"geo_column": "g"
		},
		"operations": {
			"AddBordersSplitSurfaces": {}
		}
	}'
);

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"tables": [
			{
				"name": "surface",
				"topogeom_columns": [
					{ "name": "g", "type": "lineal" }
				]
			}
		],
		"surface_layer": {
			"table_name": "surface",
			"geo_column": "g"
		},
		"operations": {
			"AddBordersSplitSurfaces": {}
		}
	}'
);

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"tables": [
			{
				"name": "surface",
				"topogeom_columns": [
					{ "name": "g", "type": "areal" }
				]
			}
		],
		"surface_layer": {
			"table_name": "surface",
			"geo_column": "g"
		},
		"operations": {
			"AddBordersSplitSurfaces": {}
		}
	}'
);

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"tables": [
			{
				"name": "surface",
				"primary_key": "k",
				"topogeom_columns": [
					{ "name": "g", "type": "areal" }
				]
			}
		],
		"surface_layer": {
			"table_name": "surface",
			"geo_column": "g"
		},
		"operations": {
			"AddBordersSplitSurfaces": {}
		}
	}'
);

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"tables": [
			{
				"name": "surface",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" }
				],
				"topogeom_columns": [
					{ "name": "g", "type": "areal" }
				]
			}
		],
		"surface_layer": {
			"table_name": "surface",
			"geo_column": "g"
		},
		"operations": {
			"AddBordersSplitSurfaces": {}
		}
	}'
);

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"tables": [
			{
				"name": "surface",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" }
				],
				"topogeom_columns": [
					{ "name": "g", "type": "areal" }
				]
			}
		],
		"surface_layer": {
			"table_name": "surface",
			"geo_column": "g"
		},
		"border_layer": 1,
		"operations": {
			"AddBordersSplitSurfaces": {}
		}
	}'
);

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"tables": [
			{
				"name": "surface",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" }
				],
				"topogeom_columns": [
					{ "name": "g", "type": "areal" }
				]
			}
		],
		"surface_layer": {
			"table_name": "surface",
			"geo_column": "g"
		},
		"border_layer": {},
		"operations": {
			"AddBordersSplitSurfaces": {}
		}
	}'
);


SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"operations": {
			"AddBordersSplitSurfaces": {}
		},
		"tables": [
			{
				"name": "surface",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" }
				],
				"topogeom_columns": [
					{ "name": "g", "type": "areal" }
				]
			}
		],
		"surface_layer": {
			"table_name": "surface",
			"geo_column": "g"
		},
		"border_layer": {
			"table_name": "border"
		}
	}'
);

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"operations": {
			"AddBordersSplitSurfaces": {}
		},
		"tables": [
			{
				"name": "surface",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" }
				],
				"topogeom_columns": [
					{ "name": "g", "type": "areal" }
				]
			}
		],
		"surface_layer": {
			"table_name": "surface",
			"geo_column": "g"
		},
		"border_layer": {
			"table_name": "border",
			"geo_column": "g"
		}
	}'
);

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"operations": {
			"AddBordersSplitSurfaces": {}
		},
		"tables": [
			{
				"name": "surface",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" }
				],
				"topogeom_columns": [
					{ "name": "g", "type": "areal" }
				]
			},
			{
				"name": "border"
			}
		],
		"surface_layer": {
			"table_name": "surface",
			"geo_column": "g"
		},
		"border_layer": {
			"table_name": "border",
			"geo_column": "g"
		}
	}'
);

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"operations": {
			"AddBordersSplitSurfaces": {}
		},
		"tables": [
			{
				"name": "surface",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" }
				],
				"topogeom_columns": [
					{ "name": "g", "type": "areal" }
				]
			},
			{
				"name": "border",
				"topogeom_columns": [
					{ "name": "g", "type": "areal" }
				]
			}
		],
		"surface_layer": {
			"table_name": "surface",
			"geo_column": "g"
		},
		"border_layer": {
			"table_name": "border",
			"geo_column": "g"
		}
	}'
);

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"operations": {
			"AddBordersSplitSurfaces": {}
		},
		"tables": [
			{
				"name": "surface",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" }
				],
				"topogeom_columns": [
					{ "name": "g", "type": "areal" }
				]
			},
			{
				"name": "border",
				"topogeom_columns": [
					{ "name": "g", "type": "lineal" }
				]
			}
		],
		"surface_layer": {
			"table_name": "surface",
			"geo_column": "g"
		},
		"border_layer": {
			"table_name": "border",
			"geo_column": "g"
		}
	}'
);

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"operations": {
			"AddBordersSplitSurfaces": {}
		},
		"tables": [
			{
				"name": "surface",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" }
				],
				"topogeom_columns": [
					{ "name": "g", "type": "areal" }
				]
			},
			{
				"name": "border",
				"primary_key": "k",
				"topogeom_columns": [
					{ "name": "g", "type": "lineal" }
				]
			}
		],
		"surface_layer": {
			"table_name": "surface",
			"geo_column": "g"
		},
		"border_layer": {
			"table_name": "border",
			"geo_column": "g"
		}
	}'
);

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"operations": {
			"AddBordersSplitSurfaces": {}
		},
		"tables": [
			{
				"name": "surface",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" }
				],
				"topogeom_columns": [
					{ "name": "g", "type": "areal",
					  "allow_crossings": false }
				]
			},
			{
				"name": "border",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" }
				],
				"topogeom_columns": [
					{ "name": "g", "type": "lineal" }
				]
			}
		],
		"surface_layer": {
			"table_name": "surface",
			"geo_column": "g"
		},
		"border_layer": {
			"table_name": "border",
			"geo_column": "g"
		}
	}'
);

-- GetFeaturesAsTopoJSON requires at least one of border_layer, surface_layer
SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"operations": {
			"GetFeaturesAsTopoJSON": {}
		},
		"tables": [
			{
				"name": "surface",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" }
				],
				"topogeom_columns": [
					{ "name": "g", "type": "areal" }
				]
			},
			{
				"name": "border",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" }
				],
				"topogeom_columns": [
					{ "name": "g", "type": "lineal" }
				]
			}
		]
	}'
);

-------------------------------------------
-- Save minimal app schema into a table
-------------------------------------------

CREATE TABLE util.minimal_app_schema AS
SELECT $$
	{
		"version": "1.0",
		"tables": [
			{
				"name": "surface",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" }
				],
				"topogeom_columns": [
					{
						"name": "g",
						"type": "areal"
					}
				]
			},
			{
				"name": "border",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" }
				],
				"topogeom_columns": [
					{
						"name": "g",
						"type": "lineal"
					}
				]
			}
		],
		"surface_layer": {
			"table_name": "surface",
			"geo_column": "g"
		},
		"border_layer": {
			"table_name": "border",
			"geo_column": "g"
		},
		"operations": {
			"AddBordersSplitSurfaces": { }
		}
	}
$$::jsonb AS cfg;

-------------------------------------------
-- t1. Minimal app schema
-------------------------------------------

SELECT * FROM topo_update.app_CreateSchema( 'test',
	( SELECT cfg FROM util.minimal_app_schema )
 );
SELECT * FROM util.check_generated_schema('t1', 'test');
SELECT * FROM util.drop_generated_schema('test');

-------------------------------------------------------------
-- t2. App schema with allow_overlaps=false for surface table
-------------------------------------------------------------

SELECT * FROM topo_update.app_CreateSchema( 'test',
	jsonb_set(
		( SELECT cfg FROM util.minimal_app_schema ),
		'{tables,0,topogeom_columns,0,allow_overlaps}',
		to_jsonb(false)
	)
);
SELECT * FROM util.check_generated_schema('t2', 'test');
SELECT * FROM util.drop_generated_schema('test');

-------------------------------------------------------------
-- t3. App schema with allow_overlaps=false for border table
-------------------------------------------------------------

SELECT * FROM topo_update.app_CreateSchema( 'test',
	jsonb_set(
		( SELECT cfg FROM util.minimal_app_schema ),
		'{tables,1,topogeom_columns,0,allow_overlaps}',
		to_jsonb(false)
	)
);
SELECT * FROM util.check_generated_schema('t3', 'test');
SELECT * FROM util.drop_generated_schema('test');

-------------------------------------------------------------
-- t4. App schema with allow_gaps=false for surface table
-------------------------------------------------------------

SELECT * FROM topo_update.app_CreateSchema( 'test',
	jsonb_set(
		( SELECT cfg FROM util.minimal_app_schema ),
		'{tables,0,topogeom_columns,0,allow_gaps}',
		to_jsonb(false)
	)
);
SELECT * FROM util.check_generated_schema('t4', 'test');
SELECT * FROM util.drop_generated_schema('test');

-------------------------------------------------------------
-- t5. App schema with allow_gaps=false for border table
-------------------------------------------------------------

SELECT * FROM topo_update.app_CreateSchema( 'test',
	jsonb_set(
		( SELECT cfg FROM util.minimal_app_schema ),
		'{tables,1,topogeom_columns,0,allow_gaps}',
		to_jsonb(false)
	)
);
SELECT * FROM util.check_generated_schema('t5', 'test');
SELECT * FROM util.drop_generated_schema('test');

-------------------------------------------------------------
-- t6. App schema with domains border table
-------------------------------------------------------------

SELECT * FROM topo_update.app_CreateSchema( 'test',
	jsonb_set(
		( SELECT cfg FROM util.minimal_app_schema ),
		'{domains}',
		jsonb_build_array(
			jsonb_build_object(
				'name','mypoint',
				'type','geometry',
				'modifiers', jsonb_build_array(
					'point',
					'4326'
				)
			),
			jsonb_build_object(
				'name','mydirection',
				'type','char',
				'allowed_values', jsonb_build_array('N','S','W','O')
			)
		)
	)
);
SELECT * FROM util.check_generated_schema('t6', 'test');
SELECT * FROM util.drop_generated_schema('test');

-------------------------------------------
-- Cleanup
-------------------------------------------

DROP SCHEMA util CASCADE;


