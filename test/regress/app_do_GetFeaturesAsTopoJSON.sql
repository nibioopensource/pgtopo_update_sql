SET client_min_messages TO WARNING;
SET extra_float_digits TO -3;

------------------------------------------------------------
-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
-------------------------------------------------------------

\i :regdir/../../src/sql/topo_update/function_03_app_do_GetFeaturesAsTopoJSON.sql

-------------------------------------------
-- Utility schema (data and functions)
-------------------------------------------

\i :regdir/utils/app_test_utils.sql

-------------------------------------------
-- Save minimal app schema into a table
-------------------------------------------

--		"surface_layer": {
--			"table": "surface",
--			"geo_column": "tg"
--		},

CREATE TABLE util.minimal_app_schema AS
SELECT $$
	{
		"version": "1.0",
		"topology": { "srid": 4326 },
		"tables": [ {
				"name": "surface",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" },
					{ "name": "label", "type": "text" }
				],
				"topogeom_columns": [
					{
						"name": "g",
						"type": "areal"
					}
				]
			}, {
				"name": "border",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" },
					{ "name": "label", "type": "text" }
				],
				"topogeom_columns": [
					{
						"name": "g",
						"type": "lineal"
					}
				]
			}, {
				"name": "path",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" },
					{ "name": "label", "type": "text" }
				],
				"topogeom_columns": [
					{
						"name": "g",
						"type": "lineal"
					}
				]
			}, {
				"name": "point",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" },
					{ "name": "label", "type": "text" }
				],
				"topogeom_columns": [
					{
						"name": "g",
						"type": "puntal"
					}
				]
			}
		],
		"border_layer": {
			"table_name": "border",
			"geo_column": "g"
		},
		"surface_layer": {
			"table_name": "surface",
			"geo_column": "g"
		},
		"path_layer": {
			"table_name": "path",
			"geo_column": "g"
		},
		"point_layer": {
			"table_name": "point",
			"geo_column": "g"
		},
		"operations": {
			"GetFeaturesAsTopoJSON": { }
		}
	}
$$::jsonb AS cfg;

-- Create the schema
SELECT * FROM topo_update.app_CreateSchema( 'test',
	( SELECT cfg FROM util.minimal_app_schema )
);

-- Add a border
INSERT INTO test.border(label, g) VALUES ( 'b1', topology.toTopoGeom(
	'SRID=4326;LINESTRING(0 0,10 0,5 10,0 0)',
	'test_sysdata_webclient',
	( select layer_id from topology.layer where table_name = 'border' )
) );

-- Add a surface
INSERT INTO test.surface(label, g) VALUES ( 's1', topology.toTopoGeom(
	'SRID=4326;POLYGON((0 0,10 0,5 10,0 0))',
	'test_sysdata_webclient',
	( select layer_id from topology.layer where table_name = 'surface' )
) );

-- Add a path
INSERT INTO test.path(label, g) VALUES ( 'p1', topology.toTopoGeom(
	'SRID=4326;LINESTRING(0 -10,10 -10)',
	'test_sysdata_webclient',
	( select layer_id from topology.layer where table_name = 'path' )
) );

-- Add a point
INSERT INTO test.point(label, g) VALUES ( 'o1', topology.toTopoGeom(
	'SRID=4326;POINT(10 40)',
	'test_sysdata_webclient',
	( select layer_id from topology.layer where table_name = 'point' )
) );


-- te1: unexisting app
SELECT 'te1', * FROM topo_update.app_do_GetFeaturesAsTopoJSON(
	'non-existing-app',
	ST_MakeEnvelope(-180, -90, 180, 90, 4326)
);

-- te2: operation is not enabled
BEGIN;
UPDATE test_sysdata_webclient_functions.app_config
SET cfg = jsonb_concat(
		cfg - 'operations',
		jsonb_build_object('operations', jsonb_build_object())
	);
SELECT 'te2', * FROM topo_update.app_do_GetFeaturesAsTopoJSON(
	'test',
	ST_MakeEnvelope(-180, -90, 180, 90, 4326)
);
ROLLBACK;

-- te3: layers not found in appconfig
-- This should be already prevented at app creation time
BEGIN;
UPDATE test_sysdata_webclient_functions.app_config
SET cfg = cfg - 'border_layer' - 'surface_layer' - 'path_layer' - 'point_layer';
SELECT 'te3', * FROM topo_update.app_do_GetFeaturesAsTopoJSON(
	'test',
	ST_MakeEnvelope(-180, -90, 180, 90, 4326)
);
ROLLBACK;

-- t1: bounding box intersects no feature
SELECT 't1', jsonb_pretty(
	topo_update.app_do_GetFeaturesAsTopoJSON(
		'test',
		ST_MakeEnvelope(-180, -90, -150, -80, 4326)
	)
);

-- t2: bounding box intersects some features
SELECT 't2', jsonb_pretty(
	topo_update.app_do_GetFeaturesAsTopoJSON(
		'test',
		ST_MakeEnvelope(-180, -90, 180, 90, 4326)
	)
);

-- t3: custom output srid and max decimal digits
WITH res AS (
	SELECT topo_update.app_do_GetFeaturesAsTopoJSON(
		'test',
		ST_MakeEnvelope(-180, -90, 180, 90, 4326),
		3857,
		3
	) j
)
SELECT 't3',
	j -> 'crs' -> 'properties' ->> 'name',
	j -> 'arcs' -> 0 -> 1 -> 0
FROM res;



-------------------------------------------
-- Cleanup
-------------------------------------------

SELECT * FROM util.drop_generated_schema('test');
SET extra_float_digits TO 0;
DROP SCHEMA util CASCADE;
