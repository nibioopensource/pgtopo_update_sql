
set client_min_messages to WARNING;

-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
\i :regdir/../../src/sql/topo_update/function_01_prepare_split_border.sql

BEGIN;


-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/287
SELECT NULL FROM topology.CreateTopology('topo', 0, '2');
SELECT NULL FROM topology.ST_CreateTopoGeo(
	'topo',
	$$
POLYGON((0 0, 5 0, 10 0,10 10,7 10,0 10,0 0))
	$$
);

SELECT 't258.0', ST_AsText(topo_update.prepare_split_border(
	'LINESTRING(6 -10, 6 20)',
	'topo'
));

SELECT 't258.1', ST_AsText(topo_update.prepare_split_border(
	'LINESTRING(6 -10, 6 20)',
	( select t from topology.topology t where name = 'topo' ),
	tolerance => 1
));

-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/288

SELECT 't288', ST_AsText(topo_update.prepare_split_border(
	'MULTILINESTRING((-5 5, 20 5),(15 3, 15 7))',
	'topo'
));
