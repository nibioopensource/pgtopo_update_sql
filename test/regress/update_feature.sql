BEGIN;

set client_min_messages to WARNING;

\i :regdir/utils/check_topology_changes.sql
\i :regdir/utils/check_layer_features.sql
\i :regdir/utils/topogeom_summary.sql

-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
\i :regdir/../../src/sql/topo_update/function_02_update_feature.sql

---------------------
-- Initialize schema
---------------------

SELECT NULL FROM CreateTopology('topo', 4326);

CREATE TABLE topo.poi (
    id INT PRIMARY KEY,
	lbl TEXT
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo',
	'poi',
	'tg',
	'POINT');

INSERT INTO topo.poi (id, lbl, tg) VALUES (
	1,
	'orig',
	toTopoGeom('SRID=4326;POINT(0 0)', 'topo', 1, 0)
);

-- Start of operations
SELECT 'start', 'topo', * FROM check_topology_changes('topo');
SELECT 'start', 'poi', *
FROM check_layer_features('topo.poi'::regclass, 'tg'::name, true);

CREATE TEMP TABLE update_results (
	test TEXT,
	updated BOOL,
	old_tg topology.TopoGeometry
);

CREATE TEMP TABLE tmp_tg (
	test TEXT,
	tg topology.TopoGeometry
);

--------------------------------------
-- upd1: Update with a GeoJSON feature
--------------------------------------

SELECT 'upd1', topogeom_summary(topogeom)
FROM topo_update.update_feature(
	$${
		"geometry": { "coordinates": [ 1,0 ], "type": "Point" },
		"properties": { "id": "1", "lbl": "new" },
		"type": "Feature"
	}$$, -- feature
	4326, -- srid
    'topo.poi', -- layerTable
    'tg', -- layerGeomColumn
    'id', -- layerIdColumn
    '1', -- id
	'{ "id": [ "id" ], "lbl": [ "lbl" ] }', -- layerColMap
	0 -- tolerance
);

SELECT 'upd1', 'topo', * FROM check_topology_changes('topo');
SELECT 'upd1', 'poi', *
FROM check_layer_features('topo.poi'::regclass, 'tg'::name, true);

--------------------------------
-- upd2: Update with a Geometry
--------------------------------

SELECT 'upd2', topogeom_summary(topogeom)
FROM topo_update.update_feature(
	'SRID=4326;POINT(2 0)'::geometry, -- feature_geometry
	$${ "id": "1", "lbl": "bygeom" }$$, -- feature_properties
    'topo.poi', -- layerTable
    'tg', -- layerGeomColumn
    'id', -- layerIdColumn
    '1', -- id
	'{ "id": [ "id" ], "lbl": [ "lbl" ] }', -- layerColMap
	0 -- tolerance
);

SELECT 'upd2', 'topo', * FROM check_topology_changes('topo');
SELECT 'upd2', 'poi', *
FROM check_layer_features('topo.poi'::regclass, 'tg'::name, true);

-----------------------------------------------------------------
-- upd3: Update with e *new* TopoGeometry, keeping the old alive
-----------------------------------------------------------------

INSERT INTO update_results(test, updated, old_tg)
SELECT 'upd3', updated, old_topogeom
FROM topo_update.update_feature(
	toTopoGeom( -- feature_topogeom
		'SRID=4326;POINT(3 0)'::geometry,
		'topo', -- topology name
		1, --layer_id,
		0 -- tolerance
	),
	$${ "id": "1", "lbl": "byNewTopoGeom" }$$, -- feature_properties
    'topo.poi', -- layerTable
    'tg', -- layerGeomColumn
    'id', -- layerIdColumn
    '1', -- id
	'{ "id": [ "id" ], "lbl": [ "lbl" ] }', -- layerColMap
	false -- keep alive the old TopoGeometry
);

SELECT 'upd3', 'upd', updated, ST_AsEWKT(old_tg)
FROM update_results WHERE test = 'upd3';

SELECT 'upd3', 'topo', * FROM check_topology_changes('topo');
SELECT 'upd3', 'poi', *
FROM check_layer_features('topo.poi'::regclass, 'tg'::name, true);

SELECT 'upd3', 'clean', topogeom_summary(clearTopoGeom(old_tg))
FROM update_results
WHERE test = 'upd3';

---------------------------------------------
-- upd4: Update with a modified TopoGeometry
---------------------------------------------

INSERT INTO update_results(test, updated, old_tg)
SELECT 'upd4', updated, old_topogeom
FROM topo_update.update_feature(
	(
		SELECT TopoGeom_addElement(tg, '{1,1}')
		FROM topo.poi WHERE id = 1
	), -- feature_topogeom
	$${ "id": "1", "lbl": "bySameTopoGeom" }$$, -- feature_properties
    'topo.poi', -- layerTable
    'tg', -- layerGeomColumn
    'id', -- layerIdColumn
    '1', -- id
	'{ "id": [ "id" ], "lbl": [ "lbl" ] }' -- layerColMap
);

SELECT 'upd4', 'upd', updated, ST_AsEWKT(old_tg)
FROM update_results WHERE test = 'upd4';

SELECT 'upd4', 'topo', * FROM check_topology_changes('topo');
SELECT 'upd4', 'poi', *
	FROM check_layer_features('topo.poi'::regclass, 'tg'::name, true);

---------------------------------------------------------------------
-- upd5: Update with e *new* TopoGeometry, modifying the old one
--------------------------------------------------------------------

INSERT INTO tmp_tg(test, tg)
VALUES (
	'upd5',
	toTopoGeom( -- feature_topogeom
		'SRID=4326;POINT(30 0)'::geometry,
		'topo', -- topology name
		1, --layer_id,
		0 -- tolerance
	)
);

INSERT INTO update_results(test, updated, old_tg)
SELECT 'upd5', updated, old_topogeom
FROM topo_update.update_feature(
	( SELECT tg FROM tmp_tg WHERE test = 'upd5' ),
	$${ "id": "1", "lbl": "byNewTopoGeom.mod" }$$, -- feature_properties
    'topo.poi', -- layerTable
    'tg', -- layerGeomColumn
    'id', -- layerIdColumn
    '1', -- id
	'{ "id": [ "id" ], "lbl": [ "lbl" ] }', -- layerColMap
	true -- modify old TopoGeometry
);

SELECT test, 'upd', updated, ST_AsEWKT(old_tg)
FROM update_results WHERE test = 'upd5';

SELECT 'upd5', 'topo', * FROM check_topology_changes('topo');
SELECT 'upd5', 'poi', *
FROM check_layer_features('topo.poi'::regclass, 'tg'::name, true);

WITH deleted AS (
	DELETE FROM tmp_tg
	WHERE test = 'upd5'
	RETURNING tg
)
SELECT 'upd5', 'clean', topogeom_summary(clearTopoGeom(tg))
FROM deleted;

--------------------------------
-- upd6: Update with a Geometry
--------------------------------

INSERT INTO update_results(test, updated, old_tg)
SELECT 'upd6', updated, old_topogeom
FROM topo_update.update_feature(
	'SRID=4326;POINT(2 0)'::geometry, -- feature_geometry
	$${ "id": "1", "lbl": "bygeom.keepalive" }$$, -- feature_properties
    'topo.poi', -- layerTable
    'tg', -- layerGeomColumn
    'id', -- layerIdColumn
    '1', -- id
	'{ "id": [ "id" ], "lbl": [ "lbl" ] }', -- layerColMap
	0, -- tolerance
	false -- keep alive the old TopoGeometry
);

SELECT test, 'upd', updated, ST_AsEWKT(old_tg)
FROM update_results WHERE test = 'upd6';

SELECT 'upd6', 'topo', * FROM check_topology_changes('topo');
SELECT 'upd6', 'poi', *
FROM check_layer_features('topo.poi'::regclass, 'tg'::name, true);

SELECT 'upd6', 'clean', topogeom_summary(clearTopoGeom(old_tg))
FROM update_results
WHERE test = 'upd6';

--------------------------------------
-- upd7: Update with a GeoJSON feature
--------------------------------------

INSERT INTO update_results(test, updated, old_tg)
SELECT 'upd7', updated, old_topogeom
FROM topo_update.update_feature(
	$${
		"geometry": { "coordinates": [ 1,0 ], "type": "Point" },
		"properties": { "id": "1", "lbl": "new" },
		"type": "Feature"
	}$$, -- feature
	4326, -- srid
    'topo.poi', -- layerTable
    'tg', -- layerGeomColumn
    'id', -- layerIdColumn
    '1', -- id
	'{ "id": [ "id" ], "lbl": [ "lbl" ] }', -- layerColMap
	0, -- tolerance
	false -- keep alive the old TopoGeometry
);

SELECT test, 'upd', updated, ST_AsEWKT(old_tg)
FROM update_results WHERE test = 'upd7';

SELECT 'upd7', 'topo', * FROM check_topology_changes('topo');
SELECT 'upd7', 'poi', *
FROM check_layer_features('topo.poi'::regclass, 'tg'::name, true);

SELECT 'upd7', 'clean', topogeom_summary(clearTopoGeom(old_tg))
FROM update_results
WHERE test = 'upd7';

----------------------------------------
-- upd8: Update with a TopoElementArray
----------------------------------------

INSERT INTO update_results(test, updated, old_tg)
SELECT 'upd8', updated, topogeom
FROM topo_update.update_feature(
	'{{3,1}}'::topology.TopoElementArray,
	$${
		"id": "1", "lbl": "byelems"
	}$$, -- feature_properties
    'topo.poi', -- layerTable
    'tg', -- layerGeomColumn
    'id', -- layerIdColumn
    '1', -- id
	'{ "id": [ "id" ], "lbl": [ "lbl" ] }' -- layerColMap
);

SELECT test, 'upd', updated, ST_AsEWKT(old_tg)
FROM update_results WHERE test = 'upd8';

SELECT 'upd8', 'topo', * FROM check_topology_changes('topo');
SELECT 'upd8', 'poi', *
FROM check_layer_features('topo.poi'::regclass, 'tg'::name, true);

----------------------------------------
-- upd9: Update with no valid json keys
----------------------------------------

INSERT INTO update_results(test, updated, old_tg)
SELECT 'upd9', updated, topogeom
FROM topo_update.update_feature(
	'{{3,1}}'::topology.TopoElementArray,
	$${
		"id_not_exist": "1", "lbl_not_exist": "byelems"
	}$$, -- feature_properties
    'topo.poi', -- layerTable
    'tg', -- layerGeomColumn
    'id', -- layerIdColumn
    '1', -- id
	'{ "id": [ "id" ], "lbl": [ "lbl" ] }' -- layerColMap
);

SELECT test, 'upd', updated, ST_AsEWKT(old_tg)
FROM update_results WHERE test = 'upd9';

SELECT 'upd9', 'topo', * FROM check_topology_changes('topo');
SELECT 'upd9', 'poi', *
FROM check_layer_features('topo.poi'::regclass, 'tg'::name, true);

--------------------------------------------------------
-- upd10: Update with geometry in different CRS
--------------------------------------------------------

INSERT INTO update_results(test, updated, old_tg)
SELECT 'upd10', updated, old_topogeom
FROM topo_update.update_feature(
	$${
		"geometry": { "coordinates": [ 111319.49079327357, 222684.20850554455 ], "type": "Point" },
		"properties": { "id": "1", "lbl": "new" },
		"type": "Feature"
	}$$, -- feature
	3857, -- srid
    'topo.poi', -- layerTable
    'tg', -- layerGeomColumn
    'id', -- layerIdColumn
    '1', -- id
	'{ "id": [ "id" ], "lbl": [ "lbl" ] }', -- layerColMap
	0, -- tolerance
	true -- destroy the old TopoGeometry
);

SELECT test, 'upd', updated, ST_AsEWKT(old_tg, 4)
FROM update_results WHERE test = 'upd10';

SELECT 'upd10', 'topo', * FROM check_topology_changes('topo');
SELECT 'upd10', 'poi', *
FROM check_layer_features('topo.poi'::regclass, 'tg'::name, true);

SELECT 'upd10', 'clean', topogeom_summary(clearTopoGeom(old_tg))
FROM update_results
WHERE test = 'upd10';

--------------------------------------------------------
-- upd11: Update with linealVersionColumn
--------------------------------------------------------

ALTER TABLE topo.poi ADD "Version" int;
INSERT INTO update_results(test, updated, old_tg)
SELECT 'upd11', updated, old_topogeom
FROM topo_update.update_feature(
	'SRID=4326;POINT(0 0)',
	'{ "id": "1", "lbl": "new", "ver": 1 }', -- props
	'topo.poi', -- layerTable
	'tg', -- layerGeomColumn
	'id', -- layerIdColumn
	'1', -- id
	'{ "id": [ "id" ], "lbl": [ "lbl" ], "Version": [ "ver" ] }', -- layerColMap
	0, -- tolerance
	modifyExistingTopoGeom => true,
	layerVersionColumn => 'Version'
);

SELECT test, 'upd', updated, ST_AsEWKT(old_tg, 4)
FROM update_results WHERE test = 'upd11';

SELECT 'upd11', 'topo', * FROM check_topology_changes('topo');
SELECT 'upd11', 'poi', *
FROM check_layer_features('topo.poi'::regclass, 'tg'::name, true);

----------------------------------------
-- Cleanup
----------------------------------------

ROLLBACK;

