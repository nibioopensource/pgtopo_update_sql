BEGIN;

\i :regdir/../../src/sql/topo_update/function_01_json_col_map_from_pg.sql

CREATE TABLE proto_base (
	base_a int,
	base_b int
);

CREATE TABLE proto_comp (
	comp_base proto_base,
	comp_a int,
	comp_b int
);

CREATE TABLE proto_nest (
	nest_comp proto_comp,
	nest_base proto_base,
	nest_a int,
	nest_b int
);

SELECT 'simple', topo_update.json_col_map_from_pg('proto_base');

SELECT 'complex', topo_update.json_col_map_from_pg('proto_comp');

SELECT 'nested', topo_update.json_col_map_from_pg('proto_nest');

-- Omit leaf elements
SELECT 'omit_leaf', topo_update.json_col_map_from_pg(
	'proto_nest',
	ARRAY[
		'nest_a',
		'nest_base.base_b',
		'nest_base.nest_comp.comp_a',
		'nest_base.nest_comp.comp_base.base_a'
	]
);

-- Omit node elements
SELECT 'omit_node', topo_update.json_col_map_from_pg(
	'proto_nest',
	ARRAY[
		'nest_comp',
		'nest_base'
	]
);


ROLLBACK;
