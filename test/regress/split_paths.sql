
set client_min_messages to WARNING;

-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
\i :regdir/../../src/sql/topo_update/function_02_split_paths.sql

-------------------------------------------
-- Utility includes
-------------------------------------------

\i :regdir/utils/normalized_geom.sql

-------------------------------------
-- Create a topology 'topo'
-------------------------------------


SELECT NULL FROM CreateTopology('topo', 4326);

-------------------------------------
-- Create Path layer
-------------------------------------

CREATE TABLE topo.path (
	k SERIAL PRIMARY KEY,
	lbl TEXT,
	lbl2 TEXT DEFAULT 'lbl2'
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo', 'path', 'g',
	'LINE'
);


-------------------------------------
-- Create paths
-------------------------------------

--
--  o-p2--------o
--
--  o-p1--------o
--
--
--
INSERT INTO topo.path(lbl,g)
SELECT
	'p' || y+1,
	topology.toTopoGeom(
		ST_SetSRID(ST_MakeLine(ST_Point(0, y), ST_Point(10, y)), 4326),
		'topo',
		(
			SELECT l.layer_id
			FROM topology.topology t
				JOIN topology.layer l ON (l.topology_id = t.id)
			WHERE l.table_name = 'path'
		)
	)
FROM generate_series(0, 1) y;


-------------------------------------
-- Create colMapProvider function
-------------------------------------

--{
CREATE FUNCTION topo.testColMapProvider(act char, usr JSONB)
RETURNS JSONB AS $BODY$
DECLARE
	procName TEXT := 'testColMapProvider';
	map JSONB;
BEGIN
	map := usr -> format('%s', act);

	--RAISE WARNING '%: map for act:% is %', procName, act, map;

	RETURN map;
END;
$BODY$ LANGUAGE 'plpgsql'; --}



------------------------------------------------------
-- Test the split_paths function
------------------------------------------------------

CREATE TABLE topo.split_paths_results(
	test TEXT,
	id TEXT,
	act CHAR,
	frm TEXT
);

-- {
CREATE FUNCTION check_split_paths_effects(
	testname TEXT
)
RETURNS TABLE(o TEXT)
AS $BODY$
DECLARE
  rec RECORD;
  sql TEXT;
  attrs TEXT[];
BEGIN

	--RAISE WARNING 'Hello from check_split_paths_effects, testname %', testname;

	RETURN QUERY
	SELECT array_to_string(ARRAY[
			testname,
			'act',
			id,
			act,
			frm
		], '|')
	FROM topo.split_paths_results
	WHERE test = testname
	ORDER BY id::int;

	RETURN QUERY
	SELECT array_to_string(ARRAY[
			testname,
			'path',
			k::text,
			lbl,
			COALESCE(lbl2, ''),
			ST_AsText(
				normalized_geom(
					ST_Simplify(
						ST_CollectionHomogenize(g),
						0
					)
				)
			)
		], '|')
	FROM topo.path
	WHERE k in (
		SELECT id::int
		FROM topo.split_paths_results
		WHERE test = testname
	)
	ORDER BY k;

END;
$BODY$ LANGUAGE 'plpgsql';
--}

--
-- t1.e1: pass a geometory which is not a line
--
--   . . . . . .
--
SELECT 't1.e1', * FROM topo_update.split_paths(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [ [ 0, 0 ], [ 1, 0 ] ],
			"type" : "MultiPoint"
		},
		"properties" : {},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.path', 'g', 'k', -- path layer identifier
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	'{}'::jsonb, -- colMapProviderParam
	minToleratedPathLength => 10 -- needs be at least 10 units to be accepted
);


--
-- t1.e2: pass a path with missing CRS indication
--

SELECT 't1.e2', * FROM topo_update.split_paths(
	$$
	{
		"geometry" : {
			"coordinates" : [ [ 0, 0 ], [ 10, 0 ] ],
			"type" : "LineString"
		},
		"properties" : {
			"lbl": "t1"
		},
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.path', 'g', 'k', -- path layer identifier
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	'{}'::jsonb -- colMapProviderParam
)
ORDER BY fid::int
;

--
-- t1: split a single path
--
--
--  o-p1--------o
--
--           .
--  o-p0--------o
--           .
--
--
INSERT INTO topo.split_paths_results
SELECT 't1', * FROM topo_update.split_paths(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [ [ 8, -0.2 ], [ 8, 0.2 ] ],
			"type" : "LineString"
		},
		"properties" : { "s": "t1-split", "m": "t1-mod" },
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.path', 'g', 'k', -- path layer identifier
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	'{ "M": { "lbl2": [ "m" ] }, "S": { "lbl2": [ "s" ] } }'::jsonb -- colMapProviderParam
);
SELECT * FROM check_split_paths_effects('t1');
SELECT 't1', 'edges', (SELECT count(*) FROM topo.edge);
SELECT 't1', 'nodes', (SELECT count(*) FROM topo.node);

--
-- t2.e1: split two paths but only one is allowed
--
--
--                    .
--  o-p2------------------o
--                    .
--                    .
--                    .
--  o-p1------o--p3-------o
--                    .
--
INSERT INTO topo.split_paths_results
SELECT 't2.e1', * FROM topo_update.split_paths(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [ [ 9, -0.2 ], [ 9, 1.2 ] ],
			"type" : "LineString"
		},
		"properties" : { "s": "t2-split", "m": "t2-mod" },
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.path', 'g', 'k', -- path layer identifier
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	'{ "M": { "lbl2": [ "m" ] }, "S": { "lbl2": [ "s" ] } }'::jsonb, -- colMapProviderParam
	maxSplits => 1
);
SELECT * FROM check_split_paths_effects('t2.e1');

--
-- t2: split two paths
--
--
--                    .
--  o-p2------------------o
--                    .
--                    .
--                    .
--  o-p1------o--p3-------o
--                    .
--
--
--
INSERT INTO topo.split_paths_results
SELECT 't2', * FROM topo_update.split_paths(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [ [ 9, -0.2 ], [ 9, 1.2 ] ],
			"type" : "LineString"
		},
		"properties" : { "s": "t2-split", "m": "t2-mod" },
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.path', 'g', 'k', -- path layer identifier
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	'{ "M": { "lbl2": [ "m" ] }, "S": { "lbl2": [ "s" ] } }'::jsonb -- colMapProviderParam
);
SELECT * FROM check_split_paths_effects('t2');
SELECT 't2', 'edges', (SELECT count(*) FROM topo.edge);
SELECT 't2', 'nodes', (SELECT count(*) FROM topo.node);

--
-- t3: split by overlap
--
--
--
--     o-p2----======----o--p6--o
--
--
--     o-----p1--o--p3---o--p7--o
--
--
--
INSERT INTO topo.split_paths_results
SELECT 't3.e1', * FROM topo_update.split_paths(
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : {
					"name" : "EPSG:4326"
				},
				"type" : "name"
			},
			"coordinates" : [ [ 2, 1 ], [ 4, 1 ] ],
			"type" : "LineString"
		},
		"properties" : { "s": "t3-split", "m": "t3-mod" },
		"type" : "Feature"
	}
	$$, -- GeoJSON line being drawn
	'topo.path', 'g', 'k', -- path layer identifier
	'topo.testColMapProvider'::regproc, -- colMapProviderFunc
	'{ "M": { "lbl2": [ "m" ] }, "S": { "lbl2": [ "s" ] } }'::jsonb -- colMapProviderParam
);
SELECT * FROM check_split_paths_effects('t3.e1');

-----------
-- Cleanup
-----------

SELECT NULL FROM DropTopology('topo');
DROP FUNCTION check_split_paths_effects(TEXT);
DROP FUNCTION normalized_geom;
