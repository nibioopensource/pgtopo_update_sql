set client_min_messages to WARNING;

-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
\i :regdir/../../src/sql/topo_update/function_02_find_interiors_intersect.sql
\i :regdir/../../src/sql/topo_update/function_02_check_unique_topo_primitive.sql

-------------------------------------
-- Create a topology 'topo'
-------------------------------------

SELECT NULL FROM CreateTopology('topo');

-------------------------------------
-- Create linear layer (1)
-------------------------------------

CREATE TABLE topo.linear_features (
	k SERIAL PRIMARY KEY
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo', 'linear_features', 'g',
	'LINEAR'
);

-------------------------------------
-- Create areal layer (2)
-------------------------------------

CREATE TABLE topo.areal_features (
	k SERIAL PRIMARY KEY
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo', 'areal_features', 'g',
	'AREAL'
);

-----------------------------------------------------------
-- t1: try to add unique topo primitive constraint
-- to a table already containing overlapping linear objects
-----------------------------------------------------------

INSERT INTO topo.linear_features(g) VALUES (
	toTopoGeom('LINESTRING(0 0, 10 0)', 'topo', 1)
);
INSERT INTO topo.linear_features(g) VALUES (
	toTopoGeom('LINESTRING(5 0, 15 0)', 'topo', 1)
);

SELECT 't1', 'attempt to constraint to table with overlapping objects';
SELECT 't1', 'add_constraint', topo_update.add_unique_topo_primitive_constraint(
	'topo.linear_features', 'g', 'k'
);

-----------------------------------------------------
-- t2: add unique topo primitive constraint
--     to a table with no overlapping linear objects
--     and try to add an overlapping object
-----------------------------------------------------

CREATE TABLE topo.linear_features_deleted AS
WITH deleted AS (
	DELETE FROM topo.linear_features WHERE k = 2
	RETURNING k,g
)
SELECT k,g FROM deleted;

SELECT 't2', 'add_constraint', topo_update.add_unique_topo_primitive_constraint(
	'topo.linear_features', 'g', 'k'
);

SELECT 't2', 'attempt to add overlapping object to constrainted lineal layer';

-- This should fail
WITH inserted AS (
	INSERT INTO topo.linear_features
	SELECT * FROM topo.linear_features_deleted
	RETURNING k
)
SELECT 't2', 'inserted', k FROM inserted;

--------------------------------------------------------
-- CLEANUP
--------------------------------------------------------

SELECT NULL FROM DropTopology('topo');
