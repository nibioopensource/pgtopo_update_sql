SET client_min_messages TO WARNING;
SET extra_float_digits TO -3;

------------------------------------------------------------
-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
-------------------------------------------------------------

\i :regdir/../../src/sql/topo_update/function_03_app_CreateSchema.sql
\i :regdir/../../src/sql/topo_update/function_03_app_do_AddBorder.sql
\i :regdir/../../src/sql/topo_update/function_02_insert_feature.sql
\i :regdir/../../src/sql/topo_update/function_02_find_interiors_intersect.sql
\i :regdir/../../src/sql/topo_update/function_02_check_unique_topo_primitive.sql

-------------------------------------------
-- Utility schema (data and functions)
-------------------------------------------

\i :regdir/utils/app_test_utils.sql

-------------------------------------------
-- Save minimal app schema into a table
-------------------------------------------

CREATE TABLE util.minimal_app_schema AS
SELECT $$
	{
		"version": "1.0",
		"topology": {
			"srid": 4326
		},
		"tables": [
			{
				"name": "surface",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" }
				],
				"topogeom_columns": [
					{
						"name": "g",
						"type": "areal"
					}
				]
			},
			{
				"name": "border",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" }
				],
				"topogeom_columns": [
					{
						"name": "g",
						"type": "lineal"
					}
				]
			}
		],
		"surface_layer": {
			"table_name": "surface",
			"geo_column": "g"
		},
		"border_layer": {
			"table_name": "border",
			"geo_column": "g"
		},
		"operations": {
			"AddBorder": { }
		}
	}
$$::jsonb AS cfg;

-----------------------------------------------------------
-- t1: Test AddBorder against application allowing overlaps
-----------------------------------------------------------

SELECT * FROM topo_update.app_CreateSchema( 'test',
		( SELECT cfg FROM util.minimal_app_schema )
);
SELECT 't1', 'b1', * FROM topo_update.app_do_AddBorder('test',
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : { "name" : "EPSG:4326" },
				"type" : "name"
			},
			"coordinates" : [ [ 0, 0 ], [ 10, 0 ] ],
			"type" : "LineString"
		},
		"type" : "Feature",
		"properties" : { }
	}
	$$
);
SELECT 't1', 'b2', * FROM topo_update.app_do_AddBorder('test',
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : { "name" : "EPSG:4326" },
				"type" : "name"
			},
			"coordinates" : [ [ 5, 0 ], [ 15, 0 ] ],
			"type" : "LineString"
		},
		"type" : "Feature",
		"properties" : { }
	}
	$$
);
SELECT 't1', 'border', 'k', ST_AsEWKT(g) FROM test.border ORDER BY k;
SELECT * FROM util.drop_generated_schema('test');

----------------------------------------------------------------
-- t2: test AddBorder against application NOT allowing overlaps
----------------------------------------------------------------

SELECT * FROM topo_update.app_CreateSchema( 'test',
	jsonb_set(
		( SELECT cfg FROM util.minimal_app_schema ),
		'{tables,1,topogeom_columns,0,allow_overlaps}',
		to_jsonb(false)
	)
);
SELECT 't2', 'b1', * FROM topo_update.app_do_AddBorder('test',
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : { "name" : "EPSG:4326" },
				"type" : "name"
			},
			"coordinates" : [ [ 0, 0 ], [ 10, 0 ] ],
			"type" : "LineString"
		},
		"type" : "Feature",
		"properties" : { }
	}
	$$
);
-- This should fail, due to overlap
SELECT 't2', 'b2', * FROM topo_update.app_do_AddBorder('test',
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : { "name" : "EPSG:4326" },
				"type" : "name"
			},
			"coordinates" : [ [ 5, 0 ], [ 15, 0 ] ],
			"type" : "LineString"
		},
		"type" : "Feature",
		"properties" : { }
	}
	$$
);
SELECT 't2', 'border', k, ST_AsEWKT(g) FROM test.border ORDER BY k;
--SELECT 't2', '----CALLING find_interiors_intersect next ----';
--SELECT 't2', 'interiors_intersect', * FROM topo_update.find_interiors_intersect('test.border','g','k');
SELECT * FROM util.drop_generated_schema('test');

-------------------------------------------
-- Cleanup
-------------------------------------------

SET extra_float_digits TO 0;
DROP SCHEMA util CASCADE;
