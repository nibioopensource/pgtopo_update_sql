BEGIN;

set client_min_messages to NOTICE;

-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
\i :regdir/../../src/sql/topo_update/function_01_record_to_jsonb.sql
\i :regdir/../../src/sql/topo_update/function_02_surfaces_as_geojson.sql
\i :regdir/../../src/sql/topo_update/function_02_get_surface_borders.sql

---------------------
-- Initialize schema
---------------------

SELECT NULL FROM CreateTopology('topo', 4326);

CREATE TYPE topo.comp AS (
  f int
);

CREATE TABLE topo.border (
  "id" TEXT PRIMARY KEY,
  "deleted" BOOLEAN DEFAULT FALSE,
  "comp" topo.comp
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo',
	'border',
	'tg',
	'LINEAL'
);

CREATE VIEW topo.live_border
AS SELECT * FROM topo.border
WHERE NOT deleted;

CREATE TABLE topo.surface (
	"id" TEXT PRIMARY KEY,
	"comp" topo.comp,
	"deleted" BOOLEAN DEFAULT FALSE
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo',
	'surface',
	'tg',
	'AREAL'
);

CREATE VIEW topo.live_surface
AS SELECT * FROM topo.surface
WHERE NOT deleted;

---------------------
-- Add Borders
---------------------

INSERT INTO topo.border (id, comp.f, tg) VALUES
(
  'b1.BR', '1',
  --
  --                   ^
  --                   |
  --                   |
  --                   |
  --                   |
  --                   |
  --                   |
  --                   |
  --                   |
  --   o---(b1.BR)-----'
  --
  toTopoGeom(
    'SRID=4326;LINESTRING(0 0,10 0,10 10)',
    'topo',
    1,
    0
  )
),
(
  'b1.LT', '1',
  --
  --   .----b1.LT----->o
  --   |               ^
  --   |               |
  --   |               |
  --   |               |
  --   |               |
  --   |               |
  --   |               |
  --   |               |
  --   |               |
  --   o---(b1.BR)-----'
  --
  toTopoGeom(
    'SRID=4326;LINESTRING(0 0,0 10,10 10)',
    'topo',
    1,
    0
  )
),
(
  'b2.TR', 1,
  --
  --   .---(b1.LT)---->o
  --   |               ^
  --   |               |
  --   |               |
  --   |  o--b2.TR--.  |
  --   |            |  |
  --   |            |  |
  --   |            v  |
  --   |               |
  --   |               |
  --   o---(b1.BR)-----'
  --
  toTopoGeom(
    'SRID=4326;LINESTRING(2 8,8 8,8 2)',
    'topo',
    1,
    0
  )
),
(
  'b2.LB', 1,
  --
  --   .---(b1.LT)---->o
  --   |               ^
  --   |               |
  --   |               |
  --   |  o-(b2.TR)-.  |
  --   |  |         |  |
  --   |  |         |  |
  --   |  |         v  |
  --   |  `-b2.LB-->o  |
  --   |               |
  --   o---(b1.BR)-----'
  --
  toTopoGeom(
    'SRID=4326;LINESTRING(2 8,2 2,8 2)',
    'topo',
    1,
    0
  )
)
;

---------------------
-- Add Surfaces
---------------------

INSERT INTO topo.surface (id, comp.f, tg) VALUES
(
  's1', 1,
  --
  --   .---(b1.LT)---->o
  --   |               ^
  --   |  s1           |
  --   |               |
  --   |  o-(b2.TR)-.  |
  --   |  |         |  |
  --   |  |         |  |
  --   |  |         v  |
  --   |  `-(b2.LB)>o  |
  --   |               |
  --   o---(b1.BR)-----'
  --
  toTopoGeom(
    'SRID=4326;POLYGON((0 0,10 0,10 10,0 10,0 0),(2 2,8 2,8 8,2 8,2 2))',
    'topo',
    2,
    0
  )
),
(
  's2', 1,
  --
  --   .---(b1.LT)---->o
  --   |               ^
  --   |  (s1)         |
  --   |               |
  --   |  o-(b2.TR)-.  |
  --   |  |         |  |
  --   |  |  s2     |  |
  --   |  |         v  |
  --   |  `-(b2.LB)>o  |
  --   |               |
  --   o---(b1.BR)-----'
  --
  toTopoGeom(
    'SRID=4326;POLYGON((2 2,8 2,8 8,2 8,2 2))',
    'topo',
    2,
    0
  )
)
;

---------------------
-- t1: simple call
---------------------

SELECT '--- t1 ---', E'\n' || jsonb_pretty(
	topo_update.surfaces_as_geojson(
		'topo.live_surface',
		'tg',
		'id',
		'{ "id": [ "key" ], "comp.f": [ "n", "p" ] }',

		'topo.border',
		'tg',
		'id',
		'{ "id": [ "k" ], "comp.f": [ "cmp_f" ] }',

		ARRAY['s1', 's2']
	)
);

-----------------------------------------
-- t2: call with feature rewrite function
-----------------------------------------

CREATE FUNCTION featureRewriter(INOUT feat JSONB, role CHAR)
AS $BODY$
BEGIN

  IF role = 'S'
  THEN -- Surface feature

    -- Drop the coordinates from surface objects
    feat := feat #- '{ geometry, coordinates }';

  ELSE -- Border feature

    -- do nothing special for borders

  END IF;

  -- Apply this to any feature role
  feat := jsonb_set(
    feat,
    '{update}',
    jsonb_build_object(
      'action',
      'Replace'
    )
  );

END;
$BODY$ LANGUAGE 'plpgsql';

SELECT '--- t2 ---', E'\n' || jsonb_pretty(
	topo_update.surfaces_as_geojson(
		'topo.live_surface',
		'tg',
		'id',
		'{ "id": [ "key" ], "comp.f": [ "n", "p" ] }',

		'topo.border',
		'tg',
		'id',
		'{ "id": [ "k" ], "comp.f": [ "cmp_f" ] }',

		ARRAY['s1', 's2'],

    'featureRewriter'::regproc
	)
);

---------------------------------------------------------------
-- t3: call with feature rewrite function and custom parameter
---------------------------------------------------------------

-- Find JSONB to merge into feature in the "usr" parameter.
-- This function will apply actions found in the JSON in this order:
--  '*': applied to all feature types
--  'B': applied to border feature types
--  'S': applied in surface feature types
-- Supported actions are:
--	'strip': remove elements from the given path (as JSONB array)
--	'merge': merge elements from the given JSONB object
CREATE FUNCTION featureRewriterGeneric(INOUT feat JSONB, role CHAR, usr JSONB)
AS $BODY$
DECLARE
	cond TEXT;
	acts JSONB;
	merge_action JSONB;
	strip_action JSONB;
	strip_path TEXT[];
BEGIN

	FOR cond IN SELECT '*' UNION SELECT role
	LOOP
		acts := usr -> cond;
		--RAISE DEBUG 'Actions for cond %: : %', cond, acts;
		IF acts IS NOT NULL
		THEN
			strip_action := acts -> 'strip';
			IF strip_action IS NOT NULL
			THEN
				SELECT array_agg(e)
				FROM jsonb_array_elements_text(strip_action) e
				INTO strip_path;
				--RAISE DEBUG 'Strip path: %', strip_path;
				feat := feat #- strip_path;
			END IF;
			merge_action := acts -> 'merge';
			IF merge_action IS NOT NULL
			THEN
				--RAISE DEBUG 'Merge action: %', merge_action;
				feat := feat || merge_action;
			END IF;
		END IF;
	END LOOP;
END;
$BODY$ LANGUAGE 'plpgsql';

SELECT '--- t3 ---', E'\n' || jsonb_pretty(
	topo_update.surfaces_as_geojson(
		'topo.live_surface',
		'tg',
		'id',
		'{ "id": [ "key" ], "comp.f": [ "n", "p" ] }',

		'topo.border',
		'tg',
		'id',
		'{ "id": [ "k" ], "comp.f": [ "cmp_f" ] }',

		ARRAY['s1', 's2'],

		'featureRewriterGeneric'::regproc,
		$$ {
			"*": {
				"strip": [ "geometry" ],
				"merge": { "origin": "test" }
			},
			"B": {
				"merge": { "type": "Border" }
			},
			"S": {
				"merge": { "type": "Surface" }
			}
		} $$::JSONB
	)
);

------------------------------------------------------------------------
-- t4: Surface with Borders defined by not-normalized edges
--
-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/99
------------------------------------------------------------------------

INSERT INTO topo.border (id, tg) VALUES
(
  'b4.T',
  --
  --   ,---(b1.LT)---->o         o------b4.T----->o
  --   |               ^
  --   |  (s1)         |
  --   |               |
  --   |  o-(b2.TR)-.  |
  --   |  |         |  |
  --   |  |  (s2)   |  |
  --   |  |         v  |
  --   |  `-(b2.LB)>o  |
  --   |               |
  --   o---(b1.BR)-----'
  --
  toTopoGeom(
	-- NOTE: it's important that we define
	--       this edge from right to left,
	--       as the resulting Border will be
	--       *normalized* to be from left to right
    'SRID=4326;LINESTRING(30 10,20 10)',
    'topo',
    1,
    0
  )
),
(
  'b4.B',
  --
  --   ,---(b1.LT)---->o         o-----(b4.T)---->o
  --   |               ^
  --   |  (s1)         |
  --   |               |
  --   |  o-(b2.TR)-.  |
  --   |  |         |  |
  --   |  |  (s2)   |  |
  --   |  |         v  |
  --   |  `-(b2.LB)>o  |
  --   |               |
  --   o---(b1.BR)-----'         o------b4.B----->o
  --
  toTopoGeom(
	-- NOTE: it's important that we define
	--       this edge from right to left,
	--       as the resulting Border will be
	--       *normalized* to be from left to right
    'SRID=4326;LINESTRING(30 0,20 0)',
    'topo',
    1,
    0
  )
),
(
  'b4.L',
  --
  --   ,---(b1.LT)---->o         o-----(b4.T)---->o
  --   |               ^         ^
  --   |  (s1)         |         |
  --   |               |         |
  --   |  o-(b2.TR)-.  |         |
  --   |  |         |  |        B4.L
  --   |  |  (s2)   |  |         |
  --   |  |         v  |         |
  --   |  `-(b2.LB)>o  |         |
  --   |               |         |
  --   o---(b1.BR)-----'         o-----(b4.B)---->o
  --
  toTopoGeom(
	-- NOTE: it's important that we define
	--       this edge from top to bottom,
	--       as the resulting Border will be
	--       *normalized* to be from bottom to top
    'SRID=4326;LINESTRING(20 10,20 0)',
    'topo',
    1,
    0
  )
),
(
  'b4.R',
  --
  --   ,---(b1.LT)---->o         o-----(b4.T)---->o
  --   |               ^         ^                ^
  --   |  (s1)         |         |                |
  --   |               |         |                |
  --   |  o-(b2.TR)-.  |         |                |
  --   |  |         |  |       (B4.L)           B3.R
  --   |  |  (s2)   |  |         |                |
  --   |  |         v  |         |                |
  --   |  `-(b2.LB)>o  |         |                |
  --   |               |         |                |
  --   o---(b1.BR)-----'         o-----(b4.B)---->o
  --
  toTopoGeom(
	-- NOTE: it's important that we define
	--       this edge from top to bottom,
	--       as the resulting Border will be
	--       *normalized* to be from bottom to top
    'SRID=4326;LINESTRING(30 10,30 0)',
    'topo',
    1,
    0
  )
)
;

INSERT INTO topo.surface (id, tg) VALUES
(
  's3',
  --
  --   ,---(b1.LT)---->o         o-----(b4.T)---->o
  --   |               ^         ^                ^
  --   |  (s1)         |         |                |
  --   |               |         |       s3       |
  --   |  o-(b2.TR)-.  |         |                |
  --   |  |         |  |       (B4.L)           B3.R
  --   |  |  (s2)   |  |         |                |
  --   |  |         v  |         |                |
  --   |  `-(b2.LB)>o  |         |                |
  --   |               |         |                |
  --   o---(b1.BR)-----'         o-----(b4.B)---->o
  --
  toTopoGeom(
    'SRID=4326;POLYGON((20 0,20 10,30 10,30 0,20 0))',
    'topo',
    2,
    0
  )
)
;

SELECT 't4', 'edge', edge_id,
	ST_AsText(geom)
FROM topo.edge
WHERE geom >> 'SRID=4326;POINT(15 0)'
ORDER BY edge_id;

SELECT 't4', 'bord', id,
	ST_AsText(ST_Normalize(ST_CollectionHomogenize(tg)))
FROM topo.live_border
WHERE id LIKE 'b4.%'
ORDER BY id;

SELECT 't4', 'surf', id,
	ST_AsText(ST_Normalize(ST_CollectionHomogenize(tg)))
FROM topo.surface
WHERE id = 's3'
ORDER BY id;

SELECT '--- t4 ---', E'\n' || jsonb_pretty(
	topo_update.surfaces_as_geojson(
		'topo.live_surface',
		'tg',
		'id',
		'{ "id": [ "key" ], "comp.f": [ "n", "p" ] }',

		'topo.border',
		'tg',
		'id',
		'{ "id": [ "k" ], "comp.f": [ "cmp_f" ] }',

		ARRAY['s3'],

		'featureRewriterGeneric'::regproc,
		$$ {
			"*": {
				"merge": { "origin": "test" }
			},
			"B": {
				"merge": { "type": "Border" }
			},
			"S": {
				"strip": [ "geometry" ],
				"merge": { "type": "Surface" }
			}
		} $$::JSONB
	)
);

--------------------------------------------------------------------------
-- t5: Reprojected GeoJSON
--
-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/102
--------------------------------------------------------------------------

INSERT INTO spatial_ref_sys values (
	910001,
	'test',
	1,
	'GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",-1,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]]',
	'+proj=longlat +datum=WGS84 +no_defs +pm=-1'
);

SELECT '--- t5 ---', E'\n' || jsonb_pretty(
	topo_update.surfaces_as_geojson(
		'topo.live_surface',
		'tg',
		'id',
		'{ "id": [ "key" ], "comp.f": [ "n", "p" ] }',

		'topo.border',
		'tg',
		'id',
		'{ "id": [ "k" ], "comp.f": [ "cmp_f" ] }',

		ARRAY['s3'],

		outSRID => 910001
	)
);

--------------------------------------------------------------------------
-- t6: hole touching shell
--
-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/149
--------------------------------------------------------------------------

-- Reset the topology
DELETE FROM topo.border;
DELETE FROM topo.surface;
DELETE FROM topo.relation;
DELETE FROM topo.edge_data;
DELETE FROM topo.node;
DELETE FROM topo.face WHERE face_id > 0;
SELECT NULL FROM setval('topo.edge_data_edge_id_seq', 1, false);
SELECT NULL FROM setval('topo.node_node_id_seq', 1, false);
SELECT NULL FROM setval('topo.face_face_id_seq', 1, false);

--
--           o
--          / \
--         /   \
--        /     \
--       /       \
--      / .-BH--. \
--     /  \     /  \
--    /    \   /    \
--   /      \ /      \
--   '--BL---o---BR--'
--
INSERT INTO topo.border (id, tg) VALUES
(
  'BL', toTopoGeom(
    'SRID=4326;LINESTRING(10 0,0 0,10 10)',
    'topo', 1, 0)
),
(
  'BR', toTopoGeom(
    'SRID=4326;LINESTRING(10 0,20 0,10 10)',
    'topo', 1, 0)
),
(
  'BH',
  toTopoGeom(
    'SRID=4326;LINESTRING(10 0,8 2,12 2,10 0)',
    'topo', 1, 0)
)
;

INSERT INTO topo.surface (id, tg) SELECT
	'S',
	CreateTopoGeom(
		'topo', 3, 2,
		TopoElementArray_agg(ARRAY[x,3])
	) FROM GetFaceByPoint('topo', 'SRID=4326;POINT(1 1)', 0) x
;

SELECT '--- t6 ---', E'\n' || jsonb_pretty(
	topo_update.surfaces_as_geojson(
		'topo.live_surface',
		'tg',
		'id',
		'{ "id": [ "key" ], "comp.f": [ "n", "p" ] }',

		'topo.border',
		'tg',
		'id',
		'{ "id": [ "k" ], "comp.f": [ "cmp_f" ] }',

		ARRAY['S']
	)
);

--------------------------------------------------------------------------
-- t7: drop duplicated vertices upon precision reduction
--
-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/150
--------------------------------------------------------------------------

-- Reset the topology
DELETE FROM topo.border;
DELETE FROM topo.surface;
DELETE FROM topo.relation;
DELETE FROM topo.edge_data;
DELETE FROM topo.node;
DELETE FROM topo.face WHERE face_id > 0;
SELECT NULL FROM setval('topo.edge_data_edge_id_seq', 1, false);
SELECT NULL FROM setval('topo.node_node_id_seq', 1, false);
SELECT NULL FROM setval('topo.face_face_id_seq', 1, false);


INSERT INTO topo.border (id, tg) VALUES
(
  'B', toTopoGeom(
    'SRID=4326;LINESTRING(0 0,0.001 0,9.999 0,10 0,5 5,0 0)',
    'topo', 1, 0)
)
;

INSERT INTO topo.surface (id, tg) SELECT
	'S',
	CreateTopoGeom(
		'topo', 3, 2,
		TopoElementArray_agg(ARRAY[x,3])
	) FROM GetFaceByPoint('topo', 'SRID=4326;POINT(5 1)', 0) x
;

SELECT '--- t7 ---', E'\n' || jsonb_pretty(
	topo_update.surfaces_as_geojson(
		'topo.live_surface', 'tg', 'id',
		'{ "id": [ "key" ], "comp.f": [ "n", "p" ] }',

		'topo.border', 'tg', 'id',
		'{ "id": [ "k" ], "comp.f": [ "cmp_f" ] }',

		ARRAY['S'],

		maxdecimaldigits => 2
	)
);


ROLLBACK;
