SET client_min_messages TO WARNING;

------------------------------------------------------------
-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
-------------------------------------------------------------

\i :regdir/../../src/sql/topo_update/function_03_app_CreateSchema.sql
\i :regdir/../../src/sql/topo_update/function_03_app_do_RemovePath.sql

-------------------------------------------
-- Utility schema (data and functions)
-------------------------------------------

\i :regdir/utils/app_test_utils.sql

-------------------------------------
-- Create the app schema
-------------------------------------

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"topology": {
			"srid": "4326"
		},
		"tables": [
			{
				"name": "path",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" },
					{ "name": "l", "type": "text", "default": "path" }
				],
				"topogeom_columns": [
					{ "name": "g", "type": "lineal" }
				]
			}
		],
		"path_layer": {
			"table_name": "path",
			"geo_column": "g"
		},
		"operations": {
			"RemovePath": {
			}
		}

	}'
);

-- Create paths

INSERT INTO test.path(l,g)
SELECT
	'p' || y,
	topology.toTopoGeom(
		ST_SetSRID(ST_MakeLine(ST_Point(0, y), ST_Point(10, y)), 4326),
		'test_sysdata_webclient',
		(
			SELECT l.layer_id
			FROM topology.topology t
				JOIN topology.layer l ON (l.topology_id = t.id)
			WHERE l.table_name = 'path'
		)
	)
FROM generate_series(0, 1) y;

SELECT 'start', ST_Extent(geom) FROM test_sysdata_webclient.edge;

------------------------------------------------------
-- Remove path P1
------------------------------------------------------

SELECT topo_update.app_do_RemovePath(
	'test',
	'2'
);
SELECT 'after t1', ST_Extent(geom) FROM test_sysdata_webclient.edge;

------------------------------------------------------
-- Attempt to remove non-existent path
------------------------------------------------------

SELECT topo_update.app_do_RemovePath(
	'test',
	'non-existent'
);

-----------
-- Cleanup
-----------

SELECT * FROM util.drop_generated_schema('test');
DROP SCHEMA util CASCADE;
