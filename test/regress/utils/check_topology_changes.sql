-- Check changes since last saving, save more
-- {
CREATE OR REPLACE FUNCTION check_topology_changes(toponame text)
RETURNS TABLE (o text)
AS $BODY$
DECLARE
	is_first_call BOOLEAN := false;
	rec RECORD;
	sql text;
BEGIN

	IF NOT EXISTS ( SELECT * FROM pg_namespace n, pg_class c WHERE n.nspname = toponame AND c.relnamespace = n.oid and c.relname = 'node_prev' )
	THEN
		is_first_call := true;
		sql := format(
			$$
				CREATE TABLE %1$I.node_prev AS SELECT * FROM %1$I.node;
				CREATE TABLE %1$I.edge_prev AS SELECT edge_id, geom FROM %1$I.edge;
				CREATE TABLE %1$I.face_prev AS SELECT * FROM %1$I.face;
			$$,
			toponame
		);
		RAISE DEBUG '%', sql;
		EXECUTE sql;
	END IF;

	-- Report added nodes
	sql :=  format(
		$$
			SELECT
				'N|' ||
				COALESCE(n.containing_face::text,'') || '|' ||
				ST_AsEWKT(ST_SnapToGrid(n.geom, 1e-2))::text as xx
			FROM
				%1$I.node n
			WHERE n.node_id NOT IN (
				SELECT node_id FROM %1$I.node_prev
			)
			ORDER BY n.geom
		$$,
		toponame
	);
	RAISE DEBUG '%', sql;

	FOR rec IN EXECUTE sql
	LOOP
		o := rec.xx;
		--RAISE WARNING 'o: %', o;
		RETURN NEXT;
	END LOOP;

	-- Report deleted nodes
	sql :=  format(
		$$
			SELECT
				'-N|' ||
				COALESCE(n.containing_face::text,'') || '|' ||
				ST_AsEWKT(ST_SnapToGrid(n.geom, 1e-2))::text as xx
			FROM
				%1$I.node_prev n
			WHERE n.node_id NOT IN (
				SELECT node_id FROM %1$I.node
			)
			ORDER BY n.geom
		$$,
		toponame
	);
	RAISE DEBUG '%', sql;

	FOR rec IN EXECUTE sql
	LOOP
		o := rec.xx;
		--RAISE WARNING 'o: %', o;
		RETURN NEXT;
	END LOOP;

	-- Report added or modified edges
	sql := format(
		$$
			SELECT
				'E|' ||
				ST_AsEWKT(ST_SnapToGrid(geom, 1e-2))::text AS xx
			FROM (
				SELECT e.edge_id, e.geom
				FROM %1$I.edge e
					EXCEPT
				SELECT oe.edge_id, oe.geom
				FROM %1$I.edge_prev oe
			) new_edges
			ORDER BY geom::geometry
		$$,
		toponame
	);
	RAISE DEBUG 'SQL: %', sql;
	FOR rec IN EXECUTE sql
	LOOP
		o := rec.xx;
		RETURN NEXT;
	END LOOP;

	-- Report removed edges
	sql := format(
		$$
			SELECT
				'-E|' ||
				ST_AsEWKT(ST_SnapToGrid(geom, 1e-2))::text AS xx
			FROM (
				SELECT e.edge_id, e.geom
				FROM %1$I.edge_prev e
					EXCEPT
				SELECT oe.edge_id, oe.geom
				FROM %1$I.edge oe
			) new_edges
			ORDER BY geom::geometry
		$$,
		toponame
	);
	RAISE DEBUG 'SQL: %', sql;
	FOR rec IN EXECUTE sql
	LOOP
		o := rec.xx;
		RETURN NEXT;
	END LOOP;

	IF NOT is_first_call THEN
		sql := format(
			$$
				TRUNCATE %1$I.node_prev;
				INSERT INTO %1$I.node_prev SELECT * FROM %1$I.node;
				TRUNCATE %1$I.edge_prev;
				INSERT INTO %1$I.edge_prev SELECT edge_id, geom FROM %1$I.edge;
				TRUNCATE %1$I.face_prev;
				INSERT INTO %1$I.face_prev SELECT * FROM %1$I.face;
			$$,
			toponame
		);
		RAISE DEBUG 'SQL: %', sql;
		EXECUTE sql;
	END IF;

END;
$BODY$ LANGUAGE 'plpgsql'
VOLATILE;
-- }

