\i :regdir/utils/normalized_geom.sql

-- Print all topology elements
-- {
CREATE OR REPLACE FUNCTION check_topology(toponame text, report_invalid boolean default false)
RETURNS TABLE (o text)
AS $BODY$
DECLARE
	is_first_call BOOLEAN := false;
	rec RECORD;
	sql text;
	old_cmm TEXT; -- old client_min_messages
BEGIN

	-- Report all nodes
	sql :=  format(
		$$
			SELECT
				'N|' ||
				COALESCE(n.containing_face::text,'') || '|' ||
				ST_AsEWKT(ST_SnapToGrid(n.geom, 1e-2))::text as xx
			FROM
				%1$I.node n
			ORDER BY n.geom
		$$,
		toponame
	);
	--RAISE DEBUG '%', sql;

	FOR rec IN EXECUTE sql
	LOOP
		o := rec.xx;
		--RAISE WARNING 'o: %', o;
		RETURN NEXT;
	END LOOP;

	-- Report all edges
	sql := format(
		$$
			SELECT
				'E|' ||
				ST_AsEWKT(ST_SnapToGrid(geom, 1e-2))::text AS xx
			FROM %1$I.edge e
			ORDER BY geom
		$$,
		toponame
	);
	--RAISE DEBUG 'SQL: %', sql;
	FOR rec IN EXECUTE sql
	LOOP
		o := rec.xx;
		RETURN NEXT;
	END LOOP;

	-- Report all faces
	sql := format(
		$$
			SELECT
				'F|' ||
				ST_AsEWKT(ST_SnapToGrid(normalized_geom(ST_GetFaceGeometry(%1$L, face_id)), 1e-2))::text AS xx
			FROM %1$I.face
			WHERE face_id > 0
			ORDER BY mbr, face_id
		$$,
		toponame
	);
	--RAISE DEBUG 'SQL: %', sql;
	FOR rec IN EXECUTE sql
	LOOP
		o := rec.xx;
		RETURN NEXT;
	END LOOP;

	-- Report invalidities if requested
	IF report_invalid THEN

		-- Hush notices from ValidateTopology
		SELECT setting FROM pg_settings WHERE name = 'client_min_messages'
		INTO old_cmm;
		SET client_min_messages TO WARNING;

		FOR rec in SELECT * FROM topology.ValidateTopology(toponame)
		LOOP
			o := format('%s: id1:%s, id2:%s', rec.error, rec.id1, rec.id2);
			RETURN NEXT;
		END LOOP;

		-- Check universal face MBR
		-- This check is missing from PostGIS as of version 3.3.0
		-- See https://trac.osgeo.org/postgis/ticket/5182
		sql := format(
			$$
				SELECT mbr FROM %1$I.face
				WHERE face_id = 0 AND mbr IS NOT NULL
			$$,
			toponame
		);
		FOR rec IN EXECUTE sql
		LOOP
			o := format('Universal face has not-null MBR');
			RETURN NEXT;
		END LOOP;


		-- Restore client_min_messages
		EXECUTE format('SET client_min_messages TO %s', old_cmm);
	END IF;

END;
$BODY$ LANGUAGE 'plpgsql'
VOLATILE;
-- }

