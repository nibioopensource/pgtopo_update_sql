-- {
CREATE OR REPLACE FUNCTION normalized_geom(
	INOUT geom geometry
)
RETURNS geometry
AS $BODY$
DECLARE
	rec RECORD;
	i INT;
	comp GEOMETRY;
BEGIN
	geom := ST_Normalize(geom);
	WITH comps AS (
		SELECT * FROM ST_Dump(geom)
	)
	SELECT ST_Collect(
		CASE
		WHEN ST_Dimension(c.geom) = 1
			 AND ST_IsClosed(c.geom)
		THEN
			-- Workaround to PostGIS issue with behavioural
			-- change depending on GEOS versions
			-- https://trac.osgeo.org/postgis/ticket/4886
			ST_Boundary(
				ST_Normalize(
					ST_MakePolygon(c.geom)
				)
			)
		ELSE
			ST_Normalize(
				c.geom
			)
		END
	)
	FROM comps c
	INTO geom;
	geom := ST_CollectionHomogenize(geom);
END;
$BODY$ LANGUAGE 'plpgsql';


