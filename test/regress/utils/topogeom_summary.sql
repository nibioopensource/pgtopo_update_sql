CREATE OR REPLACE FUNCTION topogeom_summary(tg TopoGeometry)
RETURNS TEXT
AS $BODY$ --{
DECLARE
	ret TEXT;
	typ INT;
	topology topology.topology;
	layer topology.layer;
BEGIN
	-- Fetch topology
	SELECT *
	FROM topology.topology
	WHERE id = topology_id(tg)
	INTO topology;

	-- Fetch layer
	IF topology IS NOT NULL THEN
		SELECT *
		FROM topology.layer
		WHERE topology_id = topology_id(tg)
		AND layer_id = layer_id(tg)
		INTO layer;
	END IF;

	-- Fetch layer info

	-- Get TopoGeometry type
	typ := type(tg);
	IF typ = 1 THEN
		ret := 'Puntual';
	ELSIF typ = 2 THEN
		ret := 'Linear';
	ELSIF typ = 3 THEN
		ret := 'Areal';
	ELSIF typ = 4 THEN
		ret := 'Collection';
	ELSE
		RAISE EXCEPTION 'Unknown TopoGeometry type %', typ;
	END IF;

	ret := ret || ' TopoGeom in ';

	-- simple or hierarchical layer ?
	IF layer.layer_id IS NULL THEN
		ret := ret || 'unknown';
	ELSIF layer.child_id IS NULL THEN
		ret := ret || 'simple';
	ELSIF layer.child_id IS NULL THEN
		ret := ret || 'hierarchical';
	END IF;

	ret := ret || ' layer of ';

	-- what topology ?
	IF topology IS NULL THEN
		ret := ret || 'unknown topology';
	ELSE
		ret := ret || format('topology %I', topology.name);
	END IF;

	IF layer.layer_id IS NOT NULL THEN
		ret := ret || format(
			' (%I.%I.%I)',
			layer.schema_name,
			layer.table_name,
			layer.feature_column
		);
	END IF;

	RETURN ret;

END;
$BODY$
LANGUAGE 'plpgsql'
VOLATILE
; --}
