set client_min_messages to WARNING;

-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
\i :regdir/../../src/sql/topo_update/function_02_find_dangling_borders.sql

\i :regdir/utils/check_topology_changes.sql
\i :regdir/utils/check_layer_features.sql

-------------------------------------
-- Create a topology 'topo'
-------------------------------------

SELECT NULL FROM CreateTopology('topo', 4326);

-------------------------------------
-- Create Surface and Border layers
-------------------------------------

CREATE TABLE topo.surface (
	k SERIAL PRIMARY KEY,
	lbl TEXT
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo', 'surface', 'g',
	'POLYGON'
);

CREATE TABLE topo.border (
	k SERIAL PRIMARY KEY,
	lbl TEXT
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo', 'border', 'g',
	'LINE'
);

--
-- Create a topology like the following:
--
--     ,------>o<------.
--     |       |       |
--     |       |   F2  |
--     |       E4      |
--     |       |       |
--     |       v       |
--     |       o       |
--     |       |       |
--     E1      E5      E2
--     |       |       |
--     |       v       |
--     |       o       |
--     |   F1  ^       |
--     |       |       |
--     |       E3      |
--     |       |       |
--     `-------o-------'


-- E1
SELECT 'E' || TopoGeo_addLineString(
	'topo',
	'SRID=4326;LINESTRING(5 0,0 0,0 10,5 10)',
	0
);

-- E2
SELECT 'E' || TopoGeo_addLineString(
	'topo',
	'SRID=4326;LINESTRING(5 0,10 0,10 10,5 10)',
	0
);

-- E3
SELECT 'E' || TopoGeo_addLineString(
	'topo',
	'SRID=4326;LINESTRING(5 0,5 2)',
	0
);

-- E4
SELECT 'E' || TopoGeo_addLineString(
	'topo',
	'SRID=4326;LINESTRING(5 0,5 -2)',
	0
);

-- E5
SELECT 'E' || TopoGeo_addLineString(
	'topo',
	'SRID=4326;LINESTRING(5 10,5 2)',
	0
);

--
-- Define Border/Surface like the following:
--
--     ,------->o<-------.
--     |        |        |
--     |        |  (S1)  |
--     |        |   F2   |
--     |        |        |
--     |        E5       |
--     |        |        |
--     |        |        |
--    (B1)      |       (B2)
--     |        |        |
--     |        |        |
--     E1       |        E2
--     |        |        |
--     |        v        |
--     |  (S1)  o        |
--     |   F1   ^        |
--     |        |        |
--     |       (B3)      |
--     |        E3       |
--     |        |        |
--     `--------o--------'
--              |
--              E4
--              |
--              v


-- S1 (F1+F2)
INSERT INTO topo.surface (lbl, g)
SELECT 'S1', topology.CreateTopoGeom(
	'topo', 3, 1,
	'{{1,3},{2,3}}'
) RETURNING 'S'||k;

-- B1 (E1)
INSERT INTO topo.border (lbl, g)
SELECT 'B1', topology.CreateTopoGeom(
	'topo', 2, 2,
	'{{1,2}}'
) RETURNING 'B'||k;

-- B2 (E2)
INSERT INTO topo.border (lbl, g)
SELECT 'B2', topology.CreateTopoGeom(
	'topo', 2, 2,
	'{{2,2}}'
) RETURNING 'B'||k;

-- B3 (E3) -- dangling
INSERT INTO topo.border (lbl, g)
SELECT 'B3', topology.CreateTopoGeom(
	'topo', 2, 2,
	'{{3,2}}'
) RETURNING 'B'||k;

-- B4 (E4) -- dangling
INSERT INTO topo.border (lbl, g)
SELECT 'B5', topology.CreateTopoGeom(
	'topo', 2, 2,
	'{{4,2}}'
) RETURNING 'B'||k;

--
-- t1: find dangling borders in area where there are none
--

SELECT 't1', count(*) FROM topo_update.find_dangling_borders(
	'topo.border', 'g', 'k', -- border layer identifier
	'topo.surface', 'g', -- surface layer
	ST_SetSRID(ST_MakeEnvelope(100,100,150,150), 4326)
);


--
-- t2: find dangling borders in area where there's a single one
--
SELECT 't2', * FROM topo_update.find_dangling_borders(
	'topo.border', 'g', 'k', -- border layer identifier
	'topo.surface', 'g', -- surface layer identifier
	ST_SetSRID(ST_MakeEnvelope(0,-1,10,-2), 4326)
) ORDER BY 2;

--
-- t3: find dangling borders in all topology
--
SELECT 't3', * FROM topo_update.find_dangling_borders(
	'topo.border', 'g', 'k', -- border layer identifier
	'topo.surface', 'g' -- surface layer identifier
) ORDER BY 2;


-----------
-- Cleanup
-----------

SELECT NULL FROM DropTopology('topo');
DROP FUNCTION check_topology_changes(text);
DROP FUNCTION check_layer_features(regclass, name, bool);

