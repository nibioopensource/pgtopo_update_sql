BEGIN;

set client_min_messages to WARNING;

\i :regdir/utils/check_topology_changes.sql
\i :regdir/utils/check_layer_features.sql
\i :regdir/utils/topogeom_summary.sql

-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
\i :regdir/../../src/sql/topo_update/function_02_update_feature_attribute.sql

---------------------
-- Initialize schema
---------------------

SELECT NULL FROM CreateTopology('topo', 4326);

CREATE TABLE topo.poi (
    id INT PRIMARY KEY,
	lbl TEXT
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo',
	'poi',
	'tg',
	'POINT');

INSERT INTO topo.poi (id, lbl, tg) VALUES (
	1,
	'orig',
	toTopoGeom('SRID=4326;POINT(0 0)', 'topo', 1, 0)
);

-- Start of operations
SELECT 'start', 'topo', * FROM check_topology_changes('topo');
SELECT 'start', 'poi', *
FROM check_layer_features('topo.poi'::regclass, 'tg'::name, true);

--------------------------------------
-- upd1: Update with a GeoJSON feature
--------------------------------------

SELECT 'upd1', topogeom_summary(topogeom)
FROM topo_update.update_feature(
	$${
		"geometry": { "coordinates": [ 1,0 ], "type": "Point" },
		"properties": { "id": "1", "lbl": "new" },
		"type": "Feature"
	}$$, -- feature
	4326, -- srid
    'topo.poi', -- layerTable
    'tg', -- layerGeomColumn
    'id', -- layerIdColumn
    '1', -- id
	'{ "id": [ "id" ], "lbl": [ "lbl" ] }', -- layerColMap
	0 -- tolerance
);

SELECT 'upd1', 'topo', * FROM check_topology_changes('topo');
SELECT 'upd1', 'poi', *
FROM check_layer_features('topo.poi'::regclass, 'tg'::name, true);

------------------------------------
-- upd2: update an existing feature
------------------------------------

SELECT 'upd2', topo_update.update_feature_attribute(
	$${
		"properties": { "id_in_json": "1", "lbl": "bygeom" },
		"type": "Feature"
	}$$, -- feature
    'topo.poi', -- layerTable
    'id', -- layerIdColumn
	'{ "id": [ "id_in_json" ], "lbl": [ "lbl" ] }' -- layerColMap
);

SELECT 'upd2', 'topo', * FROM check_topology_changes('topo');
SELECT 'upd2', 'poi', *
FROM check_layer_features('topo.poi'::regclass, 'tg'::name, true);

------------------------------------------------------
-- upd3_unknown: attempt to update an unknown feature
------------------------------------------------------

SELECT 'upd3_unknown', topo_update.update_feature_attribute(
	$${
		"properties": { "id_in_json": "10", "lbl": "bygeom" },
		"type": "Feature"
	}$$, -- feature
    'topo.poi', -- layerTable
    'id', -- layerIdColumn
	'{ "id": [ "id_in_json" ], "lbl": [ "lbl" ] }' -- layerColMap
);

ROLLBACK;

