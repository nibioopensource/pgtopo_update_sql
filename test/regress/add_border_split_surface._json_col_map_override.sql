BEGIN;

SET client_min_messages TO WARNING;

\i :regdir/../../src/sql/topo_update/function_02_add_border_split_surface.sql


SELECT 't1', topo_update._json_col_map_override(
	ARRAY[
		'{"a": ["m1", "a"], "b": ["m1", "b"]}'::jsonb,
		'{"a": ["m2", "a"]}'
	]
);
