SET client_min_messages TO WARNING;
SET extra_float_digits TO -3;

------------------------------------------------------------
-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
-------------------------------------------------------------

\i :regdir/../../src/sql/topo_update/function_03_app_CreateSchema.sql
\i :regdir/../../src/sql/topo_update/function_03_app_do_AddPoint.sql
\i :regdir/../../src/sql/topo_update/function_02_insert_feature.sql

-------------------------------------------
-- Utility schema (data and functions)
-------------------------------------------

\i :regdir/utils/app_test_utils.sql

-------------------------------------------
-- Save minimal app schema into a table
-------------------------------------------

CREATE TABLE util.minimal_app_schema AS
SELECT $$
	{
		"version": "1.0",
		"topology": {
			"srid": 4326
		},
		"tables": [
			{
				"name": "point",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" }
				],
				"topogeom_columns": [
					{
						"name": "g",
						"type": "puntal"
					}
				]
			}
		],
		"point_layer": {
			"table_name": "point",
			"geo_column": "g"
		},
		"operations": {
			"AddPoint": { }
		}
	}
$$::jsonb AS cfg;

SELECT * FROM topo_update.app_CreateSchema( 'test',
		( SELECT cfg FROM util.minimal_app_schema )
);

-----------------------------------------------------------
-- t1: Test AddPoint
-----------------------------------------------------------


SELECT 't1', 'add', * FROM topo_update.app_do_AddPoint('test',
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : { "name" : "EPSG:4326" },
				"type" : "name"
			},
			"coordinates" : [ 0, 0 ],
			"type" : "Point"
		},
		"type" : "Feature",
		"properties" : { }
	}
	$$
);
SELECT 't1', 'point', k, ST_AsEWKT(g) FROM test.point ORDER BY k;


-----------------------------------------------------------
-- t2: Test AddPoint over an existing point
-----------------------------------------------------------

SELECT 't2', 'add', * FROM topo_update.app_do_AddPoint('test',
	$$
	{
		"geometry" : {
			"crs" : {
				"properties" : { "name" : "EPSG:4326" },
				"type" : "name"
			},
			"coordinates" : [ 0, 0 ],
			"type" : "Point"
		},
		"type" : "Feature",
		"properties" : { }
	}
	$$
);
SELECT 't2', 'point', k, ST_AsEWKT(g) FROM test.point ORDER BY k;



-------------------------------------------
-- Cleanup
-------------------------------------------

SELECT * FROM util.drop_generated_schema('test');

SET extra_float_digits TO 0;
DROP SCHEMA util CASCADE;
