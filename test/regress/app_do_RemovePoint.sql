SET client_min_messages TO WARNING;

------------------------------------------------------------
-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
-------------------------------------------------------------

\i :regdir/../../src/sql/topo_update/function_03_app_CreateSchema.sql
\i :regdir/../../src/sql/topo_update/function_03_app_do_RemovePoint.sql

-------------------------------------------
-- Utility schema (data and functions)
-------------------------------------------

\i :regdir/utils/app_test_utils.sql

-------------------------------------
-- Create the app schema
-------------------------------------

SELECT * FROM topo_update.app_CreateSchema(
	'test', '{
		"version": "1.0",
		"topology": {
			"srid": "4326"
		},
		"tables": [
			{
				"name": "poi",
				"primary_key": "k",
				"attributes": [
					{ "name": "k", "type": "serial" }
				],
				"topogeom_columns": [
					{ "name": "g", "type": "puntal" }
				]
			}
		],
		"point_layer": {
			"table_name": "poi",
			"geo_column": "g"
		},
		"operations": {
			"RemovePoint": {
			}
		}

	}'
);

-- Create pois

INSERT INTO test.poi(g)
SELECT
	topology.toTopoGeom(
		ST_SetSRID(ST_MakePoint(n, n), 4326),
		'test_sysdata_webclient',
		(
			SELECT l.layer_id
			FROM topology.topology t
				JOIN topology.layer l ON (l.topology_id = t.id)
			WHERE l.table_name = 'poi'
		)
	)
FROM generate_series(0, 1) n;

SELECT 'start', ST_Extent(geom) FROM test_sysdata_webclient.node;

------------------------------------------------------
-- Remove point
------------------------------------------------------

SELECT topo_update.app_do_RemovePoint(
	'test',
	'2'
);
SELECT 'after t1', ST_Extent(geom) FROM test_sysdata_webclient.node;

------------------------------------------------------
-- Attempt to remove non-existent point
------------------------------------------------------

SELECT topo_update.app_do_RemovePoint(
	'test',
	'non-existent'
);

-----------
-- Cleanup
-----------

SELECT * FROM util.drop_generated_schema('test');
DROP SCHEMA util CASCADE;
