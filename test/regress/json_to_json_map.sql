
BEGIN;

\i :regdir/../../src/sql/topo_update/function_01_json_to_json_map.sql

set client_min_messages to WARNING;

-- from-path-not-found
DO LANGUAGE 'plpgsql' $$ BEGIN
	PERFORM topo_update.json_to_json_map(
		'{ }',
		'[ { "to": [ "x" ], "from": [ "non-exitent" ] } ]'
	);
	RAISE WARNING 'No exception thrown on from-path-not-found';
EXCEPTION WHEN OTHERS THEN RAISE WARNING 'THROWN: %', SQLERRM; END; $$;

-- map-not-array
DO LANGUAGE 'plpgsql' $$ BEGIN
	PERFORM topo_update.json_to_json_map(
		'{ }',
		'{ }'
	);
	RAISE WARNING 'No exception thrown on map-not-array';
EXCEPTION WHEN OTHERS THEN RAISE WARNING 'THROWN: %', SQLERRM; END; $$;

-- target exits both as object and non-object
DO LANGUAGE 'plpgsql' $$ BEGIN
	PERFORM topo_update.json_to_json_map(
		'{ "a": 1}',
		'[
			{ "to": [ "x" ],   "from": [ "a" ] },
			{ "to": [ "x", "y" ], "from": [ "a" ] }
		 ]'
	);
	RAISE WARNING 'No exception thrown on target-both-object-and-simple';
EXCEPTION WHEN OTHERS THEN RAISE WARNING 'THROWN: %', SQLERRM; END; $$;

SELECT 'to_flat', topo_update.json_to_json_map(
	'{
		"simp": 1,
		"comp": {
			"item": "c1",
			"nest": {
				"item": "sc1"
			}
		}
	}',
	'[
	  { "to": [ "from_simp" ], "from": [ "simp" ] },
	  { "to": [ "from_comp" ], "from": [ "comp", "item" ] },
	  { "to": [ "from_nest" ], "from": [ "comp", "nest", "item" ] }
	]'
);

SELECT 'to_nested', topo_update.json_to_json_map(
	'{
		"simp": 1,
		"comp": {
			"item": "c1",
			"nest": {
				"item": "sc1"
			}
		}
	}',
	'[
	  { "to": [ "simp" ], "from": [ "comp", "nest", "item" ] },
	  { "to": [ "comp", "nest", "item1" ], "from": [ "simp" ] },
	  { "to": [ "comp", "nest", "item2" ], "from": [ "comp", "item" ] }
	]'
);

