BEGIN;

set client_min_messages to WARNING;

\i :regdir/utils/check_topology_changes.sql
\i :regdir/utils/check_layer_features.sql
\i :regdir/utils/reset_topology.sql

-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
\i :regdir/../../src/sql/topo_update/function_02_update_features.sql
\i :regdir/../../src/sql/topo_update/function_02_upsert_feature.sql
\i :regdir/../../src/sql/topo_update/function_02_update_feature.sql
\i :regdir/../../src/sql/topo_update/function_02_insert_feature.sql
\i :regdir/../../src/sql/topo_update/function_02_cleanup_topology.sql

SET search_path TO public;

CREATE SCHEMA update_features_test;

-- Create resulting feature validator
CREATE FUNCTION feature_validator(
	role CHAR,
	fid TEXT,
	geom GEOMETRY,
	tg topology.TopoGeometry,
	tol FLOAT8
)
RETURNS VOID
AS $BODY$ --{
DECLARE
	resulting_geom GEOMETRY;
	hdist FLOAT8;
BEGIN
	IF role != 'S'
	THEN
		RETURN;
	END IF;

	resulting_geom := ST_CollectionHomogenize(
		tg::geometry
	);
	IF GeometryType(resulting_geom) != GeometryType(geom)
	THEN
		RAISE EXCEPTION
			'Input Surface % around % changed type from % to %'
			' (too much snapping ?)',
			fid,
			ST_AsEWKT(
				ST_PointOnSurface(
					ST_SymDifference(
						geom,
						resulting_geom
					)
				)
			),
			GeometryType(geom),
			GeometryType(resulting_geom)
		;
	END IF;

	hdist := ST_HausdorffDistance(geom, resulting_geom);
	IF hdist > tol
	THEN
		RAISE EXCEPTION
			'Input Surface % around % drifted % units more than tolerated'
			' (too much snapping ?)',
			fid,
			ST_AsEWKT(
				ST_PointOnSurface(
					ST_SymDifference(
						geom,
						resulting_geom
					)
				)
			),
			hdist - tol
		;
	END IF;

END;
$BODY$ LANGUAGE 'plpgsql';

SELECT NULL FROM topology.CreateTopology('update_features_test_topo', 4326);


CREATE TABLE update_features_test.composite (
  a int,
  b text
);

-- This is like topo_ar5.webclient_flate but has an UUID field
CREATE TABLE update_features_test.areal (
    "id" TEXT PRIMARY KEY,
    "composite" update_features_test.composite
);
SELECT NULL FROM topology.AddTopoGeometryColumn(
	'update_features_test_topo',
	'update_features_test',
	'areal',
	'tg',
	'AREAL'
);

CREATE TABLE update_features_test.lineal (
    "id" TEXT PRIMARY KEY,
    "mixedCased" int
);
SELECT NULL FROM topology.AddTopoGeometryColumn(
	'update_features_test_topo',
	'update_features_test',
	'lineal',
	'tg',
	'LINEAL'
);


-- Create mapping table
CREATE TABLE update_features_test.json_mapping(
	target_table regclass PRIMARY KEY,
	mapping_from_ngis jsonb,
	mapping_from_webclient jsonb
);
INSERT INTO update_features_test.json_mapping (target_table, mapping_from_ngis)
VALUES (
	'update_features_test.areal',
	'{
      "id": [ "identification", "id" ],
      "composite.a": [ "Obj1", "Elm1" ],
      "composite.b": [ "Obj2", "Elm2" ]
	}'
),(
	'update_features_test.lineal',
	'{
      "id": [ "identification", "id" ],
      "mixedCased": [ "mixedCased" ]
	}'
);

-- Create input json table
CREATE TABLE update_features_test.json_input(
	id serial PRIMARY KEY,
	label text UNIQUE,
	source text,
	payload jsonb
);

INSERT INTO update_features_test.json_input(label, source, payload) VALUES
(
	'empty',
	'ngis',
	'{
    "crs": {
        "properties": { "name": "EPSG:4326" },
        "type": "name"
    },
    "features": [],
    "type": "FeatureCollection"
	}'
),
(
	'test1',
	'ngis',
	'{
    "crs": {
        "properties": {
            "name": "EPSG:4326"
        },
        "type": "name"
    },
    "features": [
        {
            "geometry": {
                "coordinates": [
                    [ 0,0 ],
                    [ 10,0 ],
                    [ 10,10 ],
                    [ 0,0 ]
                ],
                "type": "LineString"
            },
            "properties": {
                "identification": {
                    "id": "092688bb-c6ef-43c9-b6f7-41b2f065537c",
                    "forgetme": "2019-11-11 11:57:47.371000"
                },
                "mixedCased": 1
            },
            "type": "Feature"
        },
        {
            "geometry": {
                "coordinates": [
                  [
                    [ 0,0 ],
                    [ 10,0 ],
                    [ 10,10 ],
                    [ 0,0 ]
                  ]
                ],
                "type": "Polygon"
            },
            "geometry_properties": {
                "exterior": [
                    "092688bb-c6ef-43c9-b6f7-41b2f065537c"
                ],
                "position": [ 0, 0 ]
            },
            "properties": {
                "identification": {
                    "id": "0008e6eb-1332-4145-9943-591020916480",
                    "forgetme": "2019-11-11 11:57:47.371000"
                },
                "Obj1": {
                    "Elm1": "82"
                },
                "Obj2": {
                    "Elm2": "12"
                }
            },
            "type": "Feature"
        }
    ],
    "type": "FeatureCollection"
	}'
),
(
	'test1shift',
	'ngis',
	'{
    "crs": {
        "properties": {
            "name": "EPSG:4326"
        },
        "type": "name"
    },
    "features": [
        {
            "geometry": {
                "coordinates": [
                    [ 5,0 ],
                    [ 15,0 ],
                    [ 15,10 ],
                    [ 5,0 ]
                ],
                "type": "LineString"
            },
            "properties": {
                "identification": {
                    "id": "00000000-0000-0000-0000-000000000001",
                    "forgetme": "2019-11-11 11:57:47.371000"
                },
                "mixedCased": 1
            },
            "type": "Feature"
        },
        {
            "geometry": {
                "coordinates": [
                  [
                    [ 5,0 ],
                    [ 15,0 ],
                    [ 15,10 ],
                    [ 5,0 ]
                  ]
                ],
                "type": "Polygon"
            },
            "geometry_properties": {
                "exterior": [
                    "00000000-0000-0000-0000-000000000001"
                ],
                "position": [ 0, 0 ]
            },
            "properties": {
                "identification": {
                    "id": "00000000-0000-0000-0000-000000000002",
                    "forgetme": "2019-11-11 11:57:47.371000"
                },
                "Obj1": {
                    "Elm1": "82"
                },
                "Obj2": {
                    "Elm2": "12"
                }
            },
            "type": "Feature"
        }
    ],
    "type": "FeatureCollection"
	}'
),
(
	'surface_by_exterior_1',
	'ngis',
	'{
    "crs": {
        "properties": { "name": "EPSG:4326" },
        "type": "name"
    },
    "features": [
        {
            "geometry": {
                "coordinates": [ [ 0,0 ], [ 10,0 ], [ 10,10 ] ],
                "type": "LineString"
            },
            "properties": {
                "identification": { "id": "brd1" },
                "mixedCased": 10
            },
            "type": "Feature"
        },
        {
            "geometry": {
                "coordinates": [ [ 0,0 ], [ 0,10 ], [ 10,10 ] ],
                "type": "LineString"
            },
            "properties": {
                "identification": { "id": "brd2" },
                "mixedCased": 11
            },
            "type": "Feature"
        },
        {
            "geometry": { "type": "Polygon" },
            "geometry_properties": {
                "exterior": [ "brd1", "-brd2" ]
            },
            "properties": {
                "identification": { "id": "srf1" },
                "Obj1": { "Elm1": "82" },
                "Obj2": { "Elm2": "12" }
            },
            "type": "Feature"
        }
    ],
    "type": "FeatureCollection"
	}'
)
,
(
	'surface_by_exterior_and_interior_1',
	'ngis',
	'{
    "crs": {
        "properties": { "name": "EPSG:4326" },
        "type": "name"
    },
    "features": [
        {
            "geometry": {
                "coordinates": [ [ 0,0 ], [ 10,0 ], [ 10,10 ] ],
                "type": "LineString"
            },
            "properties": {
                "identification": { "id": "brd1" },
                "mixedCased": 20
            },
            "type": "Feature"
        },
        {
            "geometry": {
                "coordinates": [ [ 0,0 ], [ 0,10 ], [ 10,10 ] ],
                "type": "LineString"
            },
            "properties": {
                "identification": { "id": "brd2" },
                "mixedCased": 21
            },
            "type": "Feature"
        },
        {
            "geometry": {
                "coordinates": [ [ 2,2 ], [ 2,4 ], [ 4,4 ] ],
                "type": "LineString"
            },
            "properties": {
                "identification": { "id": "brd3" },
                "mixedCased": 22
            },
            "type": "Feature"
        },
        {
            "geometry": {
                "coordinates": [ [ 2,2 ], [ 4,2 ], [ 4,4 ] ],
                "type": "LineString"
            },
            "properties": {
                "identification": { "id": "brd4" },
                "mixedCased": 23
            },
            "type": "Feature"
        },
        {
            "geometry": { "type": "Polygon" },
            "geometry_properties": {
                "exterior": [ "brd1", "-brd2" ],
                "interiors": [ [ "brd3", "-brd4" ] ]
            },
            "properties": {
                "identification": { "id": "srf1" },
                "Obj1": { "Elm1": "82" },
                "Obj2": { "Elm2": "12" }
            },
            "type": "Feature"
        }
    ],
    "type": "FeatureCollection"
	}'
),
(
	'surface_and_its_hole',
	'ngis',
	'{
    "crs": {
        "properties": { "name": "EPSG:4326" },
        "type": "name"
    },
    "features": [
        {
            "geometry": {
                "coordinates": [ [0,0], [10,0], [5,10], [0,0] ],
                "type": "LineString"
            },
            "properties": {
                "id": "brd1"
            },
            "type": "Feature"
        },
        {
            "geometry": {
                "coordinates": [ [1,1], [9,1], [5,9], [1,1] ],
                "type": "LineString"
            },
            "properties": {
                "id": "brd2"
            },
            "type": "Feature"
        },
        {
            "geometry": { "type": "Polygon" },
            "geometry_properties": {
                "exterior": [ "brd1" ],
                "interiors": [ [ "-brd2" ] ]
            },
            "properties": {
                "id": "srf-outer"
            },
            "type": "Feature"
        },
        {
            "geometry": { "type": "Polygon" },
            "geometry_properties": {
                "exterior": [ "brd2" ]
            },
            "properties": {
                "id": "srf-inner"
            },
            "type": "Feature"
        }
    ],
    "type": "FeatureCollection"
	}'
),
(
	'nearby-borders-1',
	'ngis',
	'{
    "crs": {
        "properties": { "name": "EPSG:4326" },
        "type": "name"
    },
    "features": [
        {
            "geometry": {
                "coordinates": [ [0,0], [2,-5], [4,-0.2], [6,-5], [8,0] ],
                "type": "LineString"
            },
            "properties": {
                "id": "brd1"
            },
            "type": "Feature"
        },
        {
            "geometry": {
                "coordinates": [ [0,0], [2,5], [4,0.2], [6,5], [8,0] ],
                "type": "LineString"
            },
            "properties": {
                "id": "brd2"
            },
            "type": "Feature"
        },
        {
            "geometry": { "type": "Polygon" },
            "geometry_properties": {
                "exterior": [ "brd1", "-brd2" ]
            },
            "properties": {
                "id": "srf"
            },
            "type": "Feature"
        }
    ],
    "type": "FeatureCollection"
	}'
),
(
	'nearby-borders-2',
	'ngis',
	'{
    "crs": {
        "properties": { "name": "EPSG:4326" },
        "type": "name"
    },
    "features": [
        {
            "geometry": {
                "coordinates": [ [0,0], [4,-0.2], [6,-5], [8,0] ],
                "type": "LineString"
            },
            "properties": {
                "id": "brd1"
            },
            "type": "Feature"
        },
        {
            "geometry": {
                "coordinates": [ [0,0], [4,0.2], [6,5], [8,0] ],
                "type": "LineString"
            },
            "properties": {
                "id": "brd2"
            },
            "type": "Feature"
        },
        {
            "geometry": { "type": "Polygon" },
            "geometry_properties": {
                "exterior": [ "brd1", "-brd2" ]
            },
            "properties": {
                "id": "srf"
            },
            "type": "Feature"
        }
    ],
    "type": "FeatureCollection"
	}'
),
(
	'reproject1',
	'ngis',
	'{
    "crs": {
        "properties": { "name": "EPSG:3857" },
        "type": "name"
    },
    "features": [
        {
            "geometry": {
                "coordinates": [
					[1113194.9079327357, 1118889.9748579597],
					[3339584.723798207, 1118889.9748579597],
					[2226389.8158654715, 2273030.926987689],
					[1113194.9079327357, 1118889.9748579597]
				],
                "type": "LineString"
            },
            "properties": {
                "id": "brd1"
            },
            "type": "Feature"
        },
        {
            "geometry": { "type": "Polygon" },
            "geometry_properties": {
                "exterior": [ "brd1" ]
            },
            "properties": {
                "id": "srf"
            },
            "type": "Feature"
        }
    ],
    "type": "FeatureCollection"
	}'
)
;


-- Start of operations
SELECT 'start', 'topo', * FROM check_topology_changes('update_features_test_topo');

-- Call the first time, should INSERT
SELECT topo_update.update_features(
	(SELECT payload FROM update_features_test.json_input WHERE label = 'test1'),

	'update_features_test.areal',
	'tg',
	'id',
	(SELECT mapping_from_ngis FROM update_features_test.json_mapping WHERE target_table = 'update_features_test.areal'::regclass),

	'update_features_test.lineal',
	'tg',
	'id',
	(SELECT mapping_from_ngis FROM update_features_test.json_mapping WHERE target_table = 'update_features_test.lineal'::regclass),

  1e-10
);

SELECT 'update_features_1', 'topo', * FROM check_topology_changes('update_features_test_topo');
SELECT 'update_features_1', 'areal', * FROM check_layer_features('update_features_test.areal'::regclass, 'tg'::name);
SELECT 'update_features_1', 'lineal', * FROM check_layer_features('update_features_test.lineal'::regclass, 'tg'::name);

-- Call the first time, should UPDATE
SELECT topo_update.update_features(
	(SELECT payload FROM update_features_test.json_input WHERE label = 'test1'),

	'update_features_test.areal',
	'tg',
	'id',
	(SELECT mapping_from_ngis FROM update_features_test.json_mapping WHERE target_table = 'update_features_test.areal'::regclass),

	'update_features_test.lineal',
	'tg',
	'id',
	(SELECT mapping_from_ngis FROM update_features_test.json_mapping WHERE target_table = 'update_features_test.lineal'::regclass),

  1e-10
);

SELECT 'update_features_2', 'topo', * FROM check_topology_changes('update_features_test_topo');
SELECT 'update_features_2', 'areal', * FROM check_layer_features('update_features_test.areal'::regclass, 'tg'::name);
SELECT 'update_features_2', 'lineal', * FROM check_layer_features('update_features_test.lineal'::regclass, 'tg'::name);

-- Call with payload using "exterior" geometry_property
SELECT topo_update.update_features(
	(SELECT payload FROM update_features_test.json_input
	 WHERE label = 'surface_by_exterior_1'),

	'update_features_test.areal',
	'tg',
	'id',
	(SELECT mapping_from_ngis FROM update_features_test.json_mapping WHERE target_table = 'update_features_test.areal'::regclass),

	'update_features_test.lineal',
	'tg',
	'id',
	(SELECT mapping_from_ngis FROM update_features_test.json_mapping WHERE target_table = 'update_features_test.lineal'::regclass),

  1e-10
);

SELECT 'by_exterior1', 'topo', * FROM check_topology_changes('update_features_test_topo');
SELECT 'by_exterior1', 'areal', * FROM check_layer_features('update_features_test.areal'::regclass, 'tg'::name);
SELECT 'by_exterior1', 'lineal', * FROM check_layer_features('update_features_test.lineal'::regclass, 'tg'::name);

-- Call with payload using "exterior" and "interiors" geometry_propertes
SELECT topo_update.update_features(
	(SELECT payload FROM update_features_test.json_input
	 WHERE label = 'surface_by_exterior_and_interior_1'),

	'update_features_test.areal',
	'tg',
	'id',
	(SELECT mapping_from_ngis FROM update_features_test.json_mapping WHERE target_table = 'update_features_test.areal'::regclass),

	'update_features_test.lineal',
	'tg',
	'id',
	(SELECT mapping_from_ngis FROM update_features_test.json_mapping WHERE target_table = 'update_features_test.lineal'::regclass),

	1e-10,

	ST_MakeEnvelope(0,0,10,10,4326) -- deleteUnknownFeaturesInBbox
);

SELECT 'by_exterior_interior1', 'topo', * FROM check_topology_changes('update_features_test_topo');
SELECT 'by_exterior_interior1', 'areal', * FROM check_layer_features('update_features_test.areal'::regclass, 'tg'::name);
SELECT 'by_exterior_interior1', 'lineal', * FROM check_layer_features('update_features_test.lineal'::regclass, 'tg'::name);

-- Call with deletion bbox
SELECT topo_update.update_features(
	(SELECT payload FROM update_features_test.json_input
	 WHERE label = 'empty'),

	'update_features_test.areal', 'tg', 'id',
	'{}'::jsonb, -- arealLayerColMap (unused)

	'update_features_test.lineal', 'tg', 'id',
	'{}'::jsonb, -- linealLayerColMap (unused)

	0, -- tolerance

	ST_MakeEnvelope(0,0,10,10,4326) -- deleteUnknownFeaturesInBbox
);

SELECT 'delete_all', 'topo_changes', * FROM check_topology_changes('update_features_test_topo');
SELECT 'delete_all', 'areal_count', count(*) FROM update_features_test.areal;
SELECT 'delete_all', 'lineal_count', count(*) FROM update_features_test.lineal;

-- Call with custom feature role/props extractor bbox
CREATE FUNCTION feature_role_props_extractor(
	feature JSONB,
	feat_srid INT,
	topo_srid INT,
	OUT role CHAR,
	OUT prop JSONB
) AS $BODY$ --{
BEGIN
	prop := feature -> 'properties' -> 'identification';
	--RAISE WARNING 'props: %', prop;
	role := CASE
		WHEN feature -> 'geometry' ->> 'type' = 'LineString' THEN 'B'
		ELSE 'S'
	END;
END;
$BODY$ LANGUAGE 'plpgsql';

SELECT topo_update.update_features(
	(SELECT payload FROM update_features_test.json_input WHERE label = 'test1'),

	'update_features_test.areal',
	'tg',
	'id',
	'{ "id": [ "id" ] }',

	'update_features_test.lineal',
	'tg',
	'id',
	'{ "id": [ "id" ] }',

	0, -- tolerance

	NULL, -- deleteUnknownInBbox

	'feature_role_props_extractor'::regproc -- extractor
);

SELECT 'extractor1', 'topo', * FROM check_topology_changes('update_features_test_topo');
SELECT 'extractor1', 'areal', * FROM check_layer_features('update_features_test.areal'::regclass, 'tg'::name);
SELECT 'extractor1', 'lineal', * FROM check_layer_features('update_features_test.lineal'::regclass, 'tg'::name);

-----------------------------------------------------------------------------
--
-- Call with payload having 2 surfaces both with interior rings
-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/76
--
-----------------------------------------------------------------------------

-- Reset topology
TRUNCATE update_features_test.areal;
TRUNCATE update_features_test.lineal;
SELECT reset_topology('update_features_test_topo');

SELECT topo_update.update_features(
	(SELECT payload FROM update_features_test.json_input WHERE label = 'surface_and_its_hole'),

	'update_features_test.areal',
	'tg',
	'id',
	'{ "id": [ "id" ] }',

	'update_features_test.lineal',
	'tg',
	'id',
	'{ "id": [ "id" ] }',

	0, -- tolerance

	NULL -- deleteUnknownInBbox
);

SELECT 'surface_and_its_hole', 'topo', * FROM check_topology_changes('update_features_test_topo');
SELECT 'surface_and_its_hole', 'areal', * FROM check_layer_features('update_features_test.areal'::regclass, 'tg'::name);
SELECT 'surface_and_its_hole', 'lineal', * FROM check_layer_features('update_features_test.lineal'::regclass, 'tg'::name);

-----------------------------------------------------------------------------
--
-- Call with payload in different projection from topology
-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/108
--
-----------------------------------------------------------------------------

-- Reset topology
TRUNCATE update_features_test.areal;
TRUNCATE update_features_test.lineal;
SELECT reset_topology('update_features_test_topo');

SELECT topo_update.update_features(
	(SELECT payload FROM update_features_test.json_input WHERE label = 'reproject1'),

	'update_features_test.areal',
	'tg',
	'id',
	'{ "id": [ "id" ] }',

	'update_features_test.lineal',
	'tg',
	'id',
	'{ "id": [ "id" ] }',

	0, -- tolerance

	NULL -- deleteUnknownInBbox
);

SELECT 'reproject1', 'topo', * FROM check_topology_changes('update_features_test_topo');
SELECT 'reproject1', 'areal', * FROM check_layer_features('update_features_test.areal'::regclass, 'tg'::name);
SELECT 'reproject1', 'lineal', * FROM check_layer_features('update_features_test.lineal'::regclass, 'tg'::name);

-- Call update_features again with same payload
-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/110
SELECT topo_update.update_features(
	(SELECT payload FROM update_features_test.json_input WHERE label = 'reproject1'),

	'update_features_test.areal',
	'tg',
	'id',
	'{ "id": [ "id" ] }',

	'update_features_test.lineal',
	'tg',
	'id',
	'{ "id": [ "id" ] }',

	0, -- tolerance

	NULL -- deleteUnknownInBbox
);

SELECT 'reproject1.2', 'topo', * FROM check_topology_changes('update_features_test_topo');
SELECT 'reproject1.2', 'areal', * FROM check_layer_features('update_features_test.areal'::regclass, 'tg'::name);
SELECT 'reproject1.2', 'lineal', * FROM check_layer_features('update_features_test.lineal'::regclass, 'tg'::name);

-----------------------------------------------------------------------------
--
-- Call first with a square and then with the same square shifted
-- in a way to intersect with the first. Expect the final result
-- to have no more vertices than found in the second square.
--
-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/150
--
-----------------------------------------------------------------------------

-- Reset topology
TRUNCATE update_features_test.areal;
TRUNCATE update_features_test.lineal;
SELECT reset_topology('update_features_test_topo');

SELECT topo_update.update_features(
	(SELECT payload FROM update_features_test.json_input WHERE label = 'test1'),
	'update_features_test.areal', 'tg', 'id', '{ "id": [ "identification", "id" ] }',
	'update_features_test.lineal', 'tg', 'id', '{ "id": [ "identification", "id" ] }',
	0, -- tolerance
	NULL -- deleteUnknownInBbox
);

SELECT topo_update.update_features(
	(SELECT payload FROM update_features_test.json_input WHERE label = 'test1shift'),
	'update_features_test.areal', 'tg', 'id', '{ "id": [ "identification", "id" ] }',
	'update_features_test.lineal', 'tg', 'id', '{ "id": [ "identification", "id" ] }',
	0, -- tolerance
	ST_MakeEnvelope(0,0,10,10,4326) -- deleteUnknownFeaturesInBbox
);

SELECT 'test1shift', 'areal', * FROM check_layer_features('update_features_test.areal'::regclass, 'tg'::name);

------------------------
--
-- Error calls
--
------------------------

-- Reset topology before starting error calls
TRUNCATE update_features_test.areal;
TRUNCATE update_features_test.lineal;
SELECT reset_topology('update_features_test_topo');

-- E1: call without CRS
DO $$
BEGIN
	BEGIN
		SELECT topo_update.update_features(
			(SELECT payload FROM update_features_test.json_input WHERE label = 'surface_and_its_hole') - 'crs',
			'update_features_test.areal', 'tg', 'id',
			'{ "id": [ "id" ] }',

			'update_features_test.lineal', 'tg', 'id',
			'{ "id": [ "id" ] }',

			0, -- tolerance
			NULL -- deleteUnknownInBbox
		);
	EXCEPTION WHEN OTHERS
	THEN
		RAISE WARNING 'Got exception %', SQLERRM;
	END;
END;
$$ LANGUAGE 'plpgsql';

-- E2: call with tolerance so big to introduce borders interior intersection
-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/104
DO $$
BEGIN
	BEGIN
		PERFORM topo_update.update_features(
			(SELECT payload FROM update_features_test.json_input WHERE label = 'nearby-borders-1'),
			'update_features_test.areal', 'tg', 'id',
			'{ "id": [ "id" ] }',

			'update_features_test.lineal', 'tg', 'id',
			'{ "id": [ "id" ] }',

			tolerance => 0.5,
			deleteUnknownFeaturesInBbox => ST_MakeEnvelope(0,0,10,10,4326),
			resultingFeatureValidator => 'feature_validator'
		);
		RAISE WARNING 'E2: unexpected success from update_features';
	EXCEPTION WHEN OTHERS
	THEN
		RAISE WARNING 'Got exception %', SQLERRM;
	END;
END;
$$ LANGUAGE 'plpgsql';

-- E3: call with tolerance so big to introduce borders edge sharing
-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/104
DO $$
BEGIN
	BEGIN
		PERFORM topo_update.update_features(
			(SELECT payload FROM update_features_test.json_input WHERE label = 'nearby-borders-2'),
			'update_features_test.areal', 'tg', 'id',
			'{ "id": [ "id" ] }',

			'update_features_test.lineal', 'tg', 'id',
			'{ "id": [ "id" ] }',

			tolerance => 0.5,
			deleteUnknownFeaturesInBbox => ST_MakeEnvelope(0,0,10,10,4326),
			resultingFeatureValidator => 'feature_validator'
		);
		RAISE WARNING 'E3: unexpected success from update_features';
	EXCEPTION WHEN OTHERS
	THEN
		RAISE WARNING 'Got exception %', SQLERRM;
	END;
END;
$$ LANGUAGE 'plpgsql';

-- E4: call with tolerance so big to introduce borders edge sharing
-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/104
DO $$
BEGIN
	BEGIN
		PERFORM topo_update.update_features(
			(SELECT payload FROM update_features_test.json_input WHERE label = 'nearby-borders-2'),
			'update_features_test.areal', 'tg', 'id',
			'{ "id": [ "id" ] }',

			'update_features_test.lineal', 'tg', 'id',
			'{ "id": [ "id" ] }',

			tolerance => 1e10,
			deleteUnknownFeaturesInBbox => ST_MakeEnvelope(0,0,10,10,4326)
		);
		RAISE WARNING 'E4: unexpected success from update_features';
	EXCEPTION WHEN OTHERS
	THEN
		RAISE WARNING 'Got exception %', SQLERRM;
	END;
END;
$$ LANGUAGE 'plpgsql';

-- E5: call with wrong winding of surface polygon
-- See https://gitlab.com/nibioopensource/pgtopo_update_sql/-/issues/112
DO $$
BEGIN
	BEGIN
		PERFORM topo_update.update_features(
			(
				SELECT jsonb_set(
					payload,
					ARRAY['features', '1', 'geometry_properties', 'exterior', '0'],
					to_jsonb(
						'-' || (
							payload #>>
							ARRAY['features', '1', 'geometry_properties', 'exterior', '0']
						)
					)
				)
				FROM update_features_test.json_input
				WHERE label = 'test1'
			),
			'update_features_test.areal', 'tg', 'id',
			(SELECT mapping_from_ngis FROM update_features_test.json_mapping WHERE target_table = 'update_features_test.areal'::regclass),

			'update_features_test.lineal', 'tg', 'id',
			(SELECT mapping_from_ngis FROM update_features_test.json_mapping WHERE target_table = 'update_features_test.lineal'::regclass),

			tolerance => 1e10,
			deleteUnknownFeaturesInBbox => ST_MakeEnvelope(0,0,10,10,4326)
		);
		RAISE WARNING 'E5: unexpected success from update_features';
	EXCEPTION WHEN OTHERS
	THEN
		RAISE WARNING 'Got exception %', SQLERRM;
	END;
END;
$$ LANGUAGE 'plpgsql';

-- E6: call with duplicated border ids
DO $$
BEGIN
	BEGIN
		PERFORM topo_update.update_features(
			(
				SELECT jsonb_set(
					payload,
					ARRAY['features', '1', 'properties', 'identification', 'id'],
					( payload #> ARRAY['features', '0', 'properties', 'identification', 'id'])
				)
				FROM update_features_test.json_input
				WHERE label = 'surface_by_exterior_1'
			),
			'update_features_test.areal', 'tg', 'id',
			(SELECT mapping_from_ngis FROM update_features_test.json_mapping WHERE target_table = 'update_features_test.areal'::regclass),

			'update_features_test.lineal', 'tg', 'id',
			(SELECT mapping_from_ngis FROM update_features_test.json_mapping WHERE target_table = 'update_features_test.lineal'::regclass),

			1e10, -- tolerance
			ST_MakeEnvelope(0,0,10,10,4326) -- deleteUnknownFeaturesInBbox
		);
		RAISE WARNING 'E6: unexpected success from update_features';
	EXCEPTION WHEN OTHERS
	THEN
		RAISE WARNING 'Got exception %', SQLERRM;
	END;
END;
$$ LANGUAGE 'plpgsql';


ROLLBACK;

