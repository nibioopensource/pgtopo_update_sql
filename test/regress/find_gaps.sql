BEGIN;

set client_min_messages to NOTICE;

\i :regdir/utils/normalized_geom.sql

-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
\i :regdir/../../src/sql/topo_update/function_02_find_gaps.sql

---------------------
-- Initialize schema
---------------------

SELECT NULL FROM CreateTopology('topo');

CREATE TABLE topo.border (
  "id" TEXT PRIMARY KEY,
  "live" BOOLEAN DEFAULT FALSE
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo',
	'border',
	'tg',
	'LINEAL'
);

CREATE TABLE topo.surface (
  "id" TEXT PRIMARY KEY,
  "live" BOOLEAN DEFAULT FALSE
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo',
	'surface',
	'tg',
	'AREAL'
);

CREATE TABLE topo.points (
  "id" TEXT PRIMARY KEY,
  "live" BOOLEAN DEFAULT FALSE
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo',
	'points',
	'tg',
	'POINT'
);

---------------------
-- Add Points
---------------------

INSERT INTO topo.points (id, live, tg) VALUES
(
  'p1', true,
  --
  --   ,----b1----.
  --   |          |
  --   |  (F1)    |
  --   |          |
  --   |          |
  --   |          |
  --   |          |
  --   |          |
  --   v          |
  --   o--(E1)----'
  --
  toTopoGeom(
    'POINT(0 0)',
    'topo',
    3,
    0
  )
),
(
  'p2', false,
  --
  --   ,----(b1)---.
  --   |           |
  --   |   (F2)    |
  --   |           |
  --   | o-b2---.  |
  --   | ^      |  |
  --   | | (F1) |  |
  --   | |      |  |
  --   | `-(E2)-'  |
  --   v           |
  --   o---(E1)----'
  --
  toTopoGeom(
    'POINT(2 8)',
    'topo',
    3,
    0
  )
)
;

---------------------
-- Add Borders
---------------------

INSERT INTO topo.border (id, live, tg) VALUES
(
  'b1', true,
  --
  --   ,----b1----.
  --   |          |
  --   |  (F1)    |
  --   |          |
  --   |          |
  --   |          |
  --   |          |
  --   |          |
  --   v          |
  --   o--(E1)----'
  --
  toTopoGeom(
    'LINESTRING(0 0,10 0,10 10,0 10,0 0)',
    'topo',
    1,
    0
  )
),
(
  'b2', false,
  --
  --   ,----(b1)---.
  --   |           |
  --   |   (F2)    |
  --   |           |
  --   | o-b2---.  |
  --   | ^      |  |
  --   | | (F1) |  |
  --   | |      |  |
  --   | `-(E2)-'  |
  --   v           |
  --   o---(E1)----'
  --
  toTopoGeom(
    'LINESTRING(2 8,8 8,8 2,2 2, 2 8)',
    'topo',
    1,
    0
  )
)
;

---------------------
-- Add Surfaces
---------------------

INSERT INTO topo.surface (id, live, tg) VALUES
(
  's1', true,
  --
  --   ,----(b1)---.
  --   | (F2)      |
  --   |           |
  --   |           |
  --   | o-(b2)-.  |
  --   | ^      |  |
  --   | |  s1  |  |
  --   | |      |  |
  --   | | (F1) |  |
  --   | |      |  |
  --   | `-(E2)-'  |
  --   v           |
  --   o---(E1)----'
  --
  CreateTopoGeom(
    'topo',
	3, -- areal
	2, -- layer_id
    '{{2,3}}'
  )
),
(
  's2', false,
  --
  --   ,----(b1)---.
  --   | (F2)      |
  --   |       s2  |
  --   |           |
  --   | o-(b2)-.  |
  --   | ^      |  |
  --   | | (F1) |  |
  --   | |      |  |
  --   | `-(E2)-'  |
  --   v           |
  --   o---(E1)----'
  --
  CreateTopoGeom(
    'topo',
	3, -- areal
	2, -- layer_id
    '{{1,3}}'
  )
)
;

---------------------
-- Check Points
---------------------

SELECT
	'point',
	id,
	live,
	ST_AsText( normalized_geom(tg) )
FROM topo.points ORDER BY id;

SELECT 'pt1', 'x', *
FROM topo_update.find_gaps(
	'topo.points',
	'tg'
);

SELECT 'pt2.live', 'x', *
FROM topo_update.find_gaps(
	'topo.points',
	'tg',
	'live'
);

---------------------
-- Check Borders
---------------------

SELECT
	'border',
	id,
	live,
	ST_AsText( normalized_geom(tg) )
FROM topo.border ORDER BY id;

SELECT 'bt1', 'x', *
FROM topo_update.find_gaps(
	'topo.border',
	'tg'
);

SELECT 'bt2.live', 'x', *
FROM topo_update.find_gaps(
	'topo.border',
	'tg',
	'live'
);

---------------------
-- Check Surfaces
---------------------

SELECT
	'surface',
	id,
	live,
	ST_AsText( normalized_geom(tg) )
FROM topo.surface ORDER BY id;

SELECT 'st1', 'x', *
FROM topo_update.find_gaps('topo.surface', 'tg');

SELECT 'st2.live', 'x', *
FROM topo_update.find_gaps(
	'topo.surface',
	'tg',
	'live'
);

