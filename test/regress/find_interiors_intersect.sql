BEGIN;

set client_min_messages to NOTICE;

\i :regdir/utils/normalized_geom.sql

-- Utility include for quick testing when ONLY changing the
-- function being tested (you still need to invoke top-level
-- `make` if other functions are changed)
\i :regdir/../../src/sql/topo_update/function_02_find_interiors_intersect.sql

---------------------
-- Initialize schema
---------------------

SELECT NULL FROM CreateTopology('topo');

CREATE TABLE topo.border (
  "id" TEXT PRIMARY KEY,
  "live" BOOLEAN DEFAULT FALSE
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo',
	'border',
	'tg',
	'LINEAL'
);

CREATE TABLE topo.surface (
  "id" TEXT PRIMARY KEY,
  "live" BOOLEAN DEFAULT FALSE
);
SELECT NULL FROM AddTopoGeometryColumn(
	'topo',
	'topo',
	'surface',
	'tg',
	'AREAL'
);

---------------------
-- Add Borders
---------------------

INSERT INTO topo.border (id, live, tg) VALUES
(
  'b1', true,
  --
  --   ,----b1----.
  --   |          |
  --   |  (F1)    |
  --   |          |
  --   |          |
  --   |          |
  --   |          |
  --   |          |
  --   v          |
  --   o--(E1)----'
  --
  toTopoGeom(
    'LINESTRING(0 0,10 0,10 10,0 10,0 0)',
    'topo',
    1,
    0
  )
),
(
  'b2', true,
  --
  --   ,----(b1)---.
  --   |           |
  --   |   (F2)    |
  --   |           |
  --   | o-b2---.  |
  --   | ^      |  |
  --   | | (F1) |  |
  --   | |      |  |
  --   | `-(E2)-'  |
  --   v           |
  --   o---(E1)----'
  --
  toTopoGeom(
    'LINESTRING(2 8,8 8,8 2,2 2, 2 8)',
    'topo',
    1,
    0
  )
),
(
  'b1+b2', false,
  --
  --   ,-----b1----.
  --   |           |
  --   |   (F2)    |
  --   |           |
  --   | o-b2---.  |
  --   | ^      |  |
  --   | | (F1) |  |
  --   | |      |  |
  --   | `-(E2)-'  |
  --   v           |
  --   o---(E1)----'
  --
  CreateTopoGeom(
    'topo',
	2, -- lineal
	1, -- layer_id
    '{{1,2},{2,2}}'
  )
)
;

-- Insert a border using the *same* TopoGeometry
-- as another border
INSERT INTO topo.border (id, live, tg)
SELECT 'b1dup', live, tg FROM topo.border
WHERE id = 'b1';

---------------------
-- Add Surfaces
---------------------

INSERT INTO topo.surface (id, live, tg) VALUES
(
  's1', true,
  --
  --   ,----(b1)---.
  --   | (F2)      |
  --   |           |
  --   |           |
  --   | o-(b2)-.  |
  --   | ^      |  |
  --   | |  s1  |  |
  --   | |      |  |
  --   | | (F1) |  |
  --   | |      |  |
  --   | `-(E2)-'  |
  --   v           |
  --   o---(E1)----'
  --
  CreateTopoGeom(
    'topo',
	3, -- areal
	2, -- layer_id
    '{{2,3}}'
  )
),
(
  's2', true,
  --
  --   ,----(b1)---.
  --   | (F2)      |
  --   |       s2  |
  --   |           |
  --   | o-(b2)-.  |
  --   | ^      |  |
  --   | | (F1) |  |
  --   | |      |  |
  --   | `-(E2)-'  |
  --   v           |
  --   o---(E1)----'
  --
  CreateTopoGeom(
    'topo',
	3, -- areal
	2, -- layer_id
    '{{1,3}}'
  )
),
(
  's1+s2', false,
  --
  --   ,----(b1)---.
  --   | (F2)      |
  --   |       s2  |
  --   |           |
  --   | o-(b2)-.  |
  --   | ^      |  |
  --   | |  s2  |  |
  --   | |      |  |
  --   | `-(E2)-'  |
  --   v           |
  --   o---(E1)----'
  --
  CreateTopoGeom(
    'topo',
	3, -- areal
	2, -- layer_id
    '{{1,3},{2,3}}'
  )
)
;

-- Insert a Surface using the *same* TopoGeometry
-- as another Surface
INSERT INTO topo.surface (id, live, tg)
SELECT 's1dup', live, tg FROM topo.surface
WHERE id = 's1';

---------------------
-- Check Borders
---------------------

SELECT
	'border',
	id,
	live,
	ST_AsText( normalized_geom(tg) )
FROM topo.border ORDER BY id;

SELECT 'bt1', 'x', *
FROM topo_update.find_interiors_intersect(
	'topo.border',
	'tg',
	'id'
);

SELECT 'bt2.live', 'x', *
FROM topo_update.find_interiors_intersect(
	'topo.border',
	'tg',
	'id',
	'live'
);

SELECT 'bt3.live.nodup', 'xcount', count(*)
FROM topo_update.find_interiors_intersect(
	'topo.border',
	'tg',
	'id',
	$$
		live and id not like '%dup'
	$$
);

INSERT INTO topo.border (id, live, tg) VALUES
(
  'b1-interior-out', false,
  --
  --   ,-----b1----o---b1-interior-out--->o
  --   |           |
  --   |   (F2)    |
  --   |           |
  --   | o-b2---.  |
  --   | ^      |  |
  --   | | (F1) |  |
  --   | |      |  |
  --   | `-(E2)-'  |
  --   v           |
  --   o---(E1)----'
  --
  toTopoGeom(
	'LINESTRING(10 10,20 10)',
	'topo',
	1,
	0
  )
);

SELECT 'bt4.shared-interior-node', 'x', *
FROM topo_update.find_interiors_intersect(
	'topo.border',
	'tg',
	'id',
	$$ id IN ( 'b1', 'b1-interior-out' ) $$
);

DELETE FROM topo.border WHERE id = 'b1-interior-out';
INSERT INTO topo.border (id, live, tg) VALUES
(
  'b1-bnd-out', false,
  --
  --                 ,-----b1----o
  --                 |           |
  --                 |   (F2)    |
  --                 |           |
  --                 | o-b2---.  |
  --                 | ^      |  |
  --                 | | (F1) |  |
  --                 | |      |  |
  --                 | `-(E2)-'  |
  --                 v           |
  -- o<--b1-bnd-out--o---(E1)----'
  --
  toTopoGeom(
	'LINESTRING(-10 0,0 0)',
	'topo',
	1,
	0
  )
);

SELECT 'bt5.shared-boundary-node', 'x', *
FROM topo_update.find_interiors_intersect(
	'topo.border',
	'tg',
	'id',
	$$ id IN ( 'b1', 'b1-bnd-out' ) $$
);

---------------------
-- Check Surfaces
---------------------

SELECT
	'surface',
	id,
	live,
	ST_AsText( normalized_geom(tg) )
FROM topo.surface ORDER BY id;

SELECT 'st1', 'x', *
FROM topo_update.find_interiors_intersect('topo.surface', 'tg', 'id');

SELECT 'st2.live', 'x', *
FROM topo_update.find_interiors_intersect(
	'topo.surface',
	'tg',
	'id',
	'live'
);

SELECT 'st3.live.nodup', 'xcount', count(*)
FROM topo_update.find_interiors_intersect(
	'topo.surface',
	'tg',
	'id',
	$$
		live and id not like '%dup'
	$$
) t;
