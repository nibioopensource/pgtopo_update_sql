BEGIN;

set client_min_messages to WARNING;

\i :regdir/utils/check_topology_changes.sql
\i :regdir/utils/check_layer_features.sql
\i :regdir/../../src/sql/topo_update/function_02_delete_unknown_features.sql

CREATE SCHEMA delete_unkown_features_test;

SELECT NULL FROM topology.CreateTopology('topo');
CREATE TABLE topo.comp ( k int );
CREATE TABLE topo.feat ( comp topo.comp);
SELECT NULL FROM
AddTopoGeometryColumn('topo', 'topo', 'feat', 'g', 'POINT');

--SELECT '_', 'topo', * FROM check_topology_changes('topo');


\set bbox ST_MakeEnvelope(0,0,200,200)

INSERT INTO topo.feat(comp.k, g) VALUES
-- inside bbox
( 10, toTopoGeom('MULTIPOINT(0 0)', 'topo', 1, 0) ),
-- inside bbox
( 11, toTopoGeom('MULTIPOINT(0 1)', 'topo', 1, 0) ),
-- on bbox boundary
( 20, toTopoGeom('MULTIPOINT(200 0)', 'topo', 1, 0) ),
-- outside bbox
( 21, toTopoGeom('MULTIPOINT(201 0)', 'topo', 1, 0) ),
-- points outside bbox but envelope intersecting it
( 30, toTopoGeom('MULTIPOINT(300 0, -300 0)', 'topo', 1, 0) ),
-- one point outside bbox, one inside
( 31, toTopoGeom('MULTIPOINT(300 0, 100 0)', 'topo', 1, 0) )
;

SELECT 'start', 'topo', * FROM check_topology_changes('topo');
SELECT 'start', 'feat', * FROM check_layer_features('topo.feat', 'g');

SELECT 'delete1', 'count', topo_update.delete_unknown_features(
	'topo.feat', 'g', 'comp.k',
	:bbox, --ST_MakeEnvelope(0,0,200,200),
	'{"11"}'::text[]
);

SELECT 'delete1', 'feat', * FROM check_layer_features('topo.feat', 'g', true);

-- Test some areal features

CREATE TABLE topo.feat_areal ( id int );
SELECT NULL FROM
AddTopoGeometryColumn('topo', 'topo', 'feat_areal', 'g', 'POLYGON');

\set bbox ST_MakeEnvelope(0,0,200,200)

INSERT INTO topo.feat_areal(id, g) VALUES
-- fully containing the bbox
( 10, toTopoGeom(ST_MakeEnvelope(-100, -100, 300, 300), 'topo', 2, 0) ),
-- fully outside the bbox
( 20, toTopoGeom(ST_MakeEnvelope(300, 300, 400, 400), 'topo', 2, 0) )
;

SELECT 'delete2', 'count', topo_update.delete_unknown_features(
	'topo.feat_areal', 'g', 'id',
	:bbox,
	'{}'::text[]
);

SELECT 'delete2', 'feat_areal', * FROM check_layer_features('topo.feat_areal', 'g', true);


ROLLBACK;

