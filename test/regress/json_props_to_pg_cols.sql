BEGIN;

\i :regdir/../../src/sql/topo_update/function_01_json_props_to_pg_cols.sql

CREATE TABLE json_mappings (id INT primary key, lbl TEXT UNIQUE, strict bool, map jsonb, props jsonb);

INSERT INTO json_mappings(id, lbl, strict, map, props) VALUES
(1, 'simple', false, '{
  "from_simple": [ "simple" ],
  "from_comp": [ "comp", "item1" ],
  "from_subcomp": [ "comp", "subcomp", "item2" ]
}', '{
  "simple": 1,
  "comp": {
    "item1": "c1",
    "subcomp": {
      "item2": "sc1"
    }
  }
}'),
(2, 'composite', false, '{
  "c.a": [ "comp", "item1" ],
  "c.nc.a": [ "comp", "subcomp", "item2" ]
}', '{
  "comp": {
    "item1": "c1",
    "subcomp": {
      "item2": "sc1"
    }
  }
}'),
(3, 'nulls', false, '{
  "to_simple_by_path": [ "a" ]
}', '{
  "a": null
}'),
(4, 'missing-non-strict', false, '{
  "k1": [ "a" ],
  "k2": [ "non-existent" ]
}', '{
  "a": 1
}')
;

SELECT lbl, colnames, colvals FROM (
  SELECT lbl, (topo_update.json_props_to_pg_cols(props, map, strict)).*
  FROM json_mappings
  ORDER BY id
) foo;

ROLLBACK;

SELECT * FROM topo_update.json_props_to_pg_cols(
	'{"a":1}'::jsonb,
	'{"k":["miss"]}'::jsonb,
	true
);

