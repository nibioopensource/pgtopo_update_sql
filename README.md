# PostgreSQL functions to manage Surface/Border topologies.

Run `make help` to get a list of available Makefile targets.

Run `make` to generate the scripts to create and populate a `topo_update`
schema with all support functions and types.

Run `make check` to run the regression testsuite.

Run `make install` to install the extension-based scripts.


## Requirements

In order to use the code and run the tests you need:

	- PostgreSQL 12+
	- GEOS 3.9.1+
	- PostGIS 3.1.0+
	- dblink PostgreSQL extension (only used in some tests)
	- uuid-ossp PostgreSQL extension

## Loading the support function

The provided functions can be loaded either by sourcing the
`topo_update.sql` (or `topo_update_debug.sql` for a debug version)
script or (which needs installation on the server system) by
running `CREATE EXTENSION topo_update`.

An utility script `topo_update.sh` is provided to simplify loading
the support schema. Try:

	topo_update.sh  --help

